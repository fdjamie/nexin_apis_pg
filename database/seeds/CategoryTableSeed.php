<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategoryTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_category')->insert([
            'directorate_id' => 1,
            'name' => 'Dummy Category',
        ]);
    }
}
