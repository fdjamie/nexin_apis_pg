<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LocationTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_location')->insert([
            'site_id' => 1,
            'group_id' => 1,
            'subgroup_id' => 1,
            'name' => 'Dummy Location',
            'service' => 1,
            'limit' => 2,
        ]);
    }
}
