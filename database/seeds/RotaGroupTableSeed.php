<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RotaGroupTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('tb_rota_groups')->insert([
            'directorate_id' => 1,
            'name' => 'Dummy RotaGroup',
            'description' => 'This Rota group is dummy RotaGroup.'
        ]);
    }
}
