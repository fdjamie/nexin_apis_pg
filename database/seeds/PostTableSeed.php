<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PostTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_post')->insert([
            'directorate_speciality_id' => 1,
            'staff_id' => 1,
            'name' => 'Dummy Post',
            'description' => 'This is a dummy post.',
            'permanent' => 1,
            'rota_template_id' => 1,
            'assigned_week' => 1,
            'start_date' => '2018-05-07',
        ]);
    }
}
