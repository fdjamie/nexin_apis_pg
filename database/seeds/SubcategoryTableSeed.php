<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SubcategoryTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_sub_category')->insert([
            'category_id' => 1,
            'name' => 'Dummy Subcategory',
            'parent_id' => 0,
            'specialities' => json_encode([1]),
        ]);
    }
}
