<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DivisionTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_division')->insert([
            'trust_id' => 1,
            'name' => 'Dummy Division',
        ]);
    }
}
