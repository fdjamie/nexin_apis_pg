<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ColorTableSeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('tb_colors')->insert([
            [
                'name' => 'Red',
                'letter' => 'R',
                'position' => 1,
                'code' => '#d9534f'
            ],
            [
                'name' => 'Orange',
                'letter' => 'O',
                'position' => 2,
                'code' => '#F0AD4E'
            ],
            [
                'name' => 'Yellow',
                'letter' => 'Y',
                'position' => 3,
                'code' => '#F5E348'
            ],
            [
                'name' => 'Green',
                'letter' => 'G',
                'position' => 4,
                'code' => '#449D44'
            ],
            [
                'name' => 'Blue',
                'letter' => 'B',
                'position' => 5,
                'code' => '#449D44'
            ],
        ]);
    }

}
