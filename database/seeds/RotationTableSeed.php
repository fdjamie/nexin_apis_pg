<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RotationTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_rotation')->insert([
            'post_id' => 1,
            'template_id' => 1,
            'staff_member' => 1,
            'template_position' => 1,
            'start_date' => '2018-05-09',
            'end_date' => '2018-06-30',
        ]);
    }
}
