<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategoryPriorityTableSeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('tb_category_priority')->insert([
            'directorate_speciality_id' => 1,
            'type' => 'category',
            'type_id' => 1
        ]);
        DB::table('tb_prioritizables')->insert([
            'category_priority_id' => 1,
            'tb_prioritizable_id' => 1,
            'tb_prioritizable_type' => 'App\Category'
        ]);
    }

}
