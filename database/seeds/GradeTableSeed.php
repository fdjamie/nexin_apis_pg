<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class GradeTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_grades')->insert([
            'directorate_id' => 1,
            'role_id' => 1,
            'name' => 'Dummy Grade',
            'position' => 0,
        ]);
    }
}
