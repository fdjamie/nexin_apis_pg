<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DirectorateServiceTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $event = [
            ['id' => 0, 'week_no' => 1, 'day' => 1, 'start' => '03:00', 'duration' => 660],
            ['id' => 1, 'week_no' => 1, 'day' => 2, 'start' => '04:00', 'duration' => 660],
            ['id' => 2, 'week_no' => 1, 'day' => 3, 'start' => '05:00', 'duration' => 660],
            ['id' => 3, 'week_no' => 1, 'day' => 4, 'start' => '06:00', 'duration' => 660],
            ['id' => 4, 'week_no' => 1, 'day' => 5, 'start' => '05:00', 'duration' => 660],
            ['id' => 5, 'week_no' => 1, 'day' => 6, 'start' => '04:00', 'duration' => 660],
            ['id' => 6, 'week_no' => 1, 'day' => 0, 'start' => '03:00', 'duration' => 660],
        ];
        $template = [
            'cycle_length' => 1,
            'start_date' => '2018-04-23',
            'end_date' => '2018-05-31',
            'starting_week' => 1,
            'events' => $event
        ];
        DB::table('tb_directorate_services')->insert([
            'directorate_id' => 1,
            'category_id' => 1,
            'sub_category_id' => 1,
            'name' => 'API test directorate service',
            'contactable' => 1,
            'min_standards' => 0,
            'specialities' => json_encode([1]),
            'template' => json_encode($template),
        ]);
    }
}
