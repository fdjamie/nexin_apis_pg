<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RotaTemplateTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_rota_template')->insert([
            'directorate_id' => 1,
            'rota_group_id' => 1,
            'name' => 'Dummy Rota Template',
            'description' => 'This Rota template is dummy template.',
            'start_date' => '2018-05-01',
            'end_date' => '2018-06-30',
            'content' => json_encode(['shift_type' => [[1, 1, 1, 1, '', 1, 1], [1, 1, 1, '', 1, 1, 1]]])
        ]);
    }
}
