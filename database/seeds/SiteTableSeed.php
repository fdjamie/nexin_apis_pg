<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SiteTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_sites')->insert([
            'trust_id' => 1,
            'name' => 'Dummy Site',
            'abbreviation' => 'DS',
            'address' => 'Dumm Address',
        ]);
    }
}
