<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AdditionalGradeTableSeed::class);
         $this->call(AdditionalParameterTableSeed::class);
         $this->call(CategoryTableSeed::class);
         $this->call(CategoryPriorityTableSeed::class);
         $this->call(ColorTableSeed::class);
         $this->call(DirectorateServiceTableSeed::class);
         $this->call(DirectorateTableSeed::class);
         $this->call(DivisionTableSeed::class);
         $this->call(GradeTableSeed::class);
         $this->call(GroupTableSeed::class);
         $this->call(LocationTableSeed::class);
         $this->call(ParameterTableSeed::class);
         $this->call(PostTableSeed::class);
         $this->call(PrioritySetTableSeed::class);
         $this->call(RoleTableSeed::class);
         $this->call(RotaGroupTableSeed::class);
         $this->call(RotationTableSeed::class);
         $this->call(RotaTemplateTableSeed::class);
         $this->call(ServiceTableSeed::class);
         $this->call(ShiftTypeTableSeed::class);
         $this->call(SiteTableSeed::class);
         $this->call(SpecialityTableSeed::class);
         $this->call(StaffTableSeed::class);
         $this->call(SubcategoryTableSeed::class);
         $this->call(SubgroupTableSeed::class);
         $this->call(TrustsTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(TboperatorsTableSeeder::class);
    }
}
