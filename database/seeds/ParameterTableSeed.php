<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ParameterTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_parameters')->insert([
            'directorate_id' => 1,
            'name' => 'Dummy Parameter',
            'operator' => json_encode([1]),
        ]);
    }
}
