<?php

use Illuminate\Database\Seeder;

class TboperatorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $operators = [
            ['name' => 'Equal to', 'value' => '=='],
            ['name' => 'Greater or Equal to', 'value' => '>='],
            ['name' => 'Greater than', 'value' => '>'],
            ['name' => 'Less or Equal to', 'value' => '<='],
            ['name' => 'Less than', 'value' => '<'],
            ['name' => 'Not equal to', 'value' => '!='],
            ['name' => 'Between', 'value' => 'between'],
            ['name' => 'Any', 'value' => 'any'],
            ['name' => 'Is', 'value' => 'is'],
            ['name' => 'Is not', 'value' => 'is_no'],
        ];
       DB::table('tb_operators')->insert($operators);
    }
}
