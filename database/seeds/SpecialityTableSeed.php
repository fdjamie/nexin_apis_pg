<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SpecialityTableSeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('tb_directorate_speciality')->insert([
            'directorate_id' => 1,
            'name' => 'Dummy Speciality',
            'abbreviation' => 'DS',
            'status' => true,
        ]);
    }

}
