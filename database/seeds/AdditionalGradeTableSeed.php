<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdditionalGradeTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_additional_grade')->insert([
            'directorate_speciality_id' => 1,
            'grade_id' => 1,
            'name' => 'Dummy Additonal Grade',
            'position' => 0,
        ]);
    }
}
