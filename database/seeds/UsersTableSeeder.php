<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_user')->insert([
            'name' => 'admin',
            'email' => 'admin@test.com',
            'password' => bcrypt('admin123'),
            'api_token' => str_random(60),
            'remember_token' => str_random(60),
            'is_admin' => true,
            'status' => true,
        ]);
        DB::table('tb_user_trust')->insert([
            'user_id' => 1,
            'trust_id' => 1,
        ]);
    }
}
