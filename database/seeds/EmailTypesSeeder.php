<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class EmailTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_email_types')->insert([
            ['name' =>'Verification Code','slug'=>'verification-code','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['name' =>'User Request Approved','slug'=>'user-request-approved','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['name' =>'User Request Rejected','slug'=>'user-request-rejected','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
           ]);

    }
}
