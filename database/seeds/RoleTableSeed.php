<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RoleTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_role')->insert([
            'directorate_id' => 1,
            'name' => 'Dummy Role',
            'position' => 0,
        ]);
    }
}
