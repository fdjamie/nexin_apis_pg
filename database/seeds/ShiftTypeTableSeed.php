<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ShiftTypeTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_shift_type')->insert([
            'directorate_id' => 1,
            'rota_group_id' => 1,
            'directorate_speciality_id' => -1,
            'name' => 'Dummy ShiftType',
            'start_time' => '05:00',
            'finish_time' => '13:00',
            'overrides_teaching' => 1,
            'nroc' => 1,
            'on_site' => 1
        ]);
    }
}
