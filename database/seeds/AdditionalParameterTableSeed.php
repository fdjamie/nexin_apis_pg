<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdditionalParameterTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_additional_parameter')->insert([
            'directorate_id' => 1,
            'parameter' => 'dummy Additional Parameter',
            'value' => ',a,b,c,none',
        ]);
    }
}
