<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SubgroupTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('tb_subgroups')->insert([
            'group_id' => 1,
            'name' => 'Dummy Subgroup',
        ]);
    }
}
