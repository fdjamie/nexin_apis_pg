<?php

use Illuminate\Database\Seeder;

class PrioritySetTableSeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('tb_priority_sets')->insert([
            'name' => 'Dummy PrioritySet',
            'directorate_speciality_id' => 1,
            'start_date' => '2018-05-01',
            'end_date' => '2018-06-30',
        ]);
        DB::table('tb_speciality_priority_set')->insert([
            'priority_set_id' => 1,
            'level' => 1,
            'from' => 1,
            'priority_id' => 0,
            'to' => 2,
        ]);
    }

}
