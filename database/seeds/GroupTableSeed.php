<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class GroupTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_groups')->insert([
            'directorate_id' => 1,
            'name' => 'Dummy Group',
        ]);
    }
}
