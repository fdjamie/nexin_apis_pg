<?php

use Illuminate\Database\Seeder;

class StaffTableSeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('tb_directorate_staff')->insert([
            'directorate_id' => 1,
            'directorate_speciality_id' => 1,
            'grade_id' => 1,
            'role_id' => 1,
            'name' => 'Dummy Staff 1',
            'gmc' => 1300213,
            'qualifications' => json_encode(["qualification_id" => [1, 3], "public" => 1]),
            'individual_bleep' => '132600^1',
            'email' => 'dummy@doctor.co',
            'mobile' => '12313853138^1',
            'appointed_from' => "2018-04-09",
            'appointed_till' => "2018-06-30",
            'profile_pic' => "default.png",
                ], [
            'directorate_id' => 1,
            'directorate_speciality_id' => 1,
            'grade_id' => 1,
            'role_id' => 1,
            'name' => 'Dummy Staff 2',
            'gmc' => 1300213,
            'qualifications' => json_encode(["qualification_id" => [1, 3], "public" => 1]),
            'individual_bleep' => '132600^1',
            'email' => 'dummy@doctor.co',
            'mobile' => '12313853138^1',
            'appointed_from' => "2018-04-09",
            'appointed_till' => "2018-06-30",
            'profile_pic' => "default.png",
        ]);
    }

}
