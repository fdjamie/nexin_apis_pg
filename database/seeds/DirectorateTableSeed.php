<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DirectorateTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_directorate')->insert([
            'trust_id' => 1,
            'division_id' => 0,
            'name' => 'Dummy Directorate',
            'service' => true,
        ]);
    }
}
