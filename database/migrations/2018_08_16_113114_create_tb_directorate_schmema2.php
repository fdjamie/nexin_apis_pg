<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbDirectorateSchmema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_directorate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trust_id');
            $table->integer('division_id')->nullable()->default(0);
            $table->tinyInteger('service')->nullable()->default(0);
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_directorate');
    }
}
