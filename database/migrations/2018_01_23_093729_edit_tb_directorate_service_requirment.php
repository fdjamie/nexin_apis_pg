<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTbDirectorateServiceRequirment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_directorate_service_requirement', function (Blueprint $table) {
            $table->string('value_2')->after('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('tb_directorate_service_requirement', function (Blueprint $table) {
            //
        });
    }
}
