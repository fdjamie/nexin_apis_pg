<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRoleIdFromTbRolePermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


        public function up()
    {
        Schema::table('tb_role_permission', function ($table) {

            $table->dropColumn('role_id');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_role_permission', function ($table) {
            $table->integer('role_id');
            $table->foreign('role_id')->references('id')->on('tb_roles');
        });
    }
}
