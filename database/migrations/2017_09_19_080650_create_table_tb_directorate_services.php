<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTbDirectorateServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_directorate_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_id');
            $table->integer('category_id')->nullable()->default(0);
            $table->integer('sub_category_id')->nullable()->default(0);
            $table->string('name');
            $table->tinyInteger('contactable')->nullable()->default(0);
            $table->tinyInteger('min_standards')->nullable()->default(0);
            $table->text('template')->nullable()->default(null);
            $table->text('specialities')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_directorate_services');
    }
}
