<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailableServicesStaffSchema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_available_services_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_staff_id');
            $table->integer('service_id');
            $table->integer('transition_time_id');
            $table->date('date');
            $table->integer('day');
            $table->integer('week')->nullable()->default(null);
            $table->string('start');
            $table->string('stop');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_available_services_staff');
    }
}
