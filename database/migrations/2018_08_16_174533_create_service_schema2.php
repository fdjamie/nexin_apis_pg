<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSchema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_speciality_id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('name');
            $table->tinyInteger('contactable');
            $table->tinyInteger('min_standards');
            $table->text('template')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_service');
    }
}
