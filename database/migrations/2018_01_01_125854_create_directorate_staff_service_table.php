<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectorateStaffServiceTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tb_directorate_staff_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_staff_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->time('start');
            $table->integer('duration');
            $table->time('end')->nullable()->default(null);
            $table->integer('day')->nullable()->default(0);
            $table->integer('end_day')->nullable()->default(0);
            $table->date('date')->nullable()->default(null);
            $table->tinyInteger('change')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tb_directorate_staff_service');
    }

}
