<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbServiceRunning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_service_running', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->integer('transition_time_id');
            $table->date('date');
            $table->integer('day');
            $table->integer('week')->nullable()->default(0);
            $table->string('start');
            $table->string('stop');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_service_running');
    }
}
