<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginDetailTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_login_details', function (Blueprint $table) {
            $table->increments('id');
           /* $table->integer('login_security_id');*/
            $table->integer('user_id');
            $table->string('ip');
            $table->string('device_type');
            $table->string('device');
            $table->string('browser');
            $table->json('verified_by')->nullable(); /* type json as we have to check verification for email,google auth app,sms*/
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('tb_user');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_login_details');
    }
}
