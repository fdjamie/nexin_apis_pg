<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbTransitionTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_transition_time', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_id')->nullable()->default(0);
            $table->integer('directorate_speciality_id')->nullable()->default(0);
            $table->string('name');
            $table->string('start_time')->nullable()->default(null);
            $table->string('end_time')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_transition_time');
    }
}
