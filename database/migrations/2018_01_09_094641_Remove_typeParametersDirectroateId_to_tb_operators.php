<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTypeParametersDirectroateIdToTbOperators extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('tb_operators', function($table) {
            $table->dropColumn('type');
            $table->dropColumn('parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tb_operators', function($table) {
            $table->text('parameters');
            $table->string('type');
        });
    }

}
