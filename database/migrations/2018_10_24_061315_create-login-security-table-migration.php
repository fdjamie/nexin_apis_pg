<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginSecurityTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_login_security', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('enable')->default(false);
            $table->string('secret')->nullable();
            $table->enum('type', ['authApp', 'email','sms']);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('tb_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_login_security');
    }
}
