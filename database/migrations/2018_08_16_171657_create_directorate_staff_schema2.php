<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectorateStaffSchema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_directorate_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_id');
            $table->integer('directorate_speciality_id');
            $table->integer('grade_id');
            $table->integer('role_id');
            $table->string('name',32);
            $table->string('gmc',16);
            $table->text('qualifications',128);
            $table->string('individual_bleep',8);
            $table->string('email',128);
            $table->string('mobile',16);
            $table->string('profile_pic',128);
            $table->date('appointed_from');
            $table->date('appointed_till')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_directorate_staff');
    }
}
