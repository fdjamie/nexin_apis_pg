<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTbRotaTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_rota_template', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_id');
            $table->integer('rota_group_id');
            $table->string('name');
            $table->json('content')->nullable()->default(null);
            $table->string('description');
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_rota_template');
    }
}
