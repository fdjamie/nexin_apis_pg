<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('from')->nullable();
            $table->longText('email');
            $table->string('type');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('tb_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_emails');
    }
}
