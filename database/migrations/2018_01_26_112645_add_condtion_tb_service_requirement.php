<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCondtionTbServiceRequirement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_service_requirement', function (Blueprint $table) {
           $table->string('condition')->nullable()->after('status');
           $table->integer('condition_id')->nullable()->after('condition');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_service_requirement', function($table) {
        $table->dropColumn('condition');
        $table->dropColumn('condtion_id');
    });
    }
}
