<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTbCategoryPriority2 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('tb_category_priority', function (Blueprint $table) {
            $table->enum('type', ['category', 'subcategory', 'service', 'general_service'])->after('requirement_order');
            $table->renameColumn('category_id', 'type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tb_category_priority', function (Blueprint $table) {
            //
        });
    }

}
