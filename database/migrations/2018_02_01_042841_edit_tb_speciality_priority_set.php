<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTbSpecialityPrioritySet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_speciality_priority_set', function (Blueprint $table) {
            $table->renameColumn('directorate_speciality_id', 'priority_set_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_speciality_priority_set', function (Blueprint $table) {
            //
        });
    }
}
