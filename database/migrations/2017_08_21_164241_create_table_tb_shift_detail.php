<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTbShiftDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_shift_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shift_id');
            $table->integer('directorate_speciality_id');
            $table->integer('service_id');
            $table->time('start_time');
            $table->time('end_time')->nullable()->default(null);
            $table->tinyInteger('overrides_teaching')->nullable()->default(0);
            $table->tinyInteger('nroc')->nullable()->default(0);
            $table->tinyInteger('on_site')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_shift_detail');
    }
}
