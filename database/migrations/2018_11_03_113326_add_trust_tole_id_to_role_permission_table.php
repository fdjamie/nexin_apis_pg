<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrustToleIdToRolePermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_role_permission', function ($table) {

            $table->integer('trust_role_id')->after('id');
            $table->foreign('trust_role_id')->references('id')->on('tb_role_trust');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_role_permission', function ($table) {
            $table->dropColumn('trust_role_id');
        });
    }
}
