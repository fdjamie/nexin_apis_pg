<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationSchema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_location', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id');
            $table->integer('group_id')->nullable()->default(0);
            $table->integer('subgroup_id')->nullable()->default(0);
            $table->string('name');
            $table->enum('service',['0','1']);
            $table->integer('limit')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_location');
    }
}
