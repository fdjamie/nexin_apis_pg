<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbPrioritizables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_prioritizables', function (Blueprint $table) {
            $table->integer('category_priority_id');
            $table->integer('tb_prioritizable_id');
            $table->string('tb_prioritizable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_prioritizable');
    }
}
