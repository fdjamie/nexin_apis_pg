<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequirementSchema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_service_requirement', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->integer('priority');
            $table->string('color');
            $table->string('parameter');
            $table->string('operator');
            $table->string('value');
            $table->string('value2');
            $table->string('status');
            $table->integer('number');
            $table->string('condition');
            $table->text('condition_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_service_requirement');
    }
}
