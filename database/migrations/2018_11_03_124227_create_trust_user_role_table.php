<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrustUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_user_trust_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_trust_role');
            $table->integer('role_trust_id');
            $table->timestamps();

            $table->foreign('user_trust_role')->references('id')->on('tb_user_trust');
            $table->foreign('role_trust_id')->references('id')->on('tb_role_trust');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_user_trust_roles');
    }
}
