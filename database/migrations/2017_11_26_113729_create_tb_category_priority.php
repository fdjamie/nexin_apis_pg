<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbCategoryPriority extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_category_priority', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_speciality_id');
            $table->integer('category_id');
            $table->integer('priority')->nullable()->default(0);
            $table->text('requirement_order')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_category_priority');
    }
}
