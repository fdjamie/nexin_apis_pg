<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTbAdditionalParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_additional_parameter', function (Blueprint $table) {
            $table->renameColumn('directorate_speciality_id', 'directorate_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_additional_parameter', function (Blueprint $table) {
            //
        });
    }
}
