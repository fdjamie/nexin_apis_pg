<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTbRotation extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tb_rotation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->integer('template_id')->nullable()->default(0);
            $table->string('staff_member');
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->integer('permanent')->nullable()->default(0);
            $table->integer('template_position')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tb_rotation');
    }

}
