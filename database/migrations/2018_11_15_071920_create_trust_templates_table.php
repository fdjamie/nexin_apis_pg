<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrustTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_trust_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trust_id');
            $table->integer('template_id');
            $table->timestamps();

            $table->foreign('trust_id')->references('id')->on('tb_trust');
            $table->foreign('template_id')->references('id')->on('tb_email_templates');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_trust_templates');
    }
}
