<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_module_permissions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('module_id');
            $table->integer('permission_id');
            $table->timestamps();
            $table->foreign('module_id')->references('id')->on('tb_module');
            $table->foreign('permission_id')->references('id')->on('tb_permission');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_module_permissions');
    }
}
