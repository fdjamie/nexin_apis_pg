<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_email_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_type_id');
            $table->longText('html');
            $table->boolean('is_default')->default(false);
            $table->timestamps();
            $table->foreign('email_type_id')->references('id')->on('tb_email_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_email_templates');
    }
}
