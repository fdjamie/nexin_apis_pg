<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbServiceRequirement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_service_requirement', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id');
            $table->integer('priority');
            $table->string('color');
            $table->string('parameter');
            $table->string('operator');
            $table->string('value');
            $table->string('status');
            $table->integer('number');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_service_requirement');
    }
}
