<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleTrustsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_role_trust', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id');
            $table->integer('trust_id');
            $table->timestamps();
            $table->foreign('trust_id')->references('id')->on('tb_trust');
            $table->foreign('role_id')->references('id')->on('tb_roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_role_trust');
    }
}
