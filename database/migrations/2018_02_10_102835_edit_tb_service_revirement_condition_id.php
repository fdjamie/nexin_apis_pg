<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTbServiceRevirementConditionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_service_requirement', function (Blueprint $table) {
            $table->text('condition_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_service_requirement', function (Blueprint $table) {
            $table->integer('condition_id')->change();
        });
    }
}
