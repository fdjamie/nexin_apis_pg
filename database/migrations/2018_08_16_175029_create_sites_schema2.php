<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesSchema2 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::connection('pgsql2')->create('tb_sites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trust_id');
            $table->string('name');
            $table->string('abbreviation');
            $table->string('address');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tb_sites');
    }

}
