<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostSchema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_post', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_speciality_id')->nullable()->default(0);
            $table->integer('rota_template_id')->nullable()->default(0);
            $table->integer('staff_id')->nullable()->default(0);
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('assigned_week');
            $table->tinyInteger('permanent');
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->text('additional_parameters')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_post');
    }
}
