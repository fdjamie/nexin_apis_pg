<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTrustUserRoleIntoTrustUserIdInUserTrustRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('tb_user_trust_roles', function(Blueprint $table) {
            $table->renameColumn('user_trust_role', 'user_trust_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('tb_user_trust_roles', function(Blueprint $table) {
            $table->renameColumn('user_trust_id', 'user_trust_role');
        });

    }
}
