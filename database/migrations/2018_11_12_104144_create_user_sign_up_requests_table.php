<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSignUpRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_signup_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('trust_id');
            $table->enum('status', ['pending', 'verified','rejected']);
            $table->integer('proceeded_by_user')->nullable();
            $table->timestamp('proceeded_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('tb_user');
            $table->foreign('trust_id')->references('id')->on('tb_trust');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_signup_requests');
    }
}
