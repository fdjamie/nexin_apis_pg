<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeSentFieldToLoginSecurityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_login_security', function (Blueprint $table) {

            $table->integer('code_sent')->unsigned()->default(0)->after('type');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_login_security', function (Blueprint $table) {

            $table->dropColumn('code_sent');

        });
    }
}
