<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHolidayStatusToDirectorateService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_directorate_services', function(Blueprint $table) {

            $table->tinyInteger('holiday_status')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_directorate_services', function(Blueprint $table) {

            $table->dropColumn('holiday_status');
        });
    }
}
