<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEmailTemplatesTableAddTrustIdToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_email_templates', function(Blueprint $table) {

            $table->integer('trust_id')->nullable();
            $table->string('subject')->nullable();
            $table->string('from')->nullable();
            $table->boolean('is_active')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_email_templates', function(Blueprint $table) {
            $table->dropColumn('trust_id');
            $table->dropColumn('is_active');
            $table->dropColumn('subject');
            $table->dropColumn('from');
        });
    }
}
