<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTbStaff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_id');
            $table->integer('directorate_speciality_id');
            $table->integer('grade_id');
            $table->integer('role_id');
            $table->string('name');
            $table->string('gmc');
            $table->string('qualifications');
            $table->string('individual_bleep');
            $table->string('email');
            $table->string('mobile');
            $table->string('profile_pic');
            $table->date('appointed_from');
            $table->date('appointed_till');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_staff');
    }
}
