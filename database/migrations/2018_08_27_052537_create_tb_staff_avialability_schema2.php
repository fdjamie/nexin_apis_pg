<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbStaffAvialabilitySchema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_staff_availability', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_staff_id');
            $table->integer('transition_time_id');
            $table->date('date');
            $table->integer('day');
            $table->integer('week')->nullable()->default(null);
            $table->string('start');
            $table->string('stop');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_staff_availability');
    }
}
