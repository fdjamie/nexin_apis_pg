<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransitiontimesServicesStaffsTableSchema2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tb_transitiontimes_services_staffs_table', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transition_times_id');
            $table->date('date');
            $table->text('services_id');
            $table->text('staff_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_transitiontimes_services_staffs_table');
    }
}
