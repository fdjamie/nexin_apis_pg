<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTbShiftType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_shift_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('directorate_id');
            $table->integer('directorate_speciality_id');
            $table->integer('service_id')->nullable()->default(0);
            $table->enum('service_type',['d','s','none'])->nullable()->default('none');
            $table->integer('rota_group_id');
            $table->string('name');
            $table->time('start_time')->nullable()->default(null);
            $table->time('finish_time')->nullable()->default(null);
            $table->tinyInteger('overrides_teaching')->nullable()->default(0);
            $table->tinyInteger('nroc')->nullable()->default(0);
            $table->tinyInteger('on_site')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_shift_type');
    }
}
