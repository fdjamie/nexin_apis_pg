<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSubcategoryTableMakingSpecailitiesNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_sub_category', function(Blueprint $table) {
            $table->text('specialities')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_sub_category', function(Blueprint $table) {
            $table->text('specialities')->nullable(false)->change();
        });
    }
}
