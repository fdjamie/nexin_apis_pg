<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTbDoctor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_doctor', function (Blueprint $table) {
            $table->increments('doctor_id');
            $table->integer('grade_id');
            $table->integer('role_id');
            $table->integer('speciality_id');
            $table->integer('rotation_id');
            $table->string('gmc');
            $table->string('qualification');
            $table->string('name');
            $table->string('bleep');
            $table->string('email');
            $table->string('mobile');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_doctor');
    }
}
