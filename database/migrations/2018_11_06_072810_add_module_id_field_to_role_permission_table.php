<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModuleIdFieldToRolePermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('tb_role_permission', function(Blueprint $table) {
            $table->integer('module_id');
            $table->foreign('module_id')->references('id')->on('tb_module');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('tb_role_permission', function(Blueprint $table) {
            $table->dropColumn('module_id');
        });

    }
}
