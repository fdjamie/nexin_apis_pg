<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransitiontimesServicesStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transitiontimes_services_staffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transition_times_id');
            $table->date('date');
            $table->text('services_id');
            $table->text('staff_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transitiontimes_services_staffs');
    }
}
