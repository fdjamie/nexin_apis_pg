<?php

use App\DirectorateSpeciality;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SpecialityTest extends TestCase
{
    private $api_token, $api_url, $api_client;
    
    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
    public function testAllSpeciality() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'directoratespeciality');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSpecialityShow() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'directoratespeciality/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreSpeciality() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test speciality',
            'abbreviation' => 'ats',
            'status' => true
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'directoratespeciality', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateSpeciality() {
        $this->setUser();
        $id = DirectorateSpeciality::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test speciality (update)',
            'abbreviation' => 'ats',
            'status' => true
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'directoratespeciality/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDeleteSpeciality() {
        $this->setUser();
        $id = DirectorateSpeciality::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'directoratespeciality/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
}
