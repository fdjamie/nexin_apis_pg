<?php

use App\Subgroup;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SubGroupTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllSubgroup() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'subgroup');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowSubgroup() {
        $this->setUser();
        $id = Subgroup::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'subgroup/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreSubgroup() {
        $this->setUser();
        $data = [
            'group_id' => 1,
            'name' => 'Api test subgroup'
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'subgroup', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateSubgroup() {
        $this->setUser();
        $id = Subgroup::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'group_id' => 1,
            'name' => 'Api test subgroup (update)'
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'subgroup/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroySubgroup(){
        $this->setUser();
        $id = Subgroup::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'subgroup/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
