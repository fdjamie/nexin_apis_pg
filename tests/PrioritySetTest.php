<?php

use App\User;
use App\PrioritySet;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PrioritySetTest extends TestCase
{
    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllPrioritySet() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'priority-set');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowPrioritySet() {
        $this->setUser();
        $id = PrioritySet::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'priority-set/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCreatePrioritySet() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => 'API test Priority Set.',
            'directorate_speciality_id' => 1,
            'start_date' => '2018-05-01',
            'end_date' => '2018-06-30',
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'priority-set', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdatePrioritySet() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => 'API test Priority Set (update)',
            'directorate_speciality_id' => 1,
            'start_date' => '2018-05-01',
            'end_date' => '2018-06-30',
        ];
        $id = PrioritySet::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("PUT", $this->api_url . 'priority-set/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyPrioritySet() {
        $this->setUser();
        $id = PrioritySet::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'priority-set/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
