<?php

use App\SubCategory;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SubcategoryTest extends TestCase {

    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllSubcategory() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'sub-category');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSubcategoryShow() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'sub-category/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreSubcategory() {
        $this->setUser();
        $data = [
            'category_id' => 1,
            'name' => 'Api test Subcategory',
            'parent_id' => 0,
            'specialities' => [1],
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'sub-category', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateSubcategory() {
        $this->setUser();
        $id = SubCategory::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'category_id' => 1,
            'name' => 'Api test Subcategory (update)',
            'parent_id' => 0,
            'specialities' => [2, 3],
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'sub-category/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSubcategoryChild() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'sub-category/' . 1 . '/child');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSubcategoryParent() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'sub-category/' . 1 . '/parents');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSubcategoryChildren() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'sub-category/' . 1 . '/children');
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testSubcategorySearch() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'category/' . 1 . '/sub-category/children');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDeleteSubcategory() {
        $this->setUser();
        $id = SubCategory::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'sub-category/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
