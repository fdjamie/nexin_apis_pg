<?php

use App\Parameter;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ParameterTest extends TestCase
{
    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
     public function testStoreGroup() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => 'Role',
            'operator' => [1],
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'service/requirement/parameter/test', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testAllParameter() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'service/requirement/parameter/test');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowParameter() {
        $this->setUser();
        $id = Parameter::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'service/requirement/parameter/test/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateParameter() {
        $this->setUser();
        $id = Parameter::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'name' => 'Role',
            'operator' => [1,2],
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'service/requirement/parameter/test/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
//    
    public function testDestroyParameter() {
        $this->setUser();
        $id = Parameter::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'service/requirement/parameter/test/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
