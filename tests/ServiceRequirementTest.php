<?php

use App\User;
use App\ServiceRequirement;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ServiceRequirementTest extends TestCase {

    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllServiceRequirement() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'service-requirement');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowServiceRequirement() {
        $this->setUser();
        $id = ServiceRequirement::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'service-requirement/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreServiceRequirement() {
        $this->setUser();
        $data = [
            [
                'service_id' => 1,
                'color' => 'red',
                'parameter' => 'role',
                'operator' => '==',
                'value' => 1,
                'value_2' => "",
                'priority' => 1,
                'status' => "present",
                'number' => 1,
                'id' => 1
            ],
            [
                'service_id' => 1,
                'color' => 'red',
                'parameter' => 'role',
                'operator' => '==',
                'value' => 2,
                'value_2' => "",
                'priority' => 1,
                'status' => "present",
                'number' => 1,
                'id' => 2
            ]
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'service-requirement', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testServiceRequirementCondtion() {
        $this->setUser();
        $data = [
            'ids' => [1, 2],
            'condtion_value' => '( 1 && 2 )'
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'service/conditions', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDestroyServiceRequirement() {
        $this->setUser();
        $id = ServiceRequirement::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'service-requirement/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
