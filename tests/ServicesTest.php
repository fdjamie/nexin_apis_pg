<?php

use App\User;
use App\Service;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ServicesTest extends TestCase {

    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllService() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'service');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testServiceShow() {
        $this->setUser();
        $id = Service::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'service/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testServiceStore() {
        $this->setUser();
        $data = [
            'directorate_speciality_id' => 1,
            'category_id' => 1,
            'sub_category_id' => 1,
            'name' => 'API test Service',
            'contactable' => 1,
            'min_standards' => 1,
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'service', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testServiceUpdate() {
        $this->setUser();
        $data = [
            'directorate_speciality_id' => 1,
            'category_id' => 1,
            'sub_category_id' => 1,
            'name' => 'API test Service (update)',
            'contactable' => 1,
            'min_standards' => 1,
        ];
        $id = Service::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("PUT", $this->api_url . 'service/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testServiceDelete() {
        $this->setUser();
        $id = Service::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'service/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
