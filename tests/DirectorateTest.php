<?php

use App\Directorate;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request; 

class DirectorateTest extends TestCase {

    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllDirectorate() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'directorate');
        $this->assertEquals(200, $response->getStatusCode());
    }
//
    public function testDirectorateShow() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'directorate/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreDirectorate() {
        $this->setUser();
        $data = [
            'trust_id' => 1,
            'division_id' => 0,
            'service' => true,
            'name' => 'Api test directorate'
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'directorate', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateDirectorate() {
        $this->setUser();
        $id = Directorate::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'trust_id' => 1,
            'division_id' => 0,
            'service' => true,
            'name' => 'Api test directorate (update)'
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'directorate/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDeleteDirectorate() {
        $this->setUser();
        $id = Directorate::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'directorate/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}