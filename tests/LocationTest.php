<?php

use App\Location;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class LocationTest extends TestCase
{
    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
    public function testAllLocation() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'location');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testLocationShow() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'location/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreLocation() {
        $this->setUser();
        $data = [
            'site_id' => 1,
            'group_id' => 1,
            'subgroup_id' => 1,
            'name' => 'Api test location',
            'service' => 1,
            'limit' => 2
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'location', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateLocation() {
        $this->setUser();
        $id = Location::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'site_id' => 1,
            'group_id' => 1,
            'subgroup_id' => 1,
            'name' => 'Api test location (update)',
            'service' => 1,
            'limit' => 2
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'location/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDeleteLocation() {
        $this->setUser();
        $id = Location::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'location/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
