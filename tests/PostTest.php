<?php

use App\User;
use App\Post;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PostTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllPost() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'post');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowPost() {
        $this->setUser();
        $id = Post::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'post/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCreatePost() {
        $this->setUser();
        $data = [
            'directorate_speciality_id' => 1,
            'name' => 'API test post.',
            'description' => 'This is a API test post.',
            'permanent' => 1,
            'rota_template_id' => 1,
            'assigned_week' => 1,
            'start_date' => '2018-05-07',
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'post', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdatePost() {
        $this->setUser();
        $data = [
            'directorate_speciality_id' => 1,
            'name' => 'API test post (update).',
            'description' => 'This is a API test post.',
            'permanent' => 1,
            'rota_template_id' => 1,
            'assigned_week' => 1,
            'start_date' => '2018-05-07',
        ];
        $id = Post::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("PUT", $this->api_url . 'post/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyPost() {
        $this->setUser();
        $id = Post::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'post/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
