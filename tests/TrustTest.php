<?php

use App\Trust;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class TrustTest extends TestCase
{
    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
     public function testAllTrust() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'trust');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testTrustShow() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'trust/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreTrust() {
        $this->setUser();
        $data = [
            'name' => 'Api test trust'
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'trust', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateTrust() {
        $this->setUser();
        $id = Trust::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'name' => 'Api test directorate (update)'
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'trust/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDeleteTrust() {
        $this->setUser();
        $id = Trust::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'trust/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
