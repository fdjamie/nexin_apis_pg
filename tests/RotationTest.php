<?php

use App\User;
use App\Rotation;
use App\Post;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RotationTest extends TestCase {

    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllRotation() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'rotation');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testRotationShow() {
        $this->setUser();
        $id = Rotation::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'rotation/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCreatePost() {
        $this->setUser();
        $data = [
            'directorate_speciality_id' => 1,
            'name' => 'API test post.',
            'description' => 'This is a API test post.',
            'permanent' => 1,
            'rota_template_id' => 1,
            'assigned_week' => 1,
            'start_date' => '2018-05-07',
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'post', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testRotationStore() {
        $this->setUser();
        $id = Post::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'staff_member' => 2,
            'post_id' => $id,
            'rota_group_id' => 1,
            'template_id' => 1,
            'template_position' => 1,
            'start_date' => '2018-05-09',
            'end_date' => '2018-06-30',
            'permanenet' => 0
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'rotation', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testRotationUpdate() {
        $this->setUser();
        $id = Post::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'staff_member' => 2,
            'post_id' => $id,
            'rota_group_id' => 1,
            'template_id' => 1,
            'template_position' => 1,
            'start_date' => '2018-05-09',
            'permanenet' => 1
        ];
        $rotation_id = Rotation::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("PUT", $this->api_url . 'rotation/' . $rotation_id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDeleteRotation() {
        $this->setUser();
        $id = Rotation::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'rota-group/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
