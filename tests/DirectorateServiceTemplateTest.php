<?php

use App\DirectorateService;
use App\User;
use App\Http\Requests;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class DirectorateServiceTemplateTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testStoreDirectorateServiceTemplate() {
        $this->setUser();
        $event = [
            ['id' => 0, 'week_no' => 1, 'day' => 1, 'start' => '03:00', 'duration' => 660],
            ['id' => 1, 'week_no' => 1, 'day' => 2, 'start' => '04:00', 'duration' => 660],
            ['id' => 2, 'week_no' => 1, 'day' => 3, 'start' => '05:00', 'duration' => 660],
            ['id' => 3, 'week_no' => 1, 'day' => 4, 'start' => '06:00', 'duration' => 660],
            ['id' => 4, 'week_no' => 1, 'day' => 5, 'start' => '05:00', 'duration' => 660],
            ['id' => 5, 'week_no' => 1, 'day' => 6, 'start' => '04:00', 'duration' => 660],
            ['id' => 6, 'week_no' => 1, 'day' => 0, 'start' => '03:00', 'duration' => 660],
        ];
        $template = [
            'cycle_length' => 1,
            'start_date' => '2018-04-23',
            'end_date' => '2018-05-31',
            'starting_week' => 1,
            'events' => $event
        ];
        $data = [
            'service_id' => 1,
            'template' => $template
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'directorate-service/template', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateDirectorateServiceTemplate() {
        $this->setUser();
        $id = DirectorateService::orderBy('id', 'desc')->pluck('id')->first();
        $event = [
            ['id' => 0, 'week_no' => 1, 'day' => 1, 'start' => '03:00', 'duration' => 660],
            ['id' => 1, 'week_no' => 1, 'day' => 2, 'start' => '04:00', 'duration' => 660],
            ['id' => 2, 'week_no' => 1, 'day' => 3, 'start' => '05:00', 'duration' => 660],
            ['id' => 3, 'week_no' => 1, 'day' => 4, 'start' => '06:00', 'duration' => 660],
            ['id' => 4, 'week_no' => 1, 'day' => 5, 'start' => '05:00', 'duration' => 660],
            ['id' => 5, 'week_no' => 1, 'day' => 6, 'start' => '04:00', 'duration' => 660],
            ['id' => 6, 'week_no' => 1, 'day' => 0, 'start' => '03:00', 'duration' => 660],
            ['id' => 7, 'week_no' => 2, 'day' => 1, 'start' => '03:00', 'duration' => 660],
            ['id' => 8, 'week_no' => 2, 'day' => 2, 'start' => '04:00', 'duration' => 660],
            ['id' => 9, 'week_no' => 2, 'day' => 3, 'start' => '05:00', 'duration' => 660],
            ['id' => 10, 'week_no' => 2, 'day' => 4, 'start' => '06:00', 'duration' => 660],
            ['id' => 11, 'week_no' => 2, 'day' => 5, 'start' => '05:00', 'duration' => 660],
            ['id' => 12, 'week_no' => 2, 'day' => 6, 'start' => '04:00', 'duration' => 660],
            ['id' => 13, 'week_no' => 2, 'day' => 0, 'start' => '03:00', 'duration' => 660],
        ];
        $template = [
            'cycle_length' => 2,
            'start_date' => '2018-04-23',
            'end_date' => '2018-05-31',
            'starting_week' => 1,
            'events' => $event
        ];
        $data = [
            'service_id' => 1,
            'template' => $template
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'directorate-service/template/'.$id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyDirectorateServiceTemplate() {
        $this->setUser();
        $id = DirectorateService::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'directorate-service/template/'.$id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
