<?php

use App\RotaTemplate;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RotaTemplateTest extends TestCase
{
    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllRotaTemplate() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'rota-template');
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testStoreRotaTemplate() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'rota_group_id' => 1,
            'name' => 'API test Rota Template',
            'description' => 'This Rota template is dummy template.',
            'start_date' => '2018-05-01',
            'end_date' => '2018-06-30',
            'content' => ['shift_type' => [[1, 1, 1, 1, '', 1, 1], [1, 1, 1, '', 1, 1, 1]]]
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'rota-template', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testUpdateRole() {
        $this->setUser();
        $id = RotaTemplate::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'rota_group_id' => 1,
            'name' => 'API test Rota Template (update)',
            'description' => 'This Rota template is dummy template.',
            'start_date' => '2018-05-01',
            'end_date' => '2018-06-30',
            'content' => ['shift_type' => [[1, 1, 1, 1, '', 1, 1], [1, 1, 1, '', 1, 1, 1]]]
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'rota-template/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testRoleDestroy() {
        $this->setUser();
        $id = RotaTemplate::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'rota-template/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
