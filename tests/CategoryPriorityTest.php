<?php

use App\CategoryPriority;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CategoryPriorityTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllCategoryPriority() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'category-priority');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowCategoryPriority() {
        $this->setUser();
        $id = CategoryPriority::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'category-priority/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreCategoryPriority() {
        $this->setUser();
        $data = [
            'directorate_speciality_id' => 1,
            'type' => 'category',
            'category_id' => 1,
            'type_id' => 1
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'category-priority', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCreateCategoryPriority() {
        $this->setUser();
        $id = CategoryPriority::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_speciality_id' => 1,
            'type' => 'subcategory',
            'category_id' => 1,
            'type_id' => 1
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'category-priority/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyCategoryPriority() {
        $this->setUser();
        $id = CategoryPriority::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'category-priority/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
