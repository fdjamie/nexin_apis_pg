<?php

use App\Grade;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class GradeTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllGrade() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'grade');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGradeShow() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'grade/28');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreGrade() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test grade',
            'role_id' => 1,
            'position' => 1,
            'place' => 0,
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'grade', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateGrade() {
        $this->setUser();
        $id = Grade::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test grade (updated)',
            'role_id' => 1,
            'position' => 1,
            'current_position' => 1,
            'place' => 0,
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'grade/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testGradeDestroy() {
        $this->setUser();
        $id = Grade::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'grade/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
