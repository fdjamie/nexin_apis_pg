<?php

use App\ShiftType;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ShiftTypeTest extends TestCase
{
    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllShiftType() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'shift-type');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShiftTypeShow() {
        $this->setUser();
        $id = ShiftType::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'shift-type/'. $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testStoreShiftType() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'rota_group_id' => 1,
            'directorate_speciality_id' => -1,
            'name' => 'API test ShiftType',
            'start_time' => '05:00',
            'finish_time' => '13:00',
            'overrides_teaching' => 1,
            'nroc' => 1,
            'on_site' => 1
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'shift-type', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testUpdateShiftType() {
        $this->setUser();
        $id = ShiftType::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'rota_group_id' => 1,
            'directorate_speciality_id' => -1,
            'name' => 'API test ShiftType (update)',
            'start_time' => '05:00',
            'finish_time' => '13:00',
            'overrides_teaching' => 1,
            'nroc' => 1,
            'on_site' => 1
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'shift-type/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testShiftTypeDestroy() {
        $this->setUser();
        $id = ShiftType::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'shift-type/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
}
