<?php

use App\User;
use App\RotaGroup;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RotaTest extends TestCase {

    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllRotaGroup() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'rota-group');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testRotaGroupShow() {
        $this->setUser();
        $id = RotaGroup::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'rota-group/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreRotaGroup() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test Rotagroup',
            'description' => 'This Rota group is for API test.'
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'rota-group', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateRotaGroup() {
        $this->setUser();
        $id = RotaGroup::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test Rotagroup (update)',
            'description' => 'This Rota group is for API test.'
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'rota-group/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDeleteRotaGroup() {
        $this->setUser();
        $id = RotaGroup::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'rota-group/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
