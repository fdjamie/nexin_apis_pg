<?php

use App\AdditionalParameter;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class AdditionalParameterTest extends TestCase
{
    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
    public function testAllAdditionalParameter() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'additional-parameter');
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testShowAdditionalParameter() {
        $this->setUser();
        $id = AdditionalParameter::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'additional-parameter/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testStoreAdditionalParameter() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'parameter' => 'Api test Additional Parameter',
            'value' => ',a,b,c,none',
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'additional-parameter', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateAdditionalParameter() {
        $this->setUser();
        $id = AdditionalParameter::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'parameter' => 'Api test Additional Parameter (updated)',
            'value' => ',a,b,c,none',
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'additional-parameter/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyCategory() {
        $this->setUser();
        $id = AdditionalParameter::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'additional-parameter/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
