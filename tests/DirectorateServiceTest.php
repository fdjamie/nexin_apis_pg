<?php

use App\User;
use App\DirectorateService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class DirectorateServiceTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllDirectorateService() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'directorate-service');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowDirectorateService() {
        $this->setUser();
        $id = DirectorateService::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'directorate-service/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreDirectorateService() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'category_id' => 1,
            'sub_category_id' => 1,
            'name' => 'API test directorate service',
            'contactable' => 1,
            'min_standards' => 0,
            'specialities' => [1],
            'template' => '',
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'directorate-service', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateDirectorateService() {
        $this->setUser();
        $id = DirectorateService::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'category_id' => 1,
            'sub_category_id' => 1,
            'name' => 'API test directorate service (update)',
            'contactable' => true,
            'min_standards' => false,
            'specialities' => [1],
            'template' => '',
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'directorate-service/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyDirectorateService() {
        $this->setUser();
        $id = DirectorateService::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'directorate-service/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
