<?php

use App\Category;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CategoryTest extends TestCase
{
    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
    
    public function testAllCategory() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'category');
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testShowCategory() {
        $this->setUser();
        $id = Category::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'category/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testStoreCategory() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test category',
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'category', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testUpdateCategory() {
        $this->setUser();
        $id = Category::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test category (update)',
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'category/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyCategory() {
        $this->setUser();
        $id = Category::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'category/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
