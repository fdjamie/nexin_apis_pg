<?php

use App\DirectorateStaff;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class StaffTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllStaff() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'directorate-staff/directorate/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStaffShow() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'directorate-staff/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

//
    public function testStoreStaff() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'directorate_speciality_id' => 1,
            'grade_id' => 1,
            'role_id' => 1,
            'name' => 'Api test staff',
            'gmc' => 1300213,
            'qualifications' => ["qualification_id" => [1], "public" => 1],
            'qualifications_public' => 1,
            'individual_bleep' => '132600^1',
            'individual_bleep_public' => 1,
            'email' => 'dummy@doctor.co',
            'mobile' => '12313853138^1',
            'mobile_public' => 1,
            'appointed_from' => "2018-04-09",
            'appointed_till' => "2018-06-30"
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'directorate-staff', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

//
    public function testUpdateStaff() {
        $this->setUser();
        $id = DirectorateStaff::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'directorate_speciality_id' => 1,
            'grade_id' => 1,
            'role_id' => 1,
            'name' => 'Api test staff (updated)',
            'gmc' => 1300213,
            'qualifications' => ["qualification_id" => [1], "public" => 1],
            'qualifications_public' => 1,
            'individual_bleep' => '132600^1',
            'individual_bleep_public' => 1,
            'email' => 'dummy@doctor.co',
            'mobile' => '12313853138^1',
            'mobile_public' => 1,
            'appointed_from' => "2018-04-09",
            'appointed_till' => "2018-06-30"
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'directorate-staff/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
//    
    public function testDeleteStaff() {
        $this->setUser();
        $id = DirectorateStaff::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'directorate-staff/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
