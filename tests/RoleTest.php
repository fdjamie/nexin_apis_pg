<?php

use App\Role;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RoleTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllRole() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'role');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testRoleShow() {
        $this->setUser();
        $id = Role::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'role/'. $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreRole() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => "Api test role",
            'position' => 1,
            'place' => 0,
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'role', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateRole() {
        $this->setUser();
        $id = Role::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'name' => "Api test role (update)",
            'position' => 1,
            'place' => 0,
            'current_position' => 1,
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'role/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testRoleDestroy() {
        $this->setUser();
        $id = Role::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'role/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
