<?php

use App\User;
use App\AdditionalGrade;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class AdditionalGradeTest extends TestCase
{
    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
    public function testAllAdditionalGrade() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'additional-grade');
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testShowAdditionalGrade() {
        $this->setUser();
        $id = AdditionalGrade::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'additional-grade/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testStoreAdditionalGrade() {
        $this->setUser();
        $data = [
            'directorate_speciality_id' => 1,
            'name' => 'Api test Additional Grade',
            'grade_id' => 1,
            'position' => 0,
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'additional-grade', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateAdditionalGrade() {
        $this->setUser();
        $id = AdditionalGrade::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_speciality_id' => 1,
            'name' => 'Api test Additional Grade (update)',
            'grade_id' => 1,
            'position' => 0,
            'current_position' => 0,
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'additional-grade/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyAdditionalGrade() {
        $this->setUser();
        $id = AdditionalGrade::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'additional-grade/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
