<?php

use App\Division;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class DivisionTest extends TestCase
{
    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
    public function testAllDivision() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'division');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDivisionShow() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'division/1');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreDivision() {
        $this->setUser();
        $data = [
            'trust_id' => 1,
            'name' => 'Api test division'
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'division', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateDivision() {
        $this->setUser();
        $id = Division::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'trust_id' => 1,
            'name' => 'Api test division (update)'
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'division/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDeleteDivision() {
        $this->setUser();
        $id = Division::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'division/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }
}

