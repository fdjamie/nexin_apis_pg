<?php

use App\Group;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class GroupTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllGroup() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'group');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowGroup() {
        $this->setUser();
        $id = Group::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'group/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreGroup() {
        $this->setUser();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test group',
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'group', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateGroup() {
        $this->setUser();
        $id = Group::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'directorate_id' => 1,
            'name' => 'Api test group (update)',
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'group/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyGroup() {
        $this->setUser();
        $id = Group::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'group/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
