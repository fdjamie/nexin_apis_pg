<?php

use App\SpecialityPrioritySet;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SpecialityPriorityTest extends TestCase {

    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllSpecialityPrioritySet() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'speciality-priority-set');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSpecialityPrioritySetShow() {
        $this->setUser();
        $id = SpecialityPrioritySet::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'speciality-priority-set/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSpecialityPrioritySetStore() {
        $this->setUser();
        $data = [
            [
                'id' => 0,
                'priority_set_id' => 1,
                'level' => 1,
                'from' => 1,
                'priority_id' => 0,
                'to' => 2,
            ]
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'speciality-priority-set/store-update', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSpecialityPrioritySetDestroy() {
        $this->setUser();
        $id = SpecialityPrioritySet::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'speciality-priority-set/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
