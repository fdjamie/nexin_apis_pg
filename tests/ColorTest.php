<?php

use App\Color;
use App\User;
use App\Http\Requests;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ColorTest extends TestCase {

    private $api_token, $api_url, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }
    
    public function testStoreColor() {
        $this->setUser();
        $data = [
            'name' => 'Test Red',
            'letter' => 'R',
            'postition' => 1,
            'code' => '#ff0000'
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'color', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testAllColor() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'color');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShowColor() {
        $this->setUser();
        $id = Color::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'color/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateColor() {
        $this->setUser();
        $id = Color::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'name' => 'Test Red API (update)',
            'letter' => 'R',
            'postition' => 1,
            'current_position' => 1,
            'code' => '#ff0000'
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'color/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testDestroyColor() {
        $this->setUser();
        $id = Color::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'color/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
