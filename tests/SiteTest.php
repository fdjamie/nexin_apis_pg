<?php

use App\Site;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SiteTest extends TestCase {

    protected $api_url, $api_token, $user, $api_client;

    protected function setUser() {
        $this->user = User::find(1);
        $this->api_url = env('APP_URL') . 'v1/';
        $this->api_token = $this->user->api_token;
        $this->api_client = new GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer ' . $this->api_token]]);
    }

    public function testAllSite() {
        $this->setUser();
        $response = $this->api_client->request("GET", $this->api_url . 'site');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSiteShow() {
        $this->setUser();
        $id = Site::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("GET", $this->api_url . 'site/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStoreSite() {
        $this->setUser();
        $data = [
            'trust_id' => 1,
            'name' => 'Api test site',
            'abbreviation' => 'Ats',
            'address' => 'Dummy Address'
        ];
        $response = $this->api_client->request("POST", $this->api_url . 'site', ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdateSite() {
        $this->setUser();
        $id = Site::orderBy('id', 'desc')->pluck('id')->first();
        $data = [
            'trust_id' => 1,
            'name' => 'Api test site (update)',
            'abbreviation' => 'Ats',
            'address' => 'Dummy Address'
        ];
        $response = $this->api_client->request("PUT", $this->api_url . 'site/' . $id, ['form_params' => $data]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDeleteSite() {
        $this->setUser();
        $id = Site::orderBy('id', 'desc')->pluck('id')->first();
        $response = $this->api_client->request("DELETE", $this->api_url . 'site/' . $id);
        $this->assertEquals(200, $response->getStatusCode());
    }

}
