package nexin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.joda.time.LocalTime;

public class TransitionTime extends Event {
	int id;
	TransitionTime nextTransitionTime;
	Interval interval; 
	String name;
	Map<Doctor, Service> manualAllocations = new TreeMap<Doctor, Service>();

	TransitionTime(final int id, Integer week, Integer day, LocalTime localTime, LocalTime endTime) {
		super(week, day, localTime);
		this.id = id;
		interval = new Interval(localTime, endTime);
	}

	public TransitionTime getNext() {
		return nextTransitionTime;
	}

	@Override
	public String toString() { 
		return "TT" + index + ": "   + name;
	}

	@Override
	boolean isAvailable(Service service) {
		// 	If a Service has any amount of time overlapping the Interval, then that Service is counted as included in the Interval
		return service.availability.overlaps(interval);
	}

	@Override
	boolean isAvailable(Doctor doctor) {
		// 	If a Staff member has any amount of time of their Shift overlapping the Interval, then that Staff Member is counted as included in the Interval
		return doctor.availabilityOverlapsWith(interval);
	}

	public void manuallyAllocate(Doctor doctor, Service service) throws NexinException {
		manualAllocations.put(doctor, service);
	}

	public void recalcAssignability(Services services, Requirements requirements, Doctors doctors) {
		// assignability pre-calculation improves performance compare to in-place requirement.isAssignable(doctor) invocations
		assignabilityDoctorByService = new boolean[services.size()][doctors.size()];
		assignabilityDoctorByRequirement = new boolean[requirements.size()][doctors.size()];
		Service service;
		Doctor doctor;
		boolean serviceAvailable, doctorAvailable, doctorIsAssignable;
		for (int s = 0; s < assignabilityDoctorByService.length; s++) {
			service = services.get(s);
			serviceAvailable = isAvailable(service);
			for (int d= 0; d < assignabilityDoctorByService[s].length; d++) {
				doctor = doctors.get(d);
				doctorAvailable = isAvailable(doctor);
				doctorIsAssignable = doctorAvailable && doctor.isAssignable(this, service);  
				assignabilityDoctorByService[s][d] = false;
				for (Requirement requirement: service.requirements) {
					if (serviceAvailable && doctorIsAssignable && requirement.isAssignable(doctor)) {
						assignabilityDoctorByRequirement[requirement.id][d] = true;
						assignabilityDoctorByService[s][d] = true;
					} else
						assignabilityDoctorByRequirement[requirement.id][d] = false;
				}
			}
		}
	}
}

@SuppressWarnings("serial")
class TransitionTimes extends ArrayList<TransitionTime> {

	public void load(final int week, final int day) throws SQLException {
		TreeMap<LocalTime, TransitionTime> map = new TreeMap<LocalTime, TransitionTime>();
		String s;
		int hourOfDay, //the hour of the day, from 0 to 23
		minuteOfHour, // the minute of the hour, from 0 to 59
		pos;
		LocalTime startTime, endTime;
		TransitionTime prevTt = null;
		clear();
		PreparedStatement stmt = Globals.connector.prepareStatement( "SELECT * FROM tb_transition_time WHERE deleted_at IS NULL ORDER BY id");
		try {
			ResultSet rs = stmt.executeQuery();
			try {
				while (rs.next()) {
					s = rs.getString("start_time");
					pos = s.indexOf(':');
					hourOfDay = Integer.parseInt(s.substring(0, pos));
					minuteOfHour = Integer.parseInt(s.substring(pos + 1));
					startTime = new LocalTime(hourOfDay, minuteOfHour);
					s = rs.getString("end_time");
					pos = s.indexOf(':');
					hourOfDay = Integer.parseInt(s.substring(0, pos));
					minuteOfHour = Integer.parseInt(s.substring(pos + 1));
					endTime = new LocalTime(hourOfDay, minuteOfHour);
					TransitionTime tt = new TransitionTime(rs.getInt("id"), week, day, startTime, endTime);
					tt.name = rs.getString("name");
					map.put(startTime, tt);
					if (prevTt !=null)
						prevTt.nextTransitionTime = tt;
					prevTt = tt;
				}
			} finally {
				rs.close();
			}
		} finally {
			stmt.close();
		}
		TransitionTime prevEvent = null;
		for (Entry<LocalTime, TransitionTime> me: map.entrySet()) {
			TransitionTime event = me.getValue();
			add(event);
			if (prevEvent != null)
				prevEvent.next = event;
			prevEvent = event;
		}
	}

	public void recalcAssignability(Services services, Requirements requirements, Doctors doctors) {
		for (TransitionTime tt: this)
			tt.recalcAssignability(services, requirements, doctors);
	}

	@Override
	public boolean add(TransitionTime event) {
		event.index = size() + 1;
		return super.add(event);
	}

	public TransitionTime getTransitionTimeById(final int ttId) {
		for (TransitionTime event: this)
			if (event instanceof TransitionTime && ((TransitionTime)event).id == ttId)
				return (TransitionTime)event;
		return null;
	}

}

