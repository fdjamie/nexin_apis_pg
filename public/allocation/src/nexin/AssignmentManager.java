package nexin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import util.CommandLineParser;
import util.LogManager;

@SuppressWarnings("serial")
public class AssignmentManager {

	protected static final Logger log = Logger.getLogger(LogManager.APP_LOGGER);

	Integer assignmentTaskId, assignmentSessionId, assignmentSessionSqnNo, assignmentChangeId;
	ExcelPrinter excelPrinter;
	int penaltyForOneDoctorTransfer = 1;

	Services services = new Services();
	Requirements requirements = new Requirements();
	Doctors doctors = new Doctors();

	PreparedStatement stmtSelectAssignmentTaskByWeekDay, stmtSelectLatestAssignmentSession, stmtWeekDayByAssignmentChangeId, 
	stmtInsertAssignmentTask, stmtInsertAssignmentSession, stmtUpdateAssignmentSession,
	stmtInsertAssignment, stmtInsertMissingResource,
	stmtDeletePhantomAssignments, stmtInsertPhantomAssignment,
	stmtSelectAssignmentChangeAllocation, stmtSelectPhantoms;

	static class AssignmentSet extends TreeSet<Assignment> {

		public Assignment get(Assignment assignment) {
			Assignment ceil  = ceiling(assignment); // least >= assignment
			Assignment floor = floor(assignment);   // highest <= assignment
			return ceil == floor? ceil : null; 
		}

	}

	TransitionTimes transitionTimes = new TransitionTimes();

	private int assignmentCount;
	protected boolean debug;

	class Assignment implements Comparable<Assignment> { // Bellman equation state
		int id;  // unique number for testing purposes
		TransitionTime transitionTime;

		// work arrays for performance improvements despite data duplications:
		boolean[][] assignments = new boolean[services.size()][doctors.size()]; // rows numbers are requirement.id. column numbers are doctor.id
		int[] unfulfilledRequirements = new int[requirements.size()]; // rows numbers are requirement.id. Count of unfulfilled requirements
		boolean[] fulfilledServices = new boolean[services.size()]; // rows numbers are service.id. True if requirement fulfilled
		boolean[] assignedDoctors = new boolean[doctors.size()]; // rows numbers are doctor.id. True if doctor is assigned

		// Bellman equation members:
		long penaltiRequirements, penaltyForDoctorTransfers, W; // Bellman cumulative loss
		boolean knownPenaltyRequirements;
		ControlBellman optimalControl;
		Assignment next; // this -> optimalControl -> next

		public Assignment() {
			id = assignmentCount++; 
		}

		public Assignment(TransitionTime tt) {
			this();
			applyEvent(null, tt);
		}

		public Assignment(Assignment parent) {
			this();
			for (int s = 0; s < parent.assignments.length; s++)
				assignments[s] = parent.assignments[s].clone();
		}

		public Assignment(Assignment parent,TransitionTime tt) { // update assignment as per event content
			this(parent);
			applyEvent(parent, tt);
		}

		private void applyEvent(Assignment parent, TransitionTime tt) {
			int d, s;
			for (Doctor doctor: doctors)
				if ( ! tt.isAvailable(doctor))  {
					d = doctor.index;
					for (s = 0; s < assignments.length; s++)
						assignments[s][d] = false;
				}
			for (Service service: services)
				if ( ! tt.isAvailable(service)) {
					s = service.index;
					for (d = 0; d < assignments[s].length; d++)
						assignments[s][d] = false;
				}

			// apply manual allocations:
			for (Doctor doctor: doctors) {
				d = doctor.index;
				Service service = doctor.getManualAllocation(tt); 
				if ( service != null && service != getService(doctor)) {
					if (parent != null) {
						if (parent.optimalControl == null)
							parent.optimalControl = new ControlAllocate();
						((ControlAllocate) parent.optimalControl).add(new ControlAssign(doctor, service));
					}
					// resign the doctor from other services:
					long penalty = 0;
					for (s = 0; s < assignments.length; s++)
						if (assignments[s][d] && d != service.index) {
							penalty = penaltyForOneDoctorTransfer;
							assignments[s][d] = false;
						}
					penaltyForDoctorTransfers += penalty;
					// assign the doctor to the service:
					assignments[service.index][d] = true;
				}
			}
		}

		private void recalcFulfilment(TransitionTime tt) {
			this.transitionTime = tt;
			Service service;
			int count;
			for (int s = 0; s < assignments.length; s++) {
				service = services.get(s);
				if (tt.isAvailable(service)) {
					for (Requirement requirement: service.requirements) {
						count = requirement.count;
						for (int d= 0; d < assignments[s].length; d++)
							if (isAssigned(requirement, doctors.get(d))) {
								count--;
								assignedDoctors[d] = true;
							}
						//assert(count >= 0);
						unfulfilledRequirements[requirement.id] = count;
					}
				} /*else
					for (Requirement requirement: service.requirements) 
						unfulfilledRequirements[requirement.id] = 0; */
			}

			for (int s = 0; s < fulfilledServices.length; s++)
				fulfilledServices[s] = isServiceFulfilled(services.get(s), tt);

			calcPenaltyRequirements(tt);
		}

		boolean isServiceFulfilled(Service service, TransitionTime tt) {
			if (tt.isAvailable(service))
				for (Requirement requirement: service.requirements)
					if (unfulfilledRequirements[requirement.id] > 0)
						return false;
			return true;
		}

		public Assignment assign(TransitionTime tt, Service service, Doctor doctor) {
			int s = service.index,  d = doctor.index;
			if ( ! tt.assignabilityDoctorByService[s][d])
				return null;
			if (fulfilledServices[s])
				return null; // the service is already fulfilled
			if (assignedDoctors[d])
				return null; // the doctor is already assigned to a service

			for (Requirement requirement: service.requirements) {
				if (tt.assignabilityDoctorByRequirement[requirement.id][d] && unfulfilledRequirements[requirement.id] > 0) {
					Assignment result = new Assignment(this);
					result.assignments[s][d] = true;
					return result;
				}
			}
			return null;
		}

		public Assignment resign(Service service, Doctor doctor) {
			int s = service.index, d = doctor.index;
			if ( ! assignments[s][d])
				return null; // the doctor is not assigned to service

			Assignment result = new Assignment(this);
			result.assignments[s][d] = false;
			return result;
		}

		public void calcPenaltyRequirements(TransitionTime tt) { // unfulfilled requirements scaled by level weight:
			if (knownPenaltyRequirements)
				return;
			penaltiRequirements = 0;
			for (Service service: services)
				penaltiRequirements += calcPenaltyRequirements(tt, service);
			knownPenaltyRequirements = true;
		}

		long getLocalLoss() {
			if (next == null || next.transitionTime != transitionTime) {
				if ( ! knownPenaltyRequirements)
					calcPenaltyRequirements(transitionTime);
				return penaltiRequirements + penaltyForDoctorTransfers;
			}
			return penaltyForDoctorTransfers;
			//return penaltiRequirements + penaltyDoctorTransfer;
		}

		public long calcPenaltyRequirements(TransitionTime tt, Service service) {	// unfulfilled requirements scaled by level weight:
			long result = 0;
			if (tt.isAvailable(service)) {
				int count;
				for (Requirement requirement: service.requirements) {
					count = requirement.count;
					for (Doctor doctor: doctors)
						if (isAssigned(requirement, doctor))
							count--;
					result += Math.max(count, 0) * services.levelWeights.get(requirement.level);
				}
			}
			return result;
		}

		public long calcPenaltyRequirements(TransitionTime tt, Requirement requirement) {
			long result = 0;
			if (tt.isAvailable(requirement.service)) {
				int count;
				count = requirement.count;
				for (Doctor doctor: doctors)
					if (isAssigned(requirement, doctor))
						count--;
				result += Math.max(count, 0) * services.levelWeights.get(requirement.level);
			}
			return result;
		}

		@Override
		public String toString() {
			String result = null, str, ds;
			Doctor doctor;
			int s;
			Set<Integer> assignedDoctors = new TreeSet<Integer>();
			for (Service service: services) {
				assignedDoctors.clear();
				s = service.index;
				for (int d= 0; d < assignments[s].length; d++)
					if (assignments[s][d])
						assignedDoctors.add(d);
				ds = "";
				for (Integer d: assignedDoctors) {
					doctor = doctors.get(d);
					ds = ds.isEmpty() ? doctor.name : ds + "," + doctor.name;
				}
				str = service.name + ":[" + ds + "]";
				result = result == null ? str : result + "; " + str;		
			}
			return result == null ? "" : result;
		}

		public boolean isAssigned(Requirement requirement, Doctor doctor) {
			return isAssigned(requirement.service, doctor) && transitionTime.assignabilityDoctorByRequirement[requirement.id][doctor.index];
		}

		public boolean isAssigned(Service service, Doctor doctor) {
			return assignments[service.index][doctor.index];
		}

		public Service getService(Doctor doctor) {
			int d = doctor.index;
			for (int s = 0; s < assignments.length; s++)
				if (assignments[s][d])
					return services.get(s);
			return null;
		}

		@Override
		public int compareTo(Assignment assignment) {
			for (int s = 0; s < assignments.length; s++)
				for (int d = 0; d < assignments[s].length; d++) {
					if (assignments[s][d] && ! assignment.assignments[s][d])
						return 1;
					if (assignment.assignments[s][d] && ! assignments[s][d])
						return -1;
				}
			/*
			for (int d = 0; d < assignedDoctors.length; d++) {
				if (assignedDoctors[d] && ! assignment.assignedDoctors[d])
					return 1;
				if (assignment.assignedDoctors[d] && ! assignedDoctors[d])
					return -1;
			}
			for (int r = 0; r < unfulfilledRequirements.length; r++) {
				if (unfulfilledRequirements[r] != assignment.unfulfilledRequirements[r])
					return unfulfilledRequirements[r] - assignment.unfulfilledRequirements[r];
			}
			 */
			return (int) (penaltyForDoctorTransfers - assignment.penaltyForDoctorTransfers);
		}

		public int getmaxFulfilledLevel() {
			for (int level: services.levelWeights.keySet())
				if ( ! isLevelFulfilled(level))
					return level - 1;
			return services.levelWeights.size() > 0 ? services.levelWeights.lastKey() : 0;
		}

		private boolean isLevelFulfilled(final int level) {
			for (int r= 0; r < unfulfilledRequirements.length; r++ )
				if ( unfulfilledRequirements[r] > 0 && requirements.get(r).level == level)
					return false;
			return true;
		}

		public RequirementsByLevel getMissingResources() {
			RequirementsByLevel missingResources = new RequirementsByLevel();
			for (int r= 0; r < unfulfilledRequirements.length; r++ )
				if ( unfulfilledRequirements[r] > 0) {
					Requirement requirement = requirements.get(r); 
					List<Requirement> resources = missingResources.get(requirement.level);
					if (resources == null) {
						resources = new ArrayList<Requirement>();
						missingResources.put(requirement.level, resources);
					}
					resources.add(requirement);
				}
			return missingResources;
		}

	}

	interface ControlBellman { // Bellman equation control 
	}

	abstract class Control implements ControlBellman, Comparable<Control>{ 
		Doctor doctor;
		Service service;

		Control(Doctor doctor, Service service) {
			this.doctor = doctor;
			this.service = service;
		}

		@Override
		public int compareTo(Control ctrl) {
			int result = doctor.compareTo(ctrl.doctor);
			return result == 0 ? service.compareTo(ctrl.service) : result;
		}
	}

	class ControlAssign extends Control {

		ControlAssign(Doctor doctor, Service service) {
			super(doctor, service);
		}

		@Override
		public String toString() {
			return doctor.name + " assigned to " + service.name;
		}
	}

	class ControlResign extends Control{

		ControlResign(Doctor doctor, Service service) {
			super(doctor, service);
		}

		@Override
		public String toString() {
			return doctor.name + " resigned from " + service.name;
		}
	}

	class ControlAllocate extends ArrayList<ControlAssign> implements ControlBellman {
		@Override
		public String toString() {
			String s = null;
			for (Control ctrl: this)
				s = s == null ? ctrl.toString() : (s + ", " + ctrl.toString());
				return s;
		}
	}

	class ControlNone implements ControlBellman {

		@Override
		public String toString() {
			return "No changes";
		}
	}

	// Bellman equation executor
	void runDynamicProgramming(TransitionTime tt, Assignment assignment, final boolean allowDoctorTransfers, Set<Control> resignments, final boolean tryToNotChangeAnything) {
		tt.evaluatedAssignments++;
		boolean isCurrentEventLeaf = true; // that's the last substep of current event
		Assignment existing, nextAssignment;
		String margin = new String(new char[tt.index]).replace('\0', '\t');
		log.log(Level.FINE, margin + tt.toString() + " :  " + assignment.toString());
		Control ctrl;

		long W =Long.MAX_VALUE;

		if (tryToNotChangeAnything) {
			// try to not change anything:
			assignment.optimalControl = new ControlNone();
			nextAssignment = new Assignment(assignment);
			assignment.next = nextAssignment;
			nextAssignment.recalcFulfilment(tt);
			runDynamicProgramming(tt, nextAssignment, false, resignments, false);
			W = nextAssignment.W;
		}

		// try to assign a doctor:
		for (Doctor doctor: doctors)
			for (Service service: services) {
				nextAssignment = assignment.assign(tt, service, doctor);
				if (nextAssignment != null) {
					ctrl = new ControlAssign(doctor, service);
					log.log(Level.FINE, margin + ctrl.toString());
					if (resignments == null || ! resignments.contains(ctrl)) {
						isCurrentEventLeaf = false;
						tt.totalAssignments++;
						nextAssignment.recalcFulfilment(tt);
						existing = tt.visitedStates.get(nextAssignment);
						if (existing == null) {
							tt.visitedStates.add(nextAssignment);
							runDynamicProgramming(tt, nextAssignment, false, resignments, false);
						} else {
							tt.revisitedStates++;
							nextAssignment = existing;
						}
						if (W > nextAssignment.W) {
							W = nextAssignment.W;
							assignment.optimalControl = ctrl; 
							assignment.next = nextAssignment;
						}
					}
				}
			}

		if (allowDoctorTransfers && W > 0) { // try doctor transfers
			// try to resign a doctor:
			for (Doctor doctor: doctors)
				for (Service service: services) {
					nextAssignment = assignment.resign(service, doctor);
					if (nextAssignment != null) {
						ctrl = new ControlResign(doctor, service);
						if (resignments == null || ! resignments.contains(ctrl)) {
							isCurrentEventLeaf = false;
							tt.totalAssignments++;
							nextAssignment.penaltyForDoctorTransfers += penaltyForOneDoctorTransfer;
							nextAssignment.recalcFulfilment(tt);
							existing = tt.visitedStates.get(nextAssignment);
							if (existing == null) {
								tt.visitedStates.add(nextAssignment);
								Set<Control> newResignments = resignments == null? new TreeSet<Control>() : new TreeSet<Control>(resignments);
								newResignments.add(ctrl);
								runDynamicProgramming(tt, nextAssignment, allowDoctorTransfers, newResignments, false);
							} else {
								tt.revisitedStates++;
								nextAssignment = existing;
							}
							if (W > nextAssignment.W) {
								W = nextAssignment.W;
								assignment.optimalControl = ctrl; 
								assignment.next = nextAssignment;
							}
						}
					}
				}
		}

		if (isCurrentEventLeaf) {
			tt.leafAssignments++;
			if (debug && tt.leafAssignments >= tt.leafAssignmentsThreshold) {
				log.log(Level.INFO, tt.toString() + " leaves :  " + tt.leafAssignments);
				logProgress();
				tt.leafAssignmentsThreshold *= 2;
			}
			tt = tt.getNext();
			if (tt == null) // that's the final Bellman's step
				assignment.W = assignment.getLocalLoss();
			else { // handle next event:
				assignment.next = new Assignment(assignment, tt);
				assignment.next.recalcFulfilment(tt);
				existing = tt.visitedStates.get(assignment.next);
				if (existing == null) {
					tt.visitedStates.add(assignment.next);
					runDynamicProgramming(tt, assignment.next, true, null, true);
				} else {
					tt.revisitedStates++;
					assignment.next = existing;
				}
				assignment.W = assignment.getLocalLoss() + assignment.next.W;
			}
		} else
			//assignment.W = W;
			//assignment.W = assignment.penaltyForDoctorTransfers + W;
			assignment.W = assignment.getLocalLoss() + W;
	}

	private void logProgress() {
		if (debug)
			for (TransitionTime tt: transitionTimes) {
				log.log(Level.INFO, String.format("%-50s", tt.getDescription()) + 
						"\t " + tt.localTime.toString(Interval.hour_min_formatter) + 
						"\tTotal assignments :  " + tt.totalAssignments + 
						"\tEvaluated assignments : " + tt.evaluatedAssignments + 
						"\tRevisited States :  " + tt.revisitedStates + 
						"\tLeaf Assignments :  " + tt.leafAssignments);
			}
	}

	// returns exit code
	int run(String[] args) {
		log.log(Level.INFO, getClass().getSimpleName());
		try {
			CommandLineParser cmdParser = new CommandLineParser(args);
			cmdParser.log(Level.INFO, log);

			debug = cmdParser.containsKey("debug");
			if (cmdParser.containsKey("output"))
				excelPrinter = new ExcelPrinter(this, cmdParser.get("output"));
			try {
				init(cmdParser);
				services.load();
				doctors.load();		
				if (services.isEmpty())
					throw new NexinException("No sevices!");
				if (doctors.isEmpty())
					throw new NexinException("No doctors!");
	
				requirements.arrange(services);
				calcLevelWeights();
	
				run(cmdParser);
			} finally {
				if (excelPrinter != null) {
					excelPrinter.closeBook();
					excelPrinter = null;
				}
			}

			return getExitCode();
		} catch (Exception e) {
			System.err.println(LogManager.getStackTraceAsString(e));
			return 3;
		}
	}

	protected int getExitCode() {
		if (LogManager.getLogCountByLevel(Level.SEVERE) > 0)
			return 2;
		if (LogManager.getLogCountByLevel(Level.WARNING) > 0)
			return 1;
		return 0;
	}

	protected void run(CommandLineParser cmdParser) throws Exception {
		if (cmdParser.containsKey("assignmentChange")) {
			assignmentChangeId = Integer.parseInt(cmdParser.get("assignmentChange"));
			stmtWeekDayByAssignmentChangeId.setInt(1, assignmentChangeId);
			ResultSet rs = stmtWeekDayByAssignmentChangeId.executeQuery();
			try {
				if (rs.next()) {
					assignmentTaskId = rs.getInt("assignment_task");
					runOneDay(rs.getInt("week"), rs.getInt("day"));
				} else
					throw new NexinException("Could not find Assignment Task with id=" + assignmentTaskId);
			} finally {
				rs.close();
			}
		} else {
			assignmentChangeId = null;
			if (! cmdParser.containsKey("week"))
				throw new NexinException("Week is not specified");
			Integer week =  Integer.parseInt(cmdParser.get("week"));
			if ( ! cmdParser.containsKey("day"))
				throw new NexinException("Day is not specified");
			Integer day = Integer.parseInt(cmdParser.get("day"));
			runOneDay(week, day);
		}
	}

	private void calcLevelWeights() {
		// calc how many assignments there are on every level:
		Integer count;
		TreeMap<Integer/*level*/, Integer/*count*/> countsByLevel = new TreeMap<Integer, Integer>();
		for (Requirement requirement: requirements) {
			count = countsByLevel.get(requirement.level);
			countsByLevel.put(requirement.level, count == null ? requirement.count : count + requirement.count);
		}
		int maxPenaltyForDoctorTransfer = penaltyForOneDoctorTransfer * doctors.size(); // for one event
		int maxReqCountPerLevel = countsByLevel.size() > 0 ? Collections.max(countsByLevel.values()) : 0;
		int lossFunctionfactor = Math.max(maxReqCountPerLevel + 1, 10) * (maxPenaltyForDoctorTransfer + 1);
		int maxLevel = services.levelWeights.size() > 0 ? Collections.max(services.levelWeights.keySet()) + 1 : 0;
		services.levelWeights.replaceAll((level, weight) -> (long) Math.pow(lossFunctionfactor, (maxLevel - level)));
		if (debug) {
			log.log(Level.INFO, "Max Penalty For Doctor Transfers per Transition Time: " + maxPenaltyForDoctorTransfer);
			log.log(Level.INFO, "Requirement Counts by level: " + countsByLevel);
			log.log(Level.INFO, "Max Requirement Count Per Level = " + maxReqCountPerLevel);
			log.log(Level.INFO, "Loss function factor = " + lossFunctionfactor);
			log.log(Level.INFO, "Loss function weights by level: " + services.levelWeights);
		}
	}

	protected void runOneDay(final int week, final int day) throws Exception {
		log.log(Level.INFO, "Running week=" + week + " day=" + day);
		
		initAssignmentSession(week, day);	

		transitionTimes.load(week, day);
		if (transitionTimes.isEmpty()) 
			throw new NexinException("No events to schedule!");

		loadAssignmentChange();

		printInputData();
		printTimeline(week, day);

		transitionTimes.recalcAssignability(services, requirements, doctors);
		Assignment dummyAssignment = new Assignment();
		print(dummyAssignment, "Assignability", false);

		log.log(Level.INFO, Globals.parser.printValueNames());

		long startTime = System.currentTimeMillis();

		TransitionTime startEvent = transitionTimes.get(0);
		log.log(Level.INFO, "Dynamic programming for event sequence starting from " + startEvent.getDescription());
		Assignment assignment = new Assignment(startEvent);
		assignment.recalcFulfilment(startEvent);
		runDynamicProgramming(startEvent, assignment, false, null, false);

		long estimatedTime = System.currentTimeMillis() - startTime;
		log.log(Level.INFO, "Optimization is accomplished in " + estimatedTime / 1000 + " seconds");
		logProgress();

		List<Assignment> debugTrack = new ArrayList<Assignment>();
		List<Assignment> solution = new ArrayList<Assignment>();
		boolean isSolution = false;// skip the initial assignment
		int count = 0;
		while (assignment != null) {
			debugTrack.add(assignment);
			if (isSolution && (assignment.next == null || assignment.next.transitionTime != assignment.transitionTime))
				solution.add(assignment);
			assignment = assignment.next;
			isSolution = true;
			if (count++ > 100)
				break;
		}
		if (debug)
			print(debugTrack, "Debug track", true);
		print(solution, "Solution", false);
	}

	protected void initAssignmentSession(final int week, final int day) throws SQLException {
		if (assignmentTaskId == null) {
			stmtSelectAssignmentTaskByWeekDay.setInt(1, week);
			stmtSelectAssignmentTaskByWeekDay.setInt(2, day);
			ResultSet rs = stmtSelectAssignmentTaskByWeekDay.executeQuery();
			try {
				if ( rs.next() ) { 
					assignmentTaskId = rs.getInt(1);
				}
			} finally {
				rs.close();
			}
		}
		if (assignmentTaskId == null ) { //this task is new
			stmtInsertAssignmentTask.setInt(1, week);
			stmtInsertAssignmentTask.setInt(2, day);
			assignmentTaskId = Globals.connector.executeAndGetGeneratedKey(stmtInsertAssignmentTask);
			assignmentSessionSqnNo = 1;
		} else {
			stmtSelectLatestAssignmentSession.setInt(1, assignmentTaskId);
			ResultSet rs = stmtSelectLatestAssignmentSession.executeQuery();
			try {
				if ( rs.next() ) { 
					assignmentSessionSqnNo = rs.getObject(1) == null ? 1 : rs.getInt(1) + 1;
				}
			} finally {
				rs.close();
			}
		}
		stmtInsertAssignmentSession.setInt(1, assignmentTaskId);
		stmtInsertAssignmentSession.setInt(2, assignmentSessionSqnNo);
		if (assignmentChangeId == null)
			stmtInsertAssignmentSession.setNull(3, java.sql.Types.INTEGER);
		else
			stmtInsertAssignmentSession.setInt(3, assignmentChangeId);
		assignmentSessionId = Globals.connector.executeAndGetGeneratedKey(stmtInsertAssignmentSession);
		log.log(Level.INFO, "Assignment Task id=" + assignmentTaskId + ", session id=" + assignmentSessionId + ", session sqn.no=" + assignmentSessionSqnNo);
	}

	private void loadAssignmentChange() throws Exception {
		if (assignmentChangeId == null) 
			return;

		log.log(Level.INFO, "Loading assignment change id=" + assignmentChangeId);
		int ttId;
		TransitionTime tt;
		stmtSelectAssignmentChangeAllocation.setInt(1, assignmentChangeId);
		ResultSet rs = stmtSelectAssignmentChangeAllocation.executeQuery();
		try {
			while (rs.next()) {
				ttId = rs.getInt("transition_time");
				tt = transitionTimes.getTransitionTimeById(ttId);
				if (tt == null)
					throw new NexinException("Could not find transition time with id=" + ttId);
				tt.manuallyAllocate(doctors.getById(rs.getInt("staff")), services.getById(rs.getInt("service")));
			}
		} finally {
			rs.close();
		}

		Phantom phantom;
		stmtSelectPhantoms.setInt(1, assignmentChangeId);
		rs = stmtSelectPhantoms.executeQuery();
		try {
			while (rs.next()) {
				phantom = new Phantom(doctors.size(), rs);
				doctors.add(phantom);
			}
		} finally {
			rs.close();
		}
	}

	protected void init(CommandLineParser cmdParser) throws Exception {
		Globals.init(cmdParser);
		stmtWeekDayByAssignmentChangeId = Globals.connector.prepareStatement(
				"SELECT c.assignment_task, t.week, t.day FROM Assignment_Change c JOIN Assignment_Task t ON t.id = c.assignment_task WHERE c.id = ?");
		stmtSelectAssignmentTaskByWeekDay = Globals.connector.prepareStatement(
				"SELECT id FROM Assignment_Task WHERE week = ? AND day = ?");
		stmtSelectLatestAssignmentSession = Globals.connector.prepareStatement(
				"SELECT MAX(sqn_no) FROM Assignment_Session WHERE assignment_task = ?");
		stmtInsertAssignmentTask = Globals.connector.prepareStatementGenerateKeys(
				"INSERT INTO Assignment_Task(week, day) VALUES(?, ?)"); 
		stmtInsertAssignmentSession = Globals.connector.prepareStatementGenerateKeys(
				"INSERT INTO Assignment_Session(assignment_task, sqn_no, assignment_change) VALUES(?, ?, ?)");
		stmtUpdateAssignmentSession = Globals.connector.prepareStatementGenerateKeys(
				"UPDATE Assignment_Session SET details = to_json(?::json) WHERE id = ?");
		stmtInsertAssignment = Globals.connector.prepareStatement(
				"INSERT INTO Assignment(assignment_session, transition_time, service, staff) VALUES(?, ?, ?, ?)"); 
		stmtInsertMissingResource = Globals.connector.prepareStatement(
				"INSERT INTO Missing_Resource(assignment_session, transition_time, level, service, requirement, count) VALUES(?, ?, ?, ?, ?, ?)"); 
		stmtDeletePhantomAssignments = Globals.connector.prepareStatement(
				"DELETE FROM Phantom_Assignment WHERE phantom IN (SELECT id FROM Phantom WHERE assignment_change= ?)"); 
		stmtInsertPhantomAssignment = Globals.connector.prepareStatement(
				"INSERT INTO Phantom_Assignment(phantom, transition_time, service) VALUES(?, ?, ?)"); 
		stmtSelectAssignmentChangeAllocation = Globals.connector.prepareStatement(
				"SELECT * FROM Assignment_Change_Allocation WHERE assignment_change = ?");
		stmtSelectPhantoms = Globals.connector.prepareStatement(
				"SELECT * FROM Phantom WHERE assignment_change = ?");
	}

	protected void printInputData() throws Exception {
		String s = "SERVICES:";
		for (Service service: services) {
			s = s + "\n\t" + service.toString();
			for (Interval interval: service.availability)
				s = s + "\t" + interval.toString();
			for (Requirement requirement: service.requirements)
				s = s + "\n\t\t" + requirement.toString();
		}
		log.log(Level.INFO, s);

		s = "STAFF:";
		for (Doctor doctor: doctors) {
			s = s + "\n\t" + doctor.toString() + " db.id=" + doctor.id + " index=" + doctor.index;
			for (Entry<ParameterDefinition, Parameter> me: doctor.parameters.entrySet())
				s = s + "\n\t\t" + me.getKey().name + " : " + me.getValue();
		}
		log.log(Level.INFO, s);
	}

	public void printTimeline(final int week, final int day) throws Exception {
		String s = "Transition times:";
		for (int i = 0; i < transitionTimes.size() ; i++) {
			transitionTimes.get(i).next = i < transitionTimes.size() - 1 ? transitionTimes.get(i + 1) : null;
			s = s + "\n\t" + transitionTimes.get(i);
		}
		log.log(Level.INFO, s);
		if (excelPrinter != null)
			excelPrinter.printTimeline(week, day);
	}

	private void print(Assignment assignment, String caption, final boolean debugInfo) throws Exception {
		List<Assignment> assignments = new ArrayList<Assignment>(1);
		assignments.add(assignment);
		print(assignments, caption, debugInfo);
	}

	private void print(List<Assignment> assignments, String caption, final boolean debugInfo) throws Exception {
		String s = caption;
		Assignment prevAssignment = null;
		for (Assignment assignment: assignments) {
			if (assignment.transitionTime != null)
				s = s + "\n\t" + assignment.transitionTime.getDescription();
			s = s + "\n\t\t(Ass.id=" + assignment.id + ")";
			s = s + "\n\t" + assignment.toString();
			if (prevAssignment != null && prevAssignment.next == assignment && prevAssignment.optimalControl != null) {
				s = s + "\n\t" + prevAssignment.optimalControl.toString();
			}
			if (assignment.transitionTime != null) {
				s = s + "\n\t\tBellman loss (a distance from the max possible score): " + assignment.W; 
				s = s + "\n\t\tDoctor transfers: " + assignment.penaltyForDoctorTransfers / penaltyForOneDoctorTransfer; 
				s = s + "\n\t\tMax fulfilled level: " + assignment.getmaxFulfilledLevel() + " from " + 
						(services.levelWeights.size() > 0 ? services.levelWeights.lastKey() : 0);
				RequirementsByLevel missingResources = assignment.getMissingResources();
				if (missingResources.size() > 0) {
					s = s + "\n\t\tMissing resources by level :";
					for (Entry<Integer, List<Requirement>> me: missingResources.entrySet()) {
						s = s + "\n\t\t\t" + me.getKey() + " :  ";
						for (Requirement requirement: me.getValue())
							s = s + "\t\t" + requirement.func.toString() + " :  " + requirement.count;
					}
				}
			}
			if (debugInfo) {
				s = s + "\n\t\tAssigned Doctors :  ";
				for (int d = 0; d < assignment.assignedDoctors.length; d++)
					if (assignment.assignedDoctors[d])
						s = s + "\n\t\t\t" + doctors.get(d).name + " :  " + assignment.getService(doctors.get(d)).toString() + " ";					
				s = s + "\n\t\tFulfilled Services :  ";
				for (int d = 0; d < assignment.fulfilledServices.length; d++)
					if (assignment.fulfilledServices[d])
						s = s + services.get(d).name + " ";					
				s = s + "\n\t\tUnFulfilled Reuqirements :  ";
				for (int d = 0; d < assignment.unfulfilledRequirements.length; d++)
					s = s + requirements.get(d).toString() + ":" + assignment.unfulfilledRequirements[d] + " ";					
			}
			prevAssignment = assignment;
		}
		log.log(Level.INFO, s);

		if (excelPrinter != null)
			excelPrinter.print(assignments, caption, debugInfo);

		if ( ! debugInfo && assignments.size() > 0 && assignments.get(0).transitionTime != null)
			saveAssignmentsToDB(assignments);
	}

	protected void saveAssignmentsToDB(List<Assignment> assignments) throws SQLException, Exception {
		if (assignmentChangeId != null) {
			stmtDeletePhantomAssignments.setInt(1,  assignmentChangeId);
			stmtDeletePhantomAssignments.executeUpdate();
		}

		stmtInsertAssignment.setInt(1, assignmentSessionId);
		stmtInsertMissingResource.setInt(1, assignmentSessionId);

		TransitionTime tt;
		Service service;
		//RequirementsByTransitionTime missingResourcesByTransitionTime = new RequirementsByTransitionTime();  
		Integer maxFulfilledLevel = null;
		for (Assignment assignment: assignments) {
			maxFulfilledLevel =  maxFulfilledLevel == null ? assignment.getmaxFulfilledLevel() : Math.min(maxFulfilledLevel, assignment.getmaxFulfilledLevel());
			tt = assignment.transitionTime;
			stmtInsertAssignment.setInt(2, tt.id);
			for (Doctor doctor: doctors) {
				service = assignment.getService(doctor);
				if (service != null) {
					if (doctor instanceof Phantom) {
						stmtInsertPhantomAssignment.setInt(1, doctor.id);
						stmtInsertPhantomAssignment.setInt(2, tt.id);
						stmtInsertPhantomAssignment.setInt(3, service.id);
						stmtInsertPhantomAssignment.executeUpdate();
					} else {
						stmtInsertAssignment.setInt(3, service.id);
						stmtInsertAssignment.setInt(4, doctor.id);
						stmtInsertAssignment.executeUpdate();
					}
				}
			}

			stmtInsertMissingResource.setInt(2, tt.id);
			RequirementsByLevel missingResourcesByLevel = assignment.getMissingResources();
			//missingResourcesByTransitionTime.put(tt, missingResourcesByLevel);
			for (Entry<Integer, List<Requirement>> me: missingResourcesByLevel.entrySet()) {
				stmtInsertMissingResource.setInt(3, me.getKey());
				for (Requirement requirement: me.getValue()) {
					stmtInsertMissingResource.setInt(4, requirement.service.id);
					stmtInsertMissingResource.setString(5, requirement.func.toString());
					stmtInsertMissingResource.setInt(6, requirement.count);
					stmtInsertMissingResource.executeUpdate();
				}
			}
		}

		JSONObject details = new JSONObject();
		details.append("Levels", services.levelWeights.size());
		details.append("Doctors", doctors.size());
		details.append("Services", services.size());
		details.append("Requirements", requirements.size());
		details.append("Transition times", transitionTimes.size());
		if (maxFulfilledLevel != null)
			details.append("maxFulfilledLevel", maxFulfilledLevel);
		if (assignments.size() > 0)
			details.append("Bellman loss", assignments.get(0).W);
		int phantomCount = 0, newPhantomCount = 0;
		for (Doctor doctor: doctors)
			if (doctor instanceof Phantom) {
				phantomCount++;
				if (((Phantom)doctor).isAssignableToAny)
					newPhantomCount++;
			}
		if (phantomCount > 0) {
			details.append("Phantoms", phantomCount);
			if (newPhantomCount > 0)
				details.append("New Phantoms", newPhantomCount);
		}
		stmtUpdateAssignmentSession.setString(1, details.toString());
		stmtUpdateAssignmentSession.setInt(2, assignmentSessionId);
		stmtUpdateAssignmentSession.executeUpdate();
		
		//PhantomManager.generatePhantoms(missingResourcesByTransitionTime);
	}

	static {
		LogManager.setup(AssignmentManager.class.getSimpleName() + ".log");
		log.setLevel(Level.INFO);
	}

}
