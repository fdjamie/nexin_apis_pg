package nexin;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.LocalTime;

import nexin.AssignmentManager.Assignment;
import util.LogManager;

public class ExcelPrinter{
	protected static final Logger log = Logger.getLogger(LogManager.APP_LOGGER);
	
	AssignmentManager assignmentManager;

	private int rowLimit = 5000;
	File excelFile;
	Workbook workBook;
	CellStyle styleTitle, styleAssignment, styleNotAssignable, styleBorderLeft, styleBorderTop, styleBold, styleBoldCenter, styleAllocation;

	private void openBook(String excelFileName) throws IOException {
		if (excelFileName == null) 
			return;
		excelFile = new File(excelFileName);
		log.log(Level.INFO, "Saving results to " + excelFile.getAbsolutePath());

		if (excelFile.exists()) {
			FileInputStream srcStream = new FileInputStream(excelFile);
			try {
				workBook = new HSSFWorkbook(srcStream); // .xls
			} catch (OfficeXmlFileException e) { 
				srcStream.close();
				srcStream = new FileInputStream(excelFile);
				workBook =  new XSSFWorkbook(srcStream); // .xlsx
			} catch (IOException e) {
				log.log(Level.WARNING, e.getMessage());
				workBook = new XSSFWorkbook();
			}
		} else 
			workBook = new XSSFWorkbook();

		Font headerFont = workBook.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setFontHeightInPoints((short) 12);

		styleBold = workBook.createCellStyle();
		styleBold.setFont(headerFont);

		styleBoldCenter = workBook.createCellStyle();
		styleBoldCenter.setAlignment(CellStyle.ALIGN_CENTER);
		styleBoldCenter.setFont(headerFont);

		styleTitle = workBook.createCellStyle();
		styleTitle.setAlignment(CellStyle.ALIGN_CENTER);
		styleTitle.setFont(headerFont);
		styleTitle.setBorderBottom(CellStyle.BORDER_THIN);
		styleTitle.setTopBorderColor(IndexedColors.BLACK.getIndex());

		styleAssignment = workBook.createCellStyle();
		styleAssignment.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
		styleAssignment.setFillPattern(CellStyle.SOLID_FOREGROUND);

		styleNotAssignable = workBook.createCellStyle();
		styleNotAssignable.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleNotAssignable.setFillPattern(CellStyle.SOLID_FOREGROUND);

		styleBorderLeft = workBook.createCellStyle();
		styleBorderLeft.setBorderLeft(CellStyle.BORDER_THIN);
		styleBorderLeft.setTopBorderColor(IndexedColors.BLACK.getIndex());

		styleBorderTop = workBook.createCellStyle();
		styleBorderTop.setBorderTop(CellStyle.BORDER_THIN);
		styleBorderTop.setTopBorderColor(IndexedColors.BLACK.getIndex());

		styleAllocation = workBook.createCellStyle();
		styleAllocation.setFillForegroundColor(IndexedColors.AQUA.getIndex());
		styleAllocation.setFillPattern(CellStyle.SOLID_FOREGROUND);
	}

	void closeBook() throws IOException {
		if (workBook == null)
			return;
		FileOutputStream out = new FileOutputStream(excelFile.getAbsolutePath());
		try {
			workBook.write(out);
		} finally {
			out.close();
		}
		workBook.close();
		if (Desktop.isDesktopSupported())
			Desktop.getDesktop().open(excelFile);
		
	}

	public void printTimeline(final int week, final int day) throws Exception {
		if (workBook == null)
			return;
		String sheetName = "Timeline";
		Sheet sheet = workBook.getSheet(sheetName);
		if (sheet != null)
			workBook.removeSheetAt(workBook.getSheetIndex(sheet));
		sheet = workBook.createSheet(sheetName);

		log.log(Level.INFO, excelFile.getAbsolutePath() + "[" + sheet.getSheetName() + "]'");

		workBook.setActiveSheet(workBook.getSheetIndex(sheet));
		workBook.setSelectedTab(workBook.getSheetIndex(sheet));

		int baseCol = 1, colCaption = baseCol - 1, col;
		int rowNo = 0;
		Cell cell;

		sheet.setColumnWidth(colCaption, 12000);
		sheet.setDefaultColumnStyle(colCaption, styleBoldCenter);
		Row row, rowLocalTime = sheet.createRow(rowNo++);
		rowLocalTime.setRowStyle(styleBoldCenter);
		Row rowEvent = sheet.createRow(rowNo++);
		rowEvent.setRowStyle(styleBoldCenter);

		cell = rowLocalTime.createCell(colCaption);
		cell.setCellValue("Time");
		cell = rowEvent.createCell(colCaption);
		cell.setCellValue("Event");

		rowNo = 3;
		Map<Service, Row> serviceRows = new TreeMap<Service, Row>(); 
		for (Service service: assignmentManager.services) {
			row = sheet.createRow(rowNo++);
			serviceRows.put(service, row);
			row.createCell(colCaption).setCellValue(service.toString());
		}
		rowNo++;
		Map<Doctor, Row> doctorRows = new TreeMap<Doctor, Row>(); 
		for (Doctor doctor: assignmentManager.doctors) {
			row = sheet.createRow(rowNo++);
			doctorRows.put(doctor, row);
			row.createCell(colCaption).setCellValue(doctor.toString());
		}

		rowNo = 2;
		col = baseCol;
		
		List<Event> events = collectAllEvents(week, day);
		LocalTime lt;
		for (Event event: events) {
			lt = event.localTime;
			cell = rowLocalTime.createCell(col);
			cell.setCellValue(lt.toString(Interval.hour_min_formatter));
			cell = rowEvent.createCell(col);
			cell.setCellValue(event.toString());
			if (event instanceof TransitionTime)
				cell.setCellStyle(styleBold);
			for (Entry<Service, Row> rr: serviceRows.entrySet())
				if (event.isAvailable(rr.getKey())) {
					cell = rr.getValue().createCell(col);
					cell.setCellStyle(styleAssignment);
				}
			Doctor doctor;
			Service service;
			for (Entry<Doctor, Row> rr: doctorRows.entrySet()) {
				doctor = rr.getKey();
				service = event instanceof TransitionTime ? doctor.getManualAllocation((TransitionTime)event) : null;
				if (service != null) {
					cell = rr.getValue().createCell(col);
					cell.setCellValue(service.toString());
					cell.setCellStyle(styleAllocation);
				} else if (event.isAvailable(doctor)) {
					cell = rr.getValue().createCell(col);
					cell.setCellStyle(styleAssignment);
				}
			}
			col++;
		}
	}

	private void append(TreeMap<LocalTime, Event> map, Integer week, Integer day, LocalTime lt, Resource resource, Boolean isOn) throws NexinException {
		//Entry<LocalTime, Event> last = map.lastEntry();
		//if (last != null && last.ge)

		Event event = map.get(lt);
		if (event == null) {
			event = new ResourceEvent(week, day, lt);
			map.put(lt, event);
			((ResourceEvent)event).resources.put(resource, isOn);
		} 
		//else if (event.get(resource) != null)
		//throw new BascionException("Event " + event + " at " + lt.toString(hour_min_formatter) + " already contains " + resource);
//		event.put(resource, isOn);
	}

	private List<Event> collectAllEvents(final int week, final int day) throws Exception {
		List<Event> events = new ArrayList<Event>();
		
		TreeMap<LocalTime, Event> map = new TreeMap<LocalTime, Event>();
		
		for (TransitionTime tt: assignmentManager.transitionTimes)
			map.put(tt.interval.getStart(), tt);

		// append events from service and doctor availability.
		LocalTime lt;
		for (Service service: assignmentManager.services) { 
			for (Interval interval: service.availability) 
				if ((interval.week == null || interval.week == week) && (interval.day == null || interval.day == day)) {
					lt = interval.getStart();
					append(map, interval.week == null ? week : interval.week, interval.day == null ? day : interval.day, lt, service, true);
					lt = interval.getEnd();
					append(map, interval.week == null ? week : interval.week, interval.day == null ? day : interval.day, lt, service, false);
				}
		}
		for (Doctor doctor: assignmentManager.doctors)
			for (Interval interval: doctor.availability)
				if ((interval.week == null || interval.week == week) && (interval.day == null || interval.day == day)) {
					lt = interval.getStart();
					append(map, interval.week == null ? week : interval.week, interval.day == null ? day : interval.day, lt, doctor, true);
					lt = interval.getEnd();
					append(map, interval.week == null ? week : interval.week, interval.day == null ? day : interval.day, lt, doctor, false);
				}
		//Event startOfChain = null, prevEvent = null;
		for (Entry<LocalTime, Event> me: map.entrySet()) {
			Event event = me.getValue();
			events.add(event); 
			if (! (event instanceof TransitionTime))
				event.index = events.size(); 
			/*
			if (startOfChain == null) {
				events.add(event);
				prevEvent = event;
				startOfChain = event;
				log.log(Level.INFO, me.getKey().toString(Interval.hour_min_formatter) + " :  " + event.getDescription());
			} else if (! (startOfChain instanceof ResourceEvent && event instanceof ResourceEvent && ((ResourceEvent)startOfChain).sameResources((ResourceEvent)event))) {
				if (prevEvent != startOfChain || 
						! (prevEvent instanceof ResourceEvent && startOfChain instanceof ResourceEvent && ((ResourceEvent)prevEvent).isRepetitionOf((ResourceEvent)startOfChain))) {
					events.add(prevEvent);
					startOfChain.next = prevEvent;
				}
				events.add(event);
				prevEvent.next = event;
				prevEvent = event;
				startOfChain = event;
				log.log(Level.INFO, me.getKey().toString(Interval.hour_min_formatter) + " :  " + event.getDescription());
			}
			*/
		}
		
		return events;
	}

	protected void print(List<Assignment> assignments, String caption, final boolean debugInfo) throws Exception {
		if (workBook == null)
			return;
		caption = caption.replace(':', '_');
		Sheet sheet = workBook.getSheet(caption);
		if (sheet != null)
			workBook.removeSheetAt(workBook.getSheetIndex(sheet));
		sheet = workBook.createSheet(caption);

		log.log(Level.INFO, excelFile.getName() + "[" + sheet.getSheetName() + "]");

		workBook.setActiveSheet(workBook.getSheetIndex(sheet));
		workBook.setSelectedTab(workBook.getSheetIndex(sheet));

		//for (Assignment assignment: assignments)
		//items.addAll(assignment.byItem.keySet()); 

		int baseCol = 0;
		int rowNo = sheet.getLastRowNum();
		int columnDebug = 5 + assignmentManager.doctors.size();
		Cell cell;

		Row rowItem = sheet.createRow(rowNo++);
		for (int i = 0; i < Globals.parser.parameters.size(); i++)
			sheet.createRow(rowNo + i);

		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		rowItem.createCell(baseCol).setCellValue(df.format(Calendar.getInstance().getTime()));
		rowItem.createCell(baseCol + 1).setCellValue(
				"Assignment Task id=" + assignmentManager.assignmentTaskId + ", session id=" + assignmentManager.assignmentSessionId + 
				", session sqn.no=" + assignmentManager.assignmentSessionSqnNo);

		int colDoctor = baseCol + 3;
		rowItem.createCell(colDoctor).setCellValue("Doctor");
		rowNo = rowItem.getRowNum() + 1;
		for (ParameterDefinition parameter: Globals.parser.parameters.values())
			sheet.getRow(rowNo++).createCell(colDoctor).setCellValue(parameter.name);
		colDoctor++;
		for (int c = colDoctor; c <= colDoctor + assignmentManager.doctors.size(); c++)
			sheet.setColumnWidth(c, 4000);
		Map<Doctor, Integer> doctorByColumn = new TreeMap<Doctor, Integer>(); 
		Parameter param;
		for (Doctor doc: assignmentManager.doctors) {
			cell = rowItem.createCell(colDoctor);
			cell.setCellValue(doc.name);
			rowNo = rowItem.getRowNum() + 1;
			for (ParameterDefinition parameter: Globals.parser.parameters.values()) {
				param = doc.parameters.get(parameter);
				if (param != null)
					sheet.getRow(rowNo).createCell(colDoctor).setCellValue(param.toString());
				rowNo++;
			}
			doctorByColumn.put(doc, colDoctor++);
		}

		rowNo = rowItem.getRowNum() + Globals.parser.parameters.size() + 1;
		Row row = sheet.createRow(rowNo++);
		row.createCell(baseCol).setCellValue("Level");
		sheet.setColumnWidth(baseCol, 3000);
		row.createCell(baseCol + 1).setCellValue("Service");
		sheet.setColumnWidth(baseCol + 1, 5000);
		row.createCell(baseCol + 2).setCellValue("N");
		sheet.setColumnWidth(baseCol + 2, 1000);
		row.createCell(baseCol + 3).setCellValue("Requirement");
		sheet.setColumnWidth(baseCol + 3, 5000);
		row.createCell(columnDebug).setCellValue("Weight");
		sheet.setColumnWidth(columnDebug, 5000);
		row.createCell(columnDebug + 1).setCellValue("Local Loss (w)");
		sheet.setColumnWidth(columnDebug + 1, 5000);
		row.createCell(columnDebug + 2).setCellValue("Bellman loss (W)");
		sheet.setColumnWidth(columnDebug + 2, 5000);
		if (debugInfo) {
			row.createCell(columnDebug + 3).setCellValue("Transfers penalty");
			sheet.setColumnWidth(columnDebug + 3, 5000);
		}

		for (int i = row.getFirstCellNum(); i <= row.getLastCellNum(); i++) {
			cell = row.getCell(i);
			if (cell != null)
				cell.setCellStyle(styleTitle);
		}

		sheet.createFreezePane(baseCol + 4, rowNo);

		int level = -1;
		Assignment prevAssignment = null;
		for (Assignment assignment: assignments) {
			if (assignment.transitionTime != null) {
				row = sheet.createRow(rowNo++);
				cell = row.createCell(baseCol);
				cell.setCellValue(assignment.transitionTime.getDescription());
				if (prevAssignment == null || prevAssignment.transitionTime != assignment.transitionTime)
					cell.setCellStyle(styleBold);
				for (Doctor doctor: assignmentManager.doctors)
					if ( ! assignment.transitionTime.isAvailable(doctor)) 
						row.createCell(doctorByColumn.get(doctor)).setCellStyle(styleNotAssignable);
			}
			row = sheet.createRow(rowNo++);
			cell = row.createCell(baseCol);
			cell.setCellValue("Ass.id=" + assignment.id);
			cell.setCellStyle(styleBold);
			
			cell = row.createCell(baseCol + 1);
			cell.setCellValue(assignment.toString());
			
			if (prevAssignment != null) {
				String optimalControls = null;
				Assignment a = prevAssignment;
				while (a != assignment) {
					if (a.optimalControl != null)
						optimalControls = (optimalControls == null ? "" : (optimalControls + ", ")) + a.optimalControl.toString();  
					a = a.next;
				}
				cell = row.createCell(columnDebug);
				cell.setCellValue(optimalControls);
				cell.setCellStyle(styleBold);
			}
			for (Requirement requirement: assignmentManager.requirements) {
				row = sheet.createRow(rowNo++);
				int col = baseCol;
				
				cell = row.createCell(col++);
				if (requirement.level != level) {
					level = requirement.level;
					cell.setCellValue(requirement.level);
					row.setRowStyle(styleBorderTop);
				}
				cell.setCellStyle(row.getRowStyle());
				
				cell = row.createCell(col++);
				cell.setCellValue(requirement.service.toString());
				if (assignment.transitionTime != null && ! assignment.transitionTime.isAvailable(requirement.service))
					cell.setCellStyle(styleNotAssignable);
				else if (assignment.transitionTime != null && assignment.isServiceFulfilled(requirement.service, assignment.transitionTime))
					cell.setCellStyle(styleAssignment);
				else
					cell.setCellStyle(row.getRowStyle());
				
				cell = row.createCell(col++);
				cell.setCellValue(requirement.count);
				if (assignment.unfulfilledRequirements[requirement.id] <= 0)
					cell.setCellStyle(styleAssignment);
				else
					cell.setCellStyle(row.getRowStyle());

				cell = row.createCell(col++);
				cell.setCellValue(requirement.func.toString());
				cell.setCellStyle(row.getRowStyle());

				for (Doctor doctor: assignmentManager.doctors) {
					colDoctor = doctorByColumn.get(doctor);
					cell = row.createCell(colDoctor);
					CellStyle cellStyle = null;
					if (assignment.isAssigned(requirement, doctor)) {
						if (doctor.isManuallyAllocated(assignment.transitionTime, requirement.service)) {
							cellStyle = styleAllocation;
							cell.setCellValue("A");
						} else {
							cellStyle = styleAssignment;
							if (prevAssignment == null || ! prevAssignment.isAssigned(requirement, doctor))
								cell.setCellValue("S");
							else
								cell.setCellValue("p");
						}
					} 
					if (cellStyle == null && ! requirement.isAssignable(doctor))
						cellStyle = styleNotAssignable;
					if (cellStyle != null)
						cell.setCellStyle(cellStyle);
				}
				row.createCell(columnDebug).setCellValue(assignmentManager.services.levelWeights.get(requirement.level));
				if (assignment.transitionTime != null) {
					long loss = assignment.calcPenaltyRequirements(assignment.transitionTime, requirement);
					if (loss != 0)
						row.createCell(columnDebug + 1).setCellValue(loss);
				}
			}

			row = sheet.createRow(rowNo++);
			row.setRowStyle(styleBorderTop);
			if (assignment.transitionTime != null) {
				cell = row.createCell(columnDebug + 1);
				cell.setCellStyle(styleTitle);
				cell.setCellValue(assignment.getLocalLoss());
				cell = row.createCell(columnDebug + 2);
				cell.setCellStyle(styleTitle);
				cell.setCellValue(assignment.W);
				if (debugInfo) {
					cell = row.createCell(columnDebug + 3);
					cell.setCellValue(assignment.penaltyForDoctorTransfers);
				}
			}
			for (Doctor doctor: assignmentManager.doctors) {
				colDoctor = doctorByColumn.get(doctor);
				cell = row.createCell(colDoctor);
				Service service = assignment.getService(doctor);
				if (service != null) {
					cell.setCellValue(service.toString());
					if (doctor.isManuallyAllocated(assignment.transitionTime, service)) 
						cell.setCellStyle(styleAllocation);
					else
						cell.setCellStyle(styleAssignment);
				}
			}
			rowNo++;

			if (rowNo > rowLimit ) {
				row = sheet.createRow(rowNo++);
				row.createCell(baseCol).setCellValue("There are " + (assignments.size() - assignments.lastIndexOf(assignment) - 1) + " assignments more...");
				break;
			}

			prevAssignment = assignment;
		}
	}

	public ExcelPrinter(AssignmentManager assignmentManager, String output) throws IOException {
		this.assignmentManager = assignmentManager;
		openBook(output);
	}

}
