package nexin;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import util.LogManager;

public class Interval {
	protected static final Logger log = Logger.getLogger(LogManager.APP_LOGGER);
	static final DateTimeZone TIME_ZONE = DateTimeZone.UTC;
	static final int MILLIS_IN_DAY = 24 * 60 * 60 * 1000;
	static final DateTimeFormatter hour_min_formatter = DateTimeFormat.forPattern("HH:mm");
	
	Integer week, day, site_id;
	org.joda.time.Interval interval;

	public Interval(JSONObject jsn_interval) {
		LocalTime startTime, endTime;
		String s_time = jsn_interval.getString("start");
		startTime = hour_min_formatter.parseLocalTime(s_time);
		//log.log(Level.INFO, "local time: " + s_time + " -> " + startTime.toString(hour_min_formatter));
		if (jsn_interval.has("duration")) {
			endTime = startTime.plusMinutes(jsn_interval.getInt("duration"));
			week = jsn_interval.getInt("week_no");
			day = jsn_interval.getInt("day");
			site_id = jsn_interval.getInt("site_id");
			//location_id = jsn_interval.getJSONArray("location_id").getInt(0);
		} else {
			s_time = jsn_interval.getString("stop");
			endTime = hour_min_formatter.parseLocalTime(s_time);
		}
		calcInterval(startTime.getMillisOfDay(), endTime.getMillisOfDay());
		//log.log(Level.INFO, "\t" + toString());
	}

	public Interval(final int startMillis, final int endMillis) {
		calcInterval(startMillis, endMillis);
	}

	private void calcInterval(int startMillis, int endMillis) {
		if (endMillis < startMillis)
			interval = new org.joda.time.Interval(startMillis, endMillis + MILLIS_IN_DAY, TIME_ZONE);
		else
			interval = new org.joda.time.Interval(startMillis, endMillis, TIME_ZONE);
	}

	public Interval(LocalTime startTime, LocalTime endTime) {
		this(startTime.getMillisOfDay(), endTime.getMillisOfDay());
	}

	public Interval(String s_time, String e_time) {
		this(hour_min_formatter.parseLocalTime(s_time), hour_min_formatter.parseLocalTime(e_time));
	}

	@Override
	public String toString() {
		return week + " " + day + " " + interval.getStart().toString(hour_min_formatter) + " - " + interval.getEnd().toString(hour_min_formatter);
	}

	public boolean overlaps(Interval interval) {
		return (week == interval.week || week == null || interval.week == null) && 
				(day == interval.day || day == null || interval.day == null) &&
				this.interval.overlaps(interval.interval);
	}

	public LocalTime getStart() {
		return interval.getStart().toLocalTime();
	}

	public LocalTime getEnd() {
		return interval.getEnd().toLocalTime();
	}

	public boolean contains(Integer week, Integer day, int millisOfDay) {
		return (this.week == null || this.week == week) && 
				(this.day == null || this.day == day) && 
				interval.contains(millisOfDay);
	}

	public boolean contains(Interval interval) {
		return getStart().compareTo(interval.getStart()) <= 0 &&
				getEnd().compareTo(interval.getEnd()) >= 0;
	}

	public boolean contains(org.joda.time.Interval interval) {
		return getStart().compareTo(interval.getStart().toLocalTime()) <= 0 &&
				getEnd().compareTo(interval.getEnd().toLocalTime()) >= 0;
	}

}

@SuppressWarnings("serial")
class Intervals extends ArrayList<Interval> {

	public void addInterval(Interval interval) throws NexinException {
		for (Interval i: this)
			if (i.overlaps(interval))
				throw new NexinException("New interval " + interval + " overlaps with existing interval " + i);
		add(interval);
	}

	public boolean includes(Event event) {
		int millis = event.localTime.getMillisOfDay();
		for (Interval interval: this)
			if (interval.contains(event.week, event.day, millis))
				return true;
		return false;
	}
	
	public boolean overlaps(Interval interval) {
		for (Interval i: this)
			if (interval.overlaps(i))
				return true;
		return false;
	}

	public void load(JSONArray jsn_intervals) throws JSONException, NexinException {
		clear();
		JSONObject jsn;
		for (int i = 0; i < jsn_intervals.length(); i++) {
			jsn = jsn_intervals.getJSONObject(i);
			try {
				Interval interval = new Interval(jsn);
				addInterval(interval);
			} catch (Exception e) {
				Interval.log.log(Level.SEVERE, "Failed to load interval " + jsn.toString() + "\n\tfrom " + jsn_intervals);
				throw e;
			}
		}
	}

	public void load(JSONObject json) throws JSONException, NexinException {
		clear();
		if (json.has("events")) { // loading service intervals (or loading from local file format)
			JSONArray jsn_intervals = json.getJSONArray("events");
			load(jsn_intervals);
		} else { // loading doctor's availability from sql db format
			Interval.log.log(Level.INFO, "No intervals!  :" + json.toString()); // temp
			//			Interval interval = new Interval(json.getString("start_time"), json.getString("end_time"));
			//addInterval(interval);
		}
	}
}

