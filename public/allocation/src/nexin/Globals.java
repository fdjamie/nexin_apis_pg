package nexin;

import util.CommandLineParser;
import util.Connector;

public class Globals {
	
	public static Connector connector;

	public static Parser parser;

	public static void init(CommandLineParser cmdParser) throws Exception {
		connector = new Connector(cmdParser.get("connection"));
		connector.getConnection().setAutoCommit(true);
		parser = new Parser();
	}
}
