package nexin;

import java.util.logging.Logger;

import util.LogManager;

public abstract class Resource implements Comparable<Resource> {
	protected static final Logger log = Logger.getLogger(LogManager.APP_LOGGER);
	
	Integer id; // DB id
	int index; // index on arrays and lists
	String name;
	Intervals availability = new Intervals();

	public Resource(final int index, String name) {
		this.index = index;
		this.name = name;
	}

	public Resource(final int index, Integer id, String name) {
		this.index = index;
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return (id == null ? "" : (id + ":")) + name;
	}

	@Override
	public int compareTo(Resource r) {
		int result = getClass().getSimpleName().compareTo(r.getClass().getSimpleName());
		return result == 0 ? Integer.signum(index - r.index) : result;// name.compareTo(r.name);
	}
}
