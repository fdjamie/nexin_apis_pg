package nexin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import functions.BoolFunc;
import functions.Dali;
import functions.IFunction;
import functions.IFunction.EFuncIsNull;
import functions.MathFunc;
import functions.StrFunc;
import functions.Dali.I32;
import functions.Dali.Type;
import functions.Dali.Variant;
import functions.Function;
import util.LogManager;

class Parameter {
	int id;
	int position;
	String name;

	Parameter(final int id, final int position,	String name) {
		this.id = id;
		this.position = position;
		this.name = name;
	}

	@Override
	public String toString() { 
		return "(id:" + id + " pos:" + position + " " + name + ")"; 
	}

	public int toInt() {
		return id;
	}

}

class ParameterDefinition implements Comparable<ParameterDefinition>{
	String name, table, field;
	Dali.Type type;
	Map<Integer /* id */, Parameter> values = new TreeMap<Integer, Parameter>();

	ParameterDefinition(String name, String table, String field, Dali.Type type) {
		this.name = name;
		this.table = table;
		this.field = field;
		this.type = type;
	}

	@Override
	public int compareTo(ParameterDefinition pm) {
		return name.compareTo(pm.name);
	}
}

class IntParameter implements MathFunc.INumeric {
	ParameterDefinition parameterDefinition;

	IntParameter(ParameterDefinition parameter) {
		super();
		this.parameterDefinition = parameter;
	}

	@Override
	public Variant calc(IAbscissa abscissa) throws EFuncIsNull {
		return new I32(getInt(abscissa));
	}

	@Override
	public Priority priority() {
		return Priority.ivpTerm;
	}

	@Override
	public Type type() {
		return Type.sftInt32;
	}

	@Override
	public boolean isRenderedAsString() {
		return false;
	}

	@Override
	public String toString() { return parameterDefinition.name; }

	@Override
	public String toString(IAbscissa abscissa) throws EFuncIsNull {
		return String.valueOf(getInt(abscissa));
	}

	@Override
	public boolean dependsOn(IFunction arg) {
		return false;
	}

	@Override
	public int getInt(IAbscissa abscissa) throws EFuncIsNull {
		Parameter value = ((Doctor) abscissa).parameters.get(parameterDefinition);
		if (value == null)
			throw new EFuncIsNull();
		return value.toInt();
	}

	@Override
	public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
		return getInt(abscissa);
	}
}

class StrParameter extends StrFunc {
	ParameterDefinition  parameterDefinition;

	StrParameter(ParameterDefinition parameter) {
		super();
		this.parameterDefinition = parameter;
	}


	@Override
	public Priority priority() {
		return Priority.ivpTerm;
	}

	@Override
	public String toString() { return parameterDefinition.name; }

	@Override
	public String toString(IAbscissa abscissa) throws EFuncIsNull{ 
		Parameter value = ((Doctor) abscissa).parameters.get(parameterDefinition);
		if (value == null)
			throw new EFuncIsNull();
		return value.name;
	}
}

@SuppressWarnings("serial")
public class Parser extends functions.Parser {
	protected static final Logger log = Logger.getLogger(LogManager.APP_LOGGER);
	Map<String /* grade. role, etc. */, ParameterDefinition> parameters = new TreeMap<String, ParameterDefinition>(String.CASE_INSENSITIVE_ORDER); 

	Parser() throws SQLException {
		super();

		parameters.put("role", new ParameterDefinition("role", "tb_role", "role_id", Type.sftInt32));
		parameters.put("grade", new ParameterDefinition("grade", "tb_grades", "grade_id", Type.sftInt32));

		int id;
		Parameter parameter;
		for (ParameterDefinition pm: parameters.values()) {
			PreparedStatement stmt = Globals.connector.prepareStatement("SELECT id, position, name FROM " + pm.table + "  WHERE deleted_at IS NULL");
			try {
				ResultSet rs = stmt.executeQuery();
				try {
					while (rs.next()) {
						id = rs.getInt(1);
						parameter = new Parameter(id, rs.getInt(2), rs.getString(3));
						pm.values.put(id,  parameter);
					}
				} finally {
					rs.close();
				}
			} finally {
				stmt.close();
			}
		}
	}

	class EfnVariableNotFound extends EfnSyntaxError {

		public EfnVariableNotFound(String sMsg, int pos, int len) {
			super(sMsg, pos, len);
		}

	}

	@Override
	protected IFunction variable(final String var) throws EfnSyntaxError {
		//if (var.equalsIgnoreCase("item"))
		//return new Doctor.Name();
		ParameterDefinition pd = parameters.get(var); 
		if (pd == null) {
			throw new EfnVariableNotFound("Variable '" + var + "' is undefined.", 0, 0);
			//log.log(Level.SEVERE, "Parameter '" + var + "' is not defined");
			//return null;
		}
		switch (pd.type) {
		case sftInt32:
			return new IntParameter(pd);
		case sftString:
			return new StrParameter(pd);
		}
		assert(false);
		return null;
	}

	public String printValueNames() {
		String s = "PARAMETERS:";
		for (ParameterDefinition pm: parameters.values()) {
			s = s + "\n\t" + pm.name.toUpperCase();
			for (Entry<Integer, Parameter> value: pm.values.entrySet())
				s = s + "\n\t\t" + value.getKey() + " : " + value.getValue();
		}
		return s;
	}

	public ParameterDefinition extractParameterMeta(BoolFunc func) {
		for (Entry<String, ParameterDefinition> me: parameters.entrySet())
			if (funcIs(func, me.getValue()))
				return me.getValue();
		return null;
	}

	private boolean funcIs(IFunction func, ParameterDefinition pd) {
		if (func instanceof IntParameter && ((IntParameter)func).parameterDefinition == pd)
			return true;
		if (func instanceof StrParameter && ((StrParameter)func).parameterDefinition == pd)
			return true;
		if (func instanceof Function)
			for (IFunction arg: ((Function)func).getArgs())
				if (funcIs(arg, pd))
					return true;
		return false;
	}

	public Parameter getMinSuitableValue(ParameterDefinition pm, BoolFunc func) throws EFuncIsNull {
		Doctor doctor = new Doctor(-1, "");
			for (Entry<Integer, Parameter> me: pm.values.entrySet()) {
				doctor.parameters.put(pm, me.getValue());
				if (func.getBoolean(doctor))
					return me.getValue();
			}
		log.log(Level.WARNING, "Requirement '" + func.toString() + "' is not satisficable using any existing entry from table " + pm.table);
		Parameter parameter;
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			parameter = new Parameter(i, i, null);
			doctor.parameters.put(pm, parameter);
			if (func.getBoolean(doctor))
				return parameter;
		}
		log.log(Level.WARNING, "Requirement '" + func.toString() + "' is not satisficable using value");
		return null;
	}

}

