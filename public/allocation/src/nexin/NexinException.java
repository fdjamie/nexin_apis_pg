/**
 * 
 */
package nexin;

/**
 * @author lev
 *
 */
@SuppressWarnings("serial")
public class NexinException extends Exception {

	public NexinException(String message, Throwable cause) {
		super(message, cause);
	}

	public NexinException(String message) {
		super(message);
	}


}
