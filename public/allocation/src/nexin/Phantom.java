package nexin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;

import org.json.JSONObject;

import functions.IFunction.EFuncIsNull;

public class Phantom extends Doctor {
	
	static PreparedStatement stmtInsertPhantom;
	Map<ParameterDefinition, Parameter> requirements = new TreeMap<ParameterDefinition, Parameter>();
	boolean isAssignableToAny = true;
	
	public Phantom(int index, String name) {
		super(index, name);
	}

	public Phantom(int index, ResultSet rs) throws Exception {
		this(index, rs.getString("name"));
		id = rs.getInt("id");
		isAssignableToAny = false;
		
		// parameters:
		JSONObject jsnParams = new JSONObject(rs.getString("parameters"));
		log.log(Level.INFO, jsnParams.toString());
		for (ParameterDefinition pm: Globals.parser.parameters.values())
			if (jsnParams.has(pm.name))
				parameters.put(pm, pm.values.get(jsnParams.getInt(pm.name)));

		// availability: always
		Interval interval = new Interval("00:00", "23:59");
		availability.addInterval(interval);
	}

	@Override
	public boolean isAssignable(TransitionTime tt, Service service) {
		return isAssignableToAny ? true : super.isAssignable(tt, service);
	}
	
	@Override
	public boolean availabilityOverlapsWith(Interval interval) {
		return isAssignableToAny ? true : super.availabilityOverlapsWith(interval);
	}

	public static void init() throws SQLException {
		stmtInsertPhantom = Globals.connector.prepareStatementGenerateKeys(
				"INSERT INTO Phantom(assignment_change, name, parameters) VALUES(?, ?, to_json(?::json))"); 
	}

	public void saveToDB(final int assignmentChangeId) throws SQLException {
		stmtInsertPhantom.setInt(1, assignmentChangeId);
		stmtInsertPhantom.setString(2, name);
		JSONObject jsnRequirements = new JSONObject();
		for (Entry<ParameterDefinition, Parameter> me: requirements.entrySet())
			jsnRequirements.append(me.getKey().name, me.getValue().toInt());
		log.log(Level.INFO, toString() + " : " + jsnRequirements.toString());
		stmtInsertPhantom.setString(3, jsnRequirements.toString());
		id = Globals.connector.executeAndGetGeneratedKey(stmtInsertPhantom);
	}

	public void adaptToRequirement(Requirement requirement) throws EFuncIsNull {
		// check what meta parameter the requirement function is based on 
		ParameterDefinition pm = Globals.parser.extractParameterMeta(requirement.func);
		if (pm != null) { // parameter value
			Parameter value = Globals.parser.getMinSuitableValue(pm, requirement.func);
			Parameter existingValue = requirements.get(pm);
			if (existingValue == null || value.toInt() > existingValue.toInt())
				requirements.put(pm, value);
		}
	}

}

@SuppressWarnings("serial")
class Phantoms extends ArrayList<Phantom> {
	
}
