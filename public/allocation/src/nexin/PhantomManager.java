package nexin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.logging.Level;

import util.CommandLineParser;

public class PhantomManager extends AssignmentManager {
	
	@SuppressWarnings("serial")
	class RequirementsByService extends TreeMap<Service, Map<String/*requirement*/, Integer/*count*/>> {
		
	}

	@SuppressWarnings("serial")
	static class RequirementsByTransitionTime extends TreeMap<Integer/*TransitionTime*/, RequirementsByService> {
	}
	
	Phantoms phantoms = new Phantoms();
	Integer previousSessionId, levelToComplete;
	RequirementsByTransitionTime missingRequirements = new RequirementsByTransitionTime(); 

	@Override
	protected void init(CommandLineParser cmdParser) throws Exception {
		super.init(cmdParser);
		Phantom.init();
	}

	protected void run(CommandLineParser cmdParser) throws Exception {
		if (cmdParser.containsKey("previousSession")) {
			previousSessionId = Integer.parseInt(cmdParser.get("previousSession"));
			log.log(Level.INFO, "Previous sesison id = " + previousSessionId);
			PreparedStatement stmtSelectMinIncompleteLevel = Globals.connector.prepareStatement(
					"SELECT MIN(level) FROM staff_allocation_schema.missing_resource WHERE assignment_session = ?");
			try {
				stmtSelectMinIncompleteLevel.setInt(1, previousSessionId);
				ResultSet rs = stmtSelectMinIncompleteLevel.executeQuery();
				try {
					if (rs.next() && rs.getObject(1) != null)
						levelToComplete = rs.getInt(1);
					else {
						log.log(Level.WARNING, "Could not find any missing resources for Assignment Session id=" + previousSessionId);
						return;
					}
					log.log(Level.INFO, "Managing phantoms to complete level " + levelToComplete);
				} finally {
					rs.close();
				}
			} finally {
				stmtSelectMinIncompleteLevel.close();
			}
			
			loadMissingRequirements();
			generatePhantoms();
			
			// add new phantoms to former staff:
			for (Doctor phantom: phantoms)
				doctors.add(phantom);
			
			// run assignment optimization
			PreparedStatement stmtWeekDayByAssignmentSessionId = Globals.connector.prepareStatement(
					"SELECT s.assignment_task, t.week, t.day FROM Assignment_Session s JOIN Assignment_Task t ON t.id = s.assignment_task WHERE s.id = ?");
			try {
				stmtWeekDayByAssignmentSessionId.setInt(1, previousSessionId);
				ResultSet rs = stmtWeekDayByAssignmentSessionId.executeQuery();
				try {
					if (rs.next()) {
						assignmentTaskId = rs.getInt("assignment_task");
						runOneDay(rs.getInt("week"), rs.getInt("day"));
					} else
						throw new NexinException("Could not find Assignment Task with id=" + assignmentTaskId);
				} finally {
					rs.close();
				}
			} finally {
				stmtWeekDayByAssignmentSessionId.close();
			}
		} else {
			previousSessionId = null;
			super.run(cmdParser);
		}
	}

	void loadMissingRequirements() throws Exception {
		missingRequirements.clear();
		int ttId;
		RequirementsByService requirementsByService;
		Service service;
		Map<String, Integer> requirementCounts;
		PreparedStatement stmt = Globals.connector.prepareStatement("SELECT transition_time, service, requirement, count" + 
				"  FROM staff_allocation_schema.missing_resource mr" + 
				"  WHERE assignment_session = ? AND  level = ?");
		try {
			stmt.setInt(1, previousSessionId);
			stmt.setInt(2, levelToComplete);
			ResultSet rs = stmt.executeQuery();
			try {
				while (rs.next()) {
					ttId = rs.getInt(1);
					requirementsByService = missingRequirements.get(ttId);
					if (requirementsByService == null) {
						requirementsByService = new RequirementsByService();
						missingRequirements.put(ttId, requirementsByService);
					}
					service = services.getById(rs.getInt(2));
					requirementCounts = requirementsByService.get(service);
					if (requirementCounts == null) {
						requirementCounts = new TreeMap<String, Integer>();
						requirementsByService.put(service, requirementCounts);
					}
					requirementCounts.put(rs.getString(3), rs.getInt(4));
				}
			} finally {
				rs.close();
			}
		} finally {
			stmt.close();
		}
	}

	void generatePhantoms() throws SQLException {
		phantoms.clear();
		// check the max count of services per transition time:
		int maxServicesPerTT = 0;
		Set<Service> distinctServicesByTT = new TreeSet<Service>();
		for (Entry<Integer, RequirementsByService> me: missingRequirements.entrySet()) {
			distinctServicesByTT.clear();
			for (Service service: me.getValue().keySet())
				distinctServicesByTT.add(service);
// TO Do - requirement counts
			maxServicesPerTT = Math.max(maxServicesPerTT, distinctServicesByTT.size());
		}
		log.log(Level.INFO, maxServicesPerTT + " phantoms are necessary");
		if (maxServicesPerTT == 0)
			return;

		Phantom phantom;
		for (int i = 0; i < maxServicesPerTT; i++) {
			phantom = new Phantom(doctors.size() + i, " Ph." + i);
			phantoms.add(phantom);
		}
	}
	
	@Override
	protected void initAssignmentSession(final int week, final int day) throws SQLException {
		super.initAssignmentSession(week, day);
		PreparedStatement stmtInsertAssignmentChange = Globals.connector.prepareStatementGenerateKeys(
				"INSERT INTO Assignment_Change(assignment_task) VALUES (?)");
		try {
			stmtInsertAssignmentChange.setInt(1, assignmentTaskId);
			assignmentChangeId = Globals.connector.executeAndGetGeneratedKey(stmtInsertAssignmentChange);
		} finally {
			stmtInsertAssignmentChange.close();
		}
	}

	@Override
	protected void saveAssignmentsToDB(List<Assignment> assignments) throws Exception {
		RequirementsByService requirementsByService;
		Service service;
		for (Assignment assignment: assignments) {
			requirementsByService = missingRequirements.get(assignment.transitionTime.id);
			if (requirementsByService != null) {
				for (Entry<Service, Map<String/*requirement*/, Integer/*count*/>> me: requirementsByService.entrySet()) {
					service = me.getKey();
					for (Requirement requirement: service.requirements)
						for (Phantom phantom: phantoms)
							if (assignment.isAssigned(requirement, phantom)) {
								phantom.adaptToRequirement(requirement);
						}
				}
			}
		}
		for (Phantom phantom: phantoms)
			phantom.saveToDB(assignmentChangeId);
		
		super.saveAssignmentsToDB(assignments);
	}
	
	public static void main(String[] args) {
		PhantomManager pm = new PhantomManager();
		int exitCode = pm.run(args);
		System.exit(exitCode);
	}

}
