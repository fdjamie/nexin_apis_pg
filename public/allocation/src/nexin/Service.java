package nexin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;

import org.json.JSONException;
import org.json.JSONObject;

import functions.BoolFunc;
import functions.IFunction.EFuncIsNull;
import functions.Parser.EfnSyntaxError;
import util.Connector;

class Requirement implements Comparable<Requirement>{
	Service service;
	int id, level, count = 1;
	String requirement;
	BoolFunc func;

	Requirement(Service service, final int level, String requirement) throws EfnSyntaxError {
		this.service = service;
		this.level = level;
		this.requirement = requirement; 
		func = (BoolFunc) Globals.parser.parse(this.requirement);
	}

	public boolean isAssignable(Doctor doctor) {
		if (doctor instanceof Phantom && ((Phantom)doctor).isAssignableToAny)
			return true;
		try {
			return func.getBoolean(doctor);
		} catch (EFuncIsNull e) {
			//e.printStackTrace();
			return false;
		}
	}

	@Override
	public String toString() {
		return level + ":(" + func.toString() + ")";
	}

	@Override
	public int compareTo(Requirement req) {
		int result = service.compareTo(req.service);
		result = result == 0 ? Integer.signum(level - req.level) : result;
		return result == 0 ? requirement.compareTo(req.requirement) : result;
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof Requirement && compareTo((Requirement) o) == 0;
	}

}

@SuppressWarnings("serial")
class RequirementsByLevel extends TreeMap<Integer/*Level*/, List<Requirement>> {
	
}

@SuppressWarnings("serial")
class Requirements extends ArrayList<Requirement> {

	public Requirement add(Service service, final int level, String expression) throws EfnSyntaxError {
		for (Requirement requirement: this) {
			if (requirement.service == service && requirement.level == level && requirement.requirement.equals(expression)) {
				requirement.count ++;
				return requirement;
			}
		}
		Requirement requirement = new Requirement(service, level, expression);
		add(requirement);
		return requirement;
	}

	public Requirement getRequirement(Requirement sample) {
		for (Requirement requirement: this)
			if (requirement.service == sample.service && requirement.level == sample.level && requirement.requirement.equals(sample.requirement))
				return requirement;
		return null;
	}

	public void arrange(Services services) {
		clear();
		for (Integer level: services.levelWeights.keySet())
			for (Service service: services)
				for (Requirement requirement: service.requirements)
					if (requirement.level == level) {
						requirement.id = size();
						add(requirement);
					}
	}
}

class Service extends Resource {

	static PreparedStatement stmtLoadRequirements;

	@Override
	public boolean equals(Object o) {
		return o instanceof Service && compareTo((Service) o) == 0;
	}

	Requirements requirements = new Requirements();
	Set<Integer> levels = new TreeSet<Integer>();

	public Service(final int index, String name) {
		super(index, name);
	}
		
	public Service(final int index, ResultSet rs) throws SQLException, JSONException, NexinException {
		super(index, rs.getString("name"));
		id = rs.getInt("id");
		String template = null;
		
		if (rs.getObject("template") != null)
			try {
				template = rs.getString("template");
				JSONObject jsn_template = new JSONObject(template);
				availability.load(jsn_template);
			} catch (Exception e) {
				log.log(Level.SEVERE, "Failed to load availability template '" + template + "' for service " + toString());
				throw e;
			}
	}

	public void loadRequirements(Connector connector) throws SQLException, EfnSyntaxError {
		int level;
		String parameter, value, expression;
		if (stmtLoadRequirements == null)
			stmtLoadRequirements = connector.prepareStatement("SELECT * FROM tb_service_requirement WHERE service_id = ? AND deleted_at IS NULL ORDER BY id");
		stmtLoadRequirements.setInt(1, id);
		ResultSet rs = stmtLoadRequirements.executeQuery();
		try {
			while (rs.next()) {
				level = rs.getInt("priority");
				levels.add(level);
				parameter = rs.getString("parameter"); 
				value = rs.getString("value");
				/*
				try {
					Integer.parseInt(value);
					Globals.parser.types.put(parameter, Dali.Type.sftInt32);
				} catch (NumberFormatException e) {
					Globals.parser.types.put(parameter, Dali.Type.sftString);
				}
				*/
				expression = parameter + rs.getString("operator") + value;
				try {
					requirements.add(this, level, expression);
				} catch (Exception e) {
					log.log(Level.WARNING, "Error when parsing requirement '" + expression + "' for service " + toString() + 
							"\n\t" + e.getMessage() + "\n\tRequirement is ignored!");
				}
			}
		} finally {
			rs.close();
		}		
	}

}

@SuppressWarnings("serial")
class Services extends ArrayList<Service> {
	TreeMap<Integer/*level*/, Long/*weight*/> levelWeights = new TreeMap<Integer, Long>();

	@Override
	public void clear() {
		super.clear();
		levelWeights.clear();
	}

	@Override
	public boolean add(Service service) {
		for (Integer level: service.levels)
			levelWeights.put(level, (long) 0);
		boolean result = super.add(service);
		return result;
	}

	public void load() throws Exception {
		Service service;
		PreparedStatement stmt = Globals.connector.prepareStatement( "SELECT * FROM tb_service WHERE deleted_at IS NULL ORDER BY id");
		try {
			ResultSet rs = stmt.executeQuery();
			try {
				while (rs.next()) {
					service = new Service(size(), rs);
					service.loadRequirements(Globals.connector);
					add(service);
				}
			} finally {
				rs.close();
			}
		} finally {
			stmt.close();
		}
	}

	public Service get(String serviceName) throws NexinException {
		for (Service service: this)
			if (service.name.equals(serviceName))
				return service;
		throw new NexinException("Cound not find service '" + serviceName + "'");
	}

	public Service getById(final int serviceId) throws NexinException {
		for (Service service: this)
			if (service.id == serviceId)
				return service;
		throw new NexinException("Cound not find service with id=" + serviceId);
	}

}

