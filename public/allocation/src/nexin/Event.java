package nexin;

import org.joda.time.LocalTime;

import nexin.AssignmentManager.AssignmentSet;

public class Event implements Comparable<Event> {
	int index;
	Integer week, day;
	LocalTime localTime;
	AssignmentSet visitedStates = new AssignmentSet();
	int totalAssignments, evaluatedAssignments, revisitedStates, leafAssignments, leafAssignmentsThreshold = 100;

	Event(Integer week, Integer day, LocalTime localTime) {
		this.week = week;
		this.day = day;
		this.localTime = localTime;
	}

	boolean[][] assignabilityDoctorByService; // rows numbers are service.id. column numbers are doctor.id
	boolean[][] assignabilityDoctorByRequirement; // rows numbers are requirement.id. column numbers are doctor.id
	public Event next;

	@Override
	public String toString() { 
		return "E" + index;
	}

	public String getDescription() { 
		return toString();
	}

	@Override
	public int compareTo(Event e) {
		return Integer.signum(e.index - index); // class Assignments needs events in reversed order!
	}

	boolean isAvailable(Doctor doctor) {
		return doctor.availability.includes(this);
	}

	boolean isAvailable(Service service) {
		return service.availability.includes(this);
	}

}
