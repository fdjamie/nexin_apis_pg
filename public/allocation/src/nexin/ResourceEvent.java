package nexin;

import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.joda.time.LocalTime;

public class ResourceEvent extends Event {
	
	Map<Resource, Boolean /* ON / OFF */> resources = new TreeMap<Resource, Boolean>();

	ResourceEvent(Integer week, Integer day, LocalTime localTime) {
		super(week, day, localTime);
	}

	private String getDescription(Resource resource, Boolean isOn) {
		if (resource instanceof Service)
			return resource.name + " " + (isOn ? "ON" : "OFF");
		if (resource instanceof Doctor)
			return resource.name + " " + (isOn ? "Available" : "Unavailable");
		assert(false);
		return null;
	}

	public String getDescription() { 
		String resources = "", s;
		for (Entry<Resource, Boolean> me: this.resources.entrySet()) {
			s = getDescription(me.getKey(), me.getValue());
			resources = resources.isEmpty() ? s : (resources + ", " + s);
		}
		return toString() + ": " + resources;
	}

	// true if events contain the same resources
	public boolean sameResources(ResourceEvent event) {
		for (Entry<Resource, Boolean> me: event.resources.entrySet())
			if (resources.get(me.getKey()) == null)
				return false;
		for (Entry<Resource, Boolean> me: resources.entrySet())
			if (event.resources.get(me.getKey()) == null)
				return false;
		return true;
	}

	public boolean isRepetitionOf(ResourceEvent event) {
		if ( ! sameResources(event))
			return false;
		for (Entry<Resource, Boolean> me: event.resources.entrySet())
			if (resources.get(me.getKey()) != me.getValue())
				return false;
		return true;
	}

}
