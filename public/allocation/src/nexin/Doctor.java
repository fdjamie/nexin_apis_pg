package nexin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import functions.IFunction.IAbscissa;
import functions.StrFunc;

public class Doctor extends Resource implements IAbscissa {
	Map<ParameterDefinition, Parameter /* value */> parameters = new TreeMap<ParameterDefinition, Parameter>();

	public Doctor(final int index, String name) {
		super(index, name);
	}
		
	public Doctor(final int index, ResultSet rs) throws Exception {
		this(index, rs.getString("name"));
		id = rs.getInt("id");
		
		int param_id;
		for (ParameterDefinition pm: Globals.parser.parameters.values()) {
			if (rs.getObject(pm.field) != null) {
				param_id = rs.getInt(pm.field);
				parameters.put(pm, pm.values.get(param_id));
			}
		}

		// availability
		Interval interval;
		PreparedStatement stmt = Globals.connector.prepareStatement("SELECT DISTINCT week, day, start, stop"
				+ " FROM tb_staff_availability WHERE directorate_staff_id = ?");
		try {
			stmt.setInt(1, id);
			ResultSet ars = stmt.executeQuery();
			try {
				while (ars.next()) {
					interval = new Interval(ars.getString("start"), ars.getString("stop"));
					interval.week = ars.getInt("week");
					interval.day = ars.getInt("day");
					availability.addInterval(interval);
				}
			} finally {
				ars.close();
			}
		} finally {
			stmt.close();
		}
		
	}

	abstract static class ItemStrTerm extends StrFunc {

		@Override
		public Priority priority() {
			return Priority.ivpTerm;
		}
	}

	static class Name extends ItemStrTerm {

		@Override
		public String toString(IAbscissa abscissa) throws EFuncIsNull{ 
			return ((Doctor) abscissa).name;
		}
	}

	Service getManualAllocation(TransitionTime tt) {
		if (tt instanceof TransitionTime) 
			return ((TransitionTime)tt).manualAllocations.get(this);
		return null;
	}

	public boolean isManuallyAllocated(TransitionTime tt, Service service) {
		if (tt instanceof TransitionTime) 
			return ((TransitionTime)tt).manualAllocations.containsValue(service);
		return false;
	}

	public boolean isAssignable(TransitionTime tt, Service service) {
		/*
		 * Staff will subsequently have a Service Availability calculated for them, i.e. which of the running Services are they present for to be assigned to. This can produce three possible outcomes:
		 * 		a)	Staff present for the entire Interval are available to work any Service within the Interval.
		 * 		b)	Staff not present for the entire Interval, but present for the entire duration of Services within that Interval can only be considered available to work for those Services.
		 * 		c)	Staff not present for the entire Interval, nor for the entire duration of any Services within that Interval, are not considered available to work for any of those Services.
		 */
		Interval eventInterval = tt.interval;
		org.joda.time.Interval eventServiceOverlapping;
		for (Interval interval: availability) {
			if (interval.contains(eventInterval))
					return true; // p. "a" above
			for(Interval serviceInterval: service.availability) {
				eventServiceOverlapping = eventInterval.interval.overlap(serviceInterval.interval);
				if (eventServiceOverlapping != null && interval.contains(eventServiceOverlapping))
					return true;
			}
			if (interval.overlaps(eventInterval)) {
				
			}
		}
		return false;
	}

	public boolean availabilityOverlapsWith(Interval interval) {
		return availability.overlaps(interval);
	}

}

@SuppressWarnings("serial")
class Doctors extends ArrayList<Doctor> {

	public void load() throws Exception {
		Doctor doctor;
		PreparedStatement stmt = Globals.connector.prepareStatement("SELECT * FROM tb_directorate_staff WHERE deleted_at IS NULL ORDER BY id");
		try {
			ResultSet rs = stmt.executeQuery();
			try {
				while (rs.next()) {
					doctor = new Doctor(size(), rs);
					add(doctor);
				}
			} finally {
				rs.close();
			}
		} finally {
			stmt.close();
		}
	}

	public Doctor getById(final int doctorId) throws NexinException {
		for (Doctor doctor: this)
			if (doctor.id == doctorId)
				return doctor;
		throw new NexinException("Could not find a staff with id=" + doctorId);
	}

}

