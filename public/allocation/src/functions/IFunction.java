package functions;

public interface IFunction {

  public static enum Priority { // descending order:
    ivpTerm, // terms - highest priority
    ivpFunction, // regular functions (math, etc...)
    ivpOneArg, // @, not
    ivpMult, // *, /, div, mod, and, shl, shr, as
    ivpSum, // +, �, or, xo
    ivpComp // =, <>, <, >, <=, >=, in, is - lowest priority
  }
  
  public static class EFuncException extends Exception {
    private static final long serialVersionUID = 1L;
  }

  public static class EFuncIsNull extends EFuncException {
    private static final long serialVersionUID = 1L;
  }

  public static class EIncorrectFuncParam extends EFuncException {
    private static final long serialVersionUID = 1L;
  }

  public static interface IAbscissa { 
  }

  // core methods:
  
  Dali.Variant calc(IAbscissa abscissa) throws EFuncIsNull;
  
  Priority priority();
  
  Dali.Type type(); // type of value returned
  
  boolean isRenderedAsString();
  
  String toString(IAbscissa abscissa) throws EFuncIsNull;
  
  boolean dependsOn(IFunction arg);
}