package functions;

public abstract class BoolFunc extends Function implements Function.IBoolean{
  @Override // IFunction
  public Dali.Type type() { return Dali.Type.sftBoolean; }
  
  @Override
  public Category category() { return Category.ivfcLogical; } 
  
  @Override
  public Dali.Variant calc(IAbscissa abscissa) throws EFuncIsNull {
    return new Dali.Bool(getBoolean(abscissa)); 
  }

  @Override // IBoolean
  public abstract boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull;
  
  public static class Const extends BoolFunc {
    public Const(final boolean b) { value = b; }

    @Override
    public Priority priority() { return Priority.ivpTerm; }

    @Override // IFunction.Boolean
    public boolean getBoolean(IAbscissa abscissa) { return value; }

    boolean value;
    
    @Override
    public String toString() { return value ? "\"TRUE\"" : "\"FALSE\""; }

  }

  public static class Variable extends Const {
    public Variable(final boolean b) { super(b); }
    public void setValue(final boolean b) { value = b; } 
  }
  
  static abstract class BooleanOfOneArg extends BoolFunc {
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { Dali.Type.sftBoolean }; 
      return result;
    }
    
    @Override
    public Priority priority() { return Priority.ivpOneArg; }
  }

  public static class Not extends BooleanOfOneArg {
    @Override
    public String shortName() { return "Not"; }


   @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return  ! ((IBoolean) args[0]).getBoolean(abscissa);
    }
  }
  
  static abstract class BooleanOf2Args extends BoolFunc {
    BooleanOf2Args() {   }
    
    protected BooleanOf2Args(IFunction x, IFunction y) { setArgs(x, y); }
    
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { Dali.Type.sftBoolean, Dali.Type.sftBoolean }; 
      return result;
    }
  }

  static abstract class Junction extends BooleanOf2Args {
    Junction() { super(); } // no-arg constructor is necessary
    
    protected Junction(IFunction x, IFunction y) { super(x, y); }
  }

  public static class And extends Junction {
    And() { super(); } // no-arg constructor is necessary
    public And(IFunction x, IFunction y) { super(x, y); }

    @Override
    public String shortName() { return "AND"; }

    @Override
    public Priority priority() { return Priority.ivpMult; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return ((IBoolean) args[0]).getBoolean(abscissa) && 
        ((IBoolean) args[1]).getBoolean(abscissa);
    }
  }

  public static class Or extends Junction {
    Or() { super(); } // no-arg constructor is necessary
    public Or(IFunction x, IFunction y) {
      super(x, y);
    }

    @Override
    public Priority priority() { return Priority.ivpSum; }

    @Override
    public String shortName() { return "OR"; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      try {
        if (((IBoolean) args[0]).getBoolean(abscissa))
          return true;
      } catch (EFuncIsNull e) {
      };
      return ((IBoolean) args[1]).getBoolean(abscissa);
    }
  }

  public static class Xor extends Junction {
    Xor() { super(); } // no-arg constructor is necessary
    public Xor(IFunction x, IFunction y) { super(x, y); }

    @Override
    public Priority priority() { return Priority.ivpSum; }

    @Override
    public String shortName() { return "XOR"; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return ((IBoolean) args[0]).getBoolean(abscissa) ^ 
        ((IBoolean) args[1]).getBoolean(abscissa);
    }
  }
  
  abstract static class IsWhat extends BooleanOfOneArg {
    @Override
    public Priority priority() { return Priority.ivpFunction; }
    
    @Override
    public Category category() { return Category.ivfcComparison; } 
    
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { null }; 
      return result;
    }
    
  } // IsWhat

  public static class IsBlank extends IsWhat {
    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      try {
        return args[0].calc(abscissa).isNull();
      } catch ( EFuncIsNull e ) {
          return true;
      }
    }
  }
  
  public static class IsNotBlank extends IsWhat {
    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      try {
        return ! args[0].calc(abscissa).isNull();
      } catch ( EFuncIsNull e ) {
          return false;
      }
    }
  }
  
  public static void registerFunctions(Registrator r) {
    /* Important: only classes with constructors without arguments can be 
       registered that way! Otherwise newInstance() fires InstantiationException.
    */   
    r.register(IsBlank.class);
    r.register(IsNotBlank.class);
  }
} // BoolFunc
