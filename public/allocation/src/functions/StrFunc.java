package functions;

import functions.Dali.*;

/**
 * Synaptris Inc.
 * @author Evgeny Lyulkov  evgueni.lioulkov@synaptris.com
 * @created Sept.2011
 * @description
 * @Delphi_prototype
 */

public abstract class StrFunc extends Function {

  @Override
  public Type type() { return Dali.Type.sftString; }

  @Override
  public Category category() { return Category.ivfcString; } 
  
  @Override // to override by descendants
  public String toString(IAbscissa abscissa) throws EFuncIsNull{ 
    assert(false); return ""; 
  }
  
  @Override
  public Variant calc(IAbscissa abscissa) throws EFuncIsNull {
    return new Dali.Str(toString(abscissa));
  }

  public static class Const extends StrFunc {
    String value;
    public Const(final String s) { value = s; } 
    
    @Override
    public Priority priority() { return Priority.ivpTerm; }

    @Override
    public String toString(IAbscissa abscissa) { return value; }
    
    @Override
    public String toString() { return '"' + value + '"'; }
    
  } // Consts
  
  abstract static class StringFunction extends StrFunc {
    @Override
    public Priority priority() { return Priority.ivpFunction; }
  }
  
  abstract static class StringToString extends StringFunction {
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { Dali.Type.sftString }; 
      return result;
    }
  }
  
  public static class Upper extends StringToString {
    
    Upper() { super(); } // no-arg constructor is necessary
    
    public Upper(IFunction s) { setArgs(s); }
    
    @Override
    public String toString(IAbscissa abscissa) throws EFuncIsNull { 
      return args[0].toString(abscissa).toUpperCase(); 
    }
  }
  
  public static class Lower extends StringToString {
    
    Lower() { super(); } // no-arg constructor is necessary
    
    public Lower(IFunction s) { setArgs(s); }
    
    @Override
    public String toString(IAbscissa abscissa) throws EFuncIsNull { 
      return args[0].toString(abscissa).toLowerCase(); 
    }
  }
  
  public static class Concat extends StringFunction {
    
    Concat() { super(); } // no-arg constructor is necessary
    
    public Concat(StrFunc s0, StrFunc s1) { setArgs(s0, s1); }
    
    @Override
    public Priority priority() { return Priority.ivpSum; }
    
    @Override
    public String shortName() { return "+"; }
    
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { Dali.Type.sftString }; 
      return result;
    }
    
    @Override
    public String toString(IAbscissa abscissa) throws EFuncIsNull {
      String s;
      try {
        s = args[0].toString(abscissa);
      } catch (EFuncIsNull e) {  
        s = "";
      }  
      try {
        return s.concat(args[1].toString(abscissa)); 
      } catch (EFuncIsNull e) { 
        return s;
      }  
    }
  } // Concat
  
  public static void registerFunctions(Registrator r) {
    /* Important: only classes with constructors without arguments can be 
       registered that way! Otherwise newInstance() fires InstantiationException.
    */   
    r.register(Upper.class);
    r.register(Lower.class);
    r.register(Concat.class);
  }
  
} // StrFunc
