package functions;

import java.util.*;

public abstract class Function implements IFunction{

  public interface IBoolean extends IFunction {
    boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull;
  }

  public interface IDateTime extends IFunction {
    Date getDate(IAbscissa abscissa) throws EFuncIsNull;
    long getMillis(IAbscissa abscissa) throws EFuncIsNull;
  }

  protected IFunction[] args = new Function[0];

  protected static final Dali.Str csInvalidValue = new Dali.Str("#VALUE!");

  public static enum Category {
    ivfcDateTime, ivfcMathOperation, ivfcMathFunc, ivfcComparison,
    ivfcLogical, ivfcString
  }

  final static String csNoArg = "(...)";

  //  protected void describe(Descriptor descriptor) {} // descendants fill that

  @SuppressWarnings("unchecked")
  protected String getId() { // descendants override that if they use another registrator
    return new Registrator().getFuncId((Class<Function>) this.getClass());
  }

  public String longName() { return getClass().getSimpleName().toLowerCase(); } 

  public String shortName() { return longName(); }

  public String returning() { return ""; }

  protected IFunction createDefaultArg(final int i) { return null; }

  public boolean isRenderedAsString() { return false; }

  public Category category() { return Category.ivfcMathFunc; } 

  String argTypeName(final int iArg) { 
    return argTypes()[iArg] == null ? "" : argTypes()[iArg].toString(); 
  }

  @Override // IFunction 
  public boolean dependsOn(IFunction arg) {
    if (this == arg)
      return true;
    for (int i = 0; i < args.length; i++)
      if (args[i].dependsOn(arg) )
        return true;
    return false;  
  }
  
  void setArgs(IFunction...args) throws IllegalArgumentException{
    Dali.Type[] xArgTypes = argTypes();
    IFunction[] xArgs = new IFunction[xArgTypes.length]; 

    for ( int  i = 0 ; i < xArgTypes.length ; i++ )
      if ( (i >= args.length) || (args[i] == null) ) {
        xArgs[i] = createDefaultArg(i);
        if ( xArgs[i] == null )
          throw new IllegalArgumentException("not enough actual parameters");
      } else {
        if ( args[i].isRenderedAsString() ?
            compatibleAsStrings(xArgTypes[i], args[i].type()) :
              compatible(xArgTypes[i], args[i].type()) )
          xArgs[i] = args[i];
        else {
          String wErrorMsg = argTypeName(i);
          if ( (category() == Category.ivfcMathOperation) || 
              (category() == Category.ivfcComparison) ||
              (this instanceof Comparison.Junction) )
            if ( wErrorMsg.isEmpty() )
              wErrorMsg = "operator " + shortName() +
              " and operand " + Integer.toString(i + 1) + " are not compatible";
            else
              wErrorMsg = "operator " + shortName() +
              " and operand '" + args[i].toString() + "' are not compatible";
          else {
            Dali.Type t = args[i].type();
            if ( wErrorMsg.isEmpty() || (t == null) || t.toString().isEmpty() )
              wErrorMsg = "function '" + longName() + "' and argument " +
              Integer.toString(i + 1) + " are not compatible.";
            else
              wErrorMsg = "function '" + longName() + "' and argument " +
              "'" + args[i].toString() + "' are not compatible. argument " +
              Integer.toString(i + 1) + " must be a " + 
              wErrorMsg.toLowerCase() + " value";
          }
          throw new IllegalArgumentException(wErrorMsg);
        };
      };

      this.args = xArgs;
  }

  @Override
  public boolean equals(Object obj) {
    if ( (obj == null) || 
        (getClass().getName() != obj.getClass().getName()) ||
        (args.length != ((Function) obj).args.length) )
      return false;
    for ( int  i = 0 ; i < args.length; i ++ ) {
      IFunction arg = ((Function) obj).args[i];
      if ( ((args[i] == null) && (arg != null))  ||
          ((args[i] != null) && ((arg == null) || ! args[i].equals(arg))) )
        return false;
    }
    return true;
  }

  @Override
  public String toString() {
    String result = "";
    if ( priority() == Priority.ivpFunction ) {
      result = shortName();
      if ( args.length > 0 ) {
        result = result.concat("(");
        for ( int  i = 0; i < argCount(); i++ ) {
          if ( i > 0 )
            result = result.concat(", ");
          if ( (i > args.length) || (args[i] == null) )
            result = result.concat(csNoArg);
          else
            result = result.concat(args[i].toString());
        };
        result = result.concat(")");
      };
    } else {
      if ( args.length > 0 ) {
        if ( (args.length < 1) || (args[0] == null) )
          result = csNoArg;
        else if ( args[0].priority().ordinal() >= priority().ordinal() )
          result = "(" + args[0].toString() + ")";
        else
          result = args[0].toString();
        result = result.concat(" ");
      };
      result = result.concat(shortName());
      if ( args.length > 1 ) {
        result = result.concat(" ");
        if ( (args.length < 2) || (args[1] == null) )
          result = result.concat(csNoArg);
        else if ( args[1].priority().ordinal() >= priority().ordinal() )
          result = result.concat("(" + args[1].toString() + ")");
        else
          result = result.concat(args[1].toString());
      };
    };
    return result;
  }

  public Dali.Type[] argTypes() { return new Dali.Type[0]; }

  public int argCount() { return argTypes().length; }

  public static final Dali.Variant calcSafe(IFunction func, IAbscissa abscissa) {
    try {
      return func.calc(abscissa);
    } catch (EFuncIsNull e) {
      return Dali.Variant.NULL;
    }
  }

  @Override // IFunction
  public String toString(IAbscissa abscissa) throws EFuncIsNull { 
    return calc(abscissa).toString(); 
  }

  /* set AllowStringOperationsForAllTypes to true to allow usage of
   *  any argument types in string operations.
   */
  static public boolean AllowStringOperationsForAllTypes = false;

  static boolean compatibleAsStrings(final Dali.Type xFormalArgType, 
      final Dali.Type xActualArgType) {
    if ( (xActualArgType == null) || (xFormalArgType == null) )
      return false;
    if ( xActualArgType == xFormalArgType )
      return true;
    return compatible(xFormalArgType, Dali.Type.sftString) ||
    compatible(xFormalArgType, xActualArgType);
  }

  static boolean compatible(final Dali.Type xFormalArgType, Dali.Type xActualArgType) {
    if (xFormalArgType == null) // 
      return true; // anyone suits
    if (xActualArgType == null)
      return false; // because certain xFormalArgType is expected 
    if ( xActualArgType == xFormalArgType )
      return true;
    switch ( xFormalArgType ) {
    case sftInt64: 
      return (xActualArgType == Dali.Type.sftInt32) || (xActualArgType == Dali.Type.sftInt64);
    case sftDouble:
      return (xActualArgType == Dali.Type.sftInt32) || (xActualArgType == Dali.Type.sftInt64) || 
        (xActualArgType == Dali.Type.sftDouble);
    case sftString:
      return AllowStringOperationsForAllTypes ||
        (xActualArgType == Dali.Type.sftString) ;
    default: return false;
    } // switch
  }

  static boolean compatible(final Set<Dali.Type> xFormalArgTypes,
      final Dali.Type xActualArgType) {
    for ( Dali.Type aDT: xFormalArgTypes)
      if ( compatible(aDT, xActualArgType) )
        return true;
    return false;
  }

  @SuppressWarnings("unchecked")
  static Class<Function> selectOverloadedFunc(Class<?> xFuncRef, IFunction...args) {
    // IV_Function.SelectOverloadedFunc
    if ((xFuncRef == MathFunc.Plus.class) && (args.length == 2)) 
      if (AllowStringOperationsForAllTypes) { 
        if (Dali.STRING_TYPES.contains(args[0].type()) 
            ||
            Dali.STRING_TYPES.contains(args[1].type()))
          xFuncRef = StrFunc.Concat.class;
      } else
        if (Dali.STRING_TYPES.contains(args[0].type()) 
            &&
            Dali.STRING_TYPES.contains(args[1].type()))
          xFuncRef = StrFunc.Concat.class;

    return (Class<Function>) xFuncRef;
  }

  public static Descriptor findFunction(final String id) {
    return registry.get(id);
  }

  public static IFunction createFunc(final Class<Function> xFuncRef, IFunction...args) 
  throws IllegalArgumentException, InstantiationException, IllegalAccessException {
    // newInstance() assumes that xFuncRef has a constructor without args 
    Function func = xFuncRef.newInstance();
    func.setArgs(args);
    return func;
  }

  public static class Descriptor {
    String shortName, longName, returning;
    Class<Function> ref;
    Dali.Type[] argTypes;
    Category category;
    String[] argTypeNames;

    public Descriptor (final String name, Class<Function> xFuncRef) { 
      shortName = name;
      longName = name;
      ref = xFuncRef;
    }

    public String argTypeName(int i) { return argTypeNames[i]; }

    public Class<Function> ref() { return ref; }
    public String shortName() { return shortName; }
    public String longName() { return longName; }
    public int argCount() { return argTypes.length; }
    Category category() { return category; }
    public String returning() { return returning; }

    void init(Function func) {
      shortName = func.shortName(); 
      longName = func.longName(); 
      category = func.category();
      argTypes = func.argTypes();
      argTypeNames = new String[argTypes.length];
      for (int i = 0; i < argTypeNames.length; i++)
        argTypeNames[i] = func.argTypeName(i);
    }

    public String getArgDescription(int iArg) {
      // TODO Auto-generated method stub
      return null;
    }
  } // Descriptor

  /* Function registry */

  static class Registry extends TreeMap<String/* id */, Descriptor> {
    private static final long serialVersionUID = 1L;
    Registry() { super(String.CASE_INSENSITIVE_ORDER); }
  }

  static Registry registry;

  static class Registrator {

    static Descriptor register(final String id, Class<Function> xFuncRef) {
      Descriptor result = registry.get(id);
      assert(result == null); // that class is not registered yet
      if (result == null) {
        result = new Descriptor(id, xFuncRef);
        registry.put(id, result);
        try { // newInstance() requires that xFuncRef has a constructor without args 
          result.init(xFuncRef.newInstance()); 
        } catch (InstantiationException e) {
          e.printStackTrace();
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }
      }  
      return result;
    }

    String getFuncId(Class<Function> xFuncRef) { // let descendants override that
      return xFuncRef.getSimpleName();
    }

    @SuppressWarnings("unchecked") // temp
    Descriptor register(Class<?> xRef) {
      Class<Function> xFuncRef = (Class<Function>) xRef; 
      return register(getFuncId(xFuncRef), xFuncRef); 
    }
  }

  public static void registerFunctions() {
    if (registry != null)
      return;

    registry = new Registry();

    Registrator r = new Registrator(); 
    /* Important: only classes with constructors without arguments can be 
       registered that way! Otherwise newInstance() fires InstantiationException.
     */
    MathFunc.registerFunctions(r);
    StrFunc.registerFunctions(r);
    BoolFunc.registerFunctions(r);
    Comparison.registerFunctions(r);
    Logic.registerFunctions(r);
  }

  static {
    registerFunctions();
  }

public IFunction[] getArgs() {
	return args;
}

} // Function
