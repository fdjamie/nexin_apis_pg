package functions;

import java.text.Format;
import java.text.ParseException; 
import java.util.*;

import functions.Function;
import functions.IFunction.EFuncIsNull;

/**
 * Synaptris Inc.
 * @author Evgeny Lyulkov  evgueni.lioulkov@synaptris.com
 * @created Aug.2011
 * @description Generic dali variant type
 * @Delphi_prototype IV_CONSTS.TDaliVariant
 */
public class Dali {

  public static enum Type {
    sftInt32,
    sftInt64,
    sftDouble,
    sftString,
    sftBoolean;
    
    @Override 
    public String toString() {
      switch (this) {
      case sftInt32: return "Integer";
      case sftInt64: return "Long Integer";
      case sftDouble: return "Number";
      default: return name().replace("sft", ""); 
      }
    }
      
    public static Type cast(final int i) { return values()[i]; } 

    public static Type superpose(final Type t1, final Type t2) {
      // returns a type compatible to both t1 and t2
      if (t1 == t2)
        return t1;
      if (t1 == null)
        return t2;
      if (t2 == null)
        return t1;
      Type result = t1.superpose(t2);
      if (result == null)
        result = t2.superpose(t1);
      return result;
    }

    private Type superpose(final Type t) {
      switch ( this ) { // scalars:
      case sftInt32: 
        return EnumSet.of(Type.sftInt64, Type.sftDouble).contains(t) ? t : null;
      case sftInt64: 
        return EnumSet.of(Type.sftDouble).contains(t) ? t : null;
      case sftDouble: return null;
      case sftString:  return null;
      default:
          return null;
      } // switch
    }

  }

  public static final Set<Type> VAR_SIZE_TYPES = EnumSet.of(
      Type.sftString);

  public static final Set<Type> NUMERIC_TYPES = EnumSet.of(
      Type.sftInt32,  Type.sftInt64, Type.sftDouble);

  public static final Set<Type> STRING_TYPES = EnumSet.of(
      Type.sftString);

  public static Variant newInstance(final Type type) {
    switch (type) {
    case sftInt32: return new I32(I32.Null);
    case sftInt64: return new I64(I64.Null);
    case sftDouble: return new Dbl(Dbl.Null);
    case sftString: return new Str("");
    case sftBoolean: return new Bool(Bool.Null);
    default: 
        assert false;
        return null;
    } // switch 
  }
  
  public static Variant getNull(final Type type) {
    switch (type) {
    case sftInt32: return I32.NULL;
    case sftInt64: return I64.NULL;
    case sftDouble: return Dbl.NULL;
    case sftString: return Str.NULL;
    case sftBoolean: return Bool.NULL;
    default: 
      assert false;
      return null;
    } // switch  
  }

  public static Variant min(Variant v1, Variant v2) { 
    return v1.compareTo(v2) < 0 ? v1 : v2; 
  }
  
  public static Variant max(Variant v1, Variant v2) { 
    return v1.compareTo(v2) > 0 ? v1 : v2; 
  }

  public abstract static class Variant implements Comparable<Variant> {

    Variant(final boolean isNull) { this.isNull = isNull; }

    public final static Variant NULL = new Bool(Bool.Null);
    
    public static Variant parseString(final String sValue) {
      return sValue.isEmpty() ? NULL : new Str(sValue);
    }

    public static Variant parseString(final String sValue, final Type type)
    throws ParseException {
      if (sValue.isEmpty())
        return NULL;
      switch (type) {
      case sftInt32: return new I32(sValue);
      case sftInt64: return new I64(sValue);
      case sftDouble: return new Dbl(sValue);
      case sftString: return new Str(sValue);
      case sftBoolean: new Bool(Boolean.parseBoolean(sValue));
      default: 
          assert false;
          return NULL;
      } // switch  
    }

    @Override
    public String toString() { return ""; }
    
    public abstract String format(Format format);

    protected Type type;
    public Type type() { return type; }

    protected boolean isNull;
    public boolean isNull() { return isNull; }
    public void setNull() { isNull = true; }

    public boolean isZero() { return false; }
    
    abstract int compare(final Variant v);

    @Override
    public int compareTo(final Variant v) {
      if ( isNull )
        return v.isNull ? 0 : -1;
      if ( v.isNull )
        return 1;
      if (type == v.type) {
        assert(getClass() == v.getClass());
        return compare(v);
      }  
      return compareMixedTypes(v);
    }
    
    protected int compareMixedTypes(Variant v) {
      return new Str(toString()).compare(new Str(v.toString()));
    }

    @Override
    public boolean equals(final Object v) { 
      return (v instanceof Variant) && compareTo((Variant) v) == 0; 
    }
    
    public void add(Variant v) { }; // implemented only for numeric subtypes
    
    @Override
    public Object clone() { assert(false); return NULL; } // just to declare it public
    
    // virtual convertors:
    public double toDouble() throws EFuncIsNull { 
      if (isNull)
        throw new Function.EFuncIsNull();
      assert(false); 
      return 0; 
    }
    
    public int toInt() throws EFuncIsNull { 
      if (isNull)
        throw new Function.EFuncIsNull();
      assert(false);
      return 0; 
    }
    
  } // Variant

  public static abstract class Numeric extends Variant {

    Numeric(final boolean isNull) { super(isNull); }
    
    @Override
    protected int compareMixedTypes(Variant v) {
      if (v instanceof Numeric)
      try {
        double x = toDouble();
        double y = v.toDouble();
        return x < y ? -1 : x > y ? 1 : 0;
      } catch (EFuncIsNull e) {
        return super.compareMixedTypes(v);
      }
      
      return super.compareMixedTypes(v);
    }
  }
  
  public static class I32 extends Numeric {
    private int value;
    
    public static final int Null = -2147483647; // cInt32Null

    public I32(final int i) {
      super(i == Null);
      value = i;
      type = Type.sftInt32;
    }
    public I32(final String i) { this(Integer.parseInt(i)); }

    public String toString() { return isNull ? "" : String.valueOf(value); }
    
    @Override
    public String format(Format format) {
      return format.format(value);
    }
    
    public int value() { return value; }

    @Override
    public boolean isZero() { return value == 0; }
    
    @Override
    int compare(final Variant v) {
      return value < ((I32) v).value ? -1 : value > ((I32) v).value ? 1 : 0;
    }

    @Override
    public void add(Variant v) {
      if (v.isNull)
        return;
      assert(v instanceof I32);
      if ( isNull ) {
        value = ((I32) v).value;
        isNull = false;
      } else
        value += ((I32) v).value;
    }
    
    @Override
    public Object clone() { return new I32(isNull ? Null : value); }
    
    @Override
    public double toDouble() throws EFuncIsNull {
      if (isNull)
        return super.toDouble();
      return value; 
    }
    
    @Override
    public int toInt() throws EFuncIsNull { 
      if (isNull)
        return super.toInt();
      return value; 
    }
  }

  public static class I64 extends Numeric{
    private long value;

    public static final long Null = -9223372036854775807L;
    public I64(final long i) {
      super(i == Null);
      value = i;
      type = Type.sftInt64;
    }
    public I64(final String i) { this(Long.parseLong(i)); }

    public String toString() { return isNull ? "" : String.valueOf(value); }
    
    @Override
    public boolean isZero() { return value == 0; }
    
    @Override
    public String format(Format format) {
      return format.format(value);
    }
    
    public long value() { return value; }

    @Override
    int compare(final Variant v) {
      return value < ((I64) v).value ? -1 : value > ((I64) v).value ? 1 : 0;
    }

    @Override
    public void add(Variant v) {
      if (v.isNull)
        return;
      switch (v.type) {
      case sftInt32:
        if ( isNull ) {
          value = ((I32) v).value;
          isNull = false;
        } else
          value += ((I32) v).value;
        return;
      case sftInt64:
        if ( isNull ) {
          value = ((I64) v).value;
          isNull = false;
        } else
          value += ((I64) v).value;
        return;
      default:
/*        
        if (v instanceof MvVariant) {
          MvVariant mv = (MvVariant) v;
          for (int i = 0; i < mv.mvCount; i++)
            add(mv.get(i));
        } else
*/
          assert(false);
      } // switch
    }
    
    @Override
    public Object clone() {
      return new I64(isNull ? Null : value);
    }
    
    @Override
    public double toDouble() throws EFuncIsNull {
      return isNull ? super.toDouble() : value; 
    }
    
    @Override
    public int toInt() throws EFuncIsNull { 
      return isNull ? super.toInt() : (int) value; 
    }
  }

  public static class Dbl extends Numeric{
    protected double value;
    
    public static final double Null = Double.NaN;
    
    public Dbl(final double d) {
      super(Double.isNaN(d));
      value = d;
      type = Type.sftDouble;
    }

    public Dbl(final String i) { this(Double.parseDouble(i)); }

    public String toString() { return isNull? "" : String.valueOf(value); }

    @Override
    public boolean isZero() { return value == 0; }
    
    @Override
    public String format(Format format) {
      return format.format(value);
    }
    
    public double value() { return value; }

    @Override
    int compare(final Variant v) {
      double diff = value - ((Dbl) v).value;
      return diff < 0 ? -1 : diff == 0 ? 0 : 1;
    }

    @Override
    public void add(Variant v) {
      if (v.isNull)
        return;
      switch (v.type) {
      case sftInt32:
        if ( isNull ) {
          value = ((I32) v).value;
          isNull = false;
        } else
          value += ((I32) v).value;
        return;
      case sftInt64:
        if ( isNull ) {
          value = ((I64) v).value;
          isNull = false;
        } else
          value += ((I64) v).value;
        return;
      case sftDouble:
        if ( isNull ) {
          value = ((Dbl) v).value;
          isNull = false;
        } else
          value += ((Dbl) v).value;
        return;
        default: assert(false);
      } // switch
    }
    
    @Override
    public Object clone() {
      return new Dbl(isNull ? Null : value);
    }
    
    @Override
    public double toDouble() throws EFuncIsNull {
      return isNull ? super.toDouble() : value; 
    }
    
    @Override
    public int toInt() throws EFuncIsNull { 
      return isNull ? super.toInt() : (int) value; 
    }
  }

  public static class Str extends Variant{
    private String str;

    public Str(final String s) {
      super(s.isEmpty());
      str = s;
      type = Type.sftString;
    }

    public String toString() { return str; }
    
    @Override
    public String format(Format format) { return str; }
    
    // public String Str() { return str; }

    @Override
    int compare(final Variant v) {
      int diff = str.compareToIgnoreCase(((Str) v).str);
      return diff < 0 ? -1 : diff == 0 ? 0 : 1;
    }

    @Override
    public Object clone() { return new Str(isNull? "" : str); }
    
    
    @Override
    protected int compareMixedTypes(Variant v) {
      return compare(new Str(v.toString()));
    }
  }

  public static class Bool extends Variant{

    public static final byte Null = (byte)0xFF;

    public Bool(final byte i) {
      super(i == Null);
      bool = (i != 0);
      type = Type.sftBoolean;
    }

    public Bool(final boolean b) { 
      super(false);
      bool = b;
      type = Type.sftBoolean;
    }

    public Bool(final String s) { this(Boolean.parseBoolean(s)); }

    public String toString() { return isNull ? "" : String.valueOf(bool); }
    
    @Override
    public String format(Format format) { return toString(); }
    
    public boolean bool() { return bool; }

    @Override
    int compare(final Variant v) {
      return ((Bool) v).bool ? ( bool ? 0 : -1 ) : bool ? 1 : 0;
    }

    @Override
    public Object clone() {
      return isNull ? new Bool(Null) :  new Bool(bool);
    }
    
    private boolean bool;
  }

} // Dali
