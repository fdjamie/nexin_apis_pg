package functions;

import java.text.*;
import java.util.*;

import functions.Dali;

/**
 * @description function.IFunction-driven expression parser
 */

public class Parser {
  
  enum LexemeType {
    ltEqual, ltLess, ltGreater, ltLessOrEqual, ltGreaterOrEqual, ltNotEqual,
    ltPlus, ltMinus,
    ltAsterisk, ltSlash, ltMod,
    ltNot,
    ltCap,
    ltAnd,
    ltOr, ltXor,
    ltLeftBracket, ltRightBracket,
    ltVariable, ltDate, ltTime, ltDateTime, ltNumber, ltStringConst,
    ltTrue, ltFalse,
    ltFunc, ltListSeparator,
    ltEnd
  }
  
  class Lexeme {
    LexemeType lexemeType;
    int pos;
    String lexeme;
    Function.Descriptor desc;
  }

  public class EfnSyntaxError extends Exception {
    String errorText;
    int errorPos, errorLen;
    
	final static String SYNTAX_ERROR = "\n\tSyntax error at position "; 
    public EfnSyntaxError(String sMsg, final int pos, final int len) { 
        super(sMsg.contains(fExpr + SYNTAX_ERROR) ? sMsg : (fExpr + SYNTAX_ERROR + String.valueOf(pos) + ".\n\t" + sMsg)); 
        errorText = sMsg;
        errorPos = pos;
        errorLen = len;
      }

    private static final long serialVersionUID = 1L;
    
    public String getErrorText() { return errorText; }
    public int getErrorPos() { return errorPos; }
    public int getErrorLen() { return errorLen; }
  } // EivSyntaxError
  
  final char csConstTerminator = '\'';
  final char csVarTerminator = '#';
  char decimalSeparator = '.';
  char dateSeparator = '/';
  char timeSeparator = ':';
  
  String fExpr;
  int fExprLen, fPos, fHintPos;
  Lexeme fCurLexeme = new Lexeme(); // current lexeme
  boolean fHintMode;
  Set<Dali.Type> fResultTypes  = EnumSet.noneOf(Dali.Type.class);
  Set<Function.Category> fCategories  = EnumSet.allOf(Function.Category.class);
  String fHintPrefix, fHintCore, fHintSuffix;
  
  /* that stuff does not reflect real locale settings
  public Parser() {
    NumberFormat f = DecimalFormat.getCurrencyInstance();
    if (f instanceof DecimalFormat)
      decimalSeparator = ((DecimalFormat) f).getDecimalFormatSymbols().getDecimalSeparator();
    // decimalSeparator = new DecimalFormatSymbols().getDecimalSeparator();
  } // Parser()
    */
  
  String read(final int iStartPos, final int iLen) {
    return fExpr.substring(iStartPos, Math.min(iStartPos + iLen, fExpr.length()));
  }
  
  public IFunction parse(final String expression) throws EfnSyntaxError {
    fExpr = expression.trim();
    fExprLen = fExpr.length();
    fPos = 0;

    next();
    IFunction result = expr();
    if ( fCurLexeme.lexemeType != LexemeType.ltEnd )
      if ( fCurLexeme.lexeme != "" )
        raiseError("Unexpected symbol '" + fCurLexeme.lexeme + "'", fCurLexeme.pos);
      else if ( fCurLexeme.pos <= fExprLen )
       raiseError("Unexpected symbol '" + fExpr.charAt(fCurLexeme.pos) + "'", fCurLexeme.pos);
      else
        raiseError("Unexpected symbol", fCurLexeme.pos);
      if ( (result != null) &&  !fResultTypes.isEmpty() &&
         ! Function.compatible(fResultTypes, result.type()) ) {
        String wText = "";
        for ( Dali.Type aFT: Dali.Type.values())
          if ( fResultTypes.contains(aFT) ) {
            String wType = aFT.toString();
            if ( wType != "" ) {
              if ( wText != "" )
                wText = wText + " or ";
              wText = wText + wType.toLowerCase();
            };
          };
        if ( wText != "" )
          wText = ". Return value must be " + wText;
        raiseError("Incorrect expression " + wText, 1);
      };
      return result;
  }

  private IFunction expr() throws EfnSyntaxError {
    IFunction result = disjunction();
    if (fCurLexeme.lexemeType == LexemeType.ltEnd)
      next();
    return result;
  }
  
  IFunction createFunc(Class<?> xFuncRef, IFunction...args) throws EfnSyntaxError {
    try {
      return Function.createFunc(Function.selectOverloadedFunc(xFuncRef, args), args);
    } catch (Exception e) {
      raiseError(e.getMessage(), fPos);
      return null;
    }
  }

  IFunction disjunction() throws EfnSyntaxError {
    IFunction result = conjunction();
    LexemeType xOperator;
    while ( (fCurLexeme.lexemeType == LexemeType.ltOr) ||
            (fCurLexeme.lexemeType == LexemeType.ltXor)) {
      xOperator = fCurLexeme.lexemeType;
      next();
      switch ( xOperator ) {
      case ltOr: result = createFunc(BoolFunc.Or.class, result, conjunction()); break;
      case ltXor: result = createFunc(BoolFunc.Xor.class, result, conjunction());
      }; // switch
    };
    return result;
  }

  IFunction conjunction() throws EfnSyntaxError {
    IFunction result = notFactor();
    while ( fCurLexeme.lexemeType == LexemeType.ltAnd) {
      next();
      result = createFunc(BoolFunc.And.class, result, notFactor());
    };
    return result;
  }

  private IFunction notFactor() throws EfnSyntaxError {
    if ( fCurLexeme.lexemeType == LexemeType.ltNot ) {
      next();
      return createFunc(BoolFunc.Not.class, boolFactor());
    } else
      return boolFactor();
  }

  private IFunction boolFactor() throws EfnSyntaxError {
    return relation();
  }

  final static Set<LexemeType> cxCompareOperators = 
    EnumSet.of(LexemeType.ltEqual, LexemeType.ltLess, LexemeType.ltGreater, 
        LexemeType.ltLessOrEqual, LexemeType.ltGreaterOrEqual, LexemeType.ltNotEqual);
  
  private IFunction relation() throws EfnSyntaxError {
    IFunction result = mathExpr();

    if ( cxCompareOperators.contains(fCurLexeme.lexemeType) ) {
      LexemeType xOperator = fCurLexeme.lexemeType;
      next();
      switch ( xOperator ) {
      case ltEqual: 
        result = createFunc(Comparison.Equal.class, result, mathExpr()); break;
      case ltLess: 
        result = createFunc(Comparison.Less.class, result, mathExpr()); break; 
      case ltGreater: 
        result = createFunc(Comparison.Greater.class, result, mathExpr()); break;
      case ltLessOrEqual: 
        result = createFunc(Comparison.LessOrEqual.class, result, mathExpr()); break;
      case ltGreaterOrEqual: 
        result = createFunc(Comparison.GreaterOrEqual.class, result, mathExpr()); break;
      case ltNotEqual: 
        result = createFunc(Comparison.NotEqual.class, result, mathExpr());
      }; // switch
    };
    return result;
  }

  private IFunction mathExpr() throws EfnSyntaxError {
    IFunction result = term();
    while ( (fCurLexeme.lexemeType == LexemeType.ltPlus) ||
            (fCurLexeme.lexemeType == LexemeType.ltMinus )) {
      LexemeType xOperator = fCurLexeme.lexemeType;
      next();
      switch ( xOperator ) {
      case ltPlus:
        result = createFunc(MathFunc.Plus.class, result, term()); break;
      case ltMinus:
        result = createFunc(MathFunc.Minus.class, result, term());
      }; // switch
    }; // while
    return result;
  }

  final static Set<LexemeType> Operator2 = EnumSet.of(LexemeType.ltAsterisk, 
      LexemeType.ltSlash, LexemeType.ltMod);
  
  private IFunction term() throws EfnSyntaxError {
    IFunction result = factor();
    while ( Operator2.contains(fCurLexeme.lexemeType) ) {
      LexemeType xOperator = fCurLexeme.lexemeType;
      next();
      switch ( xOperator ) {
      case ltAsterisk: 
        result = createFunc(MathFunc.Multiply.class, result, factor()); break;
      case ltSlash: 
        result = createFunc(MathFunc.Divide.class, result, factor()); break;
      case ltMod: 
        result = createFunc(MathFunc.Remainder.class, result, factor());
      }; // switch
    }; // while
    return result;
  }

  private IFunction factor() throws EfnSyntaxError {
    switch ( fCurLexeme.lexemeType ) {
    case ltPlus:
      next();
      return factor();
    case ltMinus:
      next();
      return createFunc(MathFunc.UnaryMinus.class, factor());
    case ltCap:
      IFunction result = base();
      try {
        next();
        return createFunc(MathFunc.Power.class, result, factor());
      } catch (Exception e) {
        return null;
      }
    default: return base();
    } // switch
  }

  private IFunction base() throws EfnSyntaxError {
    IFunction result = null;
    switch ( fCurLexeme.lexemeType ) {
    case ltLeftBracket:
      next();
      result = disjunction();
      if ( fCurLexeme.lexemeType != LexemeType.ltRightBracket )
        raiseError("')' is expected", fCurLexeme.pos);
      next();
      return result;
    case ltVariable:
      result = variable();
      next();
      return result;
    case ltNumber:
      try {
        result = new MathFunc.Integer.Const(Integer.parseInt(fCurLexeme.lexeme));
      } catch (NumberFormatException e) { 
        result = new MathFunc.Float.Const(Double.parseDouble(fCurLexeme.lexeme));
      }  
      next();
      return result;
    case ltStringConst:
      result = new StrFunc.Const(fCurLexeme.lexeme);
      next();
      return result;
    case ltTrue: ;
    case ltFalse:
      result = new BoolFunc.Const(fCurLexeme.lexemeType == LexemeType.ltTrue);
      next();
      return result;
    case ltFunc:
      return func();
    case ltEnd:
      raiseError("incomplete expression", fCurLexeme.pos);
      return null;
    default:
      raiseError("unexpected symbol" + lexeme2ErrorStr(fCurLexeme.lexeme), fCurLexeme.pos);
      return null;
    } // switch
  }

  void prepareHint(Function.Descriptor desc, final int iArg ) {
    // prepare a hint as an argument description:
    if ( fHintPrefix + fHintCore + fHintSuffix != "" )
      return; // already prepared for a nested function
    fHintPrefix = desc.shortName();
    if ( desc.argCount() > 0 )
      fHintPrefix = fHintPrefix + "(";
    for ( int  i = 0; i < Math.min(iArg, desc.argCount())  ; i++ ) {
      fHintPrefix = fHintPrefix + desc.getArgDescription(i);
      if ( i < desc.argCount() - 1 )
        fHintPrefix = fHintPrefix + ", ";
    };
    
    fHintCore = iArg >= desc.argCount() ? "" : desc.getArgDescription(iArg);
  
    fHintSuffix = "";
    for ( int  i = iArg + 1; i < desc.argCount()  ; i++ )
      fHintSuffix = fHintSuffix + ", " + desc.getArgDescription(i);
    if ( desc.argCount() > 0 )
      fHintSuffix = fHintSuffix + ")";
  }

  private IFunction func() throws EfnSyntaxError {
    Function.Descriptor desc = fCurLexeme.desc;
    if ( desc == null )
      raiseError("undefined function " + fCurLexeme.lexeme, fCurLexeme.pos);
    next();

    List<IFunction> xArgs = new ArrayList<IFunction>();
    if ( desc.argCount() > 0 ) {
      if ( fCurLexeme.lexemeType != LexemeType.ltLeftBracket )
        raiseError("'(' is expected", fCurLexeme.pos);
      do {
        next();
        try {
          xArgs.add(disjunction());
        } catch (Exception e) {
          prepareHint(desc, xArgs.size());
          raiseError(e.getMessage(), fCurLexeme.pos);
        };
      } while (fCurLexeme.lexemeType == LexemeType.ltListSeparator);
      if ( fCurLexeme.lexemeType != LexemeType.ltRightBracket ) {
        if ( (fCurLexeme.lexemeType == LexemeType.ltEnd) && (xArgs.size() > 0) )
          prepareHint(desc, xArgs.size() - 1);
        else  
          prepareHint(desc, xArgs.size());
        if ( (xArgs.size() < desc.argCount()) &&
           (fCurLexeme.lexeme != "") )
          raiseError("undefined identifier" + lexeme2ErrorStr(fCurLexeme.lexeme), fCurLexeme.pos);
        else
          raiseError("')' is expected", fCurLexeme.pos);
      };
      next();
    };
    return createFunc(desc.ref(), xArgs.toArray(new IFunction[xArgs.size()]));
  }

  protected IFunction variable(final String var) throws EfnSyntaxError { // to override in descendants
    return null;
  }
  
  private IFunction variable() throws EfnSyntaxError {
    IFunction result = null;
    try {
      result = variable(fCurLexeme.lexeme);
    } catch (Exception e) {
      raiseError(e.getMessage(), fCurLexeme.pos);
    };
    if ( result == null )
      raiseError("undefined identifier" + lexeme2ErrorStr(fCurLexeme.lexeme), fCurLexeme.pos);
    return result;
  }

  private String lexeme2ErrorStr(String lex) {
    return lex.isEmpty() ? "" : " '" + lex + "'";  
  }

  void next() throws EfnSyntaxError {
    if ( fPos < fExprLen ) {
      skipWhiteSpace();
      extractLexeme();
    } else
      putLexeme(LexemeType.ltEnd, fPos, "");
  }
  
  private void putLexeme(final LexemeType lt, final int iPos, final String lex) {
    fCurLexeme.lexemeType = lt;
    fCurLexeme.pos = iPos;
    fCurLexeme.lexeme = lex;
  }

  private void skipWhiteSpace() {
    while ( fPos < fExprLen) {
      char ch = fExpr.charAt(fPos);
      if ((ch != ' ') && (ch != 0x9) && (ch != 0x13) && (ch != 0x10))
        return;
      fPos++;
    }
  }

  private void extractLexeme() throws EfnSyntaxError {
    if ( fPos >= fExprLen )
      return;
    switch ( fExpr.charAt(fPos) ) {
    case '!':
        if ( (fPos < fExprLen - 1) && (fExpr.charAt(fPos + 1) == '=') ) {
          putLexeme(LexemeType.ltNotEqual, fPos, "");
          fPos += 2;
        } else
          putLexeme(LexemeType.ltNot, fPos++, "");
        return;
    case csConstTerminator:
        if ( read(fPos, 6).equalsIgnoreCase("\"TRUE\"" )) {
          putLexeme(LexemeType.ltTrue, fPos, "");
          fPos += 6;
        } else if ( read(fPos, 7).equalsIgnoreCase("\"FALSE\"" )) {
          putLexeme(LexemeType.ltFalse, fPos, "");
          fPos += 7;
        } else { // read string const:
          int iInitPos = fPos + 1;
          do {
            if ( ++fPos >= fExprLen )
              raiseError("Unterminated String", iInitPos);
          } while (fExpr.charAt(fPos) != csConstTerminator);
          fPos++;
          putLexeme(LexemeType.ltStringConst, iInitPos, read(iInitPos, fPos - 1 - iInitPos));
        }
        return;
    case '%': putLexeme(LexemeType.ltMod, fPos++, ""); return;
    case '&':
        if ( fPos >= fExprLen - 1)
          raiseError("Unexpected symbol '&'", fPos);
        if ( fExpr.charAt(fPos + 1) == '&' ) {
          putLexeme(LexemeType.ltAnd, fPos, "");
          fPos += 2;
        } else
          raiseError("Unexpected symbol '" + fExpr.charAt(fPos + 1) + "'", fPos + 1);
        return;
    /*
    case csVarTerminator: { // read variable:
      int iInitPos = fPos + 1;
      do {
        if ( ++fPos >= fExprLen )
          raiseError("Unterminated variable name", iInitPos);
      } while ( fExpr.charAt(fPos) != csVarTerminator );
      fPos++;
      putLexeme(LexemeType.ltVariable, iInitPos, read(iInitPos, fPos - 1 - iInitPos));
      return;
    }
    */
    case '(': putLexeme(LexemeType.ltLeftBracket, fPos++, "("); return;
    case ')': putLexeme(LexemeType.ltRightBracket, fPos++, ")"); return;
    case '*': putLexeme(LexemeType.ltAsterisk, fPos++, ""); return;
    case '+': putLexeme(LexemeType.ltPlus, fPos++, ""); return;
    case '-': putLexeme(LexemeType.ltMinus, fPos++, ""); return;
    case ',': putLexeme(LexemeType.ltListSeparator, fPos++, ""); return;
    case '/': putLexeme(LexemeType.ltSlash, fPos++, ""); return;
    case ';': putLexeme(LexemeType.ltEnd, fPos++, ""); return;
    case '<':
        if ( (fPos < fExprLen - 1) && (fExpr.charAt(fPos + 1) == '=') ) {
          putLexeme(LexemeType.ltLessOrEqual, fPos, "");
          fPos += 2;
        } else if ( (fPos < fExprLen - 1) && (fExpr.charAt(fPos + 1) == '>') ) {
          putLexeme(LexemeType.ltNotEqual, fPos, "");
          fPos += 2;
        } else
          putLexeme(LexemeType.ltLess, fPos++, "");
        return;
    case '=':
        if ( (fPos < fExprLen - 1) && (fExpr.charAt(fPos + 1) == '=') ) {
          putLexeme(LexemeType.ltEqual, fPos, "");
          fPos += 2;
        } else
          putLexeme(LexemeType.ltEqual, fPos++, "");
        return;
    case '>':
        if ( (fPos < fExprLen - 1) && (fExpr.charAt(fPos + 1) == '=') ) {
          putLexeme(LexemeType.ltGreaterOrEqual, fPos, "");
          fPos += 2;
        } else
          putLexeme(LexemeType.ltGreater, fPos++, "");
        return;
    case '^': putLexeme(LexemeType.ltCap, fPos++, ""); return;
    case '|':
        if ( fPos >= fExprLen )
          raiseError("Unexpected symbol '|'", fPos);
        if ( fExpr.charAt(fPos + 1) == '|' ) {
          putLexeme(LexemeType.ltOr, fPos, "");
          fPos += 2;
        } else
          raiseError("Unexpected symbol '" + fExpr.charAt(fPos + 1) + "'", fPos + 1);
        return;
    default:
      char ch = fExpr.charAt(fPos);
      if (Character.isDigit(ch)) {
        if ( ! readDateTime() ) { // read number:
          int iInitPos = fPos;
          do {
            fPos++;
          } while ((fPos < fExprLen) && Character.isDigit(fExpr.charAt(fPos)));
          if ( (fPos < fExprLen) &&
             (fExpr.charAt(fPos) == decimalSeparator)) {
            fPos++;
            if ( (fPos >= fExprLen) || ! Character.isDigit(fExpr.charAt(fPos)) )
              fPos--;
            else
              do
                fPos++;
              while ((fPos < fExprLen) && Character.isDigit(fExpr.charAt(fPos)));
          }
          if ( (fPos < fExprLen) && 
              (Character.toUpperCase(fExpr.charAt(fPos)) == 'E') ) {
            int iRollbackPos = fPos++;
            if ( fPos >= fExprLen )
              fPos = iRollbackPos;
            else {
              if ( (fExpr.charAt(fPos) == '+') || (fExpr.charAt(fPos) == '-'))
                fPos++;
              if ( fPos >= fExprLen )
                fPos = iRollbackPos;
              else if ( ! Character.isDigit(fExpr.charAt(fPos)) ) {
                if ( ! isAlpha(fExpr.charAt(fPos)) ) // for unexpected alphas see raiseError below
                  fPos = iRollbackPos;
              } else
                do
                  fPos++;
                while ((fPos < fExprLen) && Character.isDigit(fExpr.charAt(fPos)));
            }
          }
          if ( (fPos < fExprLen) && isAlpha(fExpr.charAt(fPos)) ) {
            while ( (fPos < fExprLen) &&
                  (isAlpha(fExpr.charAt(fPos)) || Character.isDigit(fExpr.charAt(fPos))))
              fPos++; // to set correct fErrorLen
            raiseError("Expression syntax error - unexpected character '" +
              read(iInitPos, fPos - iInitPos) + "'", iInitPos);
          };
          putLexeme(LexemeType.ltNumber, iInitPos, read(iInitPos, fPos - iInitPos));
        }
      return;  
    }
    if (isAlpha(ch)) { // read regular word:
      switch ( ch ) {
        case 'A': ; 
        case 'a':
          if ( read(fPos, 3).equalsIgnoreCase("AND") ) {
            putLexeme(LexemeType.ltAnd, fPos, "");
            fPos += 3;
            return;
          };
          break;
        case 'F': ; 
        case 'f':
          if ( read(fPos, 5).equalsIgnoreCase("FALSE") ) {
            putLexeme(LexemeType.ltFalse, fPos, "");
            fPos += 5;
            return;
          };
          break;
        case 'M': ; 
        case 'm':
          if ( read(fPos, 3).equalsIgnoreCase("MOD") ) {
            putLexeme(LexemeType.ltMod, fPos, "");
            fPos += 3;
            return;
          };
          break;
        case 'N': ; 
        case 'n':
          if ( read(fPos, 3).equalsIgnoreCase("NOT") ) {
            putLexeme(LexemeType.ltNot, fPos, "");
            fPos += 3;
            return;
          };
          break;
        case 'O': ; 
        case 'o':
          if ( read(fPos, 2).equalsIgnoreCase("OR") ) {
            putLexeme(LexemeType.ltOr, fPos, "");
            fPos += 2;
            return;
          };
          break;
        case 'T': ; 
        case 't':
          if ( read(fPos, 4).equalsIgnoreCase("TRUE") ) {
            putLexeme(LexemeType.ltTrue, fPos, "");
            fPos += 4;
            return;
          };
          break;
        case 'X': ; 
        case 'x':
          if ( read(fPos, 3).equalsIgnoreCase("XOR") ) {
            putLexeme(LexemeType.ltXor, fPos, "");
            fPos += 3;
            return;
          };
      }; // switch

      int iInitPos = fPos++;
      while ( (fPos < fExprLen) &&
            isAlphaOrDigit(fExpr.charAt(fPos)))
        fPos++;
      String wId = read(iInitPos, fPos - iInitPos);
      Function.Descriptor desc = Function.findFunction(wId);
      if ( (desc == null) || ! (fCategories.contains(desc.category())) )
        putLexeme(LexemeType.ltVariable, iInitPos, wId);
      else {
        putLexeme(LexemeType.ltFunc, iInitPos, "");
        fCurLexeme.desc = desc;
      } 
    } else  
      raiseError("Unexpected symbol '" + fExpr.charAt(fPos) + "'", fPos);
    }; // switch
  }

  boolean isAlpha(final char ch) {
    return ((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z')) ||
      (ch == '_') || (ch > 255);
  }
  
  private boolean isAlphaOrDigit(final char ch) {
    return Character.isDigit(ch) || isAlpha(ch);
  }

  private boolean readDateTime() {
    int iDateSeparatorCount = 0;
    boolean bTime = false;
    int iPos = fPos;
    int iInitPos = iPos;
    do {
      if ( fExpr.charAt(iPos) == dateSeparator )
        iDateSeparatorCount++;
      bTime = bTime || (fExpr.charAt(iPos) == timeSeparator);
      iPos++;
    } while ((iPos < fExprLen) &&
        (Character.isDigit(fExpr.charAt(iPos)) ||
         (fExpr.charAt(iPos) == dateSeparator) ||
         (fExpr.charAt(iPos) == timeSeparator)));
    boolean bDate = iDateSeparatorCount == 2;
    boolean result = (iPos < fExprLen) && (bDate || bTime);
    if ( result ) {
      String wsLexeme = read(iInitPos, iPos - iInitPos);
      try {
        if ( bDate ) 
          if ( bTime )
            DateFormat.getTimeInstance().parse(wsLexeme);
          else
            DateFormat.getDateInstance(DateFormat.SHORT).parse(wsLexeme);
        else
          DateFormat.getTimeInstance().parse(wsLexeme);
      } catch (ParseException e) {
        result = false;
      }
      if ( result ) {
        fPos = iPos;
        if ( bDate )
          if ( bTime )
            putLexeme(LexemeType.ltDateTime, iInitPos, wsLexeme);
          else
            putLexeme(LexemeType.ltDate, iInitPos, wsLexeme);
        else
          putLexeme(LexemeType.ltTime, iInitPos, wsLexeme);
      };
    };
    return result;
  }

  void raiseError(final String wsMessage, final int iPosition) throws EfnSyntaxError {
    if ( fHintMode )
      fHintPos = iPosition;
    throw new EfnSyntaxError(wsMessage, iPosition, fPos - iPosition);
  }
  
} // Parser
