package functions;

import functions.Function.Registrator;
import functions.Dali.Variant;

public abstract class MathFunc {
  
  static class EivIntOverflow extends ArithmeticException {
    private static final long serialVersionUID = 1L;
  }

  public interface INumeric extends IFunction{
    int getInt(IAbscissa abscissa) throws EFuncIsNull;
    double getDouble(IAbscissa abscissa) throws EFuncIsNull;
  }
  
  abstract static class Numeric extends Function implements INumeric{
    @Override
    public Dali.Variant calc(IAbscissa abscissa) throws EFuncIsNull {
      return new Dali.Dbl(getDouble(abscissa));
    }
  
    @Override
    public Dali.Type type() {
      Dali.Type result = Dali.Type.sftInt32, t;
      for (int i = 0; i < args.length; i++) {
        t = args[i].type();
        if (t == null)
          return null;
        switch (t) {
        case sftInt32: break;
        case sftInt64: result = Dali.Type.sftInt64; break;
        case sftDouble: return Dali.Type.sftDouble;
        default: assert(false); return Dali.Type.sftDouble;
        } // switch
      }
      return result; 
    }

    @Override
    public Category category() { return Category.ivfcMathFunc; } 
    
    public abstract int getInt(IAbscissa abscissa) throws EFuncIsNull;
    public abstract double getDouble(IAbscissa abscissa) throws EFuncIsNull;
  } // Numeric
  
  abstract static class Integer extends Numeric {
    @Override
    public Dali.Type type() { return Dali.Type.sftInt32; }

    @Override
    public Dali.Variant calc(IAbscissa abscissa) throws EFuncIsNull {
      return new Dali.I32(getInt(abscissa));
    }

    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      return getInt(abscissa);
    }
    
    public static class Const extends Integer {
      int value;
      public Const(final int i) { value = i; } 
      
      @Override
      public Priority priority() { return Priority.ivpTerm; }

      @Override
      public int getInt(IAbscissa abscissa) { return value; }
      
      @Override
      public String toString() { return String.valueOf(value); }
      
    } // Const
    
    abstract static class Func extends Integer { // TivIntegerFunc
      @Override
      public Priority priority() { return Priority.ivpFunction; }
    } // Func
    
  } // Integer
  
  abstract static class Float extends Numeric {
    @Override
    public Priority priority() { return Priority.ivpFunction; }

    @Override
    public Variant calc(IAbscissa abscissa) throws EFuncIsNull {
      return new Dali.Dbl(getDouble(abscissa));
    }

    @Override
    public int getInt(IAbscissa abscissa) throws EFuncIsNull {
      return (int) getDouble(abscissa);
    }
    
    public static class Const extends Float {
      double value;
      public Const(final double d) { value = d; } 
      
      @Override
      public Dali.Type type() { return Dali.Type.sftDouble; }

      @Override
      public Priority priority() { return Priority.ivpTerm; }

      @Override
      public double getDouble(IAbscissa abscissa) { return value; }
      
      @Override
      public String toString() { return String.valueOf(value); }
    } // Const
  } // Float
  
  abstract static class FloatFunc extends Float {
    
  } // FloatFunc
  
  abstract static class FloatToFloat extends FloatFunc {
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { Dali.Type.sftDouble }; 
      return result;
    }
    
    @Override
    public Variant calc(IAbscissa abscissa) throws EFuncIsNull {
      if (args[0].type() == Dali.Type.sftInt32) 
        return new Dali.I32(getInt(abscissa));
      else
        return new Dali.Dbl(getDouble(abscissa));
     }
  } // FloatToFloat
  
  public static class UnaryMinus extends FloatToFloat {
    @Override
    public String shortName() { return "-"; }
    
    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      return - ((INumeric) args[0]).getDouble(abscissa);
    }
  } // UnaryMinus
  
  public static class Abs extends FloatToFloat {
    @Override
    public int getInt(IAbscissa abscissa) throws EFuncIsNull {
      return Math.abs(((INumeric) args[0]).getInt(abscissa));
    }
    
    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      return Math.abs(((INumeric) args[0]).getDouble(abscissa));
    }
  } // Abs
  
  abstract static class NumericOf2Args extends Numeric {
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { Dali.Type.sftDouble, Dali.Type.sftDouble }; 
      return result;
    }
    
    @Override
    public Variant calc(IAbscissa abscissa) throws EFuncIsNull {
      assert(args[0] instanceof INumeric);
      assert(args[1] instanceof INumeric);
      try {
        if (type() == Dali.Type.sftInt32) 
          try {
            return new Dali.I32(getInt(abscissa));
          } catch (EFuncIsNull e) {
            throw e;
          } catch (Exception e) {
            return new Dali.Dbl(getDouble(abscissa));
          }
        else
          return new Dali.Dbl(getDouble(abscissa));
      } catch (EFuncIsNull e) {
        throw e;
      } catch (Exception e) {    
          return csInvalidValue;
      }
    }
  }
  
  abstract static class Arithmetic extends NumericOf2Args {
  }
  
  abstract static class Summation extends Arithmetic {
    @Override
    public Priority priority() { return Priority.ivpSum; }
  }
  
  abstract static class Multiplication extends Arithmetic {
    @Override
    public Priority priority() { return Priority.ivpMult; }
  }
  
  public static class Plus extends Summation {
    @Override
    public String shortName() { return "+"; }

    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      double aV1;
      try {
        aV1 = ((INumeric) args[0]).getDouble(abscissa);
      } catch (EFuncIsNull E) {
        return ((INumeric) args[1]).getDouble(abscissa);
      }
      try {
        return aV1 + ((INumeric) args[1]).getDouble(abscissa);
      } catch ( EFuncIsNull e) {
        return aV1;
      }
    }

    @Override
    public int getInt(IAbscissa abscissa) throws EFuncIsNull {
      int aV1;
      try {
        aV1 = ((INumeric) args[0]).getInt(abscissa);
      } catch (EFuncIsNull E) {
        return ((INumeric) args[1]).getInt(abscissa);
      }
      try {
        return aV1 + ((INumeric) args[1]).getInt(abscissa);
      } catch ( EFuncIsNull e) {
        return aV1;
      }
    }
  } // Plus
  
  public static class Minus extends Summation {
    @Override
    public String shortName() { return "-"; }

    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      double aV1;
      try {
        aV1 = ((INumeric) args[0]).getDouble(abscissa);
      } catch (EFuncIsNull E) {
        return - ((INumeric) args[1]).getDouble(abscissa);
      }
      try {
        return aV1 - ((INumeric) args[1]).getDouble(abscissa);
      } catch ( EFuncIsNull e) {
        return aV1;
      }
    }

    @Override
    public int getInt(IAbscissa abscissa) throws EFuncIsNull {
      int aV1;
      try {
        aV1 = ((INumeric) args[0]).getInt(abscissa);
      } catch (EFuncIsNull E) {
        return - ((INumeric) args[1]).getInt(abscissa);
      }
      try {
        return aV1 - ((INumeric) args[1]).getInt(abscissa);
      } catch ( EFuncIsNull e) {
        return aV1;
      }
    }
  } // Minus
  
  public static class Multiply extends Multiplication {
    @Override
    public String shortName() { return "*"; }

    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      return ((INumeric) args[0]).getDouble(abscissa) * 
        ((INumeric) args[1]).getDouble(abscissa);
    }

    @Override
    public int getInt(IAbscissa abscissa) throws EFuncIsNull {
      long iInt64;
        iInt64 = (long) ((INumeric) args[0]).getInt(abscissa) * 
          ((INumeric) args[1]).getInt(abscissa);
        if ((iInt64 >= java.lang.Integer.MIN_VALUE) && 
            (iInt64 <= java.lang.Integer.MAX_VALUE) )
          return (int) iInt64;
        throw new EivIntOverflow(); // to handle in NumericOf2Args.calc
    }
  } // Multiply
  
  public static class Divide extends Multiplication {
    @Override
    public String shortName() { return "/"; }

    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      Double d = ((INumeric) args[0]).getDouble(abscissa) / 
        ((INumeric) args[1]).getDouble(abscissa);
      if (Double.isInfinite(d))
        throw new ArithmeticException();
      return d;
    }

    @Override
    public int getInt(IAbscissa abscissa) throws EFuncIsNull {
      return (int) getDouble(abscissa);
    }
  } // Divide
  
  public static class Remainder extends Multiplication {
    @Override
    public String shortName() { return "%"; }

    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      return getInt(abscissa);
    }

    @Override
    public int getInt(IAbscissa abscissa) throws EFuncIsNull {
      return ((INumeric) args[0]).getInt(abscissa) % 
        ((INumeric) args[1]).getInt(abscissa);
    }
  } // Remainder
  
  public static class Power extends Arithmetic {
    @Override
    public String shortName() { return "^"; }
    
    @Override
    public Priority priority() { return Priority.ivpTerm; }

    @Override
    public double getDouble(IAbscissa abscissa) throws EFuncIsNull {
      return Math.pow(((INumeric) args[0]).getDouble(abscissa), 
          ((INumeric) args[1]).getDouble(abscissa));
    }

    @Override
    public int getInt(IAbscissa abscissa) throws EFuncIsNull {
      long i64 = (long) getDouble(abscissa);
      if ((i64 >= java.lang.Integer.MIN_VALUE) && 
          (i64 <= java.lang.Integer.MAX_VALUE) )
        return (int) i64;
      throw new EivIntOverflow(); // to handle in NumericOf2Args.calc
    }
  } // Power
  
  public static void registerFunctions(Registrator r) {
    r.register(Abs.class);
  }
} // MathFunc
