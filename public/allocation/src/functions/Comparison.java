package functions;

public abstract class Comparison extends BoolFunc.BooleanOf2Args {

  Comparison() { super(); } // no-arg constructor is necessary

  Comparison(IFunction x, IFunction y) { super(x, y); }
  
  @Override
  public Priority priority() { return Priority.ivpComp; }

  @Override
  public Dali.Type[] argTypes() {
    Dali.Type[] result = { null, null }; 
    return result;
  }
  
  @Override
  public Category category() { return Category.ivfcComparison; } 
  
  public static class Equal extends Comparison {
    Equal() { super(); } // no-arg constructor is necessary
    
    public Equal(IFunction x, IFunction y) { super(x, y); }
    
    @Override
    public String shortName() { return "="; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return args[0].calc(abscissa).compareTo(args[1].calc(abscissa)) == 0;
    }
  }

  public static class NotEqual extends Comparison {
    NotEqual() {  };
    public NotEqual(IFunction x, IFunction y) { super(x, y); }

    @Override
    public String shortName() { return "!="; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return args[0].calc(abscissa).compareTo(args[1].calc(abscissa)) != 0;
    }
  }
  
  public static class Greater extends Comparison {
    Greater() { super(); } // no-arg constructor is necessary
    public Greater(IFunction x, IFunction y) { }

    @Override
    public String shortName() { return ">"; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return args[0].calc(abscissa).compareTo(args[1].calc(abscissa)) > 0;
    }
  }
  
  public static class GreaterOrEqual extends Comparison {
    GreaterOrEqual() { super(); } // no-arg constructor is necessary
    public GreaterOrEqual(IFunction x, IFunction y) { super(x, y); }

    @Override
    public String shortName() { return ">="; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return args[0].calc(abscissa).compareTo(args[1].calc(abscissa)) >= 0;
    }
  }
  
  public static class Less extends Comparison {
    Less() { super(); } // no-arg constructor is necessary
    public Less(IFunction x, IFunction y) { }

    @Override
    public String shortName() { return "<"; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return args[0].calc(abscissa).compareTo(args[1].calc(abscissa)) < 0;
    }
  }
  
  public static class LessOrEqual extends Comparison {
    LessOrEqual() { super(); } // no-arg constructor is necessary
    public LessOrEqual(IFunction x, IFunction y) { }

    @Override
    public String shortName() { return "<="; }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return args[0].calc(abscissa).compareTo(args[1].calc(abscissa)) <= 0;
    }
  }
  
  public static class Like extends Comparison {
    Like() { super(); } // no-arg constructor is necessary
    
    public Like(IFunction text, IFunction pattern, BoolFunc caseSensitive) { 
      setArgs(text, pattern, caseSensitive);
    }
    
    public Like(IFunction text, IFunction pattern, final boolean bCaseSensitive) { 
      this(text, pattern, new BoolFunc.Const(bCaseSensitive)); 
    }
    
    @Override
    public String shortName() { return "Like"; }
    
    @Override
    public Priority priority() { return Priority.ivpFunction; }

    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { null, null, Dali.Type.sftBoolean }; 
      return result;
    }
    
    @Override
    protected IFunction createDefaultArg(final int i) {
      return i == 2 ? new BoolFunc.Const(false) : null;
    }
    
    private boolean isCaseSensitive(IAbscissa abscissa) throws EFuncIsNull {
      return ((BoolFunc) args[2]).getBoolean(abscissa);
    }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      Dali.Variant v1 = args[1].calc(abscissa);
      if ( v1.isNull() )
        throw new EFuncIsNull();
      
      String s0 = args[0].calc(abscissa).toString();
      String s1 = v1.toString();
      if ( ! isCaseSensitive(abscissa) ) {
        s0 = s0.toUpperCase();
        s1 = s1.toUpperCase();
      }
      
      for ( int iPos = 0; iPos < s1.length(); iPos++ )
        if ( s1.charAt(iPos) == '?' ) {
          if ( iPos >= s0.length() )
            return false;
          s1 = s1.substring(0, iPos - 1) + s0.charAt(iPos) + s1.substring(iPos + 1); 
        }
      
//      s1 = s1.replaceAll("**", "*");
      int iPos = s1.indexOf('*');
      while ( (iPos >= 0) && (iPos < s0.length()) ) {
        if (! s0.substring(0, iPos).equals(s1.substring(0, iPos)) )
          return false;
        s0 = s0.substring(iPos);
        s1 = s1.substring(iPos);
        if ( s1.equals("*") )
          return true;
        s1 = s1.substring(1);
        iPos = s1.indexOf('*');
        if ( iPos < 0 ) {
          if ( s1.equals(s0.substring(s0.length() - s1.length() - 1)) ) 
            return true;
        } else {
          iPos = s1.substring(0, iPos - 1).indexOf(s0);
          if ( iPos < 0 )
            return false;
          else
            s0 = s0.substring(iPos);
        }
        iPos = s1.indexOf('*');
      }
      return s0.equals(s1);
    }
  } // Like

  public static class NotLike extends Like {
    NotLike() { super(); } // no-arg constructor is necessary
    
    public NotLike(IFunction text, IFunction pattern, BoolFunc caseSensitive) { 
      super(text, pattern, caseSensitive);
    }
    
    public NotLike(IFunction text, IFunction pattern, final boolean bCaseSensitive) { 
      super(text, pattern, bCaseSensitive); 
    }
    
    @Override
    public String shortName() { return "NotLike"; }
    
    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return ! super.getBoolean(abscissa);
    }
  } // NotLike  
  
  public static class Contains extends Comparison {
    Contains() { super(); } // no-arg constructor is necessary
    
    public Contains(IFunction text, IFunction substr, BoolFunc caseSensitive) { 
      setArgs(text, substr, caseSensitive);
    }
    
    public Contains(IFunction text, IFunction substr, final boolean bCaseSensitive) { 
      this(text, substr, new BoolFunc.Const(bCaseSensitive)); 
    }
    
    @Override
    public String shortName() { return "Contains"; }
    
    @Override
    public Priority priority() { return Priority.ivpFunction; }

    @Override
    public Category category() { return Category.ivfcString; } 
    
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { null, null, Dali.Type.sftBoolean }; 
      return result;
    }
    
    @Override
    protected IFunction createDefaultArg(final int i) {
      return i == 2 ? new BoolFunc.Const(false) : null;
    }
    
    private boolean isCaseSensitive(IAbscissa abscissa) throws EFuncIsNull {
      return ((BoolFunc) args[2]).getBoolean(abscissa);
    }

    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      Dali.Variant v1 = args[1].calc(abscissa);
      if ( v1.isNull() )
        throw new EFuncIsNull();
      
      String s0 = args[0].calc(abscissa).toString();
      String s1 = args[1].calc(abscissa).toString();
      if ( ! isCaseSensitive(abscissa) ) {
        s0 = s0.toUpperCase();
        s1 = s1.toUpperCase();
      }
      
      return s0.contains(s1);
    }
  } // Contains

  public static class NotContains extends Contains {
    NotContains() { super(); } // no-arg constructor is necessary
    
    public NotContains(IFunction text, IFunction substr, BoolFunc caseSensitive) { 
      super(text, substr, caseSensitive);
    }
    
    public NotContains(IFunction text, IFunction substr, final boolean bCaseSensitive) { 
      super(text, substr, bCaseSensitive); 
    }
    
    @Override
    public String shortName() { return "NotContains"; }
    
    @Override
    public boolean getBoolean(IAbscissa abscissa) throws EFuncIsNull {
      return ! super.getBoolean(abscissa);
    }
  } // NotContains  
  
  public static void registerFunctions(Registrator r) {
    /* Important: only classes with constructors without arguments can be 
       registered that way! Otherwise newInstance() fires InstantiationException.
    */   
    r.register(Like.class);
    r.register(NotLike.class);
    r.register(Contains.class);
    r.register(NotContains.class);
  }
} // Comparison
