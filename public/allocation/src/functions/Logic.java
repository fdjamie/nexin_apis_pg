package functions;

public abstract class Logic extends Function {

  @Override
  public Category category() { return Category.ivfcLogical; } 
  
  public static class If extends Logic {
    
    If() { super(); } // no-arg constructor is necessary
    
    public If(final IBoolean test, IFunction value_if_true, 
        IFunction value_if_false) { 
      setArgs(test, value_if_true, value_if_false); 
    }

    @Override
    public String shortName() { return "If"; }

    @Override
    public Dali.Variant calc(IAbscissa abscissa) throws EFuncIsNull {
      return ((IBoolean) args[0]).getBoolean(abscissa) ? 
          args[1].calc(abscissa) : args[2].calc(abscissa);
    }

    @Override
    public Priority priority() { return IFunction.Priority.ivpFunction; }

    @Override
    public Dali.Type type() { 
      return args.length > 2 ? 
          Dali.Type.superpose(args[1].type(), args[2].type()) : null; 
    }     
        
    @Override
    public Dali.Type[] argTypes() {
      Dali.Type[] result = { Dali.Type.sftBoolean, null, null }; 
      return result;
    }
  }

  public static void registerFunctions(Registrator r) {
    r.register(If.class);
  }
  
} // Logic
