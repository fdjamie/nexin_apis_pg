package util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LogManager {

	static public String 
	APP_LOGGER = "APP_LOGGER";

	static final String 
	DEFAULT_LOG_FILE = System.getProperty("user.dir") + File.separator + "Bascion" + ".log";

	static public boolean SHOW_TIMESTAMP = true;

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	static public String formatDate(final long millis) {
		if (SHOW_TIMESTAMP) {
			DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.DEFAULT);
			return df.format(new Date(millis));
		}
		return "";
	}

	static String formatRecord(LogRecord record) {
		if ( ! record.getLoggerName().equals(APP_LOGGER))
			return ""; // suppress messages from 3-rd parties
		if (record.getMessage().equals("\n"))
			return record.getMessage(); 
		Throwable t = record.getThrown();
		return formatDate(record.getMillis()) + " " + 
		(record.getLevel().intValue() >= Level.WARNING.intValue() ? record.getLevel().getName() : (SHOW_TIMESTAMP ? "\t" : "")) + 
		" " + record.getMessage() + LINE_SEPARATOR +
		(t == null ? "" : t.getMessage() + LINE_SEPARATOR);
	}

	static public void setup() {
		LogManager.setup(LogManager.DEFAULT_LOG_FILE);
	}


	static public void setup(String logFileName) {
		try {

			// Get the global logger to configure it
			Logger logger = Logger.getLogger("");
			logger.setLevel(Level.ALL);

			// setup console logger:
			Handler[] handlers = logger.getHandlers();
			if (handlers.length > 0) {
				handlers[0].setLevel(Level.INFO); // console output for info, warning and severe
				// Create txt Formatter
				SimpleFormatter formatterTxt = new SimpleFormatter() {
					@Override
					public String format(LogRecord record) { 
						return formatRecord(record);
					}
				};
				handlers[0].setFormatter(formatterTxt);
			}

			FileHandler fileTxt;
			try {
				fileTxt = new FileHandler(logFileName, true);
				fileTxt.setLevel(Level.ALL); // file output for fine, info, warning and severe
			} catch (Exception e) {
				logger.severe(e.toString());
				fileTxt = new FileHandler(DEFAULT_LOG_FILE, false);
				logger.warning("Log file: " + new File(DEFAULT_LOG_FILE).getAbsolutePath());
			}			

			// Create txt Formatter
			SimpleFormatter formatterTxt = new SimpleFormatter() {
				@Override
				public String format(LogRecord record) {
					return formatRecord(record);
				}
			};
			fileTxt.setFormatter(formatterTxt);
			logger.addHandler(fileTxt);

			LogStatisticHandler logStatisticHandler = new LogStatisticHandler();
			logger.addHandler(logStatisticHandler);
			
			final String BLANKS = LINE_SEPARATOR + "                       \t ";
			logger = Logger.getLogger(APP_LOGGER);
			logger.log(Level.INFO, 
					"================================================================================" +
							BLANKS + getJarInfo() +
							BLANKS + "\t" + "MD5 check sum " + getMD5() +
							BLANKS + "Java " + System.getProperty("java.version") +
							BLANKS + "Log file: " + new File(logFileName).getAbsolutePath());
		} catch (IOException e) {
			System.err.println("Problems with creating log files");
			e.printStackTrace();
		}
	}

	// this function works properly only when running from jar file
	private static String getMD5() {
		try {
			File jar = getJarFile();
			return MD5Checksum.getMD5Checksum(jar.getAbsolutePath());
		} catch (Exception e) {
			return "";
		}
	}

	private static File getJarFile() throws URISyntaxException {
		return new java.io.File(LogManager.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
	}

	// this function works properly only when running from jar file
	private static String getJarInfo() {
		try {
			File jar = getJarFile();
			SimpleDateFormat sdf = new SimpleDateFormat();
			return jar.getName() + ": " + sdf.format(jar.lastModified()) + " size:" + String.valueOf(jar.length());
		} catch (Exception e) {
			return "";
		}
	}

	public static String getStackTraceAsString(Throwable e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();	
	}

	static Map<Integer/* Level*/, Integer/*count*/> logMsgsByLevel = new TreeMap<Integer, Integer>();
	
	static class LogStatisticHandler extends java.util.logging.Handler {

		LogStatisticHandler() {
			super();
			setLevel(Level.FINEST);
		}

		@Override
		public void flush() {
		}

		@Override
		public void publish(LogRecord record) {
			if (isLoggable(record)) {
				Integer level = record.getLevel().intValue();
				Integer count = logMsgsByLevel.get(level); 
				logMsgsByLevel.put(level, count == null ? 1 : (count + 1));
			}
		}

		@Override
		public void close() throws SecurityException {
		}

	}

	public static int getLogCountByLevel(Level level) {
		Integer count = logMsgsByLevel.get(level.intValue());
		return  count == null ? 0 : count;
	}

}
