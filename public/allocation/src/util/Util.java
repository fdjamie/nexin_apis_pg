package util;

public class Util {

	public static String changeFileExtension (String source, String newExtension)
	{
		int pos = source.indexOf('.');
		return pos > 0 ? (source.substring(0, pos + 1) + newExtension) : (source + "." + newExtension);
	}

}
