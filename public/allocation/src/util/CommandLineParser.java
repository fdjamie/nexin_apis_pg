package util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("serial")
public class CommandLineParser extends TreeMap<String, String>{
	
	List<String> positionalArgs = new ArrayList<String>();
	
	public CommandLineParser(String[] args) {
		super(String.CASE_INSENSITIVE_ORDER);
		int pos;
		for (String arg: args) {
			pos = arg.indexOf('=');
			if (pos > 0)
				put(arg.substring(0, pos), arg.substring(pos+1));
			else
				positionalArgs.add(arg);
		}
	}
	
	public String get(String key, String defaultValue) {
		String result = get(key);
		return result == null ? defaultValue : result;
	}
	
	public String get(String key, int defaultPosition) {
		String result = get(key);
		return result == null ? (defaultPosition < positionalArgs.size() ? positionalArgs.get(defaultPosition) : null) : result;
	}

	public void log(final Level level, Logger log) {
		for (String arg: positionalArgs)
			log.log(level, arg);
		for (Map.Entry<String, String> me: entrySet())
			log.log(level, me.getKey() + '=' + (me.getKey().equalsIgnoreCase("password") ? "..." : me.getValue()));
	}
	
	@Override
    public boolean containsKey(Object key) {
		return super.containsKey(key) || positionalArgs.contains(key);
	}
	
}
