package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Connector {
	
	 @SuppressWarnings("serial")
	public class ConnectorException extends SQLException {

		public ConnectorException(String msg) {
			super(msg);
		}
		 
	 }
	
	Connection connection;
	
	private static final Logger log = Logger.getLogger( LogManager.APP_LOGGER );	
	
	public Connection getConnection() { return connection ; }
	
	public Connector(final String connectionString) throws ClassNotFoundException, SQLException {
		connection = connect(connectionString);
		log.log(Level.INFO, "Connected to database " + getConnectionStringExt() + ".  " + (connection.getAutoCommit() ? "Autocommit": "Explicit commit"));
	}
	
	public Connector(final String connectionString, final String user, final String pwd) throws ClassNotFoundException, SQLException {
		connection = connect(connectionString, user, pwd);
		log.log(Level.INFO, "Connected to database " + getConnectionStringExt());
	}
	
	static public Connection connect(final String connectionString) throws ClassNotFoundException, SQLException {
		setDriver(connectionString);

		// establish a connection:
		return DriverManager.getConnection(connectionString);
	}
	
	static public Connection connect(final String connectionString, final String user, final String pwd) throws ClassNotFoundException, SQLException {
		setDriver(connectionString);

		// establish a connection:
		return DriverManager.getConnection(connectionString, user, pwd);
	}
	
	private static void setDriver(String connectionString) throws ClassNotFoundException {
		if (connectionString.contains("jtds")) // Connection via jTDS:
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
		else if (connectionString.contains("redshift")) // JDBC RedShift connection:
			Class.forName("com.amazon.redshift.jdbc41.Driver"); 		
		else if (connectionString.contains("postgresql")) // JDBC PostgreSQL connection:
			Class.forName("org.postgresql.Driver"); 		
		else if (connectionString.contains("mysql")) // JDBC MySQL connection:
			Class.forName("com.mysql.jdbc.Driver"); 		
		else if (connectionString.contains("sqlserver")) // MS SQL Server connection via JDBC:
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		// JDBC Type 2: Program files\IBM\SQLLIB\java\db2jcc  must be in class path
		// JDBC Type 4: Program files\IBM\SQLLIB\java\db2jcc4 must be in class path
		else if (connectionString.contains("db2")) // DB2 connection:
			Class.forName("com.ibm.db2.jcc.DB2Driver");
	}

	public String getDatabaseName() throws SQLException { 
		java.sql.DatabaseMetaData md = connection.getMetaData(); 
		String connStr = md.getURL();
		int pos = connStr.indexOf("pass");
		if (pos > 0)
			connStr = connStr.substring(0, pos) + "...";
		return md.getDatabaseProductName() + ": " + connStr + " via " + md.getDriverName();
	}

	public void close() throws SQLException {
		connection.close();
	}

	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return connection.prepareStatement(sql);
	}

	public PreparedStatement prepareStatementGenerateKeys(String sql) throws SQLException {
		return connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
	}
	
	public int executeAndGetGeneratedKey(PreparedStatement stmt) throws SQLException, ConnectorException {
		stmt.executeUpdate();
		ResultSet rs = stmt.getGeneratedKeys();
		if ( rs.next() ) 
			return rs.getInt(1);
		throw new ConnectorException("Could not get id for a new record");
	}

	public Statement createStatement() throws SQLException {
		return connection.createStatement();
	}

	public String getConnectionString() throws SQLException {
		return connection.getMetaData().getURL();
	}

	public String getConnectionStringExt() throws SQLException { 
		java.sql.DatabaseMetaData md = connection.getMetaData(); 
		String connStr = md.getURL();
		int pos = connStr.indexOf("pass");
		if (pos > 0)
			connStr = connStr.substring(0, pos) + "...";
		return md.getDatabaseProductName() + ": " + connStr + " via " + md.getDriverName();
	}

	/**
	 * @param args
	 * args[0] - SQL DB connection string
	 */
	public static void main(String[] args) {
		try {
			if (args.length < 1)
				System.exit(1);
				
			Connector connector = new Connector(args[0]);
			try {
				System.out.print(connector.getDatabaseName());
			} finally {
				connector.close();				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public static void setIntParam(PreparedStatement stmt, final int parameterIndex, Integer value) throws SQLException {
		if (value == null)
			stmt.setNull(parameterIndex, java.sql.Types.INTEGER);
		else
			stmt.setInt(parameterIndex, value);
	}

	public static void setIntParam(PreparedStatement stmt, final int parameterIndex, String value) throws SQLException {
		if (value == null)
			stmt.setNull(parameterIndex, java.sql.Types.INTEGER);
		else
			stmt.setInt(parameterIndex, Integer.parseInt(value));
	}

	public static void setDoubleParam(PreparedStatement stmt, final int parameterIndex, Double value) throws SQLException {
		if (value == null)
			stmt.setNull(parameterIndex, java.sql.Types.DOUBLE);
		else
			stmt.setDouble(parameterIndex, value);
	}

	public static void setDoubleParam(PreparedStatement stmt, final int parameterIndex, String value) throws SQLException {
		if (value == null)
			stmt.setNull(parameterIndex, java.sql.Types.DOUBLE);
		else
			stmt.setDouble(parameterIndex, Double.parseDouble(value));
	}

	public static void setTimestampParam(PreparedStatement stmt, final int i, Timestamp ts) throws SQLException {
		if (ts == null)
			stmt.setNull(i,  java.sql.Types.TIMESTAMP);
		else
			stmt.setTimestamp(i, ts);
	}

	public static void setTimestampParam(PreparedStatement stmt, final int i, Long millis) throws SQLException {
		setTimestampParam(stmt, i, millis == null ? null : new Timestamp(millis));
	}

	public static void setStringParam(PreparedStatement stmt, final int i, String str) throws SQLException {
		if (str == null)
			stmt.setNull(i,  java.sql.Types.VARCHAR);
		else
			stmt.setString(i, str);
	}

	public static void setNullParameters(PreparedStatement stmt) throws SQLException {
		ParameterMetaData md = stmt.getParameterMetaData();
		for( int i = 1; i <= md.getParameterCount(); i++)
			stmt.setNull(i, md.getParameterType(i));
	}

	public String buildSelectQuery(String table1, String field1, String table2, String field2) throws SQLException {
		return "SELECT " + table1 + "." + field1 + ", " + table2 + "." + field2 + " FROM " + table1 + getJoinClause(table1, table2);
	}

	@SuppressWarnings("serial")
	public class SQLConnectorException extends SQLException {

		public SQLConnectorException(String msg) {
			super(msg);
		}
		
	}
	
	private String getJoinClause(String table1, String table2) throws SQLException {
		if (table1.equalsIgnoreCase(table2))
			return "";
		
		PreparedStatement stmtFindNto1or1toNreference = prepareStatement(
					"SELECT TABLE_NAME,COLUMN_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE " +
					"WHERE (REFERENCED_TABLE_NAME = ? AND TABLE_NAME = ?) OR ((TABLE_NAME = ? AND REFERENCED_TABLE_NAME = ?))");

		stmtFindNto1or1toNreference.setString(1, table1);
		stmtFindNto1or1toNreference.setString(2, table2);
		stmtFindNto1or1toNreference.setString(3, table1);
		stmtFindNto1or1toNreference.setString(4, table2);
		ResultSet rs = stmtFindNto1or1toNreference.executeQuery();
		try {
			if (rs.next()) {
				String result = " JOIN " + table2 + " ON " + rs.getString(1) + "." + rs.getString(2) + " = " + rs.getString(3) + "." + rs.getString(4);
				if (rs.next())
					throw new SQLConnectorException("Ambiquous SQL JOIN conditions for tables " + table1 + " and " + table2);
				return result;
			}
		} finally {
			rs.close();
		}

		PreparedStatement stmtFindSharedNto1reference = prepareStatement(
					"SELECT fk1.TABLE_NAME, fk1.COLUMN_NAME, fk2.TABLE_NAME, fk2.COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE fk1 " +
							"JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE fk2 ON fk2.REFERENCED_TABLE_NAME = fk1.REFERENCED_TABLE_NAME AND fk2.REFERENCED_COLUMN_NAME = fk1.REFERENCED_COLUMN_NAME " +
					"WHERE fk1.TABLE_NAME = ? AND fk2.TABLE_NAME = ?");
		stmtFindSharedNto1reference.setString(1, table1);
		stmtFindSharedNto1reference.setString(2, table2);
		rs = stmtFindSharedNto1reference.executeQuery();
		try {
			if (rs.next()) {
				String result = " JOIN " + table2 + " ON " + rs.getString(1) + "." + rs.getString(2) + " = " + rs.getString(3) + "." + rs.getString(4);
				if (rs.next())
					throw new SQLConnectorException("Ambiquous SQL JOIN conditions for tables " + table1 + " and " + table2);
				return result;
			}
		} finally {
			rs.close();
		}

		PreparedStatement stmtFindNtoNreference = prepareStatement(
					"SELECT fk1.TABLE_NAME, fk1.COLUMN_NAME, fk1.REFERENCED_TABLE_NAME, fk1.REFERENCED_COLUMN_NAME, " +
							"fk2.TABLE_NAME, fk2.COLUMN_NAME, fk2.REFERENCED_TABLE_NAME, fk2.REFERENCED_COLUMN_NAME " + 
							"FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE fk1 " +
							"JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE fk2 ON fk2.TABLE_NAME = fk1.TABLE_NAME " +
					"WHERE fk1.REFERENCED_TABLE_NAME = ? AND fk2.REFERENCED_TABLE_NAME = ?");
		stmtFindNtoNreference.setString(1, table1);
		stmtFindNtoNreference.setString(2, table2);
		rs = stmtFindNtoNreference.executeQuery();
		try {
			if (rs.next()) {
				String result = " JOIN " + rs.getString(1) + " ON " + rs.getString(1) + "." + rs.getString(2) + " = " + rs.getString(3) + "." + rs.getString(4) + 
						" JOIN " + table2 + " ON " + rs.getString(5) + "." + rs.getString(6) + " = " + rs.getString(7) + "." + rs.getString(8);
				if (rs.next())
					throw new SQLConnectorException("Ambiquous SQL JOIN conditions for tables " + table1 + " and " + table2);
				return result;
			}
		} finally {
			rs.close();
		}
		// try the same in opposite order:
		stmtFindNtoNreference.setString(1, table2);
		stmtFindNtoNreference.setString(2, table1);
		rs = stmtFindNtoNreference.executeQuery();
		try {
			if (rs.next()) {
				String result = " JOIN " + rs.getString(1) + " ON " + rs.getString(1) + "." + rs.getString(2) + " = " + rs.getString(3) + "." + rs.getString(4) + 
						" JOIN " + table2 + " ON " + rs.getString(5) + "." + rs.getString(6) + " = " + rs.getString(7) + "." + rs.getString(8);
				if (rs.next())
					throw new SQLConnectorException("Ambiquous SQL JOIN conditions for tables " + table1 + " and " + table2);
				return result;
			}
		} finally {
			rs.close();
		}
		
		PreparedStatement stmtFindTransitiveNto1reference = prepareStatement(
					"SELECT fk1.TABLE_NAME, fk1.COLUMN_NAME, fk1.REFERENCED_TABLE_NAME, fk1.REFERENCED_COLUMN_NAME, " +
					"fk2.TABLE_NAME, fk2.COLUMN_NAME, fk2.REFERENCED_TABLE_NAME, fk2.REFERENCED_COLUMN_NAME " + 
					"FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE fk1 " +
					"JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE fk2 ON fk2.TABLE_NAME = fk1.REFERENCED_TABLE_NAME " +
					"WHERE fk1.TABLE_NAME = ? AND fk2.REFERENCED_TABLE_NAME = ?");
		stmtFindTransitiveNto1reference.setString(1, table1);
		stmtFindTransitiveNto1reference.setString(2, table2);
		rs = stmtFindTransitiveNto1reference.executeQuery();
		try {
			if (rs.next()) {
				String result = " JOIN " + rs.getString(3) + " ON " + rs.getString(1) + "." + rs.getString(2) + " = " + rs.getString(3) + "." + rs.getString(4) + 
						" JOIN " + table2 + " ON " + rs.getString(5) + "." + rs.getString(6) + " = " + rs.getString(7) + "." + rs.getString(8);
				if (rs.next())
					throw new SQLConnectorException("Ambiquous SQL JOIN conditions for tables " + table1 + " and " + table2);
				return result;
			}
		} finally {
			rs.close();
		}
		
		throw new SQLException("Could not find a SQL JOIN condition for tables " + table1 + " and " + table2);
	}

}
