@echo off

REM 
REM	Parameters:
REM connection=<sql connection string> previousSession=<previous session id>
REM
REM	connection=<sql connection string>
REM Parameter previousSession=.. is used to re-run an optimization with automated phantoms creation to compolete next level
REM	Example:
REM	java -cp OptimalAssignments.jar nexin.PhantomManager "connection=jdbc:postgresql://109.72.212.33:5432/postgres?currentSchema=nexin_seprate&user=postgres&password=1amK1ng" previousSession=21
REM
REM Exit codes:
REM		3 if execution crashed;
REM		2 if there were severe errors;
REM		1 is there were warnings;
REM		0 if execution was successful.
REM

java -cp OptimalAssignments.jar nexin.PhantomManager "connection=jdbc:postgresql://localhost:5432/postgres?currentSchema=staff_allocation_schema&user=postgres&password=admin123" previousSession=1
REM java -cp OptimalAssignments.jar nexin.PhantomManager "connection=jdbc:postgresql://68.66.248.8:5432/sitedire_nexin?currentSchema=staff_allocation_schema&user=sitedire_nexin_user&password=!!tiptop123!!X" previousSession=21
set exitcode=%ERRORLEVEL%
call echo exit code %exitcode%

