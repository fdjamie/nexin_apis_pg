@echo off

REM 
REM	Parameters:
REM connection=<sql connection string> { (week=<week number> day=<day number>) | assignmentChange=<assignment change id> }
REM
REM	connection=<sql connection string>
REM Parameters week=... and day=... are used to run optimization for specified week day first time 
REM Parameter aassignmentChange=.. is used to re-run an optimization with applying a manually made change
REM	Examples:
REM	java -jar OptimalAssignments.jar "connection=jdbc:postgresql://109.72.212.33:5432/postgres?currentSchema=nexin_seprate&user=postgres&password=1amK1ng" week=1 day=2
REM	java -jar OptimalAssignments.jar "connection=jdbc:postgresql://109.72.212.33:5432/postgres?currentSchema=nexin_seprate&user=postgres&password=1amK1ng" assignmentChange=4
REM
REM Exit codes:
REM		3 if execution crashed;
REM		2 if there were severe errors;
REM		1 is there were warnings;
REM		0 if execution was successful.
REM

java -jar OptimalAssignments.jar "connection=jdbc:postgresql://localhost:5432/%3?currentSchema=%4&user=%5&password=%6" week=%1 day=%2
REM java -jar OptimalAssignments.jar "connection=jdbc:postgresql://68.66.248.8:5432/sitedire_nexin?currentSchema=staff_allocation_schema&user=sitedire_nexin_user&password=!!tiptop123!!X" week=1 day=2
set exitcode=%ERRORLEVEL%
call echo %exitcode%

