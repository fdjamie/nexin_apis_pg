/* 
  This script contains DDL statements to create ouput tables for Bascion Assignment Manager application.
  Author: Evgeny Lyulkov     evgeny.lyulkov@gmail.com
  Date: Aug,11 2018
*/
-- DROP TABLE IF EXISTS staff_allocation_schema.Assignment_Task CASCADE;
-- DELETE FROM staff_allocation_schema.Assignment_Task;

CREATE TABLE staff_allocation_schema.Assignment_Task (
  Id Serial CONSTRAINT Assignment_Task_PK PRIMARY KEY,
  week INT NOT NULL,
  day INT NOT NULL,
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT Assignment_Task_AK UNIQUE(week, day)
);

--DROP TABLE IF EXISTS staff_allocation_schema.Assignment_Change CASCADE;

CREATE TABLE staff_allocation_schema.Assignment_Change (
  -- set of manual assignment changes
  Id Serial CONSTRAINT Assignment_Change_PK PRIMARY KEY,
  assignment_task INT NOT NULL CONSTRAINT Assignment_change_task_FK REFERENCES staff_allocation_schema.Assignment_Task(id) ON DELETE CASCADE,
  sqn_no INT NOT NULL, -- 1,2,3...
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT Assignment_Change_AK UNIQUE(assignment_task, sqn_no)
);

CREATE OR REPLACE FUNCTION staff_allocation_schema.insert_Assignment_Change()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $function$
BEGIN
  IF New.sqn_no IS NULL THEN
    SELECT COALESCE(MAX(sqn_no), 0) + 1 INTO New.sqn_no FROM Assignment_Change ac
      WHERE ac.assignment_task  = NEW.assignment_task;
  END IF;
  RETURN NEW;
END;
$function$;

CREATE TRIGGER Assignment_Change_insert_trgr
    BEFORE INSERT ON staff_allocation_schema.Assignment_Change 
        FOR EACH ROW EXECUTE PROCEDURE staff_allocation_schema.insert_Assignment_Change();	

--DROP TABLE IF EXISTS staff_allocation_schema.Assignment_Session CASCADE;

CREATE TABLE staff_allocation_schema.Assignment_Session (
  Id Serial CONSTRAINT Assignment_Session_PK PRIMARY KEY,
  assignment_task INT NOT NULL CONSTRAINT Assignment_session_task_FK REFERENCES staff_allocation_schema.Assignment_Task(id) ON DELETE CASCADE,
  assignment_change INT CONSTRAINT Assignment_session_change_FK REFERENCES staff_allocation_schema.Assignment_Change(id) ON DELETE CASCADE,
  sqn_no INT NOT NULL, -- 1,2,3...
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  details JSONB,
  CONSTRAINT Assignment_Session_AK UNIQUE(assignment_task, sqn_no)
);

--DROP TABLE IF EXISTS staff_allocation_schema.Assignment;

CREATE TABLE staff_allocation_schema.Assignment (
  Id Serial CONSTRAINT Assignment_PK PRIMARY KEY,
  assignment_session INT NOT NULL CONSTRAINT Assignment_session_FK REFERENCES staff_allocation_schema.Assignment_Session(id) ON DELETE CASCADE,
  transition_time INT NOT NULL CONSTRAINT Assignment_transition_time_FK REFERENCES staff_allocation_schema.tb_transition_time(id) ON DELETE CASCADE,
  service INT NOT NULL CONSTRAINT Assignment_service_FK REFERENCES staff_allocation_schema.tb_service(id),
  staff INT NOT NULL CONSTRAINT Assignment_staff_FK REFERENCES staff_allocation_schema.tb_directorate_staff(id),
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT Assignment_AK UNIQUE(assignment_session, transition_time, staff)
);

--DROP TABLE IF EXISTS staff_allocation_schema.Assignment_Change_Allocation;

CREATE TABLE staff_allocation_schema.Assignment_Change_Allocation (
  -- manual staff to service allocation
  Id Serial CONSTRAINT Assignment_Change_Allocation_PK PRIMARY KEY,
  assignment_change INT NOT NULL CONSTRAINT Assignment_Change_Allocation_Change_FK REFERENCES staff_allocation_schema.Assignment_Change(id) ON DELETE CASCADE,
  transition_time INT NOT NULL CONSTRAINT Assignment_Change_Allocation_transition_time_FK REFERENCES staff_allocation_schema.tb_transition_time(id) ON DELETE CASCADE,
  service INT NOT NULL CONSTRAINT Assignment_Change_Allocation_service_FK REFERENCES staff_allocation_schema.tb_service(id),
  staff INT NOT NULL CONSTRAINT Assignment_Change_Allocation_staff_FK REFERENCES staff_allocation_schema.tb_directorate_staff(id),
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT Assignment_Change_Allocation_AK UNIQUE(assignment_change, transition_time, staff)
);

--DROP TABLE IF EXISTS staff_allocation_schema.Missing_Resource;

CREATE TABLE staff_allocation_schema.Missing_Resource (
  Id Serial CONSTRAINT Missing_Resource_PK PRIMARY KEY,
  assignment_session INT NOT NULL CONSTRAINT Missing_Resource_session_FK REFERENCES staff_allocation_schema.Assignment_Session(id) ON DELETE CASCADE,
  transition_time INT NOT NULL CONSTRAINT Missing_Resource_transition_time_FK REFERENCES staff_allocation_schema.tb_transition_time(id) ON DELETE CASCADE,
  service INT NOT NULL CONSTRAINT Missing_Resource_service_FK REFERENCES staff_allocation_schema.tb_service(id),
  level INT NOT NULL,
  requirement TEXT NOT NULL,
  count INT NOT NULL,
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT Missing_Resource_AK UNIQUE(assignment_session, transition_time, service, level, requirement)
);

--DROP TABLE IF EXISTS staff_allocation_schema.Phantom;

CREATE TABLE staff_allocation_schema.Phantom (
  -- Phantom
  Id Serial CONSTRAINT Phantom_PK PRIMARY KEY,
  assignment_change INT NOT NULL CONSTRAINT Phantom_Change_FK REFERENCES staff_allocation_schema.Assignment_Change(id) ON DELETE CASCADE,
  name TEXT NOT NULL,
  parameters JSONB NOT NULL,
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT Phantom_AK UNIQUE(assignment_change, name)
);

--DROP TABLE IF EXISTS staff_allocation_schema.Phantom_Allocation;

CREATE TABLE staff_allocation_schema.Phantom_Allocation (
  -- manual Phantom allocation
  Id Serial CONSTRAINT Phantom_Allocation_PK PRIMARY KEY,
  phantom INT NOT NULL CONSTRAINT Phantom_Allocation_Phantom_FK REFERENCES staff_allocation_schema.Phantom(id) ON DELETE CASCADE,
  transition_time INT NOT NULL CONSTRAINT Phantom_Allocation_transition_time_FK REFERENCES staff_allocation_schema.tb_transition_time(id) ON DELETE CASCADE,
  service INT NOT NULL CONSTRAINT Phantom_Allocation_service_FK REFERENCES staff_allocation_schema.tb_service(id),
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT Phantom_Allocation_AK UNIQUE(phantom, transition_time)
);

--DROP TABLE IF EXISTS staff_allocation_schema.Phantom_Assignment;

CREATE TABLE staff_allocation_schema.Phantom_Assignment (
  Id Serial CONSTRAINT Phantom_Assignment_PK PRIMARY KEY,
  phantom INT NOT NULL CONSTRAINT Phantom_Allocation_Phantom_FK REFERENCES staff_allocation_schema.Phantom(id) ON DELETE CASCADE,
  transition_time INT NOT NULL CONSTRAINT Phantom_Assignment_transition_time_FK REFERENCES staff_allocation_schema.tb_transition_time(id) ON DELETE CASCADE,
  service INT NOT NULL CONSTRAINT Phantom_Assignment_service_FK REFERENCES staff_allocation_schema.tb_service(id),
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT Phantom_Assignment_AK UNIQUE(phantom, transition_time)
);

------------------------------------------------------------------------------------

-- DROP VIEW IF EXISTS staff_allocation_schema.Most_Recent_Session;
CREATE OR REPLACE VIEW staff_allocation_schema.Most_Recent_Session AS
  WITH Latest_Sessions AS (
    SELECT assignment_task, MAX(sqn_no) sqn_no
    FROM staff_allocation_schema.assignment_session
    GROUP BY assignment_task
  )
  SELECT ass.assignment_task, ass.id assignment_session, ass.sqn_no, assignment_change,       ass.updated_on,       ass.updated_by
    FROM Latest_Sessions ls
    JOIN staff_allocation_schema.assignment_session ass ON ass.assignment_task = ls.assignment_task AND ass.sqn_no = ls.sqn_no;

-- DROP VIEW IF EXISTS staff_allocation_schema.Most_Recent_Change;
CREATE OR REPLACE VIEW staff_allocation_schema.Most_Recent_Change AS
  WITH Latest_Changes AS (
    SELECT assignment_task, MAX(sqn_no) sqn_no
    FROM staff_allocation_schema.assignment_change
    GROUP BY assignment_task
  )
  SELECT ac.assignment_task, ac.id assignment_change, ac.sqn_no, ac.updated_on,       ac.updated_by
    FROM Latest_Changes lc
    JOIN staff_allocation_schema.assignment_change ac ON ac.assignment_task = lc.assignment_task AND ac.sqn_no = lc.sqn_no;

----------------------------------------
CREATE OR REPLACE FUNCTION staff_allocation_schema.set_modified_xx()
RETURNS trigger LANGUAGE plpgsql AS $body$
        BEGIN
            NEW.updated_on = now();
            NEW.updated_by = CURRENT_USER;
            RETURN NEW;
        END;
        $body$
;
        
CREATE TRIGGER Assignment_Session_Update_trgr
  BEFORE UPDATE ON staff_allocation_schema.Assignment_Session
    FOR EACH ROW EXECUTE PROCEDURE staff_allocation_schema.set_modified_xx();
    
CREATE TRIGGER Assignment_Task_Update_trgr
  BEFORE UPDATE ON staff_allocation_schema.Assignment_Task
    FOR EACH ROW EXECUTE PROCEDURE staff_allocation_schema.set_modified_xx();
    
----------------------------------------
/* Assignment change testing
INSERT INTO staff_allocation_schema.assignment_change(assignment_task, sqn_no) Values(1, 1);

SELECT id FROM staff_allocation_schema.assignment_change WHERE assignment_task = 1 AND sqn_no = 1;

INSERT INTO staff_allocation_schema.assignment_change_allocation(assignment_change, transition_time, service, staff)
VALUES(1, 2, 21, 1);

INSERT INTO staff_allocation_schema.Phantom(assignment_change,  name, parameters)
VALUES(1, 'Ph1', '{"role":2}');

INSERT INTO staff_allocation_schema.phantom_allocation(phantom, transition_time, service) VALUES(5, 3, 22);
);

*/

--DROP TABLE staff_allocation_schema.tb_staff_availability;
/*
CREATE TABLE staff_allocation_schema.tb_staff_availability (
  Id Serial CONSTRAINT staff_availability_PK PRIMARY KEY,
  directorate_staff INT NOT NULL CONSTRAINT staff_availability_directorate_staff_FK REFERENCES staff_allocation_schema.tb_directorate_staff(id) ON DELETE CASCADE,
  week INT NOT NULL,
  day INT NOT NULL,
  start TIME NOT NULL,
  stop TIME NOT NULL, 
  updated_on TIMESTAMP DEFAULT now() NOT NULL,
  updated_by VARCHAR DEFAULT CURRENT_USER NOT NULL,
  CONSTRAINT staff_availability_AK UNIQUE(directorate_staff, week, day, start)
);
*/
