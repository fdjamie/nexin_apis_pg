CREATE SCHEMA nexin_seprate;

CREATE TABLE nexin_seprate.service_data
(
  id serial NOT NULL,
  event_time character varying(255) NOT NULL,
  event_date date NOT NULL,
  level character varying(255) NOT NULL,
  service json,
  staff json,
  CONSTRAINT service_data_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE nexin_seprate.service_data
  OWNER TO postgres;
