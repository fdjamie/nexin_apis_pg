/*
  Assignment change iterations
  All examples with WHERE clause contain constants give just for example. Please replace them with actual values!
 */

-- List of assignment tasks
SELECT id,       week,       day,       updated_on,       updated_by 
  FROM staff_allocation_schema.Assignment_Task 
  ORDER BY week, day;
  
-- Assignment task for given week and day
SELECT id,       week,       day,       updated_on,       updated_by 
  FROM staff_allocation_schema.Assignment_Task 
  WHERE week = 1 and day = 2
  ORDER BY id;
  
-- List of sessions for given assignment task id
SELECT id,       assignment_task,       assignment_change,       sqn_no,       details, updated_on,       updated_by
  FROM staff_allocation_schema.assignment_session
  WHERE assignment_task = 1 -- replace with actual value
  ORDER BY sqn_no desc;
  
-- List of assignment tasks with most recent sessions
SELECT mrs.assignment_session,       mrs.assignment_task, at.week, at.day,      mrs.sqn_no,       mrs.assignment_change,       mrs.updated_on,       mrs.updated_by
  FROM staff_allocation_schema.Most_Recent_Session mrs
  JOIN staff_allocation_schema.Assignment_Task at ON at.id = mrs.assignment_task
  ORDER BY mrs.assignment_task;
  
-- Staff and phantom assignments for given assignment session id
WITH MostRecentSession AS (
  SELECT ass.assignment_task, ass.id assignment_session, ass.sqn_no, ass.assignment_change, at.week, at.day 
    FROM staff_allocation_schema.Assignment_Session ass
    JOIN staff_allocation_schema.Assignment_Task at ON at.id = ass.assignment_task
    WHERE ass.id = 20 -- replace with actual value
)
, Staff_and_Phantoms_Assignments AS (
  -- real staff:
  SELECT a.id assignment_id, a.assignment_session,  a.transition_time, a.service, a.staff, ds.name staff_name, NULL is_phantom
    FROM MostRecentSession mrs
    JOIN staff_allocation_schema.assignment a ON mrs.assignment_session = a.assignment_session
    JOIN staff_allocation_schema.tb_directorate_staff ds ON ds.id = a.staff
  UNION
  -- phantoms:
  SELECT a.id assignment_id, mrs.assignment_session,  a.transition_time, a.service, a.phantom, p.name staff_name, 'phantom' is_phantom
    FROM MostRecentSession mrs
    JOIN staff_allocation_schema.assignment_change ac ON ac.id = mrs.assignment_change
    JOIN staff_allocation_schema.phantom p ON p.assignment_change = ac.id
    JOIN staff_allocation_schema.phantom_assignment a ON a.phantom = p.id
)
SELECT a.assignment_id, mrs.assignment_task, mrs.week, mrs.day, a.assignment_session, mrs.sqn_no session_sqn_no,
  a.transition_time, tt.name tt_name, tt.start_Time,
  a.service, s.name service_name,      
  a.staff, a.staff_name, a.is_phantom
  FROM Staff_and_Phantoms_Assignments a
  JOIN MostRecentSession mrs ON TRUE
  JOIN staff_allocation_schema.tb_transition_time tt ON tt.id = a.transition_time
  JOIN staff_allocation_schema.tb_service s ON s.id = a.service
  ORDER BY tt.start_Time, s.name, a.staff_name;
  
-- Staff and phantom assignments for the most recent session of given assignment task id
WITH MostRecentSession AS (
  SELECT mrs.assignment_task, mrs.assignment_session, mrs.sqn_no, mrs.assignment_change, at.week, at.day 
    FROM staff_allocation_schema.Most_Recent_Session mrs
    JOIN staff_allocation_schema.Assignment_Task at ON at.id = mrs.assignment_task
    WHERE mrs.assignment_task = 6 -- replace with actual value
)
, Staff_and_Phantoms_Assignments AS (
  -- real staff:
  SELECT a.id assignment_id, a.assignment_session,  a.transition_time, a.service, a.staff, ds.name staff_name, NULL is_phantom
    FROM MostRecentSession mrs
    JOIN staff_allocation_schema.assignment a ON mrs.assignment_session = a.assignment_session
    JOIN staff_allocation_schema.tb_directorate_staff ds ON ds.id = a.staff
  UNION
  -- phantoms:
  SELECT a.id assignment_id, mrs.assignment_session,  a.transition_time, a.service, a.phantom, p.name staff_name, 'phantom' is_phantom
    FROM MostRecentSession mrs
    JOIN staff_allocation_schema.assignment_change ac ON ac.id = mrs.assignment_change
    JOIN staff_allocation_schema.phantom p ON p.assignment_change = ac.id
    JOIN staff_allocation_schema.phantom_assignment a ON a.phantom = p.id
)
SELECT a.assignment_id, mrs.assignment_task, mrs.week, mrs.day, a.assignment_session, mrs.sqn_no session_sqn_no,
  a.transition_time, tt.name tt_name, tt.start_Time,
  a.service, s.name service_name,      
  a.staff, a.staff_name, a.is_phantom
  FROM Staff_and_Phantoms_Assignments a
  JOIN MostRecentSession mrs ON TRUE
  JOIN staff_allocation_schema.tb_transition_time tt ON tt.id = a.transition_time
  JOIN staff_allocation_schema.tb_service s ON s.id = a.service
  ORDER BY tt.start_Time, s.name, a.staff_name;
  
-- List of missing resources for given assignment session id
SELECT mr.id, ass.assignment_task, at.week, at.day, mr.assignment_session,       
  mr.transition_time, tt.name tt_name, tt.start_Time,
  mr.service,  s.name service_name,       mr.level,       mr.requirement,       mr.count
  FROM staff_allocation_schema.missing_resource mr
  JOIN staff_allocation_schema.Assignment_Session ass ON ass.id = mr.assignment_session
  JOIN staff_allocation_schema.Assignment_Task at ON at.id = ass.assignment_task
  JOIN staff_allocation_schema.tb_transition_time tt ON tt.id = mr.transition_time
  JOIN staff_allocation_schema.tb_service s ON s.id = mr.service
  WHERE mr.assignment_session = 165 -- replace with actual value
  ORDER BY tt.start_Time, mr.level, s.name, mr.requirement;

-- List of missing resources for the most recent session of given assignment task id
SELECT mr.id, mrs.assignment_task, at.week, at.day, mr.assignment_session,       
  mr.transition_time, tt.name tt_name, tt.start_Time,
  mr.service,  s.name service_name,       mr.level,       mr.requirement,       mr.count
  FROM staff_allocation_schema.missing_resource mr
  JOIN staff_allocation_schema.Most_Recent_Session mrs ON mrs.assignment_session = mr.assignment_session
  JOIN staff_allocation_schema.Assignment_Task at ON at.id = mrs.assignment_task
  JOIN staff_allocation_schema.tb_transition_time tt ON tt.id = mr.transition_time
  JOIN staff_allocation_schema.tb_service s ON s.id = mr.service
  WHERE mrs.assignment_task = 1 -- replace with actual value
  ORDER BY tt.start_Time, mr.level, s.name, mr.requirement;
  
-- List of changes for given assignment task id
SELECT ac.id,       ac.assignment_task,       ac.sqn_no,       ac.updated_on,       ac.updated_by
  FROM staff_allocation_schema.assignment_change ac  
  WHERE ac.assignment_task = 1 -- replace with actual value
  ORDER BY ac.sqn_no;

-- List of assignment tasks with most recent changes
SELECT mrc.assignment_change,       mrc.assignment_task, at.week, at.day,      mrc.sqn_no,       mrc.updated_on,       mrc.updated_by
  FROM staff_allocation_schema.Most_Recent_Change mrc
  JOIN staff_allocation_schema.Assignment_Task at ON at.id = mrc.assignment_task
  ORDER BY mrc.assignment_task;
-----------------------
-- List of manual allocations for given assignment change id
SELECT aca.id, ac.assignment_task, at.week, at.day, aca.assignment_change, ac.sqn_no change_sqn_no,
  aca.transition_time, tt.name tt_name, tt.start_Time,
  aca.service, s.name service_name,      
  aca.staff, ds.name staff_name
  FROM staff_allocation_schema.assignment_change_allocation aca
  JOIN staff_allocation_schema.Assignment_Change ac ON ac.id = aca.Assignment_Change
  JOIN staff_allocation_schema.Assignment_Task at ON at.id = ac.assignment_task
  JOIN staff_allocation_schema.tb_transition_time tt ON tt.id = aca.transition_time
  JOIN staff_allocation_schema.tb_service s ON s.id = aca.service
  JOIN staff_allocation_schema.tb_directorate_staff ds ON ds.id = aca.staff
  WHERE aca.Assignment_Change = 1 -- replace with actual value
  ORDER BY tt.start_Time, s.name, ds.name;

-- List of manual allocations for the most recent change of given assignment task id
SELECT aca.id, mrc.assignment_task, at.week, at.day, aca.assignment_change, mrc.sqn_no change_sqn_no,
  aca.transition_time, tt.name tt_name, tt.start_Time,
  aca.service, s.name service_name,      
  aca.staff, ds.name staff_name
  FROM staff_allocation_schema.assignment_change_allocation aca
  JOIN staff_allocation_schema.Most_Recent_Change mrc ON mrc.Assignment_Change = aca.Assignment_Change
  JOIN staff_allocation_schema.Assignment_Task at ON at.id = mrc.assignment_task
  JOIN staff_allocation_schema.tb_transition_time tt ON tt.id = aca.transition_time
  JOIN staff_allocation_schema.tb_service s ON s.id = aca.service
  JOIN staff_allocation_schema.tb_directorate_staff ds ON ds.id = aca.staff
  WHERE mrc.assignment_task = 1 -- replace with actual value
  ORDER BY tt.start_Time, s.name, ds.name;
  
