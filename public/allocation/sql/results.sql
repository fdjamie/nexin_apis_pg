-- Staff
SELECT id,       name,       updated_on,       updated_by
FROM staff_allocation_schema.staff
order by id;

-- Service
SELECT id,       name,       updated_on,       updated_by
FROM staff_allocation_schema.service
order by id;

-- Assignments: all
SELECT service_data, week, day,       
  service, se.name, staff, st.name,      start,       stop
FROM staff_allocation_schema.assignment_session s
JOIN staff_allocation_schema.Assignment a ON a.assignment_session = s.id
JOIN staff_allocation_schema.service se ON se.id = a.service
JOIN staff_allocation_schema.staff st ON st.id = a.staff
ORDER BY week, day, start;

-- Assignments: by given service_data.id
SELECT service_data, week, day,       
  service, se.name, staff, st.name,      start,       stop
FROM staff_allocation_schema.assignment_session s
JOIN staff_allocation_schema.Assignment a ON a.assignment_session = s.id
JOIN staff_allocation_schema.service se ON se.id = a.service
JOIN staff_allocation_schema.staff st ON st.id = a.staff
WHERE service_data = 27       --       REPLACE WITH PROPER DESIRED VALUE !!!!
ORDER BY week, day, start;

-- Assignments: by given staff.id
SELECT service_data, week, day,       
  service, se.name, staff, st.name,      start,       stop
FROM staff_allocation_schema.assignment_session s
JOIN staff_allocation_schema.Assignment a ON a.assignment_session = s.id
JOIN staff_allocation_schema.service se ON se.id = a.service
JOIN staff_allocation_schema.staff st ON st.id = a.staff
WHERE st.id = 1       --       REPLACE WITH PROPER DESIRED VALUE !!!!
ORDER BY week, day, start;

-- Assignments: by given service.id
SELECT service_data, week, day,       
  service, se.name, staff, st.name,      start,       stop
FROM staff_allocation_schema.assignment_session s
JOIN staff_allocation_schema.Assignment a ON a.assignment_session = s.id
JOIN staff_allocation_schema.service se ON se.id = a.service
JOIN staff_allocation_schema.staff st ON st.id = a.staff
WHERE se.id = 1       --       REPLACE WITH PROPER DESIRED VALUE !!!!
ORDER BY week, day, start;

-- Assignments: by given staff.id and service.id
SELECT service_data, week, day,       
  service, se.name, staff, st.name,      start,       stop
FROM staff_allocation_schema.assignment_session s
JOIN staff_allocation_schema.Assignment a ON a.assignment_session = s.id
JOIN staff_allocation_schema.service se ON se.id = a.service
JOIN staff_allocation_schema.staff st ON st.id = a.staff
WHERE st.id = 1       --       REPLACE WITH PROPER DESIRED VALUE !!!!
  AND se.id = 1       --       REPLACE WITH PROPER DESIRED VALUE !!!!
ORDER BY week, day, start;

-- Assignments: by given week
SELECT service_data, week, day,       
  service, se.name, staff, st.name,      start,       stop
FROM staff_allocation_schema.assignment_session s
JOIN staff_allocation_schema.Assignment a ON a.assignment_session = s.id
JOIN staff_allocation_schema.service se ON se.id = a.service
JOIN staff_allocation_schema.staff st ON st.id = a.staff
WHERE week = 1       --       REPLACE WITH PROPER DESIRED VALUE !!!!
ORDER BY week, day, start;

-- Assignments: by given day
SELECT service_data, week, day,       
  service, se.name, staff, st.name,      start,       stop
FROM staff_allocation_schema.assignment_session s
JOIN staff_allocation_schema.Assignment a ON a.assignment_session = s.id
JOIN staff_allocation_schema.service se ON se.id = a.service
JOIN staff_allocation_schema.staff st ON st.id = a.staff
WHERE day = 3       --       REPLACE WITH PROPER DESIRED VALUE !!!!
ORDER BY week, day, start;

-------------------------------------------------------------------------------------
-- Assignment sessions
SELECT id,       service_data,       week,       day,       updated_on,       updated_by
FROM staff_allocation_schema.assignment_session
ORDER BY id;

--  DELETE FROM staff_allocation_schema.assignment_session;

-- Assignments
SELECT id,       assignment_session,       service,       staff,       start,       stop,
       updated_on,       updated_by
FROM staff_allocation_schema.assignment
order by id;

