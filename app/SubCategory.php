<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model {

    use SoftDeletes;

    protected $table = 'tb_sub_category';
    protected $fillable = ['name', 'category_id', 'specialities', 'parent_id'];

    public function setSpecialitiesAttribute($value) {
        $this->attributes['specialities'] = json_encode($value);
    }

    public function getSpecialitiesAttribute($value) {
        return json_decode($value);
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function service() {
        return $this->hasMany('App\Service');
    }

    public function directorate_service() {
        return $this->hasMany('App\DirectorateService');
    }

    public function child_subcategory() {
        return $this->hasMany('App\SubCategory', 'parent_id');
    }

    public function child_recursive() {
        return $this->child_subcategory()->with('child_recursive');
    }

    public function parent_subcategory() {
        return $this->belongsTo('App\SubCategory', 'parent_id');
    }

    public function parent_recursive() {
        return $this->parent_subcategory()->with('parent_recursive');
    }
    
    public function priority() {
        return $this->morphToMany('App\CategoryPriority','tb_prioritizable');
    }

    protected static function boot() {
        parent::boot();
        self::deleting(function (SubCategory $subcategory) {
            foreach ($subcategory->child_recursive as $child) {
                $child->delete();
            }
        });
    }

}
