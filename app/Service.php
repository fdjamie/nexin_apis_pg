<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model {

    use SoftDeletes;

    protected $table = 'tb_service';
    protected $fillable = ['directorate_speciality_id', 'category_id','sub_category_id', 'name', 'contactable', 'min_standards','template','holiday_status'];

    public function setTemplateAttribute($value) {
        $this->attributes['template'] = json_encode($value);
    }

    public function holidays()
    {
        return $this->hasMany('App\Holiday_service','service_id');
    }

    public function getTemplateAttribute($value) {
        return json_decode($value);
    }

    public function directorate_speciality() {
        return $this->belongsTo('App\DirectorateSpeciality');
    }
    
    public function category() {
        return $this->belongsTo('App\Category');
    }
    
    public function sub_category() {
        return $this->belongsTo('App\SubCategory');
    }

    public function shift_type() {
        return $this->hasMany('App\ShiftType');
    }
    
    public function shift_detail() {
        return $this->hasMany('App\ShiftDetail');
    }
    
    public function requirement() {
        return $this->hasMany('App\ServiceRequirement','service_id');
    }
    
    public function priority() {
        return $this->morphToMany('App\CategoryPriority','tb_prioritizable');
    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (Service $service) {

            foreach ($service->requirement as $requirement) {
                $requirement->delete();
            }
        });
        self::restoring(function (Service $service) {
            $requirements = $service->requirement;
            foreach ($requirements as $requirement) {
                $requirement->restore();
            }
        });
    }
    public function directorateStaff() {
        return $this->belongsToMany('\App\DirectorateStaff' , 'tb_directorate_staff_service')->withPivot('directorate_staff_id', 'service_id');
    }

}
