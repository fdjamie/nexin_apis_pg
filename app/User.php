<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'tb_user';
    protected $fillable = [
        'name', 'email', 'password', 'api_token', "trust_id","phone",'signup'
    ];
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];
    
    /*public function trust(){
        return $this->belongsTo('App\Trust');
    }*/

    public function trusts(){
        return $this->belongsToMany('App\Trust','tb_user_trust')->withPivot('id');
    }

    public function userTrusts(){
        return $this->hasMany('App\UserTrust','user_id');
    }

    public function roles(){
        return $this->belongsToMany('App\Roles','tb_user_roles','user_id','role_id');
    }


}
