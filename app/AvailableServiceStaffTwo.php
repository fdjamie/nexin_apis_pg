<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AvailableServiceStaffTwo extends Model {

    use SoftDeletes;

    protected $connection = 'pgsql2';
    protected $table = 'tb_available_services_staff';
    protected $fillable = ['directorate_staff_id', 'service_id', 'transition_time_id', 'date', 'day', 'week', 'start', 'stop'];
    
    public function directorate_staff(){
        return $this->belongsTo('App\DirectorateStaffTwo');
    }
    
    public function service(){
        return $this->belongsTo('App\ServiceTwo');
    }
    
    public function transition_time(){
        return $this->belongsTo('App\TransitionTimeTwo');
    }

}
