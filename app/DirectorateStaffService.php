<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DirectorateStaffService extends Model {

    use SoftDeletes;

    protected $table = 'tb_directorate_staff_service';
//    protected $fillable = ['directorate_staff_id', 'service_id'];

    protected $fillable = ['directorate_staff_id', 'service_id', 'start', 'duration', 'end', 'day', 'end_day', 'date', 'change'];

    public function setStartAttribute($value) {
        $time = new Carbon($value);
        $this->attributes['start'] = $time->format('H:i');
    }

    public function getStartAttribute($value) {
        $time = new Carbon($value);
        return $time->format('H:i');
    }

    public function setEndAttribute($value) {
        $time = new Carbon($value);
        $this->attributes['end'] = $time->format('H:i');
    }

    public function getEndAttribute($value) {
        $time = new Carbon($value);
        return $time->format('H:i');
    }

    public function staff() {
        return $this->belongsTo('App\DirectorateStaff', 'directorate_staff_id');
    }

    public function service() {
        return $this->belongsTo('App\Service');
    }

}
