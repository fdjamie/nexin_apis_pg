<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class ShiftType extends Model
{
    use SoftDeletes;    
    protected $table = 'tb_shift_type';
    protected $fillable = ['directorate_id','rota_group_id','rota_template_id','name','directorate_speciality_id','service_id','service_type','start_time','finish_time','overrides_teaching', 'nroc', 'on_site'];
    
    public function getStartTimeAttribute($value) {
        $value =new Carbon($value);
        $value = $value->format('H:i');
        return $value;
    }
   
    public function getFinishTimeAttribute($value) {
        $value =new Carbon($value);
        $value = $value->format('H:i');
        return $value;
    }
    public function directorate(){
        return $this->belongsTo('App\Directorate');
    }
    
    public function service(){
        return $this->belongsTo('App\Service');
    }
    
    public function directorate_service(){
        return $this->belongsTo('App\DirectorateService','service_id');
    }
    
    public function directorate_speciality(){
        return $this->belongsTo('App\DirectorateSpeciality');
    }
    
    public function rota_template() {
        return $this->belongsTo('App\RotaTemplate','rota_template_id');
    }

    public function rota_group(){
        return $this->belongsTo('App\RotaGroup');
    }
    
    public function shift_detail(){
        return $this->hasMany('App\ShiftDetail','shift_id');
    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (ShiftType $shift_type) {

            foreach ($shift_type->shift_detail as $shift_detail) {
                $shift_detail->delete();
            }
        });
    }
    
}
