<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocationTwo extends Model
{
    use SoftDeletes;

    protected $connection = 'pgsql2';
    protected $table = 'tb_location';
    protected $fillable = ['site_id', 'group_id', 'subgroup_id', 'name', 'service', 'limit'];

    public function site() {
        return $this->belongsTo('App\SiteTwo','site_id');
    }
}
