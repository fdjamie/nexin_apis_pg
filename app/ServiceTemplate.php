<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceTemplate extends Model
{
    use SoftDeletes;    
    protected $table = 'tb_service_template';
    protected $fillable = ['location_id','service_id', 'content'];
    
    public function setContentAttribute($value) {
        $this->attributes['content'] = json_encode($value);
    }
    
    public function getContentAttribute($value) {
        return json_decode($value,TRUE);
    }
    
    public function service(){
        return $this->belongsTo('App\Service');
    }
    
    public function location(){
        return $this->belongsTo('App\Location');
    }
}
