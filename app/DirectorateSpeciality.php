<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DirectorateSpeciality extends Model {
    
    use SoftDeletes;
    protected $table = 'tb_directorate_speciality';
    protected $fillable = ['directorate_id', 'name','abbreviation', 'status'];

    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }

    public function post() {
        return $this->hasMany('App\Post');
    }
    
     public function service() {
        return $this->hasMany('App\Service');
    }
    
    public function shift_type(){
        return $this->hasMany('App\ShiftType');
    }
    
    public function staff (){
        return $this->hasMany('App\DirectorateStaff');
    }
    
    public function shift_detail (){
        return $this->hasMany('App\ShiftDetail');
    }
    
    public function additional_grade (){
        return $this->hasMany('App\AdditionalGrade');
    }
    
    public function category_priority() {
        return $this->hasMany('App\CategoryPriority');
    }
    
//    public function priority_set() {
//        return $this->hasMany('App\SpecialityPrioritySet');
//    }
    
    public function priority_set() {
        return $this->hasMany('App\PrioritySet');
    }
    

        protected static function boot() {
        parent::boot();
        self::deleting(function (DirectorateSpeciality $directorate_speciality) {

            foreach ($directorate_speciality->service as $service) {
                $service->delete();
            }
            
            foreach ($directorate_speciality->post as $post) {
                $post->delete();
            }
            
            foreach ($directorate_speciality->additional_grade as $grade) {
                $grade->delete();
            }
            
            foreach ($directorate_speciality->category_priority as $category_priority) {
                $category_priority->delete();
            }
        });
    }
    

}
