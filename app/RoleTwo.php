<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleTwo extends Model
{
    use SoftDeletes; 
    
    protected $connection = 'pgsql2';
    protected $table = 'tb_role';
    protected $fillable = ['name','directorate_id','role_id','position'];
    
    public function directorate() {
        return $this->belongsTo('App\DirectorateTwo','directorate_id');
    }
    
//    public function grade() {
//        return $this->hasMany('App\Grade');
//    }
    
}
