<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model {

    protected $connection = 'pgsql2';
    protected $table = 'assignment';
    protected $fillable = ['assignment_session', 'transition_time', 'service', 'staff', 'updated_on', 'updated_by'];

    public function assignment_session() {
        return $this->belongsTo('App\AssignmentSession', 'assignment_session');
    }

    public function transition_time() {
        return $this->belongsTo('App\TransitionTimeTwo', 'transition_time');
    }

    public function service() {
        return $this->belongsTo('App\ServiceTwo', 'service');
    }

    public function staff() {
        return $this->belongsTo('App\DirectorateStaffTwo', 'staff');
    }

}
