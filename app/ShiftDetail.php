<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ShiftDetail extends Model {

    use SoftDeletes;

    protected $table = 'tb_shift_detail';
    protected $fillable = ['shift_id', 'start_time', 'end_time', 'directorate_speciality_id', 'service_id', 'service_type', 'overrides_teaching', 'nroc', 'on_site'];

    public function getStartTimeAttribute($value) {
        $value = new Carbon($value);
        $value = $value->format('H:i');
        return $value;
    }

    public function getEndTimeAttribute($value) {
        $value = new Carbon($value);
        $value = $value->format('H:i');
        return $value;
    }

    public function shift_type() {
        return $this->belongsTo('App\ShiftType', 'shift_id');
    }

    public function speciality() {
        return $this->belongsTo('App\DirectorateSpeciality', 'directorate_speciality_id');
    }

    public function service() {
        return $this->belongsTo('App\Service', 'service_id');
    }

    public function directorate_service() {
        return $this->belongsTo('App\DirectorateService', 'service_id');
    }

}
