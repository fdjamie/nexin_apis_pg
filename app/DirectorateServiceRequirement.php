<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DirectorateServiceRequirement extends Model {

    use SoftDeletes;

    protected $table = 'tb_directorate_service_requirement';
    protected $fillable = ['service_id', 'priority', 'color', 'status', 'parameter', 'operator', 'value','value_2', 'status', 'number'];
    
    public function service() {
        return $this->belongsTo('App\DirectorateService','service_id');
    }

}
