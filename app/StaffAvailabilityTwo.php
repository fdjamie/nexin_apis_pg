<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaffAvailabilityTwo extends Model
{
    use SoftDeletes;
    
    protected $connection = 'pgsql2';
    protected $table = 'tb_staff_availability';
    protected $fillable = ['directorate_staff_id', 'transition_time_id', 'date', 'day', 'week', 'start', 'stop'];
    
    public function directorate_staff() {
        return $this->belongsTo('App\DirectorateStaffTwo');
    }
    
    public function transition_time() {
        return $this->belongsTo('App\TransitionTimeTwo');
    }
}
