<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
//Route::get('/', function () {    return view('welcome');    });
//print_r(request()->header('Authorization'));die;

//Route::get('/', 'UnauthenticatedApisController@index');
Route::get('/', function(){ return ['here'];});
//Route::get('/login', function() {
//    $request = new \Illuminate\Http\Request();
//    return response()->json([
//                'error' => 'yes',
//                'message' => 'pass api token to access api',
//                'detail' => $request->all()
//    ]);
//});
Route::post('/login/', 'UnauthenticatedApisController@doLogin');
Route::post('/register', 'UnauthenticatedApisController@createUser');
Route::get('/v1/get-all-trusts', 'UnauthenticatedApisController@getAllTrusts');
Route::post('/v1/signup-request', 'UnauthenticatedApisController@storeSignupRequest');

Route::group([
    'prefix' => 'v1',
    'middleware' => 'auth:api'
        ], function () {
    Route::get('/testing', 'LocationController@testing');

    Route::get('/user', 'AuthenticatedApisController@showAllUsers');
    Route::post('/create-trust-user', 'AuthenticatedApisController@createTrustUser');
    Route::get('/trust-user/{id}', 'AuthenticatedApisController@getTrustUsers');
    Route::get('/user/{id}', 'AuthenticatedApisController@editUser');
    Route::put('/user/{id}', 'AuthenticatedApisController@updateUser');
    Route::delete('/user/{id}', 'AuthenticatedApisController@destroyUser');

//    Route::get('/speciality/new', 'AuthenticatedApisController@createSpeciality');
    Route::get('/speciality', 'AuthenticatedApisController@indexSpeciality');           //  to show all specialities
    Route::get('/speciality/{id}', 'AuthenticatedApisController@showSpeciality');       //  to show single speciality
    Route::post('/speciality/', 'AuthenticatedApisController@storeSpeciality');         //  to create a new speciality
    Route::put('/speciality/{id}', 'AuthenticatedApisController@updateSpeciality');     //  to update existing speciality
    Route::delete('/speciality/{id}', 'AuthenticatedApisController@destroySpeciality'); //  to delete existing speciality
    Route::post('/speciality/upload', 'AuthenticatedApisController@uploadSpeciality');   // to upload a new specialities


    Route::get('/holiday/{id}', 'HolidayController@show');
    Route::post('/updateHoliday/{id}', 'HolidayController@update');
    Route::get('/holidays/{directorate_id}', 'HolidayController@dir_holidays');
    Route::post('/holidays', 'HolidayController@store');
    Route::post('/runningServices', 'HolidayController@availableServicesOnHoliday');

    Route::get('/trust', 'TrustController@index');
    Route::get('/trust/{id}', 'TrustController@show');
    Route::get('/trust/{id}/directorate-list', 'AuthenticatedApisController@showTrustDirectorates');
    Route::post('/trust/', 'TrustController@store');
    Route::put('/trust/{id}', 'TrustController@update');
    Route::delete('/trust/{id}', 'TrustController@destroy');
    Route::post('/trust/upload', 'TrustController@upload');

    Route::get('/division', 'DivisionController@index');
    Route::get('/division/deleted/{id}', 'DivisionController@delted');
    Route::get('/division/restore/{id}', 'DivisionController@restore');
    Route::get('/division/{id}', 'DivisionController@show');
    Route::post('/division/', 'DivisionController@store');
    Route::put('/division/{id}', 'DivisionController@update');
    Route::delete('/division/{id}', 'DivisionController@destroy');
    Route::post('/division/upload', 'DivisionController@upload');

    Route::get('/directorate', 'DirectorateController@index');
    Route::get('/directorate/{id}', 'DirectorateController@show');
    Route::post('/directorate/', 'DirectorateController@store');
    Route::put('/directorate/{id}', 'DirectorateController@update');
    Route::delete('/directorate/{id}', 'DirectorateController@destroy');
    Route::post('/directorate/upload', 'DirectorateController@upload');

    Route::get('/directoratespeciality', 'SpecialityController@index');
    Route::post('/directoratespeciality/', 'SpecialityController@store');
    Route::put('/directoratespeciality/{id}', 'SpecialityController@update');
    Route::get('/directoratespeciality/{id}', 'SpecialityController@show');
    Route::delete('/directoratespeciality/{id}', 'SpecialityController@destroy');
    Route::post('/directoratespeciality/upload', 'SpecialityController@upload');

    Route::get('/site', 'SiteController@index');
    Route::get('/site/{id}', 'SiteController@show');
    Route::post('/site/', 'SiteController@store');
    Route::put('/site/{id}', 'SiteController@update');
    Route::delete('/site/{id}', 'SiteController@destroy');
    Route::post('/site/upload', 'SiteController@upload');

    Route::get('/location', 'LocationController@index');
    Route::get('/location/{id}', 'LocationController@show');
    Route::post('/location/', 'LocationController@store');
    Route::put('/location/{id}', 'LocationController@update');
    Route::delete('/location/{id}', 'LocationController@destroy');
    Route::post('/location/upload', 'LocationController@upload');

    Route::get('/staff', 'AuthenticatedApisController@indexStaff');
    Route::get('/staff/{id}', 'AuthenticatedApisController@showStaff');
    Route::post('/staff/', 'AuthenticatedApisController@storeStaff');
    Route::put('/staff/{id}', 'AuthenticatedApisController@updateStaff');
    Route::delete('/staff/{id}', 'AuthenticatedApisController@destroyStaff');
    Route::post('/staff/upload', 'AuthenticatedApisController@uploadStaff');

    Route::get('/directorate-staff/directorate/{id}', 'DirectorateStaffController@index');
    Route::get('/directorate-staff/trust/{id}', 'DirectorateStaffController@index');
    Route::post('/directorate-staff/', 'DirectorateStaffController@store');
    Route::get('/directorate-staff/{id}', 'DirectorateStaffController@show');
    Route::put('/directorate-staff/{id}', 'DirectorateStaffController@update');
    Route::delete('/directorate-staff/{id}', 'DirectorateStaffController@destroy');
    Route::post('/directorate-staff/upload', 'DirectorateStaffController@upload');
    Route::post('/directorate-staff/availability', 'DirectorateStaffController@availability');
    Route::get('/directorate-staff/export/{id}', 'DirectorateStaffController@export');
    
    
    Route::get('/grade', 'GradeController@index');
    Route::get('/grade/{id}', 'GradeController@show');
    Route::post('/grade/', 'GradeController@store');
    Route::put('/grade/{id}', 'GradeController@update');
    Route::delete('/grade/{id}', 'GradeController@destroy');
    Route::post('/trust/{id}/directorategrade/upload', 'GradeController@upload');

    Route::get('/role', 'RoleController@index');
    Route::get('/role/{id}', 'RoleController@show');
    Route::post('/role/', 'RoleController@store');
    Route::put('/role/{id}', 'RoleController@update');
    Route::delete('/role/{id}', 'RoleController@destroy');
    Route::post('/trust/{id}/directoraterole/upload', 'RoleController@upload');

    Route::get('/shift', 'AuthenticatedApisController@indexShift');
    Route::get('/shift/{id}', 'AuthenticatedApisController@showShift');
    Route::post('/shift/', 'AuthenticatedApisController@storeShift');
    Route::put('/shift/{id}', 'AuthenticatedApisController@updateShift');
    Route::delete('/shift/{id}', 'AuthenticatedApisController@destroyShift');
    Route::get('/get-tree', 'AuthenticatedApisController@getJSTreeData');
    Route::get('/get-tree/{id}', 'AuthenticatedApisController@getTreeDataByID');

    Route::get('/get-user-trusts-tree/{id}/{tree?}', 'AuthenticatedApisController@getUserTrustsTree');

    Route::get('/template/rotation', 'AuthenticatedApisController@indexRotationTemplate');
    Route::get('/template/rotation/{id}', 'AuthenticatedApisController@showRotationTemplate');
    Route::post('/template/rotation/', 'AuthenticatedApisController@storeRotationTemplate');
    Route::put('/template/rotation/{id}', 'AuthenticatedApisController@updateRotationTemplate');
    Route::delete('/template/rotation/{id}', 'AuthenticatedApisController@destroyTemplate');

    Route::get('/post', 'PostController@index');
    Route::get('/post/{id}', 'PostController@show');
    Route::post('/post/', 'PostController@store');
    Route::put('/post/{id}', 'PostController@update');
    Route::delete('/post/{id}', 'PostController@destroy');
    Route::post('/post/{id}/additional-parameter', 'PostController@updateAdditionalParameter');
    Route::post('/posts/upload', 'PostController@postUpload');

    Route::get('/post-details/{id}', 'AuthenticatedApisController@showPostDetails');
    Route::post('/post-details/', 'AuthenticatedApisController@storePostDetails');
    Route::put('/post-details/{id}', 'AuthenticatedApisController@updatePostDetails');

    Route::get('/rotation', 'RotationController@index');
    Route::get('/rotation/{id}', 'RotationController@show');
    Route::post('/rotation/', 'RotationController@store');
    Route::put('/rotation/{id}', 'RotationController@update');
    Route::delete('/rotation/{id}', 'RotationController@destroy');

    Route::get('/service/template', 'AuthenticatedApisController@indexServiceTemplate');
    Route::get('/service', 'ServiceController@index');
    Route::get('/service/{id}', 'ServiceController@show');
    Route::post('/service/', 'ServiceController@store');
    Route::put('/service/{id}', 'ServiceController@update');
    Route::delete('/service/{id}', 'ServiceController@destroy');
    Route::post('/service/upload', 'ServiceController@upload');
    Route::post('/service/upload-reference', 'ServiceController@uploadReference');
    Route::get('/service/template/{id}', 'AuthenticatedApisController@showServiceTemplate');
    Route::post('/service/template', 'AuthenticatedApisController@storeServiceTemplate');
    Route::put('/service/template/{id}', 'AuthenticatedApisController@updateServiceTemplate');
    Route::get('/service/template/delete/{id}', 'AuthenticatedApisController@destroyServiceTemplate');
//    Route::post('/service/requirement', 'ServiceController@storeRequirement');
    Route::get('/deleted/service/{id}', 'ServiceController@deletedService');
    Route::post('/service/running', 'ServiceController@running');
    Route::post('/service/staff/available', 'ServiceController@availableStaff');
    
    Route::post('/assign/service' , 'DirectorateStaffController@assignStaffService');
    Route::get('/assign/staff' , 'DirectorateStaffController@getAssignStaff');
    Route::put('/assign/staff/update' , 'DirectorateStaffController@updateAssignStaff');

    Route::get('/shift-type', 'ShiftTypeController@index');
    Route::post('/shift-type/', 'ShiftTypeController@store');
    Route::get('/shift-type/{id}', 'ShiftTypeController@show');
    Route::put('/shift-type/{id}', 'ShiftTypeController@update');
    Route::post('/shift-type/upload', 'ShiftTypeController@upload');
    Route::delete('/shift-type/{id}', 'ShiftTypeController@destroy');
    Route::get('/shift-type/{id}/export', 'ShiftTypeController@export');
    Route::post('/shift-type/import', 'ShiftTypeController@import');
    Route::post('/shift-type/upload', 'ShiftTypeController@upload');
    Route::post('/shift-type/reference-data/upload', 'ShiftTypeController@uploadReferenceData');

    Route::get('/rota-template', 'RotaTemplateController@index');
    Route::post('/rota-template', 'RotaTemplateController@store');
    Route::get('/rota-template/{id}', 'RotaTemplateController@show');
    Route::put('/rota-template/{id}', 'RotaTemplateController@update');
    Route::delete('/rota-template/{id}', 'RotaTemplateController@destroy');

    Route::get('/category', 'CategoryController@index');
    Route::post('/category', 'CategoryController@store');
    Route::get('/category/{id}', 'CategoryController@show');
    Route::put('/category/{id}', 'CategoryController@update');
    Route::delete('/category/{id}', 'CategoryController@destroy');
    Route::post('/category/upload','CategoryController@upload')->name('category-upload');
    /*
     * sub categories
     */
     Route::resource('sub-category', 'SubCategoryController');
     Route::get('sub-category/{id}/child','SubCategoryController@childIndex');
     Route::get('/sub-category/{id}/parents', 'SubCategoryController@parents');
     Route::get('/sub-category/{id}/children', 'SubCategoryController@child');
     Route::get('/category/{id}/sub-category/{query}', 'SubCategoryController@searchSubcategory');
    
     
    Route::get('shift/{id}/shift-detail', 'AuthenticatedApisController@indexShiftDetail');
    Route::post('/shift-detail', 'AuthenticatedApisController@storeShiftDetail');
    Route::get('/shift-detail/{id}', 'AuthenticatedApisController@showShiftDetail');
    Route::put('/shift-detail/{id}', 'AuthenticatedApisController@updateShiftDetail');
    Route::delete('/shift-detail/{id}', 'AuthenticatedApisController@destroyShiftDetail');

    Route::get('/search/staff/{query}', 'AuthenticatedApisController@searchStaff');
    Route::get('/search/directorate/{id}/staff/{query}', 'AuthenticatedApisController@searchStaffDirectorate');

    Route::get('/rota-group', 'RotaGroupController@index');
    Route::get('/rota-group/{id}', 'RotaGroupController@show');
    Route::post('/rota-group', 'RotaGroupController@store');
    Route::put('/rota-group/{id}', 'RotaGroupController@update');
    Route::delete('/rota-group/{id}', 'RotaGroupController@destroy');
    Route::post('/trust/{id}/directorate/rota-group/upload', 'RotaGroupController@upload');

    Route::get('/additional-grade', 'AuthenticatedApisController@indexAdditionalGrade');
    Route::get('/additional-grade/{id}', 'AuthenticatedApisController@showAdditionalGrade');
    Route::post('/additional-grade/', 'AuthenticatedApisController@storeAdditionalGrade');
    Route::put('/additional-grade/{id}', 'AuthenticatedApisController@updateAdditionalGrade');
    Route::delete('/additional-grade/{id}', 'AuthenticatedApisController@destroyAdditionalGrade');
    Route::post('/trust/{id}/directorate/speciality/additional-grade/upload', 'AuthenticatedApisController@uploadAdditionalGrade');

    Route::get('/group', 'GroupController@index');
    Route::post('/group', 'GroupController@store');
    Route::get('/group/{id}', 'GroupController@show');
    Route::put('/group/{id}', 'GroupController@update');
    Route::post('/group/upload', 'GroupController@upload');
    Route::delete('/group/{id}', 'GroupController@destroy');
    Route::get('/trust/{id}/group', 'GroupController@trust');

    Route::get('/subgroup', 'SubgroupController@index');
    Route::post('/subgroup', 'SubgroupController@store');
    Route::get('/subgroup/{id}', 'SubgroupController@show');
    Route::put('/subgroup/{id}', 'SubgroupController@update');
    Route::post('/subgroup/upload', 'SubgroupController@upload');
    Route::delete('/subgroup/{id}', 'SubgroupController@destroy');

    Route::get('/directorate-service', 'DirectorateServicesController@index');
    Route::post('/directorate-service', 'DirectorateServicesController@store');
    Route::get('/directorate-service/{id}', 'DirectorateServicesController@show');
    Route::get('/deleted/directorate-service/{id}', 'DirectorateServicesController@deletedServices');
    Route::put('/directorate-service/{id}', 'DirectorateServicesController@update');
    Route::delete('/directorate-service/{id}', 'DirectorateServicesController@destroy');
    Route::post('/directorate-service/upload', 'DirectorateServicesController@upload');
    Route::post('/directorate-service/template', 'DirectorateServicesController@storeServiceTemplate');
    Route::put('/directorate-service/template/{id}', 'DirectorateServicesController@updateServiceTemplate');
    Route::delete('/directorate-service/template/{id}', 'DirectorateServicesController@destroyServiceTemplate');
    Route::post('/directorate-service/reference-data/upload', 'DirectorateServicesController@uploadReferenceData');

    Route::get('/qualification', 'QualificationController@index');
    Route::post('/qualification', 'QualificationController@store');
    Route::get('/qualification/{id}', 'QualificationController@show');
    Route::put('/qualification/{id}', 'QualificationController@update');
    Route::delete('/qualification/{id}', 'QualificationController@destroy');
    Route::post('qualification/upload','QualificationController@uploadQualification')->name('upload-qualification');

    Route::get('/directorate-service/requirement', 'DirectorateServiceRequirementController@index');
    Route::post('/directorate-service/requirement', 'DirectorateServiceRequirementController@store');
    Route::get('/directorate-service/requirement/{id}', 'DirectorateServiceRequirementController@show');
    Route::put('/directorate-service/requirement/{id}', 'DirectorateServiceRequirementController@update');
    Route::delete('/directorate-service/requirement/{id}', 'DirectorateServiceRequirementController@destroy');

//    Route::get('/service/requirement', function(){return 'here';});
    Route::get('/service-requirement', 'ServiceRequirementController@index');
    Route::post('/service-requirement', 'ServiceRequirementController@store');
    Route::get('/service-requirement/{id}', 'ServiceRequirementController@show');
    Route::put('/service-requirement/{id}', 'ServiceRequirementController@update');
    Route::delete('/service-requirement/{id}', 'ServiceRequirementController@destroy');
     /*
     * make condition
     */
    Route::post('service/conditions','ServiceRequirementController@makeCondition');
    Route::any('/service/get-requirement-data', 'ServiceRequirementController@getRequirementData');
    Route::any('/service/save-requirement-condition', 'ServiceRequirementController@saveRequirementCondition');
    Route::any('service/delete-requirement-condition', 'ServiceRequirementController@deleteRequirementCondition');

    Route::get('/additional-parameter', 'AdditionalParameterController@index');
    Route::post('/additional-parameter', 'AdditionalParameterController@store');
    Route::get('/additional-parameter/{id}', 'AdditionalParameterController@show');
    Route::put('/additional-parameter/{id}', 'AdditionalParameterController@update');
    Route::delete('/additional-parameter/{id}', 'AdditionalParameterController@destroy');
    Route::post('additional-parameter/additionalparameter-upload', 'AdditionalParameterController@directorateAdditionalParameterUpload');
    Route::any('additional-parameter/{value}/{directorate_id}', 'AdditionalParameterController@getAdditionalParameter');

    Route::get('/category-priority', 'CategoryPriorityController@index');
    Route::post('/category-priority', 'CategoryPriorityController@store');
    Route::get('/category-priority/{id}', 'CategoryPriorityController@show');
    Route::put('/category-priority/{id}', 'CategoryPriorityController@update');
    Route::delete('/category-priority/{id}', 'CategoryPriorityController@destroy');
    Route::put('/category-priority/update/requirement-order', 'CategoryPriorityController@updateRequirementOrder');

    Route::get('/color', 'ColorController@index');
    Route::post('/color', 'ColorController@store');
    Route::get('/color/{id}', 'ColorController@show');
    Route::put('/color/{id}', 'ColorController@update');
    Route::delete('/color/{id}', 'ColorController@destroy');
    
    Route::get('/speciality-priority-set', 'SpecialityPrioritySetController@index');
    Route::post('/speciality-priority-set', 'SpecialityPrioritySetController@store');
    Route::post('/speciality-priority-set/store-update', 'SpecialityPrioritySetController@storeUpdate');
    Route::get('/speciality-priority-set/{id}', 'SpecialityPrioritySetController@show');
    Route::put('/speciality-priority-set/{id}', 'SpecialityPrioritySetController@update');
    Route::delete('/speciality-priority-set/{id}', 'SpecialityPrioritySetController@destroy');
    
    Route::get('/priority-set', 'PrioritySetController@index');
    Route::post('/priority-set', 'PrioritySetController@store');
    Route::get('/priority-set/{id}', 'PrioritySetController@show');
    Route::put('/priority-set/{id}', 'PrioritySetController@update');
    Route::delete('/priority-set/{id}', 'PrioritySetController@destroy');
    
    Route::get('/transition-time', 'TransitionTimeController@index');
    Route::get('/transition-time/directorate/{id}', 'TransitionTimeController@directoratesTransitionTime');
    Route::post('/transition-time', 'TransitionTimeController@store');
    Route::get('/transition-time/{id}', 'TransitionTimeController@show');
    Route::put('/transition-time/{id}', 'TransitionTimeController@update');
    Route::delete('/transition-time/{id}', 'TransitionTimeController@destroy');

    //########################################## RELATIONAL ROUTS #####################################
    Route::get('/trust/{id}/division', 'AuthenticatedApisController@trustRelationalDivision');
    Route::get('/trust/{id}/division/directorate', 'AuthenticatedApisController@trustRelationalDivisionDirectorate');

    Route::get('/trust/{id}/directorates', 'AuthenticatedApisController@trustDirectorates');
    Route::get('/trust/{id}/directorate/staff', 'AuthenticatedApisController@trustDirectorateStaff');
    Route::get('/trust/{id}/directorate/grade-role', 'AuthenticatedApisController@directorateRelationalGradeRole');
    Route::get('/trust/{id}/directorate/specilality-grade', 'AuthenticatedApisController@directorateRelationalSpecialitiesGrade');
    Route::get('/trust/{id}/directorate/specilality/additionalgrade', 'AuthenticatedApisController@directorateRelationalSpecialitiesAdditionalGrade');
    Route::get('/trust/{id}/category-priority', 'CategoryPriorityController@getCategoryPriorityByDirectorate');
    Route::get('/directoratespeciality/{id}/category-priority', 'CategoryPriorityController@getCategoryPriorityBySpeciality');
    Route::get('/trust/{id}/directorate/additionalparameter', 'AuthenticatedApisController@directorateAdditionalParameter');
    Route::get('/trust/{id}/directorate/specilality/service', 'AuthenticatedApisController@directorateRelationalSpecialitiesService');
    Route::get('/trust/{id}/directorate/specilality/post', 'AuthenticatedApisController@directorateRelationalSpecialitiesPost');
    Route::get('/trust/{id}/directorate/all-data', 'AuthenticatedApisController@directorateRelationalAllData');
    Route::get('/trust/{id}/directorate/rota-group', 'AuthenticatedApisController@trustRelationalDirectorateRotaGroup');
    Route::get('/trust/{id}/directorate/groups/subgroup', 'AuthenticatedApisController@trustDirectorateGroupSubgroup');
    Route::get('/trust/{id}/directorate/category/subcategory', 'AuthenticatedApisController@trustDirectorateCategory');
    Route::get('/directorate/{id}/category', 'AuthenticatedApisController@directorateCategoryTree');
    Route::get('/trust/{id}/directorate/service', 'AuthenticatedApisController@trustDirectorateService');
    Route::get('/directorate/{id}/speciality/{sid}/category/service-requirement', 'ServiceRequirementController@testing');
    Route::get('/directorate/{id}/speciality/{sid}/post', 'AuthenticatedApisController@directorateSpecialityPost');
    Route::get('/directorate/{id}/transition-time', 'TransitionTimeController@directorateTimes');
    

    Route::post('/service/post/view', 'AuthenticatedApisController@servicePostView');
    Route::get('/service/overview/{id}/{date}', 'AuthenticatedApisController@ServiceOverview');

    Route::get('/trust/{id}/site', 'AuthenticatedApisController@trustRelationalSite');
    
    Route::get('/directorate/{id}/speciality/{sid}/date/{date}/over-view', 'ServiceRequirementController@overview');
    Route::get('/directorate/{id}/speciality/{sid}/priority-set/date/{date}', 'ServiceController@specialityPrioritySet');

    Route::get('/service/requirement/parameter/test', 'ServiceRequirementParameterController@index');
    Route::post('/service/requirement/parameter/test/', 'ServiceRequirementParameterController@store');
    Route::get('/service/requirement/parameter/test/{id}', 'ServiceRequirementParameterController@show');
    Route::put('/service/requirement/parameter/test/{id}', 'ServiceRequirementParameterController@update');
    Route::delete('/service/requirement/parameter/test/{id}', 'ServiceRequirementParameterController@destroy');
    Route::get('/service/requirement/get-parameter/test/{id}', 'ServiceRequirementParameterController@getParameter');
    Route::any('/service/get-parameter/operator', 'ServiceRequirementParameterController@getOperatorbyParameter');

    Route::any('/service/get-parameter-type','ServiceRequirementParameterController@getParameterType')->name('get-parameter-type');

    Route::get('/trust/{id}/directorate/parameter', 'ServiceRequirementParameterController@directorateParameter');







    /*
     * operator routes
     */
    Route::any('service/get-operators' , 'ServiceRequirementParameterController@getOperator')->name('get-operator');
    Route::any('service/get-operators-value' , 'ServiceRequirementParameterController@getOperatorValue')->name('get-operator');

    /*Trust User Roles Start HERE --------By William*/

    Route::get('/user-roles/{id}', 'TrustController@getTrusRoles');
    Route::post('/user-roles', 'RolesContoller@store');
    Route::put('/user-roles/{id}', 'RolesContoller@update');
    Route::delete('/user-roles/{id}', 'RolesContoller@destroy');

    Route::get('/get-module-permissions/{id}', 'RolesContoller@getModulesAndPermissions');
    Route::post('/store-role-permission', 'RolesContoller@storeRolePermissions');


    Route::post('/user-permission', 'RolesContoller@storeRolePermissions');
    Route::get('/get-user-role-permission/{id}', 'RolesContoller@getRolePermisions');

    /*Trust User Roles End HERE*/

    /*Trust SIGN UP Request START HERE*/

    Route::get('/get-all-requests/{id}', 'UserRequestController@getRequests');

    Route::get('/get-user-account-status/{id}', 'UserRequestController@getUserStatus');
    Route::post('/update-request-status', 'UserRequestController@updateUserStatus');


    /*Trust SIGN UP Request END HERE*/


    /*EMAIL TEMPLATYES ROUTES START HERE*/

    Route::get('/email-templates/{id}', 'EmailTemplatesController@getAllTemplates');
    Route::get('/trust-template-html/{id}', 'EmailTemplatesController@getTemplateHtml');
    Route::get('/email-type-templates/{templateTypeId}/{trustId}', 'EmailTemplatesController@getEmailTypeTemplate');
    Route::post('/change-template', 'EmailTemplatesController@updateTemplate');
    Route::post('/create-new-template', 'EmailTemplatesController@createTemplate');


    /*EMAIL TEMPLATYES ROUTES END*/

//    Route::get('/service/operator/test', 'OperatorController@index');
//    Route::post('/service/operator/test/', 'OperatorController@store');
//    Route::get('/service/operator/test/{id}', 'OperatorController@show');
//    Route::put('/service/operator/test/{id}', 'OperatorController@update');
//    Route::delete('/service/operator/test/{id}', 'OperatorController@destroy');
   
    Route::get('/command/{date}','CommandController@index');
    Route::get('/optimal-assignment/{date}','CommandController@optimalAssignment');

    Route::post('/send-email','AuthenticatedApisController@sendEmail');
    Route::get('/get-user-emails/{id}','AuthenticatedApisController@getUserEmails');

});
