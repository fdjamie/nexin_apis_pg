<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceParameterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'directorate_id'=>'required|integer|unique_with:tb_parameters,name,directorate_id',
            'name'=>'required',
            'type'=>'required'
        ];
    }
}
