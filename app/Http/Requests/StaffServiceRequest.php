<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StaffServiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'directorate_staff_id' => 'required|integer|exists:tb_directorate_staff,id',
                'service_id' => 'required|integer|exists:tb_service,id',
        ];
    }
}
