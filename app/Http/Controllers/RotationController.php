<?php

namespace App\Http\Controllers;

use File;
use App\Rotation;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RotationController extends Controller {

    protected function validateRotation(array $data, $id = null) {
        return Validator::make($data, [
                    'post_id' => 'required|integer',
                    'staff_member' => 'required|integer|unique_with:tb_rotation,post_id,' . $id,
                    'template_id' => 'required|integer',
                    'start_date' => 'required',
                    'template_position' => 'required|integer',
        ]);
    }

    public function index() {
        return response()->json(Rotation::all());
    }

    public function store(Request $request) {
        $validator = $this->validateRotation($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = Rotation::create($request->all());
            $post = \App\Post::find($request->post_id)->update(['staff_id' => $request->staff_member]);
            if ($new && $post)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record not added.']);
        }
    }

    public function show($id) {
        $record = Rotation::with('post.directorate_speciality.directorate', 'post.rota_template', 'staff')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        if (!isset($request->end_date))
            $request->merge(['end_date' => $request->end_date]);
        $validator = $this->validateRotation($request->all(), $id);
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = Rotation::find($id);
            if ($record) {
                $update = $record->update($request->all());
                $post = \App\Post::find($request->post_id)->update(['staff_id' => $request->staff_member]);
                if ($update && $post)
                    return response()->json(['success' => 'Record updated successfully']);
                else
                    return response()->json(['error' => 'changes not updated']);
            } else
                return response()->json(['error' => 'unable to find record']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $record = Rotation::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'record deleted successfully']);
            } else {
                return response()->json(['error' => 'record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'unable to find record']);
        }
    }

}
