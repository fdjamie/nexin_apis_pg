<?php

namespace App\Http\Controllers;

use File;
use Validator;
use App\Http\Requests;
use App\Http\Helpers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use DB;

class DirectorateServicesController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateService(array $data) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'category_id' => 'integer',
                    'sub_category_id' => 'integer',
                    'name' => 'required',
                    'contactable' => 'boolean',
                    'min_standards' => 'boolean',
                    'specialities' => 'array',
        ]);
    }

    protected function validateServiceTemplate(array $data) {
        return Validator::make($data, [
                    'service_id' => 'required|integer',
                    'content' => 'array',
        ]);
    }

    public function index()
    {
        return response()->json(\App\DirectorateService::all());
    }

    public function store(Request $request) {
        $validator = $this->validateService($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        } else {
            $new = \App\DirectorateService::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function show($id) {
        $record = \App\DirectorateService::with('directorate', 'category', 'sub_category', 'requirement')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateService($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = \App\DirectorateService::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['updated' => 'Record updated successfully']);
                else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {
        $record = \App\DirectorateService::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'record deleted successfully']);
            } else {
                return response()->json(['error' => 'record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'unable to find record']);
        }
    }

    public function storeServiceTemplate(Request $request) {
        $validator = $this->validateServiceTemplate($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $service = \App\DirectorateService::find($request->get('service_id'));
            if ($service) {
                $service->template = $request->get('template');
                $result = $service->save();
            }
            return response()->json(['status' => 'ok', 'data' => $result]);
        }
    }

    public function updateServiceTemplate(Request $request, $id) {
        $validator = $this->validateServiceTemplate($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $service = \App\DirectorateService::find($request->get('service_id'));
            if ($service) {
                $service->template = $request->get('template');
                $result = $service->save();
                return response()->json(['record updated successfully', $result]);
            } else {
                return response()->json(['changes not updated']);
            }
        }
    }

    public function destroyServiceTemplate($id) {
        $record = \App\DirectorateService::find($id);
        if ($record) {
            $record->template = NULL;
            $result = $record->save();
            if ($result)
                return response()->json(['deleted' => 'service template deleted successfully']);
            else
                return response()->json(['deleted' => 'record is not deleted']);
        } else {
            return response()->json(['not_found' => 'unable to find record']);
        }
    }

    public function upload(Request $request) {
        $data_store = $formated_data = $records = [];
        $formated_data = $request->data;
        $data_store = $this->getDataToStore($formated_data, $request->trust_id);
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
//        return [$data_store];
        foreach ($data_store as $node) {
            $records[] = \App\DirectorateService::updateOrCreate(['id' => $node['id']], $node);
        }
        return response()->json(['success' => count($records) . " Records added or updated successfully"]);
    }

    public function deletedServices($id) {
        $services = \App\Directorate::with(['services' => function($q) {
                        $q->withTrashed();
                    }, 'services.category', 'services.sub_category', 'speciality', 'category.sub_category'])->where('trust_id', $id)->withTrashed()->get();
        return response()->json($services);
    }

    public function uploadReferenceData(Request $request) {
        $directorates = \App\Directorate::with(['services' => function($q) {
                        $q->withTrashed();
                    }, 'services.category', 'services.sub_category', 'speciality', 'category.sub_category'])->where('trust_id', $request->trust_id)->withTrashed()->get();
        $master_data_ids = $this->getMasterDataWithId($directorates);
        $reference_add = $master_check = $checked_reference = [];
        $success_count = $error_count = 0;
        $reference_add = $this->getReferenceData($request->reference_data);
        $master_check = $this->getMasterData($request->master_data);
        $checked_reference = $this->validatedDataFromMasterData($reference_add, $master_check);
        $store_data = $this->validateDataFromDbase($checked_reference, $master_data_ids);
        list($error_count, $success_count) = $this->storeMaserData($store_data);
        return response()->json(['error' => ($error_count != 0 ? $error_count . ', records could not be updated successfully.' : '' ), 'success' => ($success_count != 0 ? $success_count . ', records updated successfully.' : '')]);
//        return response()->json('here');
    }

    //########################## PROTECTED FUNCTIONS ############################

    protected function getService($record) {
        $data = [];
        $data['directorate'] = isset($record['directorate']) ? $record['directorate'] : "n/a";
        $data['category'] = isset($record['category']) ? $record['category'] : "n/a";
        $data['subcategory'] = isset($record['subcategory']) ? $record['subcategory'] : "n/a";
        $data['name'] = isset($record['name']) ? $record['name'] : "n/a";
        $data['contactable'] = isset($record['contactable']) ? $record['contactable'] : "n/a";
        $data['min_standards'] = isset($record['min_standards']) ? $record['min_standards'] : "n/a";
        $data['specialities'] = (isset($record['specialities']) ? $record['specialities'] : null);
        return $data;
    }

    protected function getTemplate($record, $site_array, $location_array) {
        $data = $events = [];
        if (isset($record['start_date']) && isset($record['start_date']) && isset($record['cycle_length']) && isset($record['starting_week'])) {
            if (isset($record['events'])) {
                $id = 1;
                foreach ($record['events'] as $event) {
                    $location_store = [];
                    if (array_search($event['site'], $site_array)) {
                        $site_id = array_search($event['site'], $site_array);
                        if (isset($event['locations'])) {
                            $service_locations = explode(',', $event['locations']);
                            foreach ($service_locations as $location) {
                                if (array_search($location, $location_array[$site_id]))
                                    $location_store [] = array_search($location, $location_array[$site_id]);
                            }
                        }
                        $events [] = $this->getEvent($event, $id, $site_id, $location_store);
                    }
                    $id++;
                }
            }
        }
        $data = ['start_date' => $record['start_date'], 'end_date' => $record['end_date'], 'cycle_length' => $record['cycle_length'], 'starting_week' => $record['starting_week'], 'events' => $events];
        return $data;
    }

    protected function getEvent($record, $id, $site_id, $location_store) {
        $event = [];
        if (isset($record['week_no']) && isset($record['day']) && isset($record['start']) && isset($record['duration']) && $site_id)
            $event = ['id' => $id, 'week_no' => $record['week_no'], 'day' => $record['day'], 'start' => $record['start'], 'duration' => $record['duration'], 'site_id' => $site_id, 'location_id' => $location_store];
        return $event;
    }

    protected function getData($record, $specialities) {
        $data = [];

        if (isset($record['directorate_id']) && isset($record['name'])) {
            $data['directorate_id'] = $record['directorate_id'];
            $data['category_id'] = isset($record['category_id']) ? $record['category_id'] : 0;
            $data['sub_category_id'] = isset($record['sub_category_id']) ? $record['sub_category_id'] : 0;
            $data['name'] = $record['name'];
            $data['contactable'] = (($record['contactable'] == 'Yes') ? 1 : 0);
            $data['min_standards'] = (($record['min_standards']) ? 1 : 0);
            $data['specialities'] = json_encode($specialities);
            $data['template'] = $record['template'];
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");
        }
        return $data;
    }

    protected function getCheckData($id) {

        $directorates = \App\Directorate::with('speciality', 'category.directorate_service', 'category.sub_category.directorate_service', 'services')->where('trust_id', $id)->get();
        $categories = Helpers::getAllNodesFromMainNode($directorates, 'category');
        $directorate_array = $this->getMainArray($directorates);
        $speciality_array = $this->getSubnodeArray($directorates, 'speciality');
        $category_array = $this->getSubnodeArray($directorates, 'category');
        $subcategory_array = $this->getSubnodeArray($categories, 'sub_category');
        $services_array = $this->getSubnodeArray($directorates, 'services');
        $sites = \App\Site::with('location')->where('trust_id', $id)->get();
        $site_array = $this->getMainArray($sites);
        $location_array = $this->getSubnodeArray($sites, 'location');
        return [$directorate_array, $speciality_array, $category_array, $subcategory_array, $services_array, $site_array, $location_array];
    }

    protected function getMainArray($nodes) {
        $result = [];
        foreach ($nodes as $node) {
            $result[$node->id] = $node->name;
        }
        return $result;
    }

    protected function getSubnodeArray($nodes, $subnode_name) {
        $result = [];
        foreach ($nodes as $node) {
            foreach ($node->$subnode_name as $child) {
                $result[$node->id][$child->id] = $child->name;
            }
        }
        return $result;
    }

    protected function getDataToStore($formated_data, $trust_id) {
        $data_store = [];
        list($directorate_array, $speciality_array, $category_array, $subcategory_array, $services_array, $site_array, $location_array) = $this->getCheckData($trust_id);
        foreach ($formated_data as $data_node) {
            $specialities_store = $template = $events = $service_locations = $location_store = [];
            $directorate_id = $category_id = $subcategory_id = $service_id = $site_id = 0;
            if (array_search($data_node['directorate'], $directorate_array))
                $directorate_id = array_search($data_node['directorate'], $directorate_array);
            if ((isset($data_node['category']) && $data_node['category'] != 'N/A') && array_search($data_node['category'], $category_array[$directorate_id])) {
                $category_id = array_search($data_node['category'], $category_array[$directorate_id]);
                if ((isset($data_node['subcategory']) && $data_node['subcategory'] != 'N/A') && array_search($data_node['subcategory'], $subcategory_array[$directorate_id]))
                    $subcategory_id = array_search($data_node['category'], $category_array[$directorate_id]);
            }
            if(isset($services_array[$directorate_id]))
            if (array_search($data_node['name'], $services_array[$directorate_id]))
                $service_id = array_search($data_node['name'], $services_array[$directorate_id]);
            $contactable = ($data_node['contactable'] == 'Yes' ? 1 : 0);
            $minstandards = ($data_node['min_standards'] == 'Yes' ? 1 : 0);
            $service_speciality = (isset($data_node['specialities']) ? explode(',', $data_node['specialities']) : []);
            foreach ($service_speciality as $speciality_node) {
                if (isset($speciality_arrray[$directorate_id]) && (array_search($speciality_node, $speciality_array[$directorate_id])))
                    $specialities_store[] = array_search($speciality_node, $speciality_array[$directorate_id]);
            }
            if (isset($data_node['template']) && $data_node['template'] != '') {
                $template = $this->getTemplate($data_node['template'], $site_array, $location_array);
            }
            $data_store[] = ['id' => $service_id, 'directorate_id' => $directorate_id, 'category_id' => $category_id, 'sub_category_id' => $subcategory_id, 'name' => $data_node['name'], 'contactable' => $contactable, 'min_standards' => $minstandards, 'specialities' => $specialities_store, 'template' => $template];
        }
        return $data_store;
    }

    protected function getReferenceData($refernce_data) {
        $data = [];
        foreach ($refernce_data as $data_node) {
            if ((isset($data_node['operations']) &&
                    (
                    $data_node['operations'] == 'create' ||
                    $data_node['operations'] == 'update' ||
                    $data_node['operations'] == 'delete' ||
                    $data_node['operations'] == 'restore'))) {
                $data[] = $data_node;
            }
        }
        return $data;
    }

    protected function getMasterData($master_data) {
        $data = [];
        foreach ($master_data as $data_node) {
            if (isset($data_node['directorate']))
                $data['directorates'][] = $data_node['directorate'];
            if (isset($data_node['category']))
                $data['categories'][] = $data_node['category'];
            if (isset($data_node['subcategory']))
                $data['subcategories'][] = $data_node['subcategory'];
            if (isset($data_node['name']))
                $data['services'][] = (isset($data_node['name']) ? $data_node['name'] : null);
            if (isset($data_node['specialities']))
                $data['specialities'][] = $data_node['specialities'];
            if (isset($data_node['min_standards']))
                $data['min_standards'][] = $data_node['min_standards'];
            if (isset($data_node['contactable']))
                $data['contactable'][] = $data_node['contactable'];
            if (isset($data_node['operations']))
                $data['operations'][] = $data_node['operations'];
        }
        return $data;
    }

    protected function validatedDataFromMasterData($reference_data, $master_check) {
        $data = [];
        foreach ($reference_data as $data_node) {
            $specialities = [];
            if (isset($data_node['specialities'])) {
                $specialities = explode(',', $data_node['specialities']);
                foreach ($specialities as $speciality) {
                    if (in_array($speciality, $master_check['specialities']))
                        $data_node['check_speciality'][] = $speciality;
                }
            }
            if (in_array($data_node['directorate'], $master_check['directorates']) &&
                    in_array($data_node['category'], $master_check['categories']) &&
                    in_array($data_node['subcategory'], $master_check['subcategories']) &&
                    in_array($data_node['name'], $master_check['services']) &&
                    in_array(strtolower($data_node['contactable']), $master_check['contactable']) &&
                    in_array(strtolower($data_node['min_standards']), $master_check['min_standards']) &&
                    in_array(strtolower($data_node['operations']), $master_check['operations'])
            )
                $data[] = $data_node;
        }
        return $data;
    }

    protected function getMasterDataWithId($directorates) {
        $data = [];
        foreach ($directorates as $directorate) {
            $data['directorate'][$directorate->id] = $directorate->name;
            foreach ($directorate->speciality as $speciality) {
                $data['speciality'][$directorate->id][$speciality->id] = $speciality->name;
            }
            foreach ($directorate->category as $category) {
                $data['category'][$directorate->id][$category->id] = $category->name;
                foreach ($category->sub_category as $subcategory) {
                    $data['subcategory'][$directorate->id][$subcategory->id] = $subcategory->name;
                }
            }
            foreach ($directorate->services as $service) {
                $data['service'][$directorate->id][$service->id] = $service->name;
            }
        }
        return $data;
    }

    protected function validateDataFromDbase($data, $check_data) {
        $result = [];
        foreach ($data as $node) {
            $specialities = null;
            $directorate_id = $category_id = $subcategory_id = $service_id = 0;
            $directorate_id = ((array_search($node['directorate'], $check_data['directorate']) ? array_search($node['directorate'], $check_data['directorate']) : null));
            if ($directorate_id) {
                $category_id = ((strtolower($node['category']) != 'n/a' && array_search($node['category'], $check_data['category'][$directorate_id]) ? array_search($node['category'], $check_data['category'][$directorate_id]) : 0));
                if ($category_id != 0) {
                    $subcategory_id = ((strtolower($node['subcategory']) != 'n/a' && array_search($node['subcategory'], $check_data['subcategory'][$category_id]) ? array_search($node['subcategory'], $check_data['subcategory'][$category_id]) : 0));
                }
                $service_id = ((array_search($node['name'], $check_data['service'][$directorate_id]) ? array_search($node['name'], $check_data['service'][$directorate_id]) : 0));
                if (isset($node['check_speciality'])) {
                    foreach ($node['check_speciality'] as $speciality) {
                        if (array_search($speciality, $check_data['speciality'][$directorate_id])) {
                            $specialities[] = array_search($speciality, $check_data['speciality'][$directorate_id]);
                        }
                    }
                }
                $result [] = [
                    'directorate_id' => $directorate_id,
                    'category_id' => $category_id,
                    'sub_category_id' => $subcategory_id,
                    'service_id' => $service_id,
                    'name' => $node['name'],
                    'contactable' => ($node['contactable'] && strtolower($node['contactable']) == 'yes' ? 1 : 0),
                    'contactable' => ($node['min_standards'] && strtolower($node['min_standards']) == 'yes' ? 1 : 0),
                    'specialities' => $specialities,
                    'template' => null,
                    'operation' => $node['operations']
                ];
            }
        }
        return $result;
    }

    protected function storeMaserData($store_data) {
        $error_count = $success_count = 0;
        foreach ($store_data as $node) {
            switch ($node['operation']) {
                case 'create':
                    if ($node['service_id'] == 0) {
                        $new = \App\DirectorateService::create($node);
                        if ($new)
                            $success_count++;
                        else
                            $error_count++;
                        break;
                    }
                    $error_count++;
                    break;


                case 'update':
                    $record = \App\DirectorateService::find($node['service_id']);
                    $update = $record->update($node);
                    if ($update)
                        $success_count++;
                    else
                        $error_count++;
                    break;

                case 'restore':
                    $record = \App\DirectorateService::onlyTrashed()->find($node['service_id']);
                    if (!$record) {
                        $error_count++;
                        break;
                    }
                    $restore = $record->restore();
                    if ($restore)
                        $success_count++;
                    else
                        $error_count++;
                    break;

                case 'delete':
                    $record = \App\DirectorateService::find($node['service_id']);
                    if (!$record) {
                        $error_count++;
                        break;
                    }
                    $delete = $record->delete();
                    if ($delete)
                        $success_count++;
                    else
                        $error_count++;
                    break;


                default:
                    if ($node['service_id'][$i] != 0) {
                        $record = \App\DirectorateService::find($node['service_id']);
                        $update = $record->update($node);
                        if ($update)
                            $success_count++;
                        else
                            $error_count++;
                        break;
                    }
                    $error_count++;
                    break;
            }
        }
       return [$error_count, $success_count];

    }

}
