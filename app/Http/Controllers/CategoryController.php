<?php

namespace App\Http\Controllers;

use File;
use App\Category;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Controllers\ServiceController;

class CategoryController extends Controller {

    protected function validateCategory(array $data) {
        return Validator::make($data, [
                    'name' => 'required|unique_with:tb_category, directorate_id',
                    'directorate_id' => 'required|integer',
        ]);
    }

    public function index() {
        return response()->json(\App\Category::with('sub_category')->get());
    }

    public function store(Request $request) {
        $validator = $this->validateCategory($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = \App\Category::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function show($id) {
        $record = Category::with('sub_category')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['error' => 'unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateCategory($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = \App\Category::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['record updated successfully', $request->all(), $record]);
                else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {
        $record = \App\Category::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $directorate_array = $category_array = $store_data = $result = [];
        $directorates = \App\Directorate::with('category')->where('trust_id', $request->trust_id)->get();
        foreach ($directorates as $directorate) {
            $directorate_array[$directorate->id] = $directorate->name;
            foreach ($directorate->category as $category) {
                $category_array[$directorate->id][$category->id] = $category->name;
            }
        }
        foreach ($request->data as $data) {
            $directorate_id = null;
            if (array_search($data['directorate'], $directorate_array))
                $directorate_id = array_search($data['directorate'], $directorate_array);
            if ($directorate_id) {
                if (isset($category_array[$directorate_id])) {
                    if (!array_search($data['name'], $category_array[$directorate_id])) {
                        $store_data [] = ['directorate_id' => $directorate_id, 'name' => $data['name']];
                    }
                } else {
                    $store_data [] = ['directorate_id' => $directorate_id, 'name' => $data['name']];
                }
            }
        }
        foreach ($store_data as $node) {
            $result [] = \App\Category::create($node);
        }
        if (count($result))
            return response(['success' => count($result) . ' Records added successfully']);
        else
            return response(['error' => 'No record added.']);
    }

}
