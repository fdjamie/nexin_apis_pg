<?php

namespace App\Http\Controllers;

use File;
use App\RotaTemplate;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RotaTemplateController extends Controller {

    protected function validateRotaTemplate(array $data, $id = null) {
        return Validator::make($data, [
                    'directorate_id' => 'required',
                    'name' => 'required|unique_with:tb_rota_template,directorate_id,' . $id,
                    'content' => 'required|array'
        ]);
    }

    public function index() {
        return response()->json(RotaTemplate::all());
    }

    public function store(Request $request) {
        $validator = $this->validateRotaTemplate($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = RotaTemplate::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function show($id) {
        $record = RotaTemplate::with('directorate', 'rota_group.shift_type')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['error' => 'unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateRotaTemplate($request->all(),$id);
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = \App\RotaTemplate::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['success' => 'record updated successfully']);
                else
                    return response()->json(['error' => 'changes not updated']);
            } else
                return response()->json(['error' => 'unable to find record']);
        }
    }

    public function destroy($id) {
        $record = RotaTemplate::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'record deleted successfully']);
            } else {
                return response()->json(['error' => 'record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'unable to find record']);
        }
    }

}
