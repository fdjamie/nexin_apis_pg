<?php

namespace App\Http\Controllers;

use File;
use App\CategoryPriority;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Controllers\ServiceController;

class CategoryPriorityController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedCategoryPriority(array $data, $id = null) {
        $rules['directorate_speciality_id'] = 'required|integer';
        if ($id == null)
            $rules['directorate_speciality_id'] = 'required|integer|unique_with:tb_category_priority,type_id,type';
//        $rules['type_id'] = 'required|integer';
//        $rules['priority'] = 'required|integer';
        return Validator::make($data, $rules);
    }

    public function index() {
        return response()->json(CategoryPriority::all());
    }

    public function store(Request $request) {
        $validator = $this->validatedCategoryPriority($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        } else {
            $prioroity = new CategoryPriority($request->all());
            $type = $this->getType($request->type, $request->type_id);
            $new = $type->priority()->save($prioroity);
            if ($new)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record added successfully.']);
        }
    }

    public function show($id) {
        $record = CategoryPriority::with('speciality.service', 'speciality.directorate.services', 'category.service', 'category.directorate_service', 'subcategory.service', 'subcategory.directorate_service', 'service.category', 'service.sub_category', 'general_service.sub_category')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['Unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validatedCategoryPriority($request->all(), $id);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = CategoryPriority::find($id);
            if ($record) {
                if ($request->priority > $record->priority) {
                    $decrement = CategoryPriority::where('directorate_speciality_id', $record->directorate_speciality_id)->whereBetween('priority', [$record->priority, $request->priority])->decrement('priority');
                    $record->update($request->all());
                    if ($record && $decrement)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'No changes updated']);
                } else if ($request->priority < $record->priority) {

                    $increment = CategoryPriority::where('directorate_speciality_id', $record->directorate_speciality_id)->where('priority', '>=', $request->priority)->where('priority', '<', $record->priority)->increment('priority');
                    $record->update($request->all());
                    if ($record && $increment)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'No changes updated']);
                }
                else if ($request->priority == $record->priority) {
                    $record->update($request->all());
                    if ($record)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'No changes updated']);
                }
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroy($id) {
        $record = CategoryPriority::find($id);
        if ($record) {
            $decrement = CategoryPriority::where('priority', '>', $record->priority)->decrement('priority');
            $delete = $record->delete();
//            if ($decrement && $delete) {
            if ($delete) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function updateRequirementOrder(Request $request) {
        $result = [];
        $count = 0;
        foreach ($request->requirement_order as $index => $value) {
            $record = CategoryPriority::find($index);
            if ($record) {
                $update = $record->update(['requirement_order' => $value]);
                if ($update)
                    $count++;
            }
        }
        if (count($count))
            return response()->json(['success' => $count . ', Priority sets added.']);
        else
            return response()->json(['error' => 'No record updated.']);
    }

    public function getCategoryPriorityByDirectorate($id) {
//        $records = \App\Directorate::with(['speciality.category_priority.category', 'speciality.category_priority.subcategory', 'speciality.category_priority.service', 'speciality.category_priority.general_service', 'category.sub_category', 'speciality.priority_set'=>function($q){$q->orderBy('level','asc');}])->where('trust_id', $id)->get();
        $records = \App\Directorate::with(['speciality.category_priority.category', 'speciality.category_priority.subcategory', 'speciality.category_priority.service', 'speciality.category_priority.general_service', 'category.sub_category', 'speciality.priority_set.priorities'=>function($q){$q->orderBy('level','asc');}])->where('trust_id', $id)->get();
        return $records;
    }

    public function getCategoryPriorityBySpeciality($id) {
        $records = CategoryPriority::with('category','subcategory','service','general_service', 'speciality.service.requirement')->where('directorate_speciality_id', $id)->orderBy('priority', 'asc')->get();
        return $records;
    }

    //############################### PROTECTED FUNCTION ##########################
    protected function getType($type, $id) {
        if ($type == 'category')
            $type = \App\Category::find($id);
        else if ($type == 'subcategory')
            $type = \App\SubCategory::find($id);
        else if ($type == 'service')
            $type = \App\Service::find($id);
        else
            $type = \App\DirectorateService::find($id);

        return $type;
    }

}
