<?php

namespace App\Http\Controllers;

use File;
use App\DirectorateStaff;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StaffServiceRequest;
use App\DirectorateStaffService;

class DirectorateStaffController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function directorateStaffValidate(array $data) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'directorate_speciality_id' => 'required|integer',
                    'grade_id' => 'required|integer',
                    'role_id' => 'required|integer',
                    'name' => 'required|max:64',
                    'gmc' => 'required',
                    'individual_bleep' => 'required',
                    'email' => 'required|email',
                    'mobile' => 'required',
                    'profile_pic' => 'image|mimes:jpeg,bmp,png',
                    'appointed_from' => 'date|date_format:Y-m-d',
                    'appointed_till' => 'date|date|date_format:Y-m-d',
        ]);
    }

    protected function assignStaffValidate(array $data) {
        return Validator::make($data, [
                    'directorate_staff_id' => 'required|integer|deletedatcheck:tb_directorate_staff_service,directorate_staff_id,' . request()->directorate_staff_id . ',service_id,' . request()->service_id,
                    'service_id' => 'required|integer',
        ]);
    }

    protected function staffAvailabilityValidate(array $data) {
        $validator = Validator::make($data, [
                    'directorate_staff_id' => 'required|integer|unique_with:tb_staff_availability,date,transition_time_id',
                    'transition_time_id' => 'required|integer',
                    'date' => 'required|date',
                    'day' => 'required|integer',
                    'week' => 'integer',
                    'start' => 'required|string',
                    'stop' => 'required|string',
        ]);
        $validator->getPresenceVerifier()->setConnection('pgsql2');
        return $validator;
    }

    public function index($id) {
        return response()->json(DirectorateStaff::with('grade', 'role', 'post')->where('directorate_id', $id)->get());
    }

    public function showAllDirectorateStaff($id) {
        return response()->json(\App\Trust::find($id)->directorate()->with('staff.role', 'staff.grade', 'staff.directorate_speciality')->get());
    }

    public function store(Request $request) {
        $directorate_staff_two = $this->getDirectorateStaffTwo();
        if (isset($request->qualifications)) {
            $request->merge(['qualifications' => json_decode($request->qualifications)]);
        } else
            $request->merge(['qualifications' => null]);
        if (!isset($request->appointed_till) || $request->appointed_till == '')
            $request->merge(['appointed_till' => null]);
        $response = array();
        if (count($request->get('staff')) > 1) {
            foreach ($request->all() as $record) {
                $validator = $this->directorateStaffValidate($request->all());
                if ($validator->fails()) {
                    return ['error' => $validator->messages()];
                }
            }
        } else {
            if (Input::hasFile('profile_pic')) {
                $upload_result = Storage::disk('staff_profile')->put(Input::file('profile_pic')->getClientOriginalName(), File::get(Input::file('profile_pic')));
            } else {
                $upload_result = FALSE;
            }
            if ($upload_result) {
                $name = Input::file('profile_pic')->getClientOriginalName();
            } else {
                $name = 'default.png';
            }
            unset($request->profile_pic);
            $request->merge(['profile_pic' => $name]);
            $validator = $this->directorateStaffValidate($request->all());
            if ($validator->fails()) {
                return ['error' => $validator->messages()];
            }
            $staff_data = $request->all();
            $staff_data['profile_pic']= $name;
            $new = DirectorateStaff::create($staff_data);
            $new2 = $directorate_staff_two->create($staff_data);
            if ($new && $new2)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record is not entered']);
        }
        return response()->json(count($request->all()));
    }

    public function show($id) {
        $record = DirectorateStaff::with('grade', 'role')->find($id);
        if ($record)
            return response()->json($record);
        else
            return response()->json(['error' => 'No record found.']);
    }

    public function update(Request $request, $id) {
        $directorate_staff_two = $this->getDirectorateStaffTwo();
        $validator = $this->directorateStaffValidate($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $request->merge(['qualifications' => ['qualification_id' => $request->qualifications, 'public' => (isset($request->qualifications_public) ? $request->qualifications_public : 0)]]);
            $record = DirectorateStaff::find($id);
            $record2 = $directorate_staff_two->find($id);
            if ($record) {
                if (Input::hasFile('profile_pic')) {
                    $upload_result = Storage::disk('staff_profile')->put(Input::file('profile_pic')->getClientOriginalName(), File::get(Input::file('profile_pic')));
                    $record->profile_pic = Input::file('profile_pic')->getClientOriginalName();
                }
                if ($record->update($request->all()) && $record2->update($request->all())) {
                    return response()->json(['Record updated successfully']);
                }
            } else {
                return response()->json(['Record not found']);
            }
        }
    }

    public function destroy($id) {
        $directorate_staff_two = $this->getDirectorateStaffTwo();
        $record = DirectorateStaff::find($id);
        $record2 = $directorate_staff_two->find($id);
        if ($record) {
            if ($record->delete() && $record2->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $data_store = [];
        $data_store = $this->getDataArray($request->data, $request->trust_id, $request->update_flag);
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
        else {
            if ($request->update_flag == 1) {
                $records = $this->createOrUpdateStaff($data_store);
                return response()->json(['success' => count($records) . ' Records created or updated successfully.']);
            } else {
                $new = DirectorateStaff::insert($data_store);
                if ($new)
                    return response()->json(['success' => count($data_store) . " Records added successfully"]);
            }
        }
    }

    public function export($id) {
        return response()->json(\App\Directorate::with('staff.grade', 'staff.role', 'staff.post', 'staff.directorate_speciality')->where('trust_id', $id)->get());
    }

    public function assignStaffService(Request $request) {
        $validator = $this->assignStaffValidate($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = \App\DirectorateStaffService::create($request->all());
            if ($record)
                return response()->json(['success' => 'Staff assigned successfully to service.']);
            else
                return response()->json(['error' => "couldn't assign staff to service."]);
        }
    }

    public function getAssignStaff() {
        $data = DirectorateStaffService::with('staff', 'service')->whereDate('created_at', '=', Carbon::today()->format('Y-m-d'))->get();
        return $data;
    }

    public function updateAssignStaff(Request $request) {
        $record = \App\DirectorateStaffService::find($request->assign_id);
        if ($record) {
            $record->end = $request->end;
            $record->change = 1;
            if ($record->save())
                return response()->json(['success' => 'Staff unassigned from service succesfully']);
            else
                return response()->json(['error' => 'Cannot unassign staff from service']);
        }else {
            return response()->json(['error' => 'No record found']);
        }
    }

    public function availability(Request $request) {
        $store_staff = $errors = $all_staff = [];
        $new = 0;
        $all_staff = $this->formatAvailableStaff($request->all());
        list($store_staff, $errors) = $this->validationAvailableStaff($all_staff);
        foreach ($store_staff as $staff) {
            if (\App\StaffAvailabilityTwo::create($staff))
                $new++;
        }
        if ($new > 0)
            return ['new' => $new++];
        else
            return ['error' => count($errors)];
    }

    //############################# PROTECTED FUNCTION ###############################

    protected function getQualificationArray() {
        $result = [];
        $qualifications = \App\Qualification::all();
        foreach ($qualifications as $qualification) {
            $result[$qualification->id] = $qualification->name;
        }
        return $result;
    }

    protected function getDataToStore($directorate, $speciality, $grade, $role, $data_node, $store_qualifications, $mobile, $bleep) {
        $data_store = [
            'name' => $data_node['name'],
            'directorate_id' => $directorate,
            'directorate_speciality_id' => $speciality,
            'grade_id' => $grade,
            'role_id' => $role,
            'gmc' => $data_node['gmc'],
            'qualifications' => $store_qualifications,
            'individual_bleep' => $data_node['individual_bleep'] . '^' . $bleep,
            'email' => $data_node['email'],
            'mobile' => $data_node['mobile'] . '^' . $mobile,
            'profile_pic' => 'default.png',
            'appointed_from' => $data_node['appointed_from'],
            'appointed_till' => $data_node['appointed_till'],
        ];
        return $data_store;
    }

    protected function getValidationData($trust_id) {
        $directorates = \App\Directorate::with('speciality', 'grade', 'role', 'staff')->where('trust_id', $trust_id)->get();
        $directorate_array = $this->getDirectorate($trust_id, $directorates);
        $speciality_array = $this->getValidationArray($directorates, 'speciality', 'id');
        $grade_array = $this->getValidationArray($directorates, 'grade', 'id');
        $role_array = $this->getValidationArray($directorates, 'role', 'id');
        $staff_array = $this->getValidationArray($directorates, 'staff', 'gmc');
        $qualification_array = $this->getQualificationArray();
        return [$directorate_array, $speciality_array, $role_array, $grade_array, $staff_array, $qualification_array];
    }

    protected function getDirectorate($trust_id, $directorates) {
        $result = [];
        foreach ($directorates as $directorate) {
            $result[$directorate->id] = $directorate->name;
        }
        return $result;
    }

    protected function getValidationArray($nodes, $subnode, $attribute) {
        $result = [];
        foreach ($nodes as $node) {
            if (isset($node->$subnode) && count($node->$subnode)) {
                foreach ($node->$subnode as $node_data) {
                    $result[$node->id][$node_data->$attribute] = $node_data->name;
                }
            }
        }
        return $result;
    }

    protected function getDataArray($data, $trust_id, $update_flag) {
        $data_store = [];
        list($directorate_array, $speciality_array, $role_array, $grade_array, $staff_array, $qualification_array) = $this->getValidationData($trust_id);
        foreach ($data as $data_node) {
            $qualifications = [];
            $staff_qualifications = explode(',', $data_node['qualifications']);
            foreach ($staff_qualifications as $qualification) {
                $qualifications [] = array_search($qualification, $qualification_array);
            }
            $directorate = array_search($data_node['directorate'], $directorate_array);
            if ($directorate) {
                $speciality = array_search($data_node['speciality'], $speciality_array[$directorate]);
                $grade = array_search($data_node['grade'], $grade_array[$directorate]);
                $role = array_search($data_node['role'], $role_array[$directorate]);
                $gmc = array_search($data_node['name'], $staff_array[$directorate]);
                $store_qualifications = ['qualifications' => $qualifications, 'public' => (strcasecmp($data_node['qualifications_public'], 'Yes') == 0 ? 1 : 0)];
                $bleep = (strcasecmp($data_node['individual_bleep_public'], 'Yes') == 0 ? 1 : 0);
                $mobile = (strcasecmp($data_node['mobile_public'], 'Yes') == 0 ? 1 : 0);
                if ($speciality && $grade && $role) {
                    if ($update_flag == 0 && $gmc != $data_node['gmc'])
                        $data_store[] = $this->getDataToStore($directorate, $speciality, $grade, $role, $data_node, json_encode($store_qualifications), $mobile, $bleep);
                    if ($update_flag == 1 && $gmc == $data_node['gmc'])
                        $data_store[] = $this->getDataToStore($directorate, $speciality, $grade, $role, $data_node, $store_qualifications, $mobile, $bleep);
                }
            }
        }
        return $data_store;
    }

    protected function createOrUpdateStaff($staffs) {
        $records = [];
        foreach ($staffs as $staff) {
            $records [] = DirectorateStaff::updateOrCreate(['directorate_id' => $staff['directorate_id'], 'gmc' => $staff['gmc']], [
                        'name' => $staff['name'],
                        'directorate_id' => $staff['directorate_id'],
                        'directorate_speciality_id' => $staff['directorate_speciality_id'],
                        'grade_id' => $staff['grade_id'],
                        'role_id' => $staff['role_id'],
                        'gmc' => $staff['gmc'],
                        'qualifications' => $staff['qualifications'],
                        'individual_bleep' => $staff['individual_bleep'],
                        'email' => $staff['email'],
                        'mobile' => $staff['mobile'],
                        'profile_pic' => $staff['profile_pic'],
                        'appointed_from' => $staff['appointed_from'],
                        'appointed_till' => $staff['appointed_till'],
            ]);
        }
        return $records;
    }

    public function getDirectorateStaffTwo() {
        $directorate_staff_two = new \App\DirectorateStaffTwo();
        $directorate_staff_two->setConnection('pgsql2');
        return $directorate_staff_two;
    }

    protected function validationAvailableStaff($all_staff) {
        $errors = $store_staff = [];
        foreach ($all_staff as $staff) {
            $validator = $this->staffAvailabilityValidate($staff);
            if (($validator->fails()))
                $errors [] = $validator->messages();
            else {
                $store_staff[] = $staff;
            }
        }
        return [$store_staff, $errors];
    }

    protected function formatAvailableStaff($all_data) {
        $all_staff = [];
        foreach ($all_data as $time_id => $values) {
            foreach ($values as $data) {
                $all_staff[] = [
                    'directorate_staff_id' => $data['id'],
                    'transition_time_id' => $time_id,
                    'date' => $data['date'],
                    'day' => $data['day'],
                    'week' => $data['week'],
                    'start' => $data['start'],
                    'stop' => $data['stop'],
                ];
            }
        }
        return $all_staff;
    }

}
