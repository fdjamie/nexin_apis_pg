<?php

namespace App\Http\Controllers;

use File;
use App\Trust;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TrustController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateTrust(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:64|unique:tb_trust',
        ]);
    }

    public function index() {
         return response()->json(Trust::all());
    }

    public function store(Request $request) {
        $validator = $this->validateTrust($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = Trust::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function show($id) {
        $record = \App\Trust::with('division.directorate.staff.post', 'division.directorate.speciality.staff.post', 'division.directorate.speciality.staff.grade', 'division.directorate.speciality.staff.role', 'division.directorate.speciality.post.rotation.rota_template', 'division.directorate.speciality.post.rotation.staff', 'division.directorate.speciality.post.rota_template', 'division.directorate.speciality.post.staff', 'division.directorate.speciality.service', 'division.directorate.speciality.directorate', 'division.directorate.speciality.additional_grade', 'division.directorate.rota_group.rota_template', 'division.directorate.rota_group.shift_type', 'division.directorate.rota_template', 'site.location', 'division.directorate.grade.role', 'division.directorate.role')->find($id);

        if ($record) {
            return response()->json($record);


        } else
            return response()->json((object)['error'=>'Unable to find record']);
    }


    public function update(Request $request, $id) {
        $validator = $this->validateTrust($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = Trust::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['record updated successfully']);
                else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {

        $record = Trust::find($id);

        if ($record) {

            if ($record->delete()) {
                return response()->json(['status' => 'ok']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $data_store = [];
        foreach ($request->data as $data_node) {
            if (!isset($data_node['name']))
                $data_node['name'] = '';
            $trust = Trust::where('name', $data_node['name'])->get();
            if (!count($trust) && $data_node['name'] != '') {
                $data_store [] = ['name' => $data_node['name']];
            }
        }
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
        $new = Trust::insert($data_store);
        if ($new)
            return response()->json(['success' => "Records added successfully"]);
    }


    public function getTrusRoles($trustId)
    {
        $response = array();
        try {

            $get=Trust::find($trustId)->trustRoles;
            return response()->json(['status' => true, 'data' =>$get ]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => [trans('messages.error')]]);
        }

    }


}
