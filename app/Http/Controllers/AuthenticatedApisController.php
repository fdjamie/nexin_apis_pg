<?php

namespace App\Http\Controllers;

use App\PostDetails;
use App\RoleTrust;
use App\UserTrust;
use App\UserTrustRoles;
use File;
use http\Env\Response;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Category;
use App\Directorate;
use DB;
use App\Jobs\SendEmail;

class AuthenticatedApisController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    public function index() {
        return response()->json([]);
    }

//    ##################################################  PROTECTED FUNCTIONS HERE ##################################################  
    protected function getCarbonYmdFormat($date) {
        $new_date = new Carbon($date);
        $new_date = $new_date->format('Y-m-d');
        return $new_date;
    }

    protected function getService($record) {
        $data = [];
        if (isset($record['directorate']) && $record['speciality'] && $record['name']) {
            $data['directorate'] = $record['directorate'];
            $data['speciality'] = $record['speciality'];
            $data['category'] = isset($record['category']) ? $record['category'] : "n/a";
            $data['subcategory'] = isset($record['subcategory']) ? $record['subcategory'] : "n/a";
            $data['name'] = $record['name'];
            $data['contactable'] = isset($record['contactable']) ? $record['contactable'] : "n/a";
            $data['min_standards'] = isset($record['min_standards']) ? $record['min_standards'] : "n/a";
        }

        return $data;
    }

    protected function getTemplate($record) {
        $data = NULL;
        if (
                isset($record['start_date']) &&
                isset($record['start_date']) &&
                isset($record['cycle_length']) &&
                isset($record['starting_week'])
        ) {
            $data['start_date'] = $record['start_date'];
            $data['end_date'] = $record['end_date'];
            $data['cycle_length'] = $record['cycle_length'];
            $data['starting_week'] = $record['starting_week'];
        }
        return $data;
    }

    protected function getEvent($record, $id) {
        $event = NULL;
        if (
                isset($record['week_no']) &&
                isset($record['start_day']) &&
                isset($record['start_time']) &&
                isset($record['duration']) &&
                isset($record['site_id'])
        ) {

            $event['id'] = $id;
            $event['week_no'] = $record['week_no'];
            $event['day'] = $record['start_day'];
            $event['start'] = $record['start_time'];
            $event['duration'] = $record['duration'];
            $event['site_id'] = $record['site_id'];
            if (isset($record['location_id']))
                $event['location_id'] = $record['location_id'];
        }
        return $event;
    }

    protected function getEventFormated($record) {
        $event = NULL;
        if (
                isset($record['week_no']) &&
                isset($record['start_day']) &&
                isset($record['start_time']) &&
                isset($record['duration']) &&
                isset($record['site'])
        ) {
            $event['week_no'] = $record['week_no'];
            $event['day'] = $record['start_day'];
            $event['start'] = $record['start_time'];
            $event['duration'] = $record['duration'];
            $event['site'] = $record['site'];
            if (isset($record['locations']))
                $event['locations'] = $record['locations'];
        }
        return $event;
    }

    protected function getData($record) {
        $data = [];

        if (isset($record['directorate_speciality_id']) && isset($record['name'])) {
            $data['directorate_speciality_id'] = $record['directorate_speciality_id'];
            $data['category_id'] = isset($record['category_id']) ? $record['category_id'] : 0;
            $data['sub_category_id'] = isset($record['sub_category_id']) ? $record['sub_category_id'] : 0;
            $data['name'] = $record['name'];
            $data['contactable'] = (($record['contactable'] == 'Yes') ? 1 : 0);
            $data['min_standards'] = (($record['min_standards']) ? 1 : 0);
            $data['template'] = $record['template'];
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['updated_at'] = date("Y-m-d h:i:s");
        }
        return $data;
    }

//    ##################################################  USER CRUD APIs HERE ##################################################  
    protected function userValidate(array $data) {
        return Validator::make($data, [
                    'is_admin' => 'required|boolean',
                    'trust_id' => 'required|integer',
                    'name' => 'required|max:255',
        ]);
    }

    public function showAllUsers() {
        $records = \App\User::with('trust')->get();
        return response()->json($records);
    }

    public function getTrustUsers($trustId) {
        /* $records = \App\Trust::with('users')->where('id',$trustId)->get(); */
        /* $records = \App\Trust::with('users')->where('id',$trustId)->get(); */
        $records = \App\UserTrust::with('user', 'userTrustRules.roles')->where('trust_id', $trustId)->get();
        /* $records = \App\Trust::with('users.userTrusts.userTrustRules.roles')->where('id',$trustId)->get(); */
        return response()->json($records);
    }

    /* public function editUser($id)
      {


      $record = \App\User::find($id);
      if ($record)
      return response()->json($record);
      else
      return response()->json(['unable to find record']);
      } */

    public function editUser($id) {
        $data = [];
        try {

            $record = \App\UserTrust::with('user', 'userTrustRules.roles')->find(base64_decode($id));
            if (!$record) {
                return response()->json(['status' => false, 'msg' => 'Record Not Found']);
            }
            $data = $record->user;
            $userRoles = $record->userTrustRules;
            foreach ($userRoles as $k => $uR) {
                $roles[$k] = $uR->roles->id;
            }
            $data['rolesIds'] = $roles;
            return response()->json(['status' => true, 'data' => $data]);
        } catch (\Exception $e) {


            return response()->json(['status' => false, 'msg' => trans('messages.error')]);
        }
    }

    public function updateUser(Request $request, $id) {
        $validator = $this->validateUpdateUser($request->all());
        if (!$validator->fails()) {
            $user = $this->updateTrustUser($request, $id);
            if ($user) {
                return response()->json(['user' => $user, 'status' => 'Successfully Updated']);
            } else {
                return response()->json(['error' => 'yes', 'messages' => ['Error Occure Try Again'], 'status' => 'Error Occur! Try Again']);
            }
        } else {

            return response()->json(['error' => 'yes', 'messages' => $validator->messages(), 'status' => 'Error Occur! Try Again.']);
        }
    }

    public function updateTrustUser($request, $id) {

        try {
            DB::beginTransaction();
            $record = \App\User::find($id);
            $record->name = $request->name;
            $record->phone = $request->phone;
            if ($record->save()) {
                $trustRoles = $request->roles;
                $userTrustId = base64_decode($request->userTrustId);

                foreach ($trustRoles as $t) {

                    $add = UserTrustRoles::firstOrCreate(['user_trust_id' => $userTrustId, 'role_trust_id' => $t]);
                    if (!$add) {
                        DB::rollback();
                        return false;
                    }
                }

                UserTrustRoles::whereNotIn('role_trust_id', $trustRoles)->where('user_trust_id', $userTrustId)->delete();
                DB::commit();
                return true;
            } else {
                DB::rollback();
                return false;
            }
        } catch (\Exception $e) {
            /*   return response()->json($e); */
            return false;
        }
    }

    public function destroyUser($id) {

        /* $record = \App\User::find($id); */
        $record = \App\UserTrust::find($id);
        if ($record) {
            UserTrustRoles::where('user_trust_id', $id)->delete();

            if ($record->delete()) {
                $message = 'record deleted successfully';
            } else {
                $message = 'record is not deleted';
            }
        } else {
            $message = 'unable to find record';
        }
        return response()->json($message);
    }

//    ##################################################  SPECIALITY CRUD APIS Start HERE ##################################################  
    /*    public function createSpeciality() {
      $speciality = new \App\Speciality();
      $speciality->name = 'enter specialty name';
      $speciality->abbreviation = 'enter abbreviation name';
      return response()->json($speciality);
      } */

    protected function validateSpeciality(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:64',
                    'abbreviation' => 'required|max:32',
        ]);
    }

//    public function indexSpeciality() {
//        $result = \App\DirectorateSpeciality::all();
//        return response()->json($result);
//    }

    public function storeSpeciality(Request $request) {
        $validator = $this->validateSpeciality($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $new = \App\Speciality::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function showSpeciality($id) {
        $record = \App\Speciality::find($id);
        if ($record)
            return response()->json($record);
        else
            return response()->json(['unable to find record']);
    }

    public function updateSpeciality(Request $request, $id) {
        $validator = $this->validateSpeciality($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = \App\Speciality::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['record updated successfully']);
                else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroySpeciality($id) {
        $record = \App\Speciality::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['record deleted successfully']);
            } else {
                return response()->json(['record is not deleted']);
            }
        } else {
            return response()->json(['unable to find record']);
        }
    }

    protected function backupAsCSV($records, $name = '') {
        $data = array();
        if ($records) {
            $data[] = implode(',', array_keys($records[0]->getAttributes()));
            foreach ($records as $record) {
                $data[] = implode(',', $record->getAttributes());
            }
        }
        $response = Storage::disk('backup')->put(date("Y/m/d/") . $name . date("_His") . '.csv', implode(PHP_EOL, $data));
        return $response;
    }

    public function uploadSpeciality(Request $request) {
        $backup = $truncate = $result = FALSE;
        $records = \App\Speciality::all();
        if (count($records))
            $backup = $this->backupAsCSV($records, 'speciality');
        if ($request->get('replace') && count($records)) {
            $truncate = \App\Speciality::truncate();
        }
        $data = $request->get('data');
        return response()->json($data);
        die;
        return response()->json(['backup' => $backup, 'truncate' => $truncate, 'result' => $data->toArray()]);
        $result = \App\Speciality::insert($request->get('data'));
        if (!$records->isEmpty()) {
            $backup = $this->backupAsCSV($records, 'speciality');
        }
        if ($request->get('replace') && !$records->isEmpty()) {
            $truncate = \App\Speciality::truncate();
        }
        $result = array();
        foreach ($request->get('data') as $record) {
            if (is_array($record)) {
                $result[] = \App\Speciality::updateOrCreate($record);
            } else {
                $row = array();
                if (isset($record->id))
                    $row['id'] = $record->id;
                if (isset($record->name))
                    $row['name'] = $record->name;
                if (isset($record->abbreviation))
                    $row['abbreviation'] = $record->abbreviation;
                $result[] = \App\Speciality::updateOrCreate($row);
            }
        }
        return response()->json(['backup' => $backup, 'truncate' => $truncate, 'result' => $result]);
    }

//  ##################################################  TRUST APIS ENDED HERE ##################################################  

    public function showTrustDirectorates($id) {
        $directorates = array();
        $record = \App\Trust::with('division.directorate.speciality')->find($id);
        if ($record) {
            foreach ($record->division as $division) {
                foreach ($division->directorate as $directorate) {
                    array_push($directorates, $directorate);
                }
            }
            return response()->json($directorates);
        } else
            return response()->json(['unable to find record']);
    }

//    ##################################################  DIRECTORATE CRUD APIS Start HERE ##################################################

    public function trustDirectorates($id) {
        $directorates = \App\Directorate::with('trust', 'division', 'speciality')->where('trust_id', $id)->get();
        return response()->json($directorates);
    }

    /*    public function trustDirectorates($userId) {
      if(!(\App\User::find($userId))|| \App\User::find($userId)->trusts->isEmpty())
      {
      return response()->json(['unable to find record']);
      }
      $userTrustsIds=\App\User::find($userId)->trusts->pluck('id');

      $directorates = \App\Directorate::with('trust', 'division', 'speciality')->whereIn('trust_id', $userTrustsIds)->get();

      if ($directorates) {

      return response()->json($directorates);
      } else
      return response()->json(['unable to find record']);
      } */

//  ##################################################  DirectorateSpeciality APIS ENDED HERE ##################################################  

    public function getDirectorateSpeciality($id) {
        $result = \App\DirectorateSpeciality::with('speciality')->where('directorate_id', '=', $id)->get();
        return response()->json($result);
    }

//    ##################################################  STAFF CRUD APIS Start HERE ##################################################  

    public function validateStaff(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:64|',
                    'gmc' => 'required',
//                    'speciality_id' => 'required|integer',
                    'grade_id' => 'required|integer',
                    'role_id' => 'required|integer',
                    'qualifications' => 'required',
                    'individual_bleep' => 'required',
                    'email' => 'required|email',
                    'mobile' => 'required',
//                    'profile_pic' => 'required|image|mimes:jpeg,bmp,png',
                    'profile_pic' => 'image|mimes:jpeg,bmp,png',
        ]);
    }

    public function indexStaff() {
        return response()->json(\App\Staff::with('speciality', 'grade', 'role')->get());
    }

    public function storeStaff(Request $request) {
        $validator = $this->validateStaff($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            if (Input::hasFile('profile_pic')) {
                $upload_result = Storage::disk('staff_profile')->put(Input::file('profile_pic')->getClientOriginalName(), File::get(Input::file('profile_pic')));
            } else {
                $upload_result = FALSE;
            }
            if ($upload_result) {
                $name = Input::file('profile_pic')->getClientOriginalName();
            } else {
                $name = 'default.png';
            }
            $new = \App\Staff::create($request->all());
            $new->profile_pic = $name;
            $new->save();
            return response()->json(['status' => 'ok', 'data' => $new, 'upload_image' => $upload_result]);
        }
    }

    public function uploadStaff(Request $request) {
        $backup = $truncate = $result = FALSE;
        $data = $request->get('data');
        $data_to_store = array();
        foreach ($data as $data_node) {
            if (isset($data_node['created_at']['date'])) {
                $date = $data_node['created_at']['date'];
                unset($data_node['created_at']);
                $data_node['created_at'] = $date;
            }
            if (isset($data_node['updated_at']['date'])) {
                $date = $data_node['updated_at']['date'];
                unset($data_node['updated_at']);
                $data_node['updated_at'] = $date;
            }
            $grade = \App\Grade::where('name', $data_node['grade'])->get();
            $role = \App\Role::where('name', $data_node['role'])->get();
            $gmc = \App\Staff::where('gmc', $data_node['gmc'])->get();
            if ($grade->isEmpty() || $role->isEmpty())
                continue;
            $data_node['grade_id'] = $grade[0]->id;
            unset($data_node['grade']);
            $data_node['role_id'] = $role[0]->id;
            unset($data_node['role']);
            if ($request->get('replace')) {
                array_push($data_to_store, $data_node);
            } else {
                if ((count($gmc) == 0)) {
                    array_push($data_to_store, $data_node);
                }
            }
        }
        if (isset($data_to_store) && count($data_to_store)) {
            $records = \App\Staff::all();
            if (count($records))
                $backup = $this->backupAsCSV($records, 'staff');
            if ($request->get('replace') && count($records)) {
                $truncate = \App\Staff::truncate();
            }
            $result = array();
            foreach ($data_to_store as $record) {
                $result[] = \App\Staff::create($record);
            }
            return response()->json(['backup' => $backup, 'truncate' => $truncate, 'result' => $result]);
        } else {
            return response()->json(['error' => 'Plese check your uploaded file, There are some errors in uploaded data']);
        }
    }

    public function updateStaff(Request $request, $id) {
        $record = \App\Staff::find($id);
        if ($record) {
            if (Input::hasFile('profile_pic')) {
                $upload_result = Storage::disk('staff_profile')->put(Input::file('profile_pic')->getClientOriginalName(), File::get(Input::file('profile_pic')));
                $record->profile_pic = Input::file('profile_pic')->getClientOriginalName();
            }
            if ($record->update($request->all())) {
                return response()->json(['Record updated successfully']);
            }
        } else {
            return response()->json(['Record not found']);
        }
    }

    public function showStaff($id) {
        $record = \App\Staff::find($id);
        if ($record) {
            $record->trust;
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function destroyStaff($id) {
        $record = \App\Staff::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

//    ##################################################  STAFF CRUD APIS Start HERE ##################################################
    protected function validatedShift(array $data) {
        return Validator::make($data, [
                    'name' => 'max:32',
                    'start_time' => 'date_format:"H:i"',
                    'working_hours' => 'integer',
        ]);
    }

    public function indexShift() {
        return response()->json(\App\Shift::all());
    }

    public function storeShift(Request $request) {
        $validator = $this->validatedShift($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $new = \App\Shift::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function showShift($id) {
        $record = \App\Shift::find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function updateShift(Request $request, $id) {
        $validator = $this->validatedShift($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = \App\Shift::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['record updated successfully', $request->all(), $record]);
                else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroyShift($id) {
        $record = \App\Shift::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['record deleted successfully']);
            } else {
                return response()->json(['record is not deleted']);
            }
        } else {
            return response()->json(['unable to find record']);
        }
    }

//    ##################################################  GET-TREE Start HERE ##################################################
    public function getTreeDataByID($id) {


//        $record = \App\Trust::with('division.directorate.speciality', 'division.directorate.speciality.service','division.directorate.shift_type', 'division.directorate.speciality.post.rotation', 'division.directorate.speciality.post.post_detail.staff', 'site.location', 'shift', 'template')->find($id);
//        $record = \App\Trust::with('division.directorate.staff.member', 'division.directorate.speciality', 'division.directorate.speciality.service', 'division.directorate.rota_template.shift_type', 'division.directorate.speciality.post.rotation', 'division.directorate.speciality.post.post_detail.staff', 'site.location', 'shift', 'template')->find($id);
//        $record = \App\Trust::with('division.directorate.staff', 'division.directorate.speciality.staff.post', 'division.directorate.speciality.staff.grade', 'division.directorate.speciality.staff.role', 'division.directorate.speciality.service', 'division.directorate.rota_group', 'division.directorate.rota_template.shift_type', 'division.directorate.speciality.post.rotation', 'division.directorate.speciality.post.staff', 'site.location', 'shift', 'template')->find($id);
//        $record = \App\Trust::with('division.directorate.staff', 'division.directorate.speciality.staff.post', 'division.directorate.rota_group.rota_template', 'division.directorate.rota_group.shift_type', 'division.directorate.grade', 'division.directorate.role', 'division.directorate.rota_template', 'division.directorate.speciality.staff', 'division.directorate.speciality.service', 'division.directorate.speciality.post.rotation', 'division.directorate.speciality.post', 'division.directorate.speciality.additional_grade', 'site.location')->find($id);
        $record = \App\Trust::with('directorate.division', 'directorate.staff', 'directorate.services', 'directorate.speciality.staff.post', 'directorate.rota_group.rota_template', 'directorate.rota_group.shift_type', 'directorate.grade', 'directorate.role', 'directorate.rota_template', 'directorate.category.sub_category', 'directorate.group.subgroup', 'directorate.speciality.staff', 'directorate.speciality.service', 'directorate.speciality.post.rotation', 'directorate.speciality.post', 'directorate.speciality.additional_grade', 'site.location')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function getUserTrustsTree($userId, $tree = null) {
        if (!(\App\User::find($userId)) || \App\User::find($userId)->trusts->isEmpty()) {
            return response()->json(['status' => false, 'msg' => 'No Any Trust Found']);
        }
        $userTrustsIds = \App\User::find($userId)->trusts->pluck('id');
        if (!empty($tree)) {
            $record = \App\Trust::orderBy('id')->with('directorate.division', 'directorate.staff', 'directorate.services', 'directorate.speciality.staff.post', 'directorate.rota_group.rota_template', 'directorate.rota_group.shift_type', 'directorate.grade', 'directorate.role', 'directorate.rota_template', 'directorate.category.sub_category', 'directorate.group.subgroup', 'directorate.speciality.staff', 'directorate.speciality.service', 'directorate.speciality.post.rotation', 'directorate.speciality.post', 'directorate.speciality.additional_grade', 'site.location')->findMany($userTrustsIds);
        } else {
            /*  $record = \App\Trust::orderBy('id')->findMany($userTrustsIds) */;
            $record = \App\Trust::with(['userTrust' => function($query) use ($userId) {
                            $query->select('id', 'user_id', 'trust_id')->where('user_id', $userId);
                        }])->orderBy('id')->findMany($userTrustsIds);
        }
        if ($record) {
            return response()->json($record);
        } else
            return response()->json([['status' => false, 'msg' => 'No Any Trust Found']]);
    }

    public function getJSTreeData() {
        $trusts = \App\Trust::all();
        $trust_nodes = array();
        foreach ($trusts as $trust) {
            $division_nodes = $directorate_nodes = $site_nodes = array();
            $trust_data = \App\Trust::find($trust->id);
            if (count($trust_data->division)) {
                foreach ($trust_data->division as $division) {
                    $division_nodes[] = ['id' => "t{$trust->id}dv{$division->id}", 'text' => $division->name, 'icon' => "fa fa-medkit",];
                }
            }
            if (count($trust_data->directorate)) {
                foreach ($trust_data->directorate as $directorate) {
                    $directorate_nodes[] = ['id' => "t{$trust->id}di{$directorate->id}", 'text' => $directorate->name, 'icon' => 'fa fa-stethoscope'];
                }
            }
            if (count($trust_data->site)) {
                foreach ($trust_data->site as $site) {
//                    $location_nodes = array();
//                    $site_data = \App\Site::find($site->id);
//                    if (count($site_data->location)) {
//                        foreach ($site_data->location as $location) {
////                            $location_nodes[] = ['id' => "location-$location->id-$site->id", 'text' => " $location->name", 'icon' => 'fa fa-wheelchair-alt'];
//                            $location_nodes[] = ['text' => " $location->name"];
//                        }
//                    }
//                    $location_node = ['Location'];
                    $site_nodes[] = ['id' => "t{$trust->id}si{$site->id}", 'text' => " $site->name", 'icon' => "fa fa-heart-o",];
//                    'children' => ['id' => "location--$site->id", 'text' => " Location", 'icon' => "fa fa-map-marker", 'children' => [$location_nodes]]
                }
            }
            $trust_nodes[] = ['id' => "t{$trust->id}", 'text' => $trust->name, 'icon' => "fa fa-bed", 'children' => [
                    ['id' => "t{$trust->id}dv", 'text' => ' Division', 'children' => $division_nodes, 'icon' => "fa fa-ambulance"],
                    ['id' => "t{$trust->id}di", 'text' => ' Directorate', 'children' => $directorate_nodes, 'icon' => 'fa fa-user-md'],
                    ['id' => "t{$trust->id}si", 'text' => ' Sites', 'icon' => "fa fa-heart", 'children' => $site_nodes],
                ]
            ];
        }
        $specialities = \App\Speciality::all();
        $speciality_nodes = array();
        foreach ($specialities as $speciality) {
            $speciality_nodes[] = ['id' => "s{$speciality->id}", 'text' => $speciality->name, 'icon' => "fa fa-hospital-o"];
        }
        $divisions = \App\Division::all();
        $divisions_nodes = array();
        foreach ($divisions as $division) {
            $divisions_nodes[] = ['id' => "dv{$division->id}", 'text' => $division->name, 'icon' => "fa fa-medkit"];
        }

        $sites = \App\Site::all();
        $sites_nodes = array();
        foreach ($sites as $site) {
            $sites_nodes[] = ['id' => "dv{$site->id}", 'text' => $site->name, 'icon' => "fa fa-heart-o"];
        }

        $directorates = \App\Directorate::all();
        $directorates_nodes = array();
        foreach ($directorates as $directorate) {
            $directorates_nodes[] = ['id' => "di{$directorate->id}", 'text' => $directorate->name, 'icon' => "fa fa-stethoscope"];
        }

        return response()->json([
                    ['id' => 't', 'text' => ' Trust', 'icon' => "fa fa-home", 'children' => $trust_nodes,],
                    ['id' => 's', 'text' => ' Speciality', 'icon' => "fa fa-h-square", 'children' => $speciality_nodes],
                    ['id' => 'dv', 'text' => ' Division', 'icon' => "fa fa-ambulance", 'children' => $divisions_nodes],
                    ['id' => 'st', 'text' => ' Site', 'icon' => "fa fa-heart", 'children' => $sites_nodes],
                    ['id' => 'di', 'text' => ' Directorate', 'icon' => "fa fa-user-md", 'children' => $directorates_nodes],
        ]);
    }

//    ##################################################  TEMPLATES APIS Start HERE ##################################################
    protected function validatedRotationTemplate(array $data) {
        return Validator::make($data, [
                    'name' => 'max:32',
                    'contents' => 'array',
        ]);
    }

    public function indexRotationTemplate() {
        return response()->json(\App\Template::where('type', 'rotation')->get());
    }

    public function showRotationTemplate($id) {
        $record = \App\Template::find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function storeRotationTemplate(Request $request) {
        $validator = $this->validatedRotationTemplate($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $request->merge(['type' => 'rotation']);
            $new = \App\Template::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function updateRotationTemplate(Request $request, $id) {
        $validator = $this->validatedRotationTemplate($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = \App\Template::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['record updated successfully', $request->all(), $record]);
                else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroyTemplate($id) {
        $record = \App\Template::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['record deleted successfully']);
            } else {
                return response()->json(['record is not deleted']);
            }
        } else {
            return response()->json(['unable to find record']);
        }
    }

//    ##################################################  POST APIS Start HERE ##################################################

    public function showPosts($id) {
        $record = \App\Post::with('post_detail', 'post_detail.staff')->where('directorate_speciality_id', $id)->get();
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

//    ##################################################  STAFF APPOINTMENT APIS Start HERE ##################################################

    protected function validatePostDetails(array $data) {
        return Validator::make($data, [
                    'post_id' => 'required|integer',
                    'rota_template_id' => 'required|integer',
                    'assigned_week' => 'integer',
        ]);
    }

    public function storePostDetails(Request $request) {
        $validator = $this->validatePostDetails($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $new = \App\PostDetails::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function showPostDetails($id) {
        $record = \App\PostDetails::with('rota_template', 'post.directorate_speciality.directorate', 'staff')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function updatePostDetails(Request $request, $id) {
        $validator = $this->validatePostDetails($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = \App\PostDetails::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['record updated successfully']);
                else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
        return response()->json($request->all());
    }

//    ##################################################  SERVICES APIS Start HERE ##################################################

    public function uploadService(Request $request) {
        $data_store = $template = $events = $formated_data = [];
        $data = $request->data;
        $previous_service_name = NULL;
        $i = -1;
        foreach ($data as $data_node) {
            if ($previous_service_name != $data_node['name']) {
                $i++;
                $previous_service_name = $data_node['name'];
                $events = $template = NULL;
                $formated_data[$i] = $this->getService($data_node);
                $formated_data [$i]['template'] = $this->getTemplate($data_node);
            }
            $event = $this->getEventFormated($data_node);
            if ($formated_data [$i]['template'] != NULL && $event != NULL) {
                $formated_data [$i]['template']['events'][] = $event;
            }
        }
//        return [$formated_data];
        foreach ($formated_data as $data_node) {
            $specialities_store = [];
            $events = $template = $record = null;
            $category_id = $subcategory_id = $directorate_id = $speciality_id = 0;
            $service_flag = false;
            $directorate = \App\Directorate::where('name', $data_node['directorate'])->where('trust_id', $request->trust_id)->get();
            if (count($directorate)) {
                $directorate_id = $directorate[0]->id;
                $speciality = \App\DirectorateSpeciality::where('directorate_id', $directorate_id)->where('name', $data_node['speciality'])->get();
                if (count($speciality)) {
                    $speciality_id = $speciality[0]->id;
                    if ($data_node['category'] != 'N/A') {
                        $category = \App\Category::where('name', $data_node['category'])->where('directorate_id', $directorate_id)->get();
                        if (count($category))
                            $category_id = $category[0]->id;
                        if ($data_node['subcategory'] != 'N/A') {
                            $subcategory = \App\SubCategory::where('name', $data_node['subcategory'])->where('category_id', $category_id)->get();
                            if (count($subcategory)) {
                                $subcategory_id = $subcategory[0]->id;
                                $service = \App\Service::where('directorate_speciality_id', $speciality_id)->where('name', $data_node['name'])->where('category_id', $category_id)->where('sub_category_id', $subcategory_id)->get();
                                if (!count($service))
                                    $service_flag = true;
                            }
                        } else {
                            $service = \App\Service::where('directorate_speciality_id', $speciality_id)->where('name', $data_node['name'])->where('category_id', $category_id)->get();
                            if (!count($service))
                                $service_flag = true;
                        }
                    } else {
                        $service = \App\Service::where('directorate_speciality_id', $speciality_id)->where('name', $data_node['name'])->get();
                        if (!count($service))
                            $service_flag = true;
                    }
                }

                if ($data_node['template'] != Null) {
                    $id = 1;
                    $template['start_date'] = $data_node['template']['start_date'];
                    $template['end_date'] = $data_node['template']['end_date'];
                    $template['cycle_length'] = $data_node['template']['cycle_length'];
                    $template['starting_week'] = $data_node['template']['starting_week'];
                    foreach ($data_node['template']['events'] as $event) {
                        if (($event['week_no'] > 0 && $event['week_no'] <= $template['cycle_length']) &&
                                ($event['day'] >= 0 && $event['day'] <= 6) &&
                                (is_numeric($event['duration']))) {
                            $site = \App\Site::where('name', $event['site'])->where('trust_id', $request->trust_id)->get();
                            if (count($site)) {
                                $event['site_id'] = $site[0]->id;
                                $locations = $site[0]->location;
                                if (isset($event['locations'])) {
                                    $location_names = explode(',', $event['locations']);
                                    foreach ($location_names as $location_node) {
                                        foreach ($locations as $location) {
                                            if ($location->name == $location_node)
                                                $event['location_id'][] = $location->id;
                                        }
                                    }
                                }
                            }
                            $template['events'][] = $this->getEvent($event, $id);
                            $id++;
                        }
                    }
                }
                if ($service_flag) {
                    $data_node['directorate_speciality_id'] = $speciality_id;
                    $data_node['category_id'] = $category_id;
                    $data_node['sub_category_id'] = $subcategory_id;
                    $data_node['template'] = json_encode($template);
                    $data_store[] = $this->getData($data_node);
                }
            }
        }
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
        $new = \App\Service::insert($data_store);
        if ($new)
            return response()->json(['success' => "Records added successfully"]);
    }

    protected function validateServiceTemplate(array $data) {
        return Validator::make($data, [
                    'service_id' => 'required|integer',
                    'content' => 'array',
        ]);
    }

    public function indexServiceTemplate() {
        return response()->json(\App\ServiceTemplate::with('service')->get());
    }

    public function storeServiceTemplate(Request $request) {
        $validator = $this->validateServiceTemplate($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $service_id = $request->get('service_id');
            $service = \App\Service::find($service_id);
//            $service2 = \App\ServiceTwo::find($service_id);
            if ($service) {
                $service->template = $request->get('template');
                $result = $service->save();
//                $service2->template = $request->get('template');
//                $result2 = $service2->save();
            }
            return response()->json(['status' => 'ok', 'data' => $result]);
        }
    }

    public function showServiceTemplate($id) {
        $record = \App\ServiceTemplate::with('service.directorate_speciality.directorate')->where('service_id', $id)->get();
        if ($record) {
            return response()->json($record);
        }
    }

    public function updateServiceTemplate(Request $request, $id) {
        $validator = $this->validateServiceTemplate($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $service = \App\Service::find($request->get('service_id'));
//            $service2 = \App\ServiceTwo::find($request->get('service_id'));
            if ($service) {
                $service->template = $request->get('template');
                $result = $service->save();
//                $service2->template = $request->get('template');
//                $result2 = $service2->save();
                return response()->json(['success' => 'record updated successfully']);
            } else {
                return response()->json(['error' => 'changes not updated']);
            }
        }
    }

    public function destroyServiceTemplate($id) {
        $record = \App\Service::find($id);
//        $record2 = \App\ServiceTwo::find($id);
        if ($record) {
            $record->template = NULL;
            $result = $record->save();
//            $record2->template = NULL;
//            $result2 = $record2->save();
            if ($result)
                return response()->json(['deleted' => 'service template deleted successfully']);
            else
                return response()->json(['deleted' => 'record is not deleted']);
        } else {
            return response()->json(['notFound' => 'unable to find record']);
        }
    }

//    ##################################################  SHIFT TYPES APIS Start HERE ##################################################

    public function showShiftType($id) {
        $record = \App\ShiftType::with('shift_detail.service.category', 'shift_detail.service.sub_category', 'shift_detail.directorate_service.category', 'shift_detail.directorate_service.sub_category', 'shift_detail.speciality', 'directorate.speciality.service', 'service', 'directorate_service', 'rota_group', 'rota_template')->orderBy('id', 'desc')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

//    ##################################################  DIRECTORATE STAFF APIS Start HERE ##################################################
    public function indexDirectorateStaff($id) {
        $records = \App\DirectorateStaff::with('grade', 'role', 'post')->where('directorate_id', $id)->get();
        return response()->json($records);
    }

    public function showAllDirectorateStaff($id) {
//        return response()->json(\App\DirectorateStaff::with('directorate', 'member')->get());
        return response()->json(\App\Trust::find($id)->directorate()->with('staff.role', 'staff.grade', 'staff.directorate_speciality')->get());
    }

    protected function directorateStaffValidate(array $data) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'directorate_speciality_id' => 'required|integer',
                    'grade_id' => 'required|integer',
                    'role_id' => 'required|integer',
                    'name' => 'required|max:64',
                    'gmc' => 'required',
                    'qualifications' => 'required|array',
                    'individual_bleep' => 'required',
                    'email' => 'required|email',
                    'mobile' => 'required',
                    'profile_pic' => 'image|mimes:jpeg,bmp,png',
//                    'profile_pic' => 'image',
                    'appointed_from' => 'date|date_format:Y-m-d',
                    'appointed_till' => 'date|date|date_format:Y-m-d',
        ]);
    }

    public function associateDirectorateStaff(Request $request) {
        if (isset($request->qualifications))
            $request->merge(['qualifications' => json_decode($request->qualifications)]);
        $response = array();
        if (count($request->get('staff')) > 1) {
            foreach ($request->all() as $record) {
                $validator = $this->directorateStaffValidate($request->all());
                if ($validator->fails()) {
                    $response[] = ['error' => $validator->messages()];
                }
            }
        } else {
            if (Input::hasFile('profile_pic')) {
                $upload_result = Storage::disk('staff_profile')->put(Input::file('profile_pic')->getClientOriginalName(), File::get(Input::file('profile_pic')));
            } else {
                $upload_result = FALSE;
            }
            if ($upload_result) {
                $name = Input::file('profile_pic')->getClientOriginalName();
            } else {
                $name = 'default.png';
            }
            unset($request->profile_pic);
            $request->merge(['profile_pic' => $name]);
            $new = \App\DirectorateStaff::create($request->all());
            $new->profile_pic = $name;
            $new->save();
            if ($new)
                return response()->json(['status' => 'ok', 'data' => $new, 'upload_image' => $upload_result]);
            else
                return response()->json(['error' => 'Record is not entered']);
        }
        return response()->json(count($request->all()));
    }

    public function showDirectorateStaff($id) {
        $record = \App\DirectorateStaff::with('grade', 'role')->find($id);
        return response()->json($record);
    }

    public function updateDirectorateStaff(Request $request, $id) {
        $validator = $this->directorateStaffValidate($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $request->merge(['qualifications' => ['qualification_id' => $request->qualifications, 'public' => (isset($request->qualifications_public) ? $request->qualifications_public : 0)]]);
            $record = \App\DirectorateStaff::find($id);
            if ($record) {
                if (Input::hasFile('profile_pic')) {
                    $upload_result = Storage::disk('staff_profile')->put(Input::file('profile_pic')->getClientOriginalName(), File::get(Input::file('profile_pic')));
                    $record->profile_pic = Input::file('profile_pic')->getClientOriginalName();
                }
                if ($record->update($request->all())) {
                    return response()->json(['Record updated successfully']);
                }
            } else {
                return response()->json(['Record not found']);
            }
        }
    }

    public function destroyDirectorateStaff($id) {
        $record = \App\DirectorateStaff::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function searchStaff($query) {
        if (is_numeric($query))
            $results = \App\Staff::with('role', 'grade')->where('gmc', 'LIKE', "%$query%")->get();
        else
            $results = \App\Staff::with('role', 'grade')->where('name', 'LIKE', "%$query%")->get();
        return response()->json($results);
    }

    public function searchStaffDirectorate($id, $query) {
        if (is_numeric($query))
            $results = \App\DirectorateStaff::with('directorate', 'role', 'grade', 'post')
                            ->whereHas('directorate', function ($q) use ($id) {
                                $q->whereDirectorateId($id);
                            })->where('gmc', 'LIKE', "%$query%")->get();
        else
            $results = \App\DirectorateStaff::with('directorate', 'role', 'grade', 'post')
                            ->whereHas('directorate', function ($q) use ($id) {
                                $q->whereDirectorateId($id);
                            })->where('name', 'LIKE', "%$query%")->get();
        return response()->json($results);
    }

    public function uploadDirectorateStaff(Request $request) {
        $data_store = [];
//        $directorate = \App\Directorate::where('name', $request->data[6]['directorate'])->where('trust_id', $request->trust_id)->get();
//        return [$directorate[0]->id];
        foreach ($request->data as $data_node) {
            $directorate = \App\Directorate::where('name', $data_node['directorate'])->where('trust_id', $request->trust_id)->get();
            if (count($directorate)) {
                $speciality = \App\DirectorateSpeciality::where('name', $data_node['speciality'])->where('directorate_id', $directorate[0]->id)->get();
                $grade = \App\Grade::where('name', $data_node['grade'])->where('directorate_id', $directorate[0]->id)->get();
                $role = \App\Role::where('name', $data_node['role'])->where('directorate_id', $directorate[0]->id)->get();
                $gmc = \App\DirectorateStaff::where('gmc', $data_node['gmc'])->where('directorate_id', $directorate[0]->id)->get();
            }
            if (count($directorate) && count($speciality) && count($grade) && count($role) && !count($gmc))
                $data_store[] = [
                    'name' => $data_node['name'],
                    'directorate_id' => $directorate[0]->id,
                    'directorate_speciality_id' => $speciality[0]->id,
                    'grade_id' => $grade[0]->id,
                    'role_id' => $role[0]->id,
                    'gmc' => $data_node['gmc'],
                    'qualifications' => $data_node['qualifications'],
                    'individual_bleep' => $data_node['individual_bleep'],
                    'email' => $data_node['email'],
                    'mobile' => $data_node['mobile'],
                    'profile_pic' => 'default.png',
                    'appointed_from' => $data_node['appointed_from'],
                    'appointed_till' => $data_node['appointed_till'],
                ];
        }
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
        $new = \App\DirectorateStaff::insert($data_store);
        if ($new)
            return response()->json(['success' => "Records added successfully"]);
    }

//    ##################################################  SERVICE CATEGORY APIS Start HERE ##################################################

    public function destroyCategory($id) {
        $record = \App\Category::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function uploadCategory(Request $request) {
        $categoryName = [];
        $directorateName = [];
        foreach ($request->data as $key => $data) {
            $directorate = Directorate::where('name', $data['directorate'])->first();
            if (!is_null($directorate)) {
                if (!empty($data['name'])) {
                    $categoryCheck = Category::where('name', $data['name'])->first();
                    if (is_null($categoryCheck)) {
                        $categoryName['name'] = $data['name'];
                        $categoryName['directorate_id'] = $directorate->id;
                        $categoryData = Category::create($categoryName);
                    }
                }
            }
        }
        return response(['success' => 'Records added successfully']);
    }

    public function trustDirectorateCategory($id) {
        $checkSubCategories = function ($checkSubCategory) {
            $checkSubCategoryParents = function ($checkSubCategoryParent) {
                $checkSubCategoryParent->where(['parent_id' => 0]);
            };
            $checkSubCategory->with(['sub_category' => $checkSubCategoryParents, 'sub_category.child_recursive']);
        };
        return response()->json(\App\Directorate::with(['category' => $checkSubCategories, 'speciality'])->where(['trust_id' => $id])->get());
    }

    public function directorateCategoryTree($id) {
        $checkSubCategoryParents = function ($checkSubCategoryParent) {
            $checkSubCategoryParent->where(['parent_id' => 0]);
        };
        return response()->json(\App\Category::with(['sub_category' => $checkSubCategoryParents, 'sub_category.child_recursive'])->where(['directorate_id' => $id])->get());
    }

//    ##################################################  SERVICE SUB CATEGORY APIS Start HERE ##################################################
//    ##################################################  SHIFT DETAIL APIS Start HERE ##################################################
    protected function validateShiftDetail(array $data) {
        return Validator::make($data, [
                    'shift_id' => 'integer',
//                    'start_time' => 'required',
//                    'end_time' => 'required',
        ]);
    }

    public function indexShiftDetail($id) {
        return response()->json(\App\ShiftDetail::with('shift_type')->where('shift_id', $id)->get());
    }

    public function storeShiftDetail(Request $request) {
//        return [$request->all()];
        $validator = $this->validateShiftDetail($request->all());
        $new = null;
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            foreach ($request->all() as $data) {
                $new = \App\ShiftDetail::create($data);
            }

            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function showShiftDetail($id) {
        $record = \App\ShiftDetail::with('shift_type.directorate.speciality.service', 'speciality.directorate.speciality.service', 'service.category')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function updateShiftDetail(Request $request, $id) {
//        return [$request->all()];
        $validator = $this->validateShiftDetail($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            foreach ($request->all() as $data) {
                if (!isset($data['id']) && isset($data['directorate_speciality_id']))
                    $record = \App\ShiftDetail::create($data);
                if (isset($data['id']) && isset($data['directorate_speciality_id'])) {
                    $update = \App\ShiftDetail::find($data['id']);
                    if ($update)
                        $records[] = $update->update($data);
                }
                if (isset($data['id']) && !isset($data['directorate_speciality_id'])) {
                    $delete = \App\ShiftDetail::find($data['id']);
                    if ($delete)
                        $records[] = $delete->delete();
                }
            }
            if ($records)
                return response()->json(['Record updated successfully', $request->all()]);
            else
                return response()->json(['changes not updated']);
        }
    }

    public function destroyShiftDetail($id) {
        $record = \App\ShiftDetail::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

//    ##################################################  RotaGroup CRUD APIs HERE ##################################################  
//    ##################################################  ADDITIONAL GRADE CRUD APIS Start HERE ##################################################      

    protected function validatedAdditionalGrade(array $data, $id = null) {
        return Validator::make($data, [
                    'directorate_speciality_id' => 'required|integer',
                    'name' => 'required|unique_with:tb_additional_grade,name,grade_id,directorate_speciality_id',
                    'grade_id' => 'required|integer',
                    'position' => 'required|integer',
        ]);
    }

    public function indexAdditionalGrade() {
        return response()->json(\App\AdditionalGrade::all());
    }

    public function showAdditionalGrade($id) {
        $record = \App\AdditionalGrade::with('directorate_speciality.directorate')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function storeAdditionalGrade(Request $request) {
        $validator = $this->validatedAdditionalGrade($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $increment = \App\AdditionalGrade::where('directorate_speciality_id', $request->directorate_speciality_id)->where('position', '>=', $request->position)->increment('position');
            $new = \App\AdditionalGrade::create($request->all());
            if ($new)
                return response()->json(['success' => 'Grade added successfully.']);
            else
                return response()->json(['error' => 'Grade not added.']);
        }
    }

    public function updateAdditionalGrade(Request $request, $id) {
        $validator = $this->validatedAdditionalGrade($request->all(), $id);
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = \App\AdditionalGrade::find($id);
            if ($record) {
                if ($request->position > $request->current_position) {
                    $decrement = \App\AdditionalGrade::where('directorate_speciality_id', $record->directorate_speciality_id)->where('position', '>', $request->current_position)->where('position', '<', $request->position)->decrement('position');
                    $request->merge(['position' => $request->position - 1]);
                    $record->update($request->all());
                    if ($record && $decrement)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'Changes not updated']);
                } else if ($request->position < $request->current_position) {

                    $increment = \App\AdditionalGrade::where('directorate_speciality_id', $record->directorate_speciality_id)->where('position', '>=', $request->position)->where('position', '<', $request->current_position)->increment('position');
                    $record->update($request->all());
                    if ($record && $increment)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'Changes not updated']);
                } else if ($request->position == $request->current_position) {
                    $record->update($request->all());
                    if ($record)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'Changes not updated']);
                }
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroyAdditionalGrade($id) {
        $record = \App\AdditionalGrade::find($id);
        if ($record) {
            $decrement = \App\AdditionalGrade::where('directorate_speciality_id', $record->directorate_speciality_id)->where('position', '>', $record->position)->orderBy('position', 'asc')->decrement('position');
            $delete = $record->delete();
            if ($decrement && $delete) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function uploadAdditionalGrade(Request $request, $id) {
//        return [$request->all()];
        $data_store = [];
        foreach ($request->data as $data_node) {
            if (!isset($data_node['directorate']))
                continue;
            $directorate = \App\Directorate::where('trust_id', $id)->where('name', $data_node['directorate'])->get();
            if (count($directorate)) {
                $grade = \App\Grade::where('directorate_id', $directorate[0]->id)->where('name', $data_node['grade'])->get();
                $speciality = \App\DirectorateSpeciality::where('directorate_id', $directorate[0]->id)->where('name', $data_node['speciality'])->get();
                if (count($speciality))
                    $additional_grade = \App\AdditionalGrade::where('directorate_speciality_id', $speciality[0]->id)->where('name', $data_node['name'])->get();
                if (count($grade) && count($speciality) && !count($additional_grade))
                    $data_store [] = ['directorate_speciality_id' => $speciality[0]->id, 'name' => $data_node['name'], 'position' => $data_node['position'], 'grade_id' => $grade[0]->id];
            }
        }
        $count = 0;
        foreach ($data_store as $data) {
            $old = \App\AdditionalGrade::where('directorate_speciality_id', $data['directorate_speciality_id'])->where('position', '>=', $data['position']);
            if (count($old))
                $old->increment('position');
            $new = \App\AdditionalGrade::create($data);
            if (count($new))
                $count++;
        }
        if ($count)
            return response()->json(['success' => "Records added successfully"]);
        else
            return response()->json(['error' => "Records are not added"]);
    }

//    ##################################################  Group APIS Start HERE ##################################################

    public function trustGroup($id) {
        return response()->json(\App\Group::with('subgroup')->where('trust_id', $id)->get());
    }

//  ##################################################  RELATIONAL DATA APIS STARTS HERE ##################################################  

    public function directorateRelationalGradeRole($id) {
        $record = \App\Directorate::with(['grade' => function ($query) {
                        $query->orderBy('position', 'asc');
                    }, 'grade.role', 'grade.additional_grade', 'role' => function ($query) {
                        $query->orderBy('position', 'asc');
                    }])->where('trust_id', $id)->get();
        if ($record)
            return response()->json($record);
        else
            return ['error' => 'no records found'];
    }

    public function trustDirectorateStaff($id) {
        $record = \App\Directorate::with('staff.post')->where('trust_id', $id)->get();
        if ($record)
            return response()->json($record);
        else
            return ['error' => 'no records found'];
    }

    public function directorateRelationalSpecialitiesGrade($id) {
        $record = \App\Directorate::with(['speciality.directorate.grade' => function ($query) {
                        $query->orderBy('position', 'asc');
                    }, 'speciality.directorate.grade.role', 'speciality.directorate.grade.additional_grade'])->where('trust_id', $id)->get();
        if ($record)
            return response()->json($record);
        else
            return ['error' => 'no records found'];
    }

    public function directorateRelationalSpecialitiesAdditionalGrade($id) {
        $record = \App\Directorate::with(['speciality.additional_grade' => function ($query) {
                        $query->orderBy('position', 'asc');
                    }, 'speciality.additional_grade.grade.role', 'grade.role'])->where('trust_id', $id)->get();
        if ($record)
            return response()->json($record);
        else
            return ['error' => 'no records found'];
    }

    public function directorateAdditionalParameter($id) {
        $record = \App\Directorate::with('additional_parameter')->where('trust_id', $id)->get();
        if ($record)
            return response()->json($record);
        else
            return ['error' => 'no records found'];
    }

    public function directorateRelationalSpecialitiesService($id) {
        $record = \App\Directorate::with('speciality.service.category', 'speciality.service.sub_category.parent_recursive')->where('trust_id', $id)->get();
        if ($record)
            return response()->json($record);
        else
            return ['error' => 'no records found'];
    }

    public function directorateRelationalSpecialitiesPost($id) {
        $record = \App\Directorate::with('speciality.post.rota_template.rota_group', 'speciality.post.rotation.staff', 'speciality.post.rotation.rota_template', 'rota_group')->where('trust_id', $id)->get();
        if ($record)
            return response()->json($record);
        else
            return ['error' => 'no records found'];
    }

    public function trustRelationalDivisionDirectorate($id) {
        $record = \App\Trust::with('division.directorate')->find($id);
        return response()->json($record);
    }

    public function trustRelationalDirectorateRotaGroup($id) {
        $record = \App\Directorate::with('rota_group.shift_type', 'rota_group.rota_template')->where('trust_id', $id)->get();
        return response()->json($record);
    }

    public function trustDirectorateGroupSubgroup($id) {
        $record = \App\Directorate::with('group.subgroup')->where('trust_id', $id)->get();
        return response()->json($record);
    }

    public function trustDirectorateService($id) {
        $record = \App\Directorate::with('speciality', 'category.directorate_service', 'category.sub_category.directorate_service', 'services.category', 'services.sub_category')->where('trust_id', $id)->where('service', 1)->get();
        return response()->json($record);
    }

    public function trustRelationalSite($id) {
        $record = \App\Site::with('location', 'trust')->where('trust_id', $id)->get();
        return response()->json($record);
    }

    public function trustRelationalDivision($id) {
        $record = \App\Trust::with('division')->find($id);
        return response()->json($record);
    }

    public function directorateRelationalAllData($id) {
        $record = \App\Directorate::with('speciality.service', 'speciality.post.rota_template', 'rota_group.rota_template', 'rota_group.shift_type', 'staff', 'role', 'grade.role', 'category.sub_category.directorate_service', 'category.sub_category.service', 'category.directorate_service', 'category.service', 'services.category', 'services.sub_category')->where('trust_id', $id)->get();
        return response()->json($record);
    }

    public function servicePostView(Request $request) {
        $record = \App\Directorate::with('services.category', 'speciality.service.category', 'speciality.post.staff')->where('trust_id', $request->trust_id)->get();
        return response()->json($record);
    }

    public function serviceOverview($id, $date) {
        $record = \App\Directorate::with(['speciality.service.requirement' => function ($q) {
                        $q->with('childRecursive');
                    }, 'speciality.service.category', 'speciality.post.rota_template', 'speciality.post.staff.grade', 'speciality.post.staff.role', 'speciality.post.staff.assign' => function ($q) use ($date) {
                        $q->whereDate('date', '=', $date);
                    }, 'grade', 'role', 'category', 'additional_parameter'])->where('trust_id', $id)->get();
        return response()->json($record);
    }

    public function directorateSpecialityPost($id, $sid) {
        $record = \App\Directorate::with(['speciality' => function ($q) use ($sid) {
                        $q->find($sid);
                    }, 'speciality.post.staff', 'speciality.post.rota_template'])->find($id);
        return response()->json($record);
    }

    public function createTrustUser(Request $request) {
        $validator = $this->validateTrustUser($request->all());
        if (!$validator->fails()) {
            $user = $this->userCreate($request);
            if ($user) {
                return response()->json(['user' => $user, 'status' => 'Registration Success']);
            } else {
                return response()->json(['error' => 'yes', 'messages' => ['Error Occure Try Again'], 'status' => 'Registration Failed']);
            }
        } else {

            return response()->json(['error' => 'yes', 'messages' => $validator->messages(), 'status' => 'Registration Failed']);
        }
    }

    public function userCreate($request) {

        DB::beginTransaction();
        $createUser = \App\User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'password' => bcrypt($request->password),
                    'api_token' => str_random(60), //slight change here
        ]);
        if ($createUser) {

            $addUserToTrust = \App\UserTrust::create([
                        'user_id' => $createUser->id,
                        'trust_id' => $request->trust_id
            ]);

            if ($addUserToTrust) {
                foreach ($request->roles as $role) {   // here role id is not form roleId table but from role_trust table

                    /*                $addRole=[];
                      $addRole['user_id']=$createUser->id;
                      $addRole['role_id']=$role;
                      $addRoleToUser=UserRole::insert($addRole); */
                    $addRole = [];
                    $addRole['user_trust_id'] = $addUserToTrust->id;
                    $addRole['role_trust_id'] = $role;
                    $addRoleToUser = UserTrustRoles::insert($addRole);
                    /* if (!$addRoleToUser) {
                      return false;
                      } */
                }
            }
        }
        if ($createUser && $addUserToTrust && $addRoleToUser) {
            DB::commit();
            return true;
        } else {
            DB::rollBack();
            return false;
        }
    }

    protected function validateTrustUser(array $data) {
        return Validator::make($data, [
                    'roles' => 'required',
                    'trust_id' => 'required',
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:tb_user',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function validateUpdateUser(array $data) {
        return Validator::make($data, [
                    'roles' => 'required',
                    'userTrustId' => 'required',
                    'name' => 'required|max:255',
        ]);
    }

    public function sendEmail(Request $request) {

        $emailData = array();
        $emailData = $request->all();
        dispatch(new SendEmail($emailData));
    }

    public function getUserEmails($id) {

        try {


            $emails = \App\Emails::where('user_id', $id)->get();
            return response()->json(['status' => true, 'data' => $emails]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'data' => []]);
        }
    }

}
