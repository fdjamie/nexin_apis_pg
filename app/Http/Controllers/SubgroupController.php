<?php

namespace App\Http\Controllers;

use File;
use App\Subgroup;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SubgroupController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateSubgroup(array $data) {
        return Validator::make($data, [
                    'group_id' => 'required|integer',
                    'name' => 'required|unique_with:tb_subgroups, group_id',
        ]);
    }

    public function index() {
        return response()->json(Subgroup::with('group')->get());
    }

    public function store(Request $request) {
        $validator = $this->validateSubgroup($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = Subgroup::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function show($id) {
        $record = Subgroup::with('group.directorate')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['error'=>'Unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateSubgroup($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = Subgroup::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['success' => 'record updated successfully']);
                else
                    return response()->json(['error' => 'changes not updated']);
            } else
                return response()->json(['error' => 'unable to find record']);
        }
    }

    public function destroy($id) {
        $record = Subgroup::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $subgroups = $this->gettingFormatedSubgroups($request->trust_id, $request->data);
        if (count($subgroups) == 0)
            return response()->json(['error' => "No record is added"]);
        else {
            $result = $this->createOrUpdateGroups($subgroups);
            if (count($result)) {
                return response()->json(['success' => count($result) . " Records updated or created successfully"]);
            } else {
                return response()->json(['success' => " no changes after upload"]);
            }
        }
    }

    //################################### PROTECRTED FUNCTIONS ###############################

    protected function getDirectorateGroupsNameArray($trust_id) {
        $directorate_names = $group_names = [];
        $directorates = \App\Directorate::with('group')->where('trust_id', $trust_id)->get();
        foreach ($directorates as $directorate) {
            $directorate_names[$directorate->id] = $directorate->name;
            if (isset($directorate->group) && count($directorate->group)) {
                foreach ($directorate->group as $group) {
                    $group_names[$directorate->id][$group->id] = $group->name;
                }
            }
        }
        return [$directorate_names, $group_names];
    }

    protected function gettingFormatedSubgroups($trust_id, $data) {
        $subgroups = $directorate_array = $group_array = [];
        list($directorate_array, $group_array) = $this->getDirectorateGroupsNameArray($trust_id);
        foreach ($data as $data_node) {
            $directorate_id = array_search($data_node['directorate'], $directorate_array);
            if ($directorate_id) {
                $group_id = array_search($data_node['group'], $group_array[$directorate_id]);
                if ($group_id)
                    $subgroups[] = ['name' => $data_node['name'], 'group_id' => $group_id];
            }
        }
        return $subgroups;
    }

    protected function createOrUpdateGroups($subgroups) {
        $records = [];
        foreach ($subgroups as $subgroup) {
            $records [] = Subgroup::updateOrCreate(['group_id' => $subgroup['group_id'], 'name' => $subgroup['name']], [
                        'group_id' => $subgroup['group_id'],
                        'name' => $subgroup['name']
            ]);
        }
        return $records;
    }

}
