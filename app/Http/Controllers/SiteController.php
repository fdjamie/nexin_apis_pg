<?php

namespace App\Http\Controllers;

use File;
use App\Site;
use App\SiteTwo;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SiteController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedSite(array $data) {
        return Validator::make($data, [
                    'trust_id' => 'required|integer',
                    'name' => 'required|max:64|',
                    'abbreviation' => 'required|max:64|',
                    'address' => 'required',
        ]);
    }

    public function index() {
        return response()->json(Site::all());
    }

    public function store(Request $request) {
        $validator = $this->validatedSite($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = Site::create($request->all());
            $new = SiteTwo::create($request->all());
            if ($new)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record not added.']);
        }
    }

    public function show($id) {
        $record = Site::with('location', 'trust')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validatedSite($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = Site::find($id);
            $record2 = SiteTwo::find($id);
            if ($record) {
                if ($record->update($request->all())) {
                    $record2->update($request->all());
                    return response()->json(['updated' => 'Record updated successfully']);
                } else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {
        $record = Site::find($id);
        $record2 = SiteTwo::find($id);
        if ($record) {
            if ($record->delete()) {
                $record2->delete();
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $data_store = [];
        foreach ($request->data as $data_node) {
            if (!isset($data_node['name']))
                $data_node['name'] = '';
            $trust = \App\Trust::find($request->trust_id);
            if (count($trust)) {
                $site = Site::where('trust_id', $request->trust_id)->where('name', $data_node['name'])->get();
                if (!count($site) && $data_node['name'] != '')
                    $data_store [] = ['trust_id' => $request->trust_id, 'name' => $data_node['name'], 'abbreviation' => $data_node['abbreviation'], 'address' => $data_node['address']];
            }
        }
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
        $new = Site::insert($data_store);
        if ($new)
            return response()->json(['success' => "Records added successfully"]);
    }

}
