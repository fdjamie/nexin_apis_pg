<?php

namespace App\Http\Controllers;

use File;
use App\PrioritySet;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PrioritySetController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedPrioritySet(array $data, $id = null) {
        return Validator::make($data, [
                    'directorate_speciality_id' => 'required|integer',
                    'name' => 'required|string|unique_with:tb_priority_sets,directorate_speciality_id,' . $id,
                    'start_date' => 'string',
                    'end_date' => 'string',
        ]);
    }

    public function index() {
        return response()->json(PrioritySet::all());
    }

    public function store(Request $request) {
        $validator = $this->validatedPrioritySet($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = PrioritySet::create($request->all());
            if ($new)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record not added.']);
        }
    }

    public function show($id) {
        $record = PrioritySet::with(['speciality', 'priorities' => function($q) {
                        $q->orderBy('level', 'asc');
                    }])->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
//        return [$request->all()];
        $validator = $this->validatedPrioritySet($request->all(), $id);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = PrioritySet::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['updated' => 'Record updated successfully']);
                else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroy($id) {
        $record = PrioritySet::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

}
