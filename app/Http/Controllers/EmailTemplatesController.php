<?php

namespace App\Http\Controllers;

use App\EmailTemplates;
use App\Template;
use App\Trust;
use App\TrustTemplates;
use http\Env\Response;
use Illuminate\Http\Request;
use App\EmailTypes;
use Validator;
use Illuminate\Support\Facades\Log;


class EmailTemplatesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getAllTemplates($trustId)
    {

        /* $getEmailTypes = EmailTypes::with('templates')->get();
         foreach ($getEmailTypes as $k => $g) {
             foreach ($g->templates as $t) {

                 $getTrustTemplate = TrustTemplates::where('trust_id', $trustId)->where('template_id', $t->id)->first();
                 if ($getTrustTemplate) {

                     $getEmailTypes[$k]['templateHtml'] = EmailTemplates::find($getTrustTemplate->template_id)->html;
                     $default = false;

                 } else {
                     $getEmailTypes[$k]['templateHtml'] = EmailTemplates::where('email_type_id', $g->id)->where('is_default', true)->first()->html;
                     $default = true;
                 }


             }

         }*/

        /*  $getEmailTypes = EmailTypes::with(['templates'=>function($query){
            $query->select('id,trust_id')->where(['trust_id'=>1,'is_active'=>true]);
          }])->get();*/
        $getEmailTypes = EmailTypes::get();
        foreach ($getEmailTypes as $k => $g) {
            $checkTemplate = EmailTemplates::where(['trust_id' => $trustId, 'email_type_id' => $g->id, 'is_active' => true])->first();
            if (empty($checkTemplate)) {
                $getEmailTypes[$k]['templateId'] = EmailTemplates::where(['email_type_id' => $g->id, 'is_default' => true])->first()->id;
            } else {
                $getEmailTypes[$k]['templateId'] = $checkTemplate->id;
            }

        }

        return response()->json(['status' => true, 'data' => $getEmailTypes]);

    }

    function getTemplateHtml($id)
    {

        try {


            $html = EmailTemplates::find($id)->html;
            return response()->json(['status' => true, 'html' => html_entity_decode($html)]);



        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => "Unknown Error Occured"]);


        }


        /*  try {
              $getEmailTypes = EmailTypes::with('templates')->find($templateTypeId);
              foreach ($getEmailTypes->templates as $t) {

                  $getTrustTemplate = TrustTemplates::where('trust_id', $trustId)->where('template_id', $t->id)->first();
                  if ($getTrustTemplate) {

                      $html = EmailTemplates::find($getTrustTemplate->template_id)->html;

                  } else {
                      $html = EmailTemplates::where('email_type_id', $getEmailTypes->id)->where('is_default', true)->first()->html;

                  }

                  return response()->json(['status' => true, 'html' => $html]);

              }

          } catch (\Exception $e) {
              return response()->json(['status' => false, 'msg' => "Unknown Error Occured"]);


          }*/


    }

    function getEmailTypeTemplate($templateTypeId,$trustId)
    {
        try {
            $getEmailTypes = EmailTypes::with(['templates'=>function($query) use ($trustId){
                $query->where('trust_id',$trustId)->orWhere('is_default',true)->get();
            }])->find($templateTypeId);
            return response()->json(['status' => true, 'data' => $getEmailTypes->templates]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => "Unknown Error Occured"]);
        }

    }


    function updateTemplate(Request $request)
    {
        $emailTypeId = $request->emailTypeId;
        $templateId = $request->templateId;
        $trustId = $request->trustId;
        $status = true;


            $update=EmailTemplates::where(['email_type_id'=>$emailTypeId,'trust_id'=>$trustId])->update(['is_active'=>'false']);
            $update=EmailTemplates::where(['id'=>$templateId,'trust_id'=>$trustId])->update(['is_active'=>'true']);

        if ($status) {
            return response()->json(['status' => true, 'msg' => 'Template Successfully Changed']);
        } else {
            return response()->json(['status' => false, 'msg' => trans('messages.error')]);
        }
    }

    function createTemplate(Request $request)
    {
        $validate = $this->validateCreateTemplate($request);
        if ($validate->fails()) {

            return response()->json(['status' => false, 'errors' => $validate->errors()]);
        }
   try{

        $checkTemplateFordynamicVariables=$this->templateVariablesValidation($request->templateType,$request->template);
        if(!$checkTemplateFordynamicVariables['status']){
            return response()->json(['status' => false, 'errors' => ['error' => [$checkTemplateFordynamicVariables['msg']]]]);
        }

        $createTemplate = EmailTemplates::create([

            'trust_id' => $request->trustId,
            'subject' => $request->subject,
            'from' => $request->from,
            'html' => $request->template,
            'email_type_id' => $request->templateType,

        ]);

        if ($createTemplate) {
            return response()->json(['status' => true, 'msg' => "Template Successfully Created."]);
        } else {
            return response()->json(['status' => false, 'errors' => ['error' => [trans('messages.error')]]]);
        }
   }catch (\Exception $e){
       return response()->json(['status' => false, 'errors' => ['error' => [trans('messages.error')]]]);

   }

    }


    private function templateVariablesValidation($tempTypeId,$html){

        $type=EmailTypes::find($tempTypeId)->slug;
        $status=true;
        $msg="";
        $html = str_replace(["&amp;name&amp;","&amp;trust&amp;","&amp;code&amp;"], ["&name&","&trust&","&code&"],$html);
        if($type=="verification-code"){
            if (strpos($html, '&name&') === false || strpos($html, '&code&') === false) {
               $status=false;
               $msg="You must set varaibles &name& and &code& in email body";
            }

        }
        elseif ($type=="user-request-approved" || $type=="user-request-rejected"){
            if (strpos($html, '&name&') === false || strpos($html, '&trust&') === false) {
                $status=false;
                $msg="You must set varaibles &name& and &trust& in email body";
            }


        }


        return ['status'=>$status,'msg'=>$msg];


    }

    private function validateCreateTemplate($data)
    {
        $validate = Validator::make($data->all(), [
            'template' => 'required',
            'templateType' => 'required',
            'trustId' => 'required',
            'from' => 'email',

        ]);

        return $validate;
    }
}

