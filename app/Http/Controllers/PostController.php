<?php

namespace App\Http\Controllers;

use File;
use App\Post;
use App\PostTwo;
use Validator;
use App\Directorate;
use App\RotaGroup;
use App\RotaTemplate;
use App\DirectorateSpeciality;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    public function validatePost(array $data, $id = null) {
        return Validator::make($data, [
                    'staff_id' => 'integer',
                    'directorate_speciality_id' => 'required|integer',
                    'name' => 'required|unique_with:tb_post,directorate_specaility_id,' . $id,
        ]);
    }

    public function index() {
//        $record = Post::with('directorate_speciality', 'directorate_speciality.directorate', 'directorate_speciality.directorate.speciality')->find($id);
//        $record = Post::all();
//        if ($record) {
//            return response()->json($record);
//        } else
//            return response()->json(['error' => 'Unable to find record']);
        return response(Post::all());
    }

    public function store(Request $request) {
        $validator = $this->validatePost($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            if (isset($request->staff_member))
                $request->merge(['staff_id' => $request->staff_member]);
            $new = Post::create($request->all());
            $new2 = PostTwo::create($request->all());
            if ($new)
                return response()->json(['success' => 'Record added successfuly.']);
            else
                return response()->json(['error' => 'Record not added.']);
        }
    }

    public function show($id) {
        $record = Post::with('directorate_speciality.directorate', 'staff', 'rota_template.rota_group.shift_type', 'rotation.staff', 'rotation.rota_template')->find($id);
        if ($record) {
            return response()->json($record);
        } else {
            return response()->json(['unable to find record']);
        }
    }

    public function update(Request $request, $id) {
        if (!isset($request->end_date))
            $request->merge(['end_date' => null]);
        $validator = $this->validatePost($request->all(), $id);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = Post::find($id);
            $record2 = PostTwo::find($id);
            if ($record) {
                if ($record->update($request->all())) {
                    $record2->update($request->all());
                    return response()->json(['success' => 'Record updated successfully']);
                } else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroy($id) {
        $record = Post::find($id);
        $record2 = PostTwo::find($id);
        if ($record) {
            if ($record->delete()) {
                $record2->delete();
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function updateAdditionalParameter(Request $request, $id) {
        if (!count($request->all()))
            return response()->json(['error' => 'No additional Parameter to add.']);
        else {
            $post = \App\Post::find($id);
            $post2 = \App\PostTwo::find($id);
            if ($post) {
                if ($post->update(['additional_parameters' => $request->all()])) {
                    $post2->update(['additional_parameters' => $request->all()]);
                    return response()->json(['success' => 'Aditional Parameters added successfully.']);
                } else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function postUpload(Request $request) {
        $directorate;
        $postData = [];
        foreach ($request->data as $key => $data) {
            $directorate = Directorate::where(['name' => $data['directorate']])->first();
            if (!empty($directorate)) {
                $rotaGroup = RotaGroup::where(['name' => $data['rota_group'], 'directorate_id' => $directorate->id])->first();
                $specicality = DirectorateSpeciality::where(['name' => $data['speciality'], 'directorate_id' => $directorate->id])->first();
                if (!empty($rotaGroup) && !empty($specicality)) {
                    $rotaTemplate = RotaTemplate::where(['name' => $data['rota_template'], 'rota_group_id' => $rotaGroup->id, 'directorate_id' => $directorate->id])->first();
                    if (!empty($rotaTemplate)) {
                        $weekCount = count($rotaTemplate->content->shift_type);
                        if ($weekCount == $data['assigned_week']) {
                            $post = Post::where(['name' => $data['name'], 'directorate_speciality_id' => $specicality->id, 'rota_template_id' => $rotaTemplate->id])->first();
                            if (empty($post)) {
                                $postData['name'] = $data['name'];
                                $postData['description'] = $data['description'];
                                $postData['start_date'] = $data['start_date'];
                                $postData['start_date'] = (isset($data['start_date']['date'])) ? date('Y-m-d', strtotime($data['start_date']['date'])) : ($data['start_date'] );
                                $postData['end_date'] = ($data['end_date'] == 'Permanent') || ($data['end_date'] == 'permanent' ) ? '1111-00-00' : ($data['end_date'] );
                                $postData['end_date'] = (isset($data['end_date']['date'])) ? date('Y-m-d', strtotime($data['end_date']['date'])) : ($data['end_date'] );
                                $postData['assigned_week'] = $data['assigned_week'];
                                $postData['rota_template_id'] = $rotaTemplate->id;
                                $postData['directorate_speciality_id'] = $specicality->id;
                                Post::create($postData);
                                PostTwo::create($postData);
                            }
                        }
                    }
                }
            }
        }

        return ['success' => 'Post Added Successfully'];
    }

}
