<?php

namespace App\Http\Controllers;

use App\Location;
use App\LocationTwo;
use App\Site;
use App\Http\Requests;
use Validator;
use Illuminate\Http\Request;

class LocationController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateLocation(array $data) {
        return Validator::make($data, [
                    'site_id' => 'required|integer',
                    'name' => 'required|max:64',
                    'service' => 'required',
                    'limit' => 'required|integer',
                    'group_id' => 'required|integer',
                    'subgroup_id' => 'required|integer',
        ]);
    }

    public function index() {
        return response()->json(Location::all());
    }

    public function store(Request $request) {
        $validator = $this->validateLocation($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        } else {
            $new = Location::create($request->all());
            $new2 = LocationTwo::create($request->all());
            if ($new)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record not added.']);
        }
    }

    public function show($id) {
        $record = Location::with('site.trust', 'group.directorate')->find($id);
        if ($record) {
            return response()->json($record);
        } else {
            return response()->json(['unable to find record']);
        }
    }

    public function update(Request $request, $id) {
        $validator = $this->validateLocation($request->all());
        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $record = Location::find($id);
            $record2 = LocationTwo::find($id);
            if ($record) {
                if ($record->update($request->all())) {
                    $record2->update($request->all());
                    return response()->json(['updated' => 'Record updated successfully']);
                } else {
                    return response()->json(['error' => 'Changes not updated']);
                }
            } else {
                return response()->json(['error' => 'Unable to find record']);
            }
        }
    }

    public function destroy($id) {
        $record = Location::find($id);
        $record2 = LocationTwo::find($id);
        if ($record) {
            if ($record->delete()) {
                $record2->delete();
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $result = [];
        $locations = $this->getFormatedLocations($request->trust_id, $request->data);
        if (count($locations) <= 0) {
            return response()->json(['error' => "No record is added"]);
        } else {
            foreach ($locations as $location) {
                $result[] = \App\Location::insert($locations);
            }

            if (count($result)) {
                return response()->json(['success' => count($result) . " Records updated or created successfully"]);
            } else {
                return response()->json(['success' => " no changes after upload"]);
            }
        }
    }

    protected function getFormatedLocations($trust_id, $records) {
        list($sites_name_array, $lcoation_array) = $this->getSiteNamesAsArray($trust_id);
        $locations = [];
        foreach ($records as $record) {
            // if name is not given or service is not equals to yes or no then ignore that record
            if (($record['service'] != 'Yes' && $record['service'] != 'No') || ($record['name'] == "")) {
                continue;
            }
            $site_id = null;
            if(isset($record['site']))
            $site_id = array_search($record['site'], $sites_name_array);
            if ($site_id) {
                $service = ($record['service'] == 'Yes' ? 1 : 0);
                array_push($locations, ['site_id' => $site_id, 'name' => $record['name'], 'service' => $service, 'limit' => (isset($record['limit']) ? $record['limit'] : 0)]);
            }
        }
        return $locations;
    }

    protected function createOrUpdateLocations($locations) {
        $result = [];
        foreach ($locations as $location) {
            $result[] = \App\Location::create($location);
        }
        return $result;
    }

    public function testing(Request $request) {
        return response()->json($this->getAllTrustLocations(12));
    }

    protected function getSiteNamesAsArray($trust_id) {
        $sites_array = $location_array = [];
        $sites = Site::with('location')->where('trust_id', $trust_id)->get();
        foreach ($sites as $site) {
            $sites_array[$site->id] = $site->name;
            foreach ($site->location as $location) {
                $location_array[$site->id][$location->id] = $location->name;
            }
        }
        return [$sites_array, $location_array];
    }

    protected function getAllTrustLocations($trust_id) {
        $records = [];
        $sites_array = $this->getSiteNamesAsArray($trust_id);
        $site_ids_array = array_keys($sites_array);
        $locations = Location::whereIn('site_id', $site_ids_array)->get();
        foreach ($locations as $location) {
            $records[$location->site_id][$location->id] = $location->name;
        }
        return $records;
    }

}
