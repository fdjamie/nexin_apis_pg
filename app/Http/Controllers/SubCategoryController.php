<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SubCategory;
use Validator;

class SubCategoryController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    public function index() {
        $response = SubCategory::all();
        return response($response);
    }

    protected function validateSubCategory(array $data, $id = null) {
        return Validator::make($data, [
                    'category_id' => 'required|integer',
                    'name' => 'required|unique_with:tb_sub_category,category_id,parent_id,' . $id,
                    'parent_id' => 'required|integer'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = $this->validateSubCategory($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = SubCategory::create($request->all());
            return response()->json(['success' => 'Subcategory created successfully.']);
        }
    }

    public function show($id) {
        $record = SubCategory::with('category.directorate')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateSubCategory($request->all(), $id);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = SubCategory::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['success' => 'Record updated successfully', $request->all(), $record]);
                else
                    return response()->json(['error' => 'changes not updated']);
            } else
                return response()->json(['error' => 'unable to find record']);
        }
        return response()->json($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $record = SubCategory::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function childIndex($id) {
        $response = SubCategory::with('child_recursive')->where(['parent_id' => $id])->get();
        return response()->json($response);
    }

    public function searchSubcategory($id, $query) {
        $result = $levels = [];
        $subcategories = SubCategory::with('parent_recursive')->where('category_id', $id)->where('name', 'LIKE', "%$query%")->get();
        foreach ($subcategories as $subcategory) {
            $this->names = [];
            if (isset($subcategory->parent_recursive)) {
                $levels = $this->breadCrumb($subcategory->toArray());
                $levels = array_reverse($levels);
            }
            $result [] = ['id' => $subcategory->id, 'name' => $subcategory->name, 'level' => implode(' > ', $levels)];
        }
        return response()->json($result);
    }

    private $names = [];

    public function parents($id) {
        $names = [];
        $result = SubCategory::with('parent_recursive')->find($id);
        $final = $this->breadCrumb($result->toArray());
        return response($final);
    }

    public function child($id) {
        $result = SubCategory :: with('child_recursive')->where('id', $id)->first();
        return response($result);
    }

    private function breadCrumb($nodes) {
        $this->names[] = $nodes['name'];
        if (isset($nodes['parent_recursive'])) {
            $this->breadCrumb($nodes['parent_recursive']);
        }
        return $this->names;
    }

}
