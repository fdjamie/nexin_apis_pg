<?php

namespace App\Http\Controllers;

use File;
use App\Service;
use App\ServiceTwo;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use App\Directorate;
use App\DirectorateSpeciality;
use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;

class ServiceController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateService(array $data, $id = null) {
        return Validator::make($data, [
                    'directorate_speciality_id' => 'required|integer',
                    'name' => 'required',
                    'contactable' => 'required',
                    'min_standards' => 'required',
        ]);
    }

    protected function serviceRunningValidate(array $data) {
        $validator = Validator::make($data, [
                    'service_id' => 'required|integer|unique_with:tb_service_running,date,transition_time_id',
                    'transition_time_id' => 'required|integer',
                    'date' => 'required|date',
                    'day' => 'required|integer',
                    'week' => 'integer',
                    'start' => 'required|string',
                    'stop' => 'required|string',
        ]);

        $validator->getPresenceVerifier()->setConnection('pgsql2');
        return $validator;
    }

    protected function serviceAvailableStaffValidate(array $data) {
        $validator = Validator::make($data, [
                    'directorate_staff_id' => 'required|integer|unique_with:tb_available_services_staff,date,transition_time_id,service_id',
                    'service_id' => 'required|integer',
                    'transition_time_id' => 'required|integer',
                    'date' => 'required|date',
                    'day' => 'required|integer',
                    'week' => 'integer',
                    'start' => 'required|string',
                    'stop' => 'required|string',
        ]);
        $validator->getPresenceVerifier()->setConnection('pgsql2');
        return $validator;
    }

    public function index() {
        return response()->json(Service::all());
    }

    public function store(Request $request) {
        $validator = $this->validateService($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = Service::create($request->all());
            $new2 = ServiceTwo::create($request->all());
            if ($new)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record not added.']);
        }
    }

    public function show($id) {
        $record = Service::with(['category', 'sub_category', 'directorate_speciality', 'directorate_speciality.directorate', 'requirement'])->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {

        $validator = $this->validateService($request->all());
        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $record = \App\Service::find($id);
            $record2 = ServiceTwo::find($id);
            if ($record) {
                if ($record->update($request->all())) {
                    $record2->update($request->all());
                    return response()->json(['record updated successfully', $request->all(), $record]);
                } else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
    }

    public function destroy($id) {
        $record = Service::find($id);
        $record2 = ServiceTwo::find($id);
        if ($record) {
            if ($record->delete()) {
                $record2->delete();
                return response()->json(['record deleted successfully']);
            } else {
                return response()->json(['record is not deleted']);
            }
        } else {
            return response()->json(['unable to find record']);
        }
    }

    public function upload(Request $request) {
        $data_store = $template = $events = $formated_data = $records = [];
        $formated_data = $request->data;
        list($directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service) = $this->getCheckData();
        $data_store = $this->getDataToStore($formated_data, $directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service, $request->trust_id);
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
        foreach ($data_store as $node) {
            $record = \App\Service::find($node['id']);
            if ($record)
                $records[] = $record->update($node);
        }

        return response()->json(['success' => count($records) . " Records Updated successfully"]);
    }

    public function uploadReference(Request $request) {
        $data_store = $template = $events = $formated_data = $records = $service_events = [];
        $formated_data = $request->data;
        $service_data = $request->service_data;
        list($directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service) = $this->getCheckData();
        $data_store = $this->getDataToStore($service_data, $directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service, $request->trust_id);
//        return [$data_store[0]['id']];
        foreach ($data_store as $record) {
            $service_events[$record['id']] = (isset($record['template']) ? $record['template'] : null);
        }
        $formated_data = $result = $data_store = [];
        $service_flag = false;
        $result ['delete'] = $result ['restore'] = $result ['update'] = $result['create'] = 0;
        $formated_data = $request->data;
        foreach ($formated_data as $node) {
            if (isset($node['operations']) && !empty($node['id'])) {
                if (strtolower($node['operations']) == 'delete') {
                    $service = \App\Service::find($node['id']);
                    $service2 = ServiceTwo::find($node['id']);
                    if (!is_null($service)) {
                        if ($service->delete()) {
                            $service2->delete();
                            $result ['delete'] ++;
                        }
                    }
                }
                if (strtolower($node['operations']) == 'restore') {
                    $service = \App\Service::with(['requirement' => function($q) {
                                    $q->onlyTrashed();
                                }])->withTrashed()->find($node['id']);
                    $service2 = \App\Service::with(['requirement' => function($q) {
                                    $q->onlyTrashed();
                                }])->withTrashed()->find($node['id']);
                    if (!is_null($service)) {
                        if ($service->restore()) {
                            $service2->restore();
                            $result ['restore'] ++;
                        }
                    }
                }
                if (strtolower($node['operations']) == 'update') {
                    $directorate = Directorate::where('name', $node['directorate'])->first();
                    if (!is_null($directorate)) {
                        $directorate_speciality = DirectorateSpeciality::where(['name' => $node['speciality'], 'directorate_id' => $directorate->id])->first();
                        $category = ($node['category'] == 'N/A' ? new \stdClass() : Category::where(['name' => $node['category'], 'directorate_id' => $directorate->id])->first());
                        if (!is_null($directorate_speciality) && !is_null($category)) {
                            $sub_category = ($node['subcategory'] == 'N/A' ? new \stdClass() : SubCategory::where(['name' => $node['subcategory'], 'category_id' => $category->id])->first());
                            $service = \App\Service::find($node['id']);
                            $service2 = \App\ServiceTwo::find($node['id']);
                            if (!is_null($sub_category)) {
                                if (!is_null($service)) {
                                    if ($service->update(['name' => $node['name'], 'directorate_speciality_id' => $directorate_speciality->id, 'category_id' => (( $category == new \stdClass()) ? 0 : $category->id), 'sub_category_id' => ( ($sub_category == new \stdClass()) ? 0 : $sub_category->id), 'contactable' => ( ($node['contactable'] == 'yes') ? 1 : 0), 'min_standards' => ( ($node['min_standards'] == 'yes') ? 1 : 0)])) {
                                        $service2->update(['name' => $node['name'], 'directorate_speciality_id' => $directorate_speciality->id, 'category_id' => (( $category == new \stdClass()) ? 0 : $category->id), 'sub_category_id' => ( ($sub_category == new \stdClass()) ? 0 : $sub_category->id), 'contactable' => ( ($node['contactable'] == 'yes') ? 1 : 0), 'min_standards' => ( ($node['min_standards'] == 'yes') ? 1 : 0)]);
                                        $result['update'] ++;
                                    }
                                }
                            } else {
                                if ($service->update(['name' => $node['name'], 'directorate_speciality_id' => $directorate_speciality->id, 'category_id' => $category->id])) {
                                    $service2->update(['name' => $node['name'], 'directorate_speciality_id' => $directorate_speciality->id, 'category_id' => $category->id]);
                                    $result['update'] ++;
                                }
                            }
                        }
                    }
                }
            }
            if (isset($node['operations']) && strtolower($node['operations']) == 'create' && empty($node['id'])) {
                $directorates = Directorate::with('speciality.service', 'category.sub_category')->where('trust_id', $request->trust_id)->get();
                if (!is_null($directorates)) {
                    list($directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service) = $this->getCheckData($request->trust_id);
                    $directorate_id = (array_search($node['directorate'], $directorate_array) ? array_search($node['directorate'], $directorate_array) : 0);
                    if ($directorate_id != 0) {
                        list($speciality_id, $category_id, $subcategory_id, $service_flag, $service) = $this->serviceStoreParameters($directorate_id, $node, $directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service, $service_flag);
                        if ($speciality_id != 0) {
                            $data_store = ['directorate_speciality_id' => $speciality_id, 'category_id' => $category_id, 'sub_category_id' => $subcategory_id, 'name' => $node['name'], 'contactable' => (strtolower($node['contactable']) == 'yes' ? 1 : 0), 'min_standards' => (strtolower($node['min_standards']) == 'yes' ? 1 : 0)];
                            $record = Service::create($data_store);
                            $record2 = ServiceTwo::create($data_store);
                            if ($record)
                                $result['create'] ++;
                        }
                    }
                }
            }
        }
        if ($result['update'] > 0 || $result['delete'] > 0 || $result['restore'] || $result['create'] > 0)
            return response()->json(['success' => " Master record Updated successfully."]);
        else
            return response()->json(['error' => " Master record not updated."]);
    }

    public function deletedService($id) {
        $record = \App\Directorate::with(['category.sub_category', 'speciality.service' => function($q) {
                        $q->withTrashed();
                    }, 'speciality.service.category', 'speciality.service.sub_category'])->where('trust_id', $id)->get();
        return response()->json($record);
    }

    public function running(Request $request) {
        $store_service = $errors = $all_service = [];
        $new = 0;
        $all_staff = $this->formatServiceRunning($request->all());
        list($store_service, $errors) = $this->validationServiceRunning($all_staff);
        foreach ($store_service as $service) {
            if (\App\ServiceRunningTwo::create($service))
                $new++;
        }
        if ($new > 0)
            return ['new' => $new++];
        else
            return ['error' => count($errors)];
    }

    public function availableStaff(Request $request) {
        $all_staff = $validated_staff = $errors = [];
        $new = 0;
        foreach ($request->all() as $service_id => $values) {
            foreach ($values as $staff) {
                $staff['service_id'] = $service_id;
                $all_staff[] = $staff;
            }
        }
        foreach ($all_staff as $staff) {
            $validator = $this->serviceAvailableStaffValidate($staff);
            if ($validator->fails()) {
                $errors [] = $validator->messages();
            } else {
                $validated_staff[] = $staff;
            }
        }
        foreach ($validated_staff as $staff) {
            if (\App\AvailableServiceStaffTwo::create($staff))
                $new++;
        }
        if ($new > 0)
            return ['new' => $new++];
        else
            return ['error' => count($errors)];
        return [$validated_staff, $errors];
    }

    public function specialityPrioritySet($id, $sid, $date) {
        $date = date('Y-m-d', $date);
        $directorate = \App\Directorate::with(['speciality' => function($q) use($sid) {
                        $q->find($sid);
                    }, 'speciality.priority_set' => function($q) use($date) {
                        $q->whereDate('start_date', '<=', $date)->whereDate('end_date', '>=', $date)->first();
                    }
                    , 'speciality.priority_set.priorities' => function($q) {
                        $q->orderBy('level', 'asc');
                    }, 'speciality.priority_set.priorities.category_priority.category', 'speciality.priority_set.priorities.category_priority.subcategory', 'speciality.priority_set.priorities.category_priority.service', 'speciality.priority_set.priorities.category_priority.general_service'])->find($id);
        foreach ($directorate->speciality as $speciality) {
            foreach ($speciality->priority_set as $priority_set) {
                foreach ($priority_set->priorities as $priority) {
                    if ($priority->priority_id == 0) {
                        $service = \App\Service::with('requirement')->where('directorate_speciality_id', $priority_set->directorate_speciality_id)->get();
                        unset($priority->category_priority);
                        $priority->category_priority = new \stdClass();
                        $priority->category_priority->services = $service;
                    }
                }
            }
        }
        return $directorate;
    }

//############################# PROTECTED FUNCTIONS#################################

    protected function getCheckData($id = null) {
        if (!is_null($id))
            $directorates = \App\Directorate::with('speciality.service', 'category.service', 'category.sub_category.service')->where('trust_id', $id)->get();
        else
            $directorates = \App\Directorate::with('speciality.service', 'category.service', 'category.sub_category.service')->get();
        $directorate_array = $this->getDirectorateList($directorates);
        list($speciality_array, $speciality_service) = $this->getSubnodesArray($directorates, 'speciality', 'service');
        list($category_array, $category_service) = $this->getSubnodesArray($directorates, 'category', 'service');
        list($category_array, $subcategory_array, $subcategory_service) = $this->getSubnodesArray($directorates, 'category', 'sub_category', 'service');
        return [$directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service];
    }

    protected function getDirectorateList($directorates) {
        $result = [];
        foreach ($directorates as $directorate) {
            $result [$directorate->id] = $directorate->name;
        }
        return $result;
    }

    protected function getSubnodesArray($nodes, $subnode, $childnode = null, $subchild_node = null) {
        $result = $subresult = $subchild_result = [];
        foreach ($nodes as $node) {
            if (isset($node->$subnode) && count($node->$subnode)) {
                foreach ($node->$subnode as $subnodevalue) {
                    $result[$node->id][$subnodevalue->id] = $subnodevalue->name;
                    if ((!is_null($childnode)) && (isset($subnodevalue->$childnode)) && count($subnodevalue->$childnode)) {
                        foreach ($subnodevalue->$childnode as $child) {
                            $subresult[$subnodevalue->id][$child->id] = $child->name;
                            if ((!is_null($subchild_node)) && (isset($child->$subchild_node)) && count($child->$subchild_node)) {
                                foreach ($child->$subchild_node as $subnode_value) {
                                    $subchild_result[$child->id][$subnode_value->id] = $subnode_value->name;
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($childnode == null)
            return $result;
        if ($childnode != null && $subchild_node == null)
            return [$result, $subresult];
        if ($childnode != null && $subchild_node != null)
            return [$result, $subresult, $subchild_result];
    }

    protected function getService($record) {
        $data = [];
        if (isset($record['directorate']) && $record['speciality'] && $record['name']) {
            $data['directorate'] = $record['directorate'];
            $data['speciality'] = $record['speciality'];
            $data['category'] = isset($record['category']) ? $record['category'] : "n/a";
            $data['subcategory'] = isset($record['subcategory']) ? $record['subcategory'] : "n/a";
            $data['name'] = $record['name'];
            $data['contactable'] = isset($record['contactable']) ? $record['contactable'] : "n/a";
            $data['min_standards'] = isset($record['min_standards']) ? $record['min_standards'] : "n/a";
        }

        return $data;
    }

    protected function getTemplate($record) {
        $data = NULL;
        if (
                isset($record['start_date']) &&
                isset($record['start_date']) &&
                isset($record['cycle_length']) &&
                isset($record['starting_week'])
        ) {
            $data['start_date'] = $record['start_date'];
            $data['end_date'] = $record['end_date'];
            $data['cycle_length'] = $record['cycle_length'];
            $data['starting_week'] = $record['starting_week'];
        }
        return $data;
    }

    protected function getEvent($record, $id) {
        $event = NULL;
        if (
                isset($record['week_no']) &&
                isset($record['day']) &&
                isset($record['start']) &&
                isset($record['duration']) &&
                isset($record['site_id'])
        ) {

            $event['id'] = $id;
            $event['week_no'] = $record['week_no'];
            $event['day'] = $record['day'];
            $event['start'] = $record['start'];
            $event['duration'] = $record['duration'];
            $event['site_id'] = $record['site_id'];
            if (isset($record['location_id']))
                $event['location_id'] = $record['location_id'];
        }
        return $event;
    }

    protected function getEventFormated($record) {
        $event = NULL;
        if (
                isset($record['week_no']) &&
                isset($record['start_day']) &&
                isset($record['start_time']) &&
                isset($record['duration']) &&
                isset($record['site'])
        ) {
            $event['week_no'] = $record['week_no'];
            $event['day'] = $record['start_day'];
            $event['start'] = $record['start_time'];
            $event['duration'] = $record['duration'];
            $event['site'] = $record['site'];
            if (isset($record['locations']))
                $event['locations'] = $record['locations'];
        }
        return $event;
    }

    protected function getData($record) {
        $data = [];

        if (isset($record['directorate_speciality_id']) && isset($record['name'])) {
            $data['id'] = $record['id'];
            $data['directorate_speciality_id'] = $record['directorate_speciality_id'];
            $data['category_id'] = isset($record['category_id']) ? $record['category_id'] : 0;
            $data['sub_category_id'] = isset($record['sub_category_id']) ? $record['sub_category_id'] : 0;
            $data['name'] = $record['name'];
            $data['contactable'] = (($record['contactable'] == 'Yes') ? 1 : 0);
            $data['min_standards'] = (($record['min_standards']) ? 1 : 0);
            $data['template'] = $record['template'];
        }
        return $data;
    }

    protected function serviceStoreParameters($directorate_id, $data_node, $directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service, $service_flag) {
        $speciality_id = (array_search($data_node['speciality'], $speciality_array[$directorate_id]) ? array_search($data_node['speciality'], $speciality_array[$directorate_id]) : 0);
        if ($speciality_id != 0) {
            $service_flag = true;
            $category_id = ($data_node['category'] == 'N/A' ? 0 : array_search($data_node['category'], $category_array[$directorate_id]));
            if ($category_id != 0)
                $subcategory_id = ($data_node['subcategory'] == 'N/A' ? 0 : array_search($data_node['subcategory'], $subcategory_array[$category_id]));
            else
                $subcategory_id = 0;
            $service = Service::where(['directorate_speciality_id' => $speciality_id, 'category_id' => $category_id, 'sub_category_id' => $subcategory_id, 'name' => $data_node['name']])->first();
        } else {
            $service_flag = false;
            $service = null;
        }
        return [$speciality_id, (isset($category_id) ? $category_id : null), (isset($subcategory_id) ? $subcategory_id : null), $service_flag, $service];
    }

    protected function getSiteLocationArray($trust_id) {
        $site_array = $location_array = [];
        $sites = \App\Site::with('location')->where('trust_id', $trust_id)->get();
        foreach ($sites as $site) {
            $site_array[$site->id] = $site->name;
            if (isset($site->location) && count($site->location)) {
                foreach ($site->location as $location) {
                    $location_array[$site->id][$location->id] = $location->name;
                }
            }
        }
        return [$site_array, $location_array];
    }

    protected function getTemplateStore($template, $trust_id) {
        $result = [];
        $result = $this->getTemplate($template, $trust_id);
        $id = 1;
        foreach ($template['events'] as $event) {
            if (($event['week_no'] > 0 && $event['week_no'] <= $template['cycle_length']) &&
                    ($event['day'] >= 0 && $event['day'] <= 6) &&
                    (is_numeric($event['duration']))) {
                list($site_array, $location_array) = $this->getSiteLocationArray($trust_id);
                $site_id = array_search($event['site'], $site_array);
                if ($site_id) {
                    $event['site_id'] = $site_id;
                    $locations = $location_array[$site_id];
                    if (isset($event['locations'])) {
                        $location_names = explode(',', $event['locations']);
                        foreach ($location_names as $location_node) {
                            $event['location_id'][] = array_search($location_node, $locations);
                        }
                    }
                }
                $result['events'][] = $this->getEvent($event, $id);
                $id++;
            }
        }
        return $result;
    }

    protected function getFormatedData($data) {
        $result = [];
        $i = -1;
        $previous_service_name = NULL;
        foreach ($data as $data_node) {
            if ($previous_service_name != $data_node['name']) {
                $i++;
                $previous_service_name = $data_node['name'];
                $events = $template = NULL;
                $result[$i] = $this->getService($data_node);
                $result [$i]['template'] = $this->getTemplate($data_node);
            }
            $event = $this->getEventFormated($data_node);
            if ($result [$i]['template'] != NULL && $event != NULL) {
                $result [$i]['template']['events'][] = $event;
            }
        }
        return $result;
    }

    protected function getDataToStore($formated_data, $directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service, $trust_id) {
        $data_store = [];
        foreach ($formated_data as $data_node) {
            $specialities_store = [];
            $events = $template = $record = null;
            $category_id = $subcategory_id = $directorate_id = $speciality_id = 0;
            $service_flag = false;
            $directorate_id = array_search($data_node['directorate'], $directorate_array);
            if ($directorate_id) {
                list($speciality_id, $category_id, $subcategory_id, $service_flag, $service) = $this->serviceStoreParameters($directorate_id, $data_node, $directorate_array, $speciality_array, $speciality_service, $category_array, $category_service, $subcategory_array, $subcategory_service, $service_flag);
                if (isset($data_node['template']) && $data_node['template'] != Null) {
                    $template = $this->getTemplateStore($data_node['template'], $trust_id);
                }
                if ($service_flag) {
                    $data_node['id'] = (isset($service->id) ? $service->id : 0);
                    $data_node['directorate_speciality_id'] = $speciality_id;
                    $data_node['category_id'] = $category_id;
                    $data_node['sub_category_id'] = $subcategory_id;
                    $data_node['template'] = $template;
                    $data_store[] = $this->getData($data_node);
                }
            }
        }
        return $data_store;
    }

    protected function formatServiceRunning($all_data) {
        $all_staff = [];
        foreach ($all_data as $time_id => $values) {
            foreach ($values as $data) {
                $all_staff[] = [
                    'service_id' => $data['id'],
                    'transition_time_id' => $time_id,
                    'date' => $data['date'],
                    'day' => $data['day'],
                    'week' => $data['week'],
                    'start' => $data['start'],
                    'stop' => $data['stop'],
                ];
            }
        }
        return $all_staff;
    }

    protected function validationServiceRunning($all_service) {
        $errors = $store_staff = [];
        foreach ($all_service as $service) {
            $validator = $this->serviceRunningValidate($service);
            if (($validator->fails()))
                $errors [] = $validator->messages();
            else {
                $store_staff[] = $service;
            }
        }
        return [$store_staff, $errors];
    }

}
