<?php

namespace App\Http\Controllers;

use File;
use App\ServiceRequirement;
use App\ServiceRequirementTwo;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Trust;
class ServiceRequirementController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedRequirement(array $data) {
        return Validator::make($data, [
                    'service_id' => 'required|integer',
                    'priority' => 'integer',
                    'color' => 'required|string',
                    'parameter' => 'required|string',
                    'operator' => 'required|string',
                    'value' => 'required|string',
                    'status' => 'required|string',
                    'number' => 'required|integer',
        ]);
    }

    public function index() {
        try {
            return response()->json(ServiceRequirement::all());
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }

    public function store(Request $request) {
        $records = $error = $store = [];
        foreach ($request->all() as $node) {
            $validator = $this->validatedRequirement($node);
            if ($validator->fails())
                $error [$node['color']][] = $validator->messages();
            else
                $store[] = $node;
        }
        foreach ($store as $value) {
            $records[] = \App\ServiceRequirement::updateOrCreate(['id' => $value['id']], [
                        'service_id' => $value['service_id'],
                        'priority' => $value['priority'],
                        'color' => $value['color'],
                        'parameter' => $value['parameter'],
                        'operator' => $value['operator'],
                        'value' => $value['value'],
                        'value_2' => isset($value['value_2']) ? $value['value_2'] : '',
                        'status' => $value['status'],
                        'number' => $value['number'],
            ]);
            $records2[] = \App\ServiceRequirementTwo::updateOrCreate(['id' => $value['id']], [
                        'service_id' => $value['service_id'],
                        'priority' => $value['priority'],
                        'color' => $value['color'],
                        'parameter' => $value['parameter'],
                        'operator' => $value['operator'],
                        'value' => $value['value'],
                        'value_2' => isset($value['value_2']) ? $value['value_2'] : '',
                        'status' => $value['status'],
                        'number' => $value['number'],
            ]);
        }
        return response()->json(['success' => count($records) . ' requirements added or updated successfully.', 'error' => $error]);
    }

    public function show($id) {
        $record = \App\ServiceRequirement::with('service')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validatedRequirement($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = \App\ServiceRequirement::find($id);
            $record2 = \App\ServiceRequirementTwo::find($id);
            if ($record) {
                if ($record->update($request->all())) {
                    $record2->update($request->all());
                    return response()->json(['updated' => 'Record updated successfully']);
                } else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroy($id) {
        $record = ServiceRequirement::find($id);
        $record2 = ServiceRequirementTwo::find($id);
        if ($record) {
//            $service = ServiceRequirement::where('condition_id' , $record->id)->get();
//            foreach($service as $deleteChild){
//               $child = ServiceRequirement::find($deleteChild->id);
//                $child->update(['condition'=>null , 'condition_id'=>null]);
//            }
//            $record->update(['condition'=>null , 'condition_id'=>null]);
            if ($record->where(['condition' => null, 'condition_id' => null, 'id' => $id])->delete()) {
                $record2->where(['condition' => null, 'condition_id' => null, 'id' => $id])->delete();
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function testing($id, $sid) {
        $record = \App\Directorate::with(['speciality' => function($query)use ($sid) {
                        $query->find($sid);
                    }, 'speciality.category_priority.category.service.requirement'])->find($id);
        return $record;
    }

    public function overview($id, $sid, $date) {
        $date = date('Y-m-d', $date);
        $directorate = \App\Directorate::with(['speciality.service'=>function($query)use($date){$query->where('template->start_date','<=',$date)->where('template->end_date','>=',$date)->get();}])->get();
        return $directorate;
    }

    public function getRequirementData(Request $request) {
        $requrement = ServiceRequirement::where(['service_id' => $request->service_id, 'color' => $request->color])->get();
        $grades = \App\Grade::where(['directorate_id' => $request->directorate_id])->get();
        $roles = \App\Role::where(['directorate_id' => $request->directorate_id])->get();
        $qualifications = \App\Qualification::get();
        $sites = \App\Site::get();
        $locations = \App\Location::get();

        return ['requirement' => $requrement, 'grades' => $grades,
            'roles' => $roles, 'qualifications' => $qualifications, 'sites' => $sites, 'locations' => $locations];
    }

    public function saveRequirementCondition(Request $request) {
        $service = new ServiceRequirement();

        $requirement1 = $service->where(['service_id' => $request->service_id, 'id' => $request->requirement1])->first();
        $requirement2 = $service->where(['service_id' => $request->service_id, 'id' => $request->requirement2])->first();
        if ($request->requirement3 && !is_null($request->condition2)) {
            $requirement3 = $service->where(['service_id' => $request->service_id, 'id' => $request->requirement3])->first();
        }
//        if ($request->requirement1 == $requirement2->condition_id && $request->requirement2 == $requirement1->condition_id) {
//            $requirement1->update(['condition' => $request->condition, 'condition_id' => 0]);
//            $requirement2->update(['condition' => $request->condition, 'condition_id' => $request->requirement1]);
//            return ['seccess' => 'data updated'];
//        }
//        if (!empty($requirement1->condition_id)) {
//            $service->where(['id' => $requirement1->condition_id])->update(['condition' => null, 'condition_id' => null]);
//        }
//        if (!empty($requirement2->condition_id)) {
//            $service->where(['id' => $requirement2->condition_id])->update(['condition' => null, 'condition_id' => null]);
//        }
//        $requirement1->update(['condition' => $request->condition, 'condition_id' => $request->requirement2]);
//        $requirement2->update(['condition' => $request->condition, 'condition_id' => $request->requirement1]);

        if (is_null($requirement1->condition_id) && is_null($requirement2->condition_id)) {
            $service->where(['id' => $requirement1->id, 'condition' => null])->update(['condition' => $request->condition1, 'condition_id' => 0]);
            $service->where(['id' => $requirement2->id, 'condition' => null])->update(['condition' => $request->condition1, 'condition_id' => $requirement1->id]);
            if (is_null($requirement3->condition_id)) {
                $requirement2 = $service->find($requirement2->id);
                if (!is_null($request->condition2) && !empty($request->condition2) && !is_null($requirement2->condition_id)) {
//                    return [$request->condition2];
                    $service->where(['id' => $requirement3->id, 'condition' => null])->update(['condition' => $request->condition2, 'condition_id' => $requirement1->id]);
                    $service->where(['id' => $requirement2->id])->update(['condition' => $request->condition2, 'condition_id' => $requirement1->id]);
                }
            }
            return ['seccess' => 'data updated'];
        }
        return ['error' => 'please remove condition first then update'];
    }

    public function deleteRequirementCondition(Request $request) {
        $requirement = new ServiceRequirement();
        $delete = $requirement->whereIn('id', $request->ids)->update(['condition' => null, 'condition_id' => null]);
        if ($delete)
            return response()->json(['success' => 'Condition deleted successfully.']);
        else
            return response()->json(['error' => 'Condition not deleted.']);
    }

    public function makeCondition(Request $request) {
        $ids = json_encode($request->ids);
        $requrirement = ServiceRequirement::whereIn('id', $request->ids)->update(['condition' => $request->condition_value, 'condition_id' => $ids]);
        if ($requrirement)
            return response()->json(['success' => 'Condition added successfully.']);
        else
            return response()->json(['error' => 'Condition not added.']);
    }

}
