<?php

namespace App\Http\Controllers;

use File;
use App\AdditionalParameter;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdditionalParameterController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedParameter(array $data) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'parameter' => 'required|string',
                    'value' => 'required|string',
        ]);
    }

    public function index() {
        return response()->json(AdditionalParameter::all());
    }

    public function store(Request $request) {
        $additional_parameter_two = $this->getAdditionalParameterTwo();
        $validator = $this->validatedParameter($request->all());
         if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        $parameter = AdditionalParameter::where(['directorate_id'=>$request->directorate_id, 'parameter'=>$request->parameter])->first();
        $parameter2 = $additional_parameter_two->where(['directorate_id'=>$request->directorate_id, 'parameter'=>$request->parameter])->first();
        if(!empty($parameter)){
            return response(['error'=>['parameter'=>['Parameter already exist this directorate']]]);
        }
        else {
            $new = AdditionalParameter::create($request->all());
            $new2 = $additional_parameter_two->create($request->all());
            if($new && $new2){
                return response()->json(['success' => 'Record added successfully.']);
            }else{
                return response()->json(['error' => 'Record not added.']);
            }
            
        }
    }

    public function show($id) {
        $record = AdditionalParameter::with('directorate')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $additional_parameter_two = $this->getAdditionalParameterTwo();
        $validator = $this->validatedParameter($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = AdditionalParameter::find($id);
            $record2 = $additional_parameter_two->find($id);
            if ($record) {
                if ($record->update($request->all()) && $record2->update($request->all()))
                    return response()->json(['updated' => 'Record updated successfully']);
                else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroy($id) {
        $additional_parameter_two = $this->getAdditionalParameterTwo();
        $record = AdditionalParameter::find($id);
        $record2 = $additional_parameter_two->find($id);
        if ($record) {
            if ($record->delete() && $record2->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }
    
    public function directorateAdditionalParameterUpload(Request $request) {
        $additional_parameter_two = $this->getAdditionalParameterTwo();
        $additionalParamenters = $request->all();
        $dataCreate = [];
        foreach ($additionalParamenters['data'] as $data){
            $directorate = \App\Directorate::where(['name'=>$data['directorate'] , 'trust_id'=>  $additionalParamenters['trust_id']])->first();
            if(!empty($directorate)){
                $additionalParamenter = AdditionalParameter::where('parameter' , $data['name'])->first();
                $additionalParamenter2 = $additional_parameter_two->where('parameter' , $data['name'])->first();
                if(empty($additionalParamenter)){
                   $dataCreate['parameter'] =  $data['name'];
                   $dataCreate['value'] =  $data['values'];
                   $dataCreate['directorate_id'] =  $directorate->id;
                   AdditionalParameter::create($dataCreate);
                   $additional_parameter_two->create($dataCreate);
                }       
            }
        }
        return ['success'=>'parameter created'];
    }
    
    public function getAdditionalParameter($value,$directorate_id) {
        $additionalParameter = AdditionalParameter::where(['parameter'=>$value , 'directorate_id'=>$directorate_id])->first();
        return response($additionalParameter);
    }
    
    public function getAdditionalParameterTwo(){
        $additional_parameter_two = new \App\AdditionalParameterTwo();
        $additional_parameter_two->setConnection('pgsql2');
        return $additional_parameter_two;


    }


}
