<?php

namespace App\Http\Controllers;

use File;
use App\Role;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RoleController extends Controller {

    protected function validatedRole(array $data, $id = null) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'name' => 'required|unique_with:tb_role, directorate_id,' . $id,
                    'position' => 'required|integer',
        ]);
    }

    public function index() {
        return response()->json(\App\Role::all());
    }

    public function store(Request $request) {
        $role_two = $this->getRoleTwo();
        $validator = $this->validatedRole($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $increment = \App\Role::where('directorate_id', $request->directorate_id)->where('position', '>=', $request->position)->increment('position');
            $increment2 = $role_two->where('directorate_id', $request->directorate_id)->where('position', '>=', $request->position)->increment('position');
            $new = \App\Role::create($request->all());
            $new2 = $role_two->create($request->all());
            if ($new && $new2)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Role is not added.']);
        }
    }

    public function show($id) {
        $record = \App\Role::find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $role_two = $this->getRoleTwo();
        $validator = $this->validatedRole($request->all(), $id);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = \App\Role::find($id);
            $record2 = $role_two->find($id);
            if ($record) {
                if ($request->position > $request->current_position) {
                    $decrement = \App\Role::where('directorate_id', $record->directorate_id)->where('position', '>', $request->current_position)->where('position', '<', $request->position)->decrement('position');
                    $decrement2 = $role_two->where('directorate_id', $record->directorate_id)->where('position', '>', $request->current_position)->where('position', '<', $request->position)->decrement('position');
                    $request->merge(['position' => $request->position - 1]);
                    if ($record->update($request->all()) && $record2->update($request->all()))
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'Changes not updated']);
                } else if ($request->position < $request->current_position) {

                    $increment = \App\Role::where('directorate_id', $record->directorate_id)->where('position', '>=', $request->position)->where('position', '<', $request->current_position)->increment('position');
                    $increment2 = $role_two->where('directorate_id', $record->directorate_id)->where('position', '>=', $request->position)->where('position', '<', $request->current_position)->increment('position');
                    $record->update($request->all());
                    if ($record->update($request->all()) && $record2->update($request->all()))
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'Changes not updated']);
                }
                else if ($request->position == $request->current_position) {
                    if ($record->update($request->all()) && $record2->update($request->all()))
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'Changes not updated']);
                }
            } else
                return response()->json(['unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {
        $role_two = $this->getRoleTwo();
        $record = \App\Role::find($id);
        $record2 = $role_two->find($id);
        if ($record) {
            $decrement = \App\Role::where('directorate_id', $record->directorate_id)->where('position', '>', $record->position)->orderBy('position', 'asc')->decrement('position');
            $decrement2 = \App\RoleTwo::where('directorate_id', $record->directorate_id)->where('position', '>', $record->position)->orderBy('position', 'asc')->decrement('position');
            if ($record->delete() && $record2->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request, $id) {
        $data_store = [];
        foreach ($request->data as $data_node) {
            $directorate = \App\Directorate::where('trust_id', $id)->where('name', $data_node['directorate'])->get();
            if (count($directorate)) {
                $role = \App\Role::where('directorate_id', $directorate[0]->id)->where('name', $data_node['name'])->get();
                if (!count($role))
                    $data_store [] = ['directorate_id' => $directorate[0]->id, 'name' => $data_node['name'], 'position' => $data_node['position']];
            }
        }
        $count = 0;
        foreach ($data_store as $data) {
            $old = \App\Role::where('directorate_id', $data['directorate_id'])->where('position', '>=', $data['position']);
            if (count($old))
                $old->increment('position');
            $new = \App\Role::create($data);
            if (count($new))
                $count++;
        }
        if ($count)
            return response()->json(['success' => "Records added successfully"]);
        else
            return response()->json(['error' => "Records are not added"]);
    }

    public function getRoleTwo() {
        $role_two = new \App\RoleTwo();
        $role_two->setConnection('pgsql2');
        return $role_two;
    }

}
