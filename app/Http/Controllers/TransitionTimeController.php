<?php

namespace App\Http\Controllers;

use File;
use App\TransitionTime;
use App\TransitionTimeTwo;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TransitionTimeController extends Controller {

    protected function validateTransitionTime(array $data, $id = null) {
        return Validator::make($data, [
                    'directorate_id' => 'integer',
                    'directorate_speciality_id' => 'integer',
                    'start_time' => 'string|required',
                    'end_time' => 'string|required',
                    'name' => 'string|required|unique_with:tb_transition_time, directorate_id,' . $id
        ]);
    }

    public function index() {
        return response()->json(TransitionTime::all());
    }

    public function directorateTransitionTime($id) {
        return response()->json(\App\Directorate::with('transition_time')->find($id));
    }

    public function directoratesTransitionTime($id) {
        return response()->json(\App\Directorate::with('transition_time')->where('trust_id', $id)->get());
    }

    public function store(Request $request) {
        $validator = $this->validateTransitionTime($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = TransitionTime::create($request->all());
            $new = TransitionTimeTwo::create($request->all());
            if ($new)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record not added.']);
        }
    }

    public function show($id) {
        $record = TransitionTime::find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['error' => 'Unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateTransitionTime($request->all(), $id);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = TransitionTime::find($id);
            $record2 = TransitionTimeTwo::find($id);
            if ($record) {
                if ($record->update($request->all())) {
                    $record2->update($request->all());
                    return response()->json(['success' => 'record updated successfully']);
                } else
                    return response()->json(['error' => 'changes not updated']);
            } else
                return response()->json(['error' => 'unable to find record']);
        }
    }
    
    public function directorateTimes($id) {
        $directorates = \App\Directorate::with('transition_time')->where('trust_id', $id)->get();
        return response()->json($directorates);
    }

    public function destroy($id) {
        $record = TransitionTime::find($id);
        $record2 = TransitionTimeTwo::find($id);
        if ($record) {
            if ($record->delete()) {
                $record2->delete();
                return response()->json(['error' => 'Record deleted successfully']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

}
