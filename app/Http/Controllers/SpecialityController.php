<?php

namespace App\Http\Controllers;

use File;
use App\DirectorateSpeciality;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SpecialityController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedDirectorateSpeciality(array $data, $id = null) {
        return Validator::make($data, [
                    'directorate_id' => 'required',
                    'name' => 'required|unique_with:tb_directorate_speciality,directorate_id,' . $id,
                    'abbreviation' => 'required',
                    'status' => 'required',
        ]);
    }

    public function index() {
        return response()->json(DirectorateSpeciality::all());
    }

    public function store(Request $request) {
        $speciality_two = $this->getSpecialityTwo();
        $validator = $this->validatedDirectorateSpeciality($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        } else {
            $new = DirectorateSpeciality::create($request->all());
            $new2 = $speciality_two->create($request->all());
            if ($new && $new2) {
                return response()->json(['success' => 'Record added successfully.']);
            } else {
                return response()->json(['error' => 'Record not added.']);
            }
        }
    }

    public function show($id) {
        $result = DirectorateSpeciality::with(['additional_grade' => function ($query) {
                        $query->orderBy('position', 'asc');
                    }, 'directorate', 'service', 'post'])->find($id);
        return response()->json($result);
    }

    public function update(Request $request, $id) {
        $speciality_two = $this->getSpecialityTwo();
        $validator = $this->validatedDirectorateSpeciality($request->all(), $id);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = DirectorateSpeciality::find($id);
            $record2 = \App\DirectorateSpecialityTwo::find($id);
            if ($record) {
                if ($record->update($request->all())) {
                    $record2->update($request->all());
                    return response()->json(['success' => 'Record updated successfully']);
                } else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroy($id) {
        $speciality_two = $this->getSpecialityTwo();
        $record = DirectorateSpeciality::find($id);
        $record2 = $speciality_two->find($id);
        if ($record) {
            if ($record->delete() && $record2->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $speciality_two = $this->getSpecialityTwo();
        $data_store = $storing = [];
        foreach ($request->data as $data_node) {
            if(empty($data_node['directorate'])) {
                continue;
            }
            $directorate = \App\Directorate::where('trust_id', $request->trust_id)->where('name', $data_node['directorate'])->get();
            if (count($directorate)) {
                $speciality = DirectorateSpeciality::where('directorate_id', $directorate[0]->id)->where('name', $data_node['name'])->get();
            }
            if (!count($speciality))
                $data_store [] = ['directorate_id' => $directorate[0]->id, 'name' => $data_node['name'], 'abbreviation' => $data_node['abbreviation']];
        }
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
        $new = DirectorateSpeciality::insert($data_store);
        $new2 = $speciality_two->insert($data_store);
        if ($new && $new2)
            return response()->json(['success' => "Records added successfully"]);
    }

    public function getSpecialityTwo() {
        $speciality_two = new \App\DirectorateSpecialityTwo();
        $speciality_two->setConnection('pgsql2');
        return $speciality_two;
    }

}
