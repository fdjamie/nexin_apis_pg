<?php

namespace App\Http\Controllers;

use File;
use App\Color;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ColorController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedColor(array $data) {
        return Validator::make($data, [
                    'name' => 'required|string',
                    'letter' => 'required|string',
                    'position' => 'required|integer',
        ]);
    }

    public function index() {
        return response()->json(Color::orderBy('position', 'asc')->get());
    }

    public function store(Request $request) {
        $validator = $this->validatedColor($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $old = Color::where('position', '>=', $request->position)->increment('position');
            $new = Color::create($request->all());
            if ($new)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record not added.']);
        }
    }

    public function show($id) {
        $record = Color::find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validatedColor($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = Color::find($id);
            if ($record) {
                if ($request->position > $record->position) {
                    $decrement = Color::whereBetween('position', [$record->position, $request->position])->decrement('position');
                    $record->update($request->all());
                    if ($record && $decrement)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'No changes updated']);
                } else if ($request->position < $record->position) {

                    $increment = Color::where('position', '>=', $request->position)->where('position', '<', $record->position)->increment('position');
                    $record->update($request->all());
                    if ($record && $increment)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'No changes updated']);
                }
                else if ($request->position == $record->position) {
                    $record->update($request->all());
                    if ($record)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'No changes updated']);
                }
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {
        $record = Color::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }



}
