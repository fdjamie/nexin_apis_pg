<?php

namespace App\Http\Controllers;

use File;
use App\Group;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GroupController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateGroup(array $data) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'name' => 'required|unique_with:tb_groups , directorate_id',
        ]);
    }

    public function index() {
        return response()->json(Group::with('subgroup')->get());
    }

    public function store(Request $request) {
        $validator = $this->validateGroup($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = Group::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function show($id) {
        $record = Group::with('subgroup.location')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateGroup($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = Group::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['updated' => 'Record updated successfully']);
                else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $groups = $this->gettingFormatedGroups($request->trust_id, $request->data);
        if (count($groups) == 0)
            return response()->json(['error' => "No record is added"]);
        else {
            $result = $this->createOrUpdateGroups($groups);
            if (count($result)) {
                return response()->json(['success' => count($result) . " Records updated or created successfully"]);
            } else {
                return response()->json(['success' => " no changes after upload"]);
            }
        }
    }

    public function destroy($id) {
        $record = Group::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

//################################### PROTECRTED FUNCTIONS ###############################

    protected function getDirectorateNameArray($trust_id) {
        $directorate_names = [];
        $directorates = \App\Directorate::with('group')->where('trust_id', $trust_id)->get();
        foreach ($directorates as $directorate) {
            $directorate_names[$directorate->id] = $directorate->name;
        }
        return $directorate_names;
    }

    protected function gettingFormatedGroups($trust_id, $data) {
        $groups = $directorate_array = [];
        $directorate_array = $this->getDirectorateNameArray($trust_id);
        foreach ($data as $data_node) {
            $directorate_id = array_search($data_node['directorate'], $directorate_array);
            if ($directorate_id) {
                $groups [] = ['directorate_id' => $directorate_id, 'name' => $data_node['name']];
            }
        }
        return $groups;
    }

    protected function createOrUpdateGroups($groups) {
        $records = [];
        foreach ($groups as $group) {
            $records [] = Group::updateOrCreate(['directorate_id' => $group['directorate_id'], 'name' => $group['name']], [
                        'directorate_id' => $group['directorate_id'],
                        'name' => $group['name']
            ]);
        }
        return $records;
    }

}
