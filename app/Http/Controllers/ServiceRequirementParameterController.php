<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parameter;
use App\Http\Requests;
use App\Http\Requests\ServiceParameterRequest;
use Validator;
use App\Operator;

class ServiceRequirementParameterController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function index() {
        $parameter = Parameter::with('directorate')->get();
        return response($parameter);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return response('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function validatedCategoryPriority(array $data, $id = null) {
//         return [$data];


        $rules['directorate_id'] = 'required|integer';
        if ($id == null) {
            if (!empty($data['name'])) {
                return Validator::make($data, [
                            'directorate_id' => 'required|integer',
                            'name' => 'required|string',
                            'operator' => 'required'
                ]);
            }
            if (!empty($data['parameters'])) {
//                $rules['parameters'] = 'required|string|unique_with:tb_parameters,directorate_id,parameters = name';
                $rules['parameters'] = 'required|string';
                $rules['operator'] = 'required';
                return Validator::make($data, $rules);
            }
        }
    }

    public function store(Request $request) {
        $validator = $this->validatedCategoryPriority($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        }
        if (!empty($request->name)) {
            $parameter = Parameter::where(['directorate_id' => $request->directorate_id, 'name' => $request->name])->first();
            if (!empty($parameter)) {
                return response(['error' => ['parameter' => ['Parameter already exist this directorate']]]);
            }
        }
        if (!empty($request->parameters)) {
            $parameter = Parameter::where(['directorate_id' => $request->directorate_id, 'name' => $request->parameters])->first();
            if (!empty($parameter)) {
                return response(['error' => ['parameter' => ['Parameter already exist this directorate']]]);
            }
        }
        $response = '';
        $data = $request->only(['directorate_id', 'operator']);
        if ($request->parameters == 'additional_parameter') {
            if (!empty($request->name)) {
                $data['name'] = $request->name;
                $response = Parameter::create($data);
            }
        } else {
            $data['name'] = $request->parameters;
            $response = Parameter::create($data);
        }
        return response($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $parameter = Parameter::with('directorate')->where('id', $id)->get();
        return response($parameter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return response('unable to access');
        $parameter = Parameter::with('directorate')->where('id', $id)->first();
        return response($parameter);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(request $request, $id) {
        $validator = Validator::make($request->all(), [
                    'operator' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        }
        $data = $request->only(['operator']);
        $record = Parameter::find($id);
        if ($record) {
            $response = Parameter::where('id', $id)->update(['operator' => json_encode($data['operator'])]);
            if ($response) {
                return response()->json(['updated' => 'Record updated successfully']);
            } else {
                return response()->json(['error' => 'Changes not updated']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Parameter::destroy($id);
        return response(['message' => 'Parameter Deleted successfully']);
    }

    public function getParameter($id) {
//        $parameter = Parameter::with('directorate')->where('directorate_id', $id)->get();
        $parameter = \App\Directorate::with('parameters')->find($id);
        return response($parameter);
    }

    public function getParameterType(Request $request) {

        $parameter = Parameter::where(['directorate_id' => $request->directorate_id, 'name' => $request->name])->first();
        if (!empty($parameter)) {
            $operators = Operator::where(['type' => $parameter->type])->get();
            return response($operators);
        }
        return response(['error' => 'not found']);
    }

    public function directorateParameter($id) {
        $records = [];
        $records = \App\Directorate::with('parameters')->where('trust_id', $id)->get();
        return response()->json($records);
        foreach ($records as $directorate) {
            foreach ($directorate->parameters as $parameter) {
                if (isset($parameter->operator))
                    $parameter->operator = Operator::whereIn('id', $parameter->operator)->get();
            }
        }
        return response()->json($records);
    }

    public function getOperator() {
        $operator = Operator::pluck('name', 'id')->all();
        return response($operator);
    }

    public function getOperatorValue() {
//    $operator = Operator::pluck('name', 'value')->all();

        $operator = Operator::all();

        return response($operator);
    }

    public function getOperatorbyParameter(Request $request) {
        $operator = Parameter::where(['name' => ucfirst($request->name), 'directorate_id' => $request->directorate_id])->get();
        foreach ($operator as $directorate) {
            $directorate->operator = Operator::whereIn('id', $directorate->operator)->pluck('name', 'value');
        }
        return response($operator);
    }

}
