<?php

namespace App\Http\Controllers;

use App\Trust;
use App\User;
use http\Exception;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\UserSignUpRequest;
use DB;

class UnauthenticatedApisController extends Controller {

    public function index() {
        $specialities = new \App\Speciality();
        return response()->json($specialities);
    }

    protected function userValidate(array $data) {
        return Validator::make($data, [
                    'is_admin' => 'required|boolean',
                    'trust_id' => 'required|integer',
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:tb_user',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function userCreate(array $data) {
        return \App\User::create([
//                    'is_admin' => isset($data['is_admin']) ? "1" : "0",
                    'is_admin' => $data['is_admin'],
                    'trust_id' => $data['trust_id'],
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'phone'=> $data['phone'],
                    'password' => bcrypt($data['password']),
                    'api_token' => str_random(60), //slight change here
        ]);
    }

    public function doLogin(Request $request) {


        $validator = Validator::make($request->all(), ['email' => 'required|email', 'password' => 'required|min:6']);
        if (!$validator->fails()) {
            if (Auth::once(array('email' => $request->email, 'password' => $request->password))) {
                return response()->json(['error' => NULL, 'user_id' => auth()->user()->id, 'status' => 'Login Success']);
            }

            return response()->json(['error' => 'yes', 'messages' => ['email' => ['Login Fail, email or password not correct']], 'status' => 'Login Fail, email or password not correct']);
        }
        return response()->json(['error' => 'yes', 'messages' => $validator->messages(), 'status' => 'Login Fail, missing credentials']);
    }

    public function createUser(Request $request) {

        $validator = $this->userValidate($request->all());
        if (!$validator->fails()) {
            $user = $this->userCreate($request->all());
            return response()->json(['user' => $user, 'status' => 'Registration Success']);
        } else {

            return response()->json(['error' => 'yes', 'messages' => $validator->messages(), 'status' => 'Registration Failed']);
        }
    }



    public function getAllTrusts()
    {
        return response()->json(Trust::all());
    }



    public function  storeSignupRequest(Request $request)
    {


        $validator = $this->validateSignupRequest($request->all());
        if (!$validator->fails()) {

                 $store=$this->createUserAndRequest($request);
                 if($store)
                 {
                     return response()->json(['status'=>true,'msg'=>'Your Account successfully Created And Request has been Sent to Trust Admin']);
                 }

                 else
                     {
                     return response()->json(['status'=>false,'errors'=>['error'=>['Unknown Error Occured! Try Again']]]);
                 }
        }
        else {

            return response()->json(['status' => false, 'errors' => $validator->errors()]);


        }


    }

    protected function createUserAndRequest($request)
    {
        try {
            DB::beginTransaction();
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'api_token' => str_random(60),
                'signup' => true
            ]);
            if ($user) {
                $storeRequest=UserSignUpRequest::create([
                    'user_id'=>$user->id,
                    'trust_id'=>$request->trust,
                    'status'=>'pending',

                ]);
                if($storeRequest)
                {
                    DB::commit();
                   return true;
                }
                else
                {
                    DB::rollBack();
                    return false;
                 }



            }
            else {
                DB::rollBack();
                return false;            }
        }

        catch (\Exception $e){

            return false;
        }

    }
    protected function validateSignupRequest(array $data) {

        return Validator::make($data,[
            'trust' => 'required',
            'name' => 'required',
            'email' => 'required|email|max:255|unique:tb_user',
            'password' => 'required|min:6|confirmed',
        ]);
    }







}
