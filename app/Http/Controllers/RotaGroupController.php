<?php

namespace App\Http\Controllers;

use File;
use App\RotaGroup;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RotaGroupController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function rotaGroupValidate(array $data) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'name' => 'required|unique_with:tb_rota_groups, directorate_id',
        ]);
    }

    protected function rotaGroupUpdateValidate(array $data) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'name' => 'required',
        ]);
    }

    public function index() {
        return response()->json(RotaGroup::with('directorate')->get());
    }

    public function store(Request $request) {
        $validator = $this->rotaGroupValidate($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = RotaGroup::create($request->all());
            if ($new)
                return response()->json(['success' => 'Rotagroup added successfully.']);
            else
                return response()->json(['error' => 'Rotagroup is not added.']);
        }
    }

    public function show($id) {
        $record = RotaGroup::with('shift_type', 'rota_template')->find($id);
        if ($record)
            return response()->json($record);
        else
            return response()->json(['Unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->rotaGroupUpdateValidate($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = RotaGroup::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['Record updated successfully']);
                else
                    return response()->json(['Changes not updated']);
            } else
                return response()->json(['Unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {
        $record = \App\RotaGroup::find($id);
        if ($record) {
            if ($record->delete()) {
                $message = 'Record deleted successfully';
            } else {
                $message = 'Record is not deleted';
            }
        } else {
            $message = 'Unable to find record';
        }
        return response()->json($message);
    }

    public function upload(Request $request, $id) {
//return [$request->all()];
        $data_store = $result = [];
        $directorates = \App\Directorate::with('rota_group')->where('trust_id', $id)->get();
//	return [$directorates];
        $directorate_array = $this->directorateArray($directorates);
//return [$directorate_array];
        $rotagroup_array = $this->rotaGroupArray($directorates);
//return [$rotagroup_array];
        foreach ($request->data as $data_node) {
            $directorate = array_search($data_node['directorate'], $directorate_array);
//		return [$directorate];
            if ($directorate) {
		if(isset($rotagroup_array[$directorate])){
$rota_group = array_search($data_node['name'], $rotagroup_array[$directorate]);
if (!$rota_group)
                    $data_store [] = ['directorate_id' => $directorate, 'name' => $data_node['name'], 'description' => (isset($data_node['description']) ? $data_node['description'] : '')];
}else{
$data_store [] = ['directorate_id' => $directorate, 'name' => $data_node['name'], 'description' => (isset($data_node['description']) ? $data_node['description'] : '')];
}
            }
        }
        if (!count($data_store)) {
            return response()->json(['error' => "No record to add."]);
        } else {
            foreach ($data_store as $node) {
                $result[] = RotaGroup::create($node);
            }
            if (count($result))
                return response()->json(['success' => count($result)." records added successfully"]);
            else
                return response()->json(['error' => "No record added"]);
        }
    }

    protected function directorateArray($directorates) {
        $result = [];
        foreach ($directorates as $driectorate) {
            $result[$driectorate->id] = $driectorate->name;
        }
        return $result;
    }

    protected function rotaGroupArray($directorates) {
        $result = [];
        foreach ($directorates as $driectorate) {
            if (isset($driectorate->rota_group) && count($driectorate->rota_group)) {
                foreach ($driectorate->rota_group as $rotagroup) {
                    $result[$driectorate->id][$rotagroup->id] = $rotagroup->name;
                }
            }
        }
        return $result;
    }

}
