<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Holiday;
use App\DirectorateService;
use App\DirectorateSpeciality;
use App\Service;
use Validator;
use Carbon\Carbon;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {

        $this->middleware('auth:api');
    }

    public function availableServicesOnHoliday(Request $request)
    {
        $holiday = $request->all();
        $date = Carbon::parse($holiday['date']);
        $directorate =  $holiday['directorate_id'];

        $generalServices = DirectorateService::select('id','name','template')->where(array('directorate_id'=>$directorate,'holiday_status'=>'1','deleted_at'=>null))->get();
        $dirSpeciality =   DirectorateSpeciality::with(['service'=>function($q) use($date){
            $q->where('template->start_date','<=',$date);
            $q->where('template->end_date','>=',$date);
            $q->where('holiday_status','1');
            $q->where('deleted_at',null);
        }])->select('id','name')->where(array('directorate_id'=>$directorate))->get();
//        $directorate = \App\Directorate::where('id',$directorate)->with(['speciality.service'=>function($query)use($date){$query->where('holiday_status',1)->where('template->start_date','<=',$date)->where('template->end_date','>=',$date)->get();}])->get();
//        $dirSpeciality = $directorate[0]->speciality;

        if($generalServices && $dirSpeciality)
            $runningServices = $this->filterServices( $generalServices , $dirSpeciality,$date );
        elseif (!$generalServices && $dirSpeciality)
            $runningServices = $this->filterServices([] , $dirSpeciality ,$date);
        elseif ($generalServices && !$dirSpeciality)
            $runningServices = $this->filterServices( $generalServices , [] ,$date);
        else
            $runningServices = [];

        if(sizeof($runningServices)==0 || (sizeof($runningServices['general'])==0 && sizeof($runningServices['speciality'])==0)){
            return response()->json(['status' => 'false', 'data' => 'no data found']);
        }else {
            return response()->json(['status' => 'true', 'data' => $runningServices]);
        }
    }

    protected function filterServices($gServices=array(),$speciality=array(),$date){

        if(!empty($gServices)){
           $generalServices =  $this->searchInGeneralServices($gServices,$date);

        }
        if(!empty($speciality)){
            $specialityServices = $this->searchInSpecialityServices($speciality,$date);
        }
        $gen = array();
            foreach ($generalServices as $general) {
                $gen[] = $general;
            }
            foreach ($specialityServices as $key=>$speServive) {
                if(sizeof($speServive->service)==0){
                    unset($specialityServices[$key]);
                }
            }
        $servicesRunning = array('general' =>$gen,'speciality'=>$specialityServices);
        return $servicesRunning;
    }

    protected function searchInGeneralServices($gServices,$date){
        foreach($gServices as $key=>$gS){
            $temp = $gS['template'];

            $decodedTemp = $temp;
            if($decodedTemp != null) {
                $gStart = Carbon::parse($decodedTemp->start_date);
                $gEnd = Carbon::parse($decodedTemp->end_date);
                if ($gStart > $date || $date > $gEnd) {
                    $gServices->forget($key);
                } else {
                    $start_week_flag = false;
                    $exist = false;
                    $weekDate = null;
                    $varDate = $gStart->subDays(1);

                    if (isset($decodedTemp->events) && sizeof($decodedTemp->events) > 0) {

                        while ($varDate <= $date) {
                            for ($j = 0; $j < sizeof($decodedTemp->events); $j++) {
                                if ($decodedTemp->events[$j]->week_no == $decodedTemp->starting_week) {
                                    $start_week_flag = true;
                                }
                                if ($decodedTemp->events[$j]->day == 0) {
                                    $decodedTemp->events[$j]->day = 7;
                                }
                                if ($start_week_flag) {
                                    for ($k = 1; $k < 8; $k++) {
                                        if ($decodedTemp->events[$j]->day == $k) {
                                            switch ($decodedTemp->events[$j]->week_no) {
                                                case 2:
                                                    $weekDate = $varDate->copy()->addDay($k + 7);
                                                    break;
                                                case 3:
                                                    $weekDate = $varDate->copy()->addDay($k + 14);
                                                    break;
                                                case 4:
                                                    $weekDate = $varDate->copy()->addDay($k + 21);
                                                    break;
                                                default:
                                                    $weekDate = $varDate->copy()->addDay($k);
                                            }
                                        }

                                        $week_num = $decodedTemp->events[$j]->week_no;
                                        if ($weekDate == $date) {
                                            $exist = true;
                                            $gS->template = $decodedTemp->events[$j];
                                            break 2;
                                        }
                                    }
                                } else {
                                    for ($k = 1; $k < 8; $k++) {
                                        if ($decodedTemp->events[$j]->day == $k) {
                                            switch ($decodedTemp->events[$j]->week_no) {
                                                case 2:
                                                    $weekDate = $varDate->copy()->addDay($k + 7);
                                                    break;
                                                case 3:
                                                    $weekDate = $varDate->copy()->addDay($k + 14);
                                                    break;
                                                case 4:
                                                    $weekDate = $varDate->copy()->addDay($k + 21);
                                                    break;
                                                default:
                                                    $weekDate = $varDate->copy()->addDay($k-1);
                                            }
                                        }
                                        $week_num = $decodedTemp->events[$j]->week_no;
                                        if ($weekDate == $date) {
                                            $exist = true;
                                            $gS->template = $decodedTemp->events[$j];
                                            break 2;
                                        }
                                    }
                                }
                            }
                            switch ($week_num) {
                                case 2:
                                    $varDate->addDay($k + 14);
                                    break;
                                case 3:
                                    $varDate->addDay($k + 21);
                                    break;
                                case 4:
                                    $varDate->addDay($k + 28);
                                    break;
                                default:
                                    $varDate->addDay(7);
                            }

                        }
                        if (!$exist) {
                            $gServices->forget($key);
                        }

                    } else {
                        $gServices->forget($key);
                    }
                }
            }else{
                $gServices->forget($key);
            }
        }
        return $gServices;
    }


    protected function searchInSpecialityServices($speciality,$date){
        foreach($speciality as $key=>$specialityService) {

            if(empty($specialityService->service)){
                $speciality->forget($key);
            }
            foreach ($specialityService->service as $key=>$sS) {
                $tempSp = $sS['template'];
                $decodedSTemp = $tempSp;
                if($decodedSTemp !=null) {
                    $spStart = Carbon::parse($decodedSTemp->start_date);
                    $spEnd = Carbon::parse($decodedSTemp->end_date);
                    if ($spStart > $date || $date > $spEnd) {
                        $specialityService->service->forget($key);
                    } else {
                        $start_week_flag = false;
                        $exist = false;
                        $weekDate = null;
                        $varDate = $spStart->subDays(1);
                        if (isset($decodedSTemp->events) && sizeof($decodedSTemp->events) > 0) {
                            while ($varDate <= $date) {
                                for ($j = 0; $j < sizeof($decodedSTemp->events); $j++) {
                                    if ($decodedSTemp->events[$j]->week_no == $decodedSTemp->starting_week) {
                                        $start_week_flag = true;
                                    }
                                    if ($decodedSTemp->events[$j]->day == 0) {
                                        $decodedSTemp->events[$j]->day = 7;
                                    }
                                    if ($start_week_flag) {
                                        for ($k = 1; $k < 8; $k++) {
                                            if ($decodedSTemp->events[$j]->day == $k) {
                                                switch ($decodedSTemp->events[$j]->week_no) {
                                                    case 2:
                                                        $weekDate = $varDate->copy()->addDay($k + 7);
                                                        break;
                                                    case 3:
                                                        $weekDate = $varDate->copy()->addDay($k + 14);
                                                        break;
                                                    case 4:
                                                        $weekDate = $varDate->copy()->addDay($k + 21);
                                                        break;
                                                    default:
                                                        $weekDate = $varDate->copy()->addDay($k);
                                                }

                                            }
                                            $week_num = $decodedSTemp->events[$j]->week_no;
                                            if ($weekDate == $date) {
                                                $exist = true;
                                                $sS->template = $decodedSTemp->events[$j];
                                                break 2;

                                            }

                                        }
                                    } else {
                                        for ($k = 1; $k < 8; $k++) {
                                            if ($decodedSTemp->events[$j]->day == $k) {
                                                switch ($decodedSTemp->events[$j]->week_no) {
                                                    case 2:
                                                        $weekDate = $varDate->copy()->addDay($k + 7);
                                                        break;
                                                    case 3:
                                                        $weekDate = $varDate->copy()->addDay($k + 14);
                                                        break;
                                                    case 4:
                                                        $weekDate = $varDate->copy()->addDay($k + 21);
                                                        break;
                                                    default:
                                                        $weekDate = $varDate->copy()->addDay($k);
                                                }
                                            }
                                            $week_num = $decodedSTemp->events[$j]->week_no;
                                            if ($weekDate == $date) {
                                                $exist = true;
                                                $sS->template = $decodedSTemp->events[$j];
                                                break 2;
                                            }
                                        }
                                    }
                                }
                                switch ($week_num) {
                                    case 2:
                                        $varDate->addDay($k + 14);
                                        break;
                                    case 3:
                                        $varDate->addDay($k + 21);
                                        break;
                                    case 4:
                                        $varDate->addDay($k + 28);
                                        break;
                                    default:
                                        $varDate->addDay(7);
                                }

                            }
                            if (!$exist) {
                                $specialityService->service->forget($key);
                            }

                        } else {
                            $specialityService->service->forget($key);
                        }
                    }
                }else{
                    $specialityService->service->forget($key);
                }
            }
        }
        return $speciality;
    }

    protected function validateHoliday(array $data) {
       //dd($data);
        $messages = ['after' => 'End date must be greater than Start Date',
                    'before' => 'Start Date must be smaller than End Date',
                    'unique' => 'Holiday Name has already be taken'
            ];

        return Validator::make($data, [
            'holidays' => 'array',
            'holidays.*.directorate_id' => 'required|integer',
//            'holidays.*.holiday_name' => 'required|unique:holidays,'.$data['holidays'][0]['directorate_id'],
            'holidays.*.holiday_name' => 'required|unique:holidays,holiday_name,NULL,id,directorate_id,'.$data['holidays'][0]['directorate_id'],
            'holidays.*.start_date' => 'required|date|before:holidays.*.end_date',
            'holidays.*.end_date' => 'required|date|after:holidays.*.start_date',
        ],$messages);
    }


    protected function validateUpdatedHoliday(array $data) {
        //dd($data);
        $messages = ['after' => 'End date must be greater than Start Date',
            'before' => 'Start Date must be smaller than End Date',
            'unique' => 'Holiday Name has already be taken'
        ];

        return Validator::make($data, [
            'holiday_name' => 'required|unique:holidays,holiday_name,NULL,id,directorate_id,'.$data['directorate_id'],
            'start_date' => 'required|date|before:end_date',
            'end_date' => 'required|date|after:start_date',
        ],$messages);
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validateHoliday($request->all());
        $data = $request->all();
        // dd($validator->messages());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            //dd($request->all());
            $new = Holiday::insert($data['holidays']);
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dir_holidays($id)
    {
        return response()->json(Holiday::where('directorate_id',$id)->get());
    }

    public function show($id)
    {
        $holidayObj = array();
        $holiday = Holiday::find($id)->getAttributes();
        if($holiday) {
            $dir = Holiday::find($id)->directorate->getAttributes();
            $holidayObj['holiday'] = $holiday;
            $holidayObj['directorate'] =  $dir;

            return response()->json(['status' => 'ok', 'data' => $holidayObj]);
        }else{
            return response()->json(['status' => 'not found']);
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($id);
        $holiday = Holiday::find($request->id);
        $validator = $this->validateUpdatedHoliday($request->all);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new =  $holiday->fill($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
