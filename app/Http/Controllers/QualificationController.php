<?php

namespace App\Http\Controllers;

use App\Qualification;
use App\QualificationTwo;
use App\Site;
use App\Http\Requests;
use Validator;
use Illuminate\Http\Request;

class QualificationController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateQualification(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:128'
        ]);
    }

    public function index() {
        return response()->json(Qualification::all());
    }

    public function store(Request $request) {
        $validator = $this->validateQualification($request->all());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        } else {
            $new = Qualification::create($request->all());
            $new2 = QualificationTwo::create($request->all());
            if ($new)
                return response()->json(['success' => 'Record added successfully.']);
            else
                return response()->json(['error' => 'Record not added']);
        }
    }

    public function show($id) {
//        $record = Qualification::with('staff')->find($id);
        $record = Qualification::find($id);
        if ($record) {
            return response()->json($record);
        } else {
            return response()->json(['unable to find record']);
        }
    }

    public function update(Request $request, $id) {
        $validator = $this->validateQualification($request->all());
        if ($validator->fails()) {
            return response()->json($validator->messages());
        } else {
            $record = Qualification::find($id);
            $record2 = QualificationTwo::find($id);
            if ($record) {
                if ($record->update($request->all())) {
                    $record2->update($request->all());
                    return response()->json(['updated' => 'Record updated successfully']);
                } else {
                    return response()->json(['error' => 'Changes not updated']);
                }
            } else {
                return response()->json(['error' => 'Unable to find record']);
            }
        }
    }

    public function destroy($id) {
        $record = Qualification::find($id);
        $record2 = QualificationTwo::find($id);
        if ($record) {
            if ($record->delete()) {
                $record2->delete();
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function uploadQualification(Request $request) {
        $qualificationData = [];
        foreach ($request->data as $data) {
            $qualification = Qualification::where(['name' => $data['name']])->first();
            if (empty($qualification)) {
                $qualificationData['name'] = $data['name'];
                Qualification::create($qualificationData);
            }
        }
        return ['success' => 'Qualification Add Successfully'];
    }

}
