<?php

namespace App\Http\Controllers;

use File;
use App\SpecialityPrioritySet;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SpecialityPrioritySetController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedPriority(array $data) {
        return Validator::make($data, [
                    'priority_set_id' => 'required|integer',
                    'priority_id' => 'required|integer',
                    'level' => 'required|integer',
                    'from' => 'string',
                    'to' => 'string'
        ]);
    }

    public function index() {
        return response()->json(SpecialityPrioritySet::all());
    }

    public function store(Request $request) {
        $validator = $this->validatedPriority($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = SpecialityPrioritySet::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function storeUpdate(Request $request) {
        $error = $store = $records = [];
        foreach ($request->all() as $node) {
            $validator = $this->validatedPriority($node);
            if ($validator->fails()) {
                $error ['level-' . $request->level][] = $validator->messages();
            } else
                $store[] = $node;
        }
        foreach ($store as $value) {
            $records[] = SpecialityPrioritySet::updateOrCreate(['id' => $value['id']], $value);
        }
        return response()->json(['success' => count($records) . ' priority levels added or updated successfully.', 'error' => $error]);
    }

    public function show($id) {
        $record = SpecialityPrioritySet::with('priority_set.speciality')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validatedPriority($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = SpecialityPrioritySet::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['updated' => 'Record updated successfully']);
                else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {
        $record = SpecialityPrioritySet::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

}
