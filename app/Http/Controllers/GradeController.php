<?php

namespace App\Http\Controllers;

use File;
use App\Grade;
use App\GradeTwo;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GradeController extends Controller {

    protected function validatedGrade(array $data, $id = null) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'role_id' => 'required|integer',
                    'name' => 'required|unique_with:tb_grades, directorate_id,' . $id,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(\App\Grade::all());
    }

    public function store(Request $request) {
        $validator = $this->validatedGrade($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $old = \App\Grade::where('directorate_id', $request->directorate_id)->where('position', '>=', $request->position)->increment('position');
            $new = \App\Grade::create($request->all());
            $old2 = \App\GradeTwo::where('directorate_id', $request->directorate_id)->where('position', '>=', $request->position)->increment('position');
            $new2 = \App\GradeTwo::create($request->all());
            if ($new && $new2)
                return response()->json(['success' => 'Record entered successfully.']);
            else
                return response()->json(['error' => 'Unable to enter record.']);
        }
    }

    public function show($id) {
        $record = \App\Grade::find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validatedGrade($request->all(), $id);
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = \App\Grade::find($id);
            $record2 = \App\GradeTwo::find($id);
            if ($record) {
                if ($request->position > $request->current_position) {
                    $decrement = \App\Grade::where('directorate_id', $record->directorate_id)->where('position', '>', $request->current_position)->where('position', '<', $request->position)->decrement('position');
                    $decrement2 = \App\GradeTwo::where('directorate_id', $record->directorate_id)->where('position', '>', $request->current_position)->where('position', '<', $request->position)->decrement('position');
                    $request->merge(['position' => $request->position - 1]);
                    if ($record && $decrement) {
                        $record2->update($request->all());
                        return response()->json(['success' => 'Record updated successfully']);
                    } else
                        return response()->json(['error' => 'No changes updated']);
                } else if ($request->position < $request->current_position) {

                    $increment = \App\Grade::where('directorate_id', $record->directorate_id)->where('position', '>=', $request->position)->where('position', '<', $request->current_position)->increment('position');
                    $increment2 = \App\GradeTwo::where('directorate_id', $record->directorate_id)->where('position', '>=', $request->position)->where('position', '<', $request->current_position)->increment('position');
                    $record->update($request->all());
                    if ($record && $increment) {
                        $record2->update($request->all());
                        return response()->json(['success' => 'Record updated successfully']);
                    } else
                        return response()->json(['error' => 'No changes updated']);
                }
                else if ($request->position == $request->current_position) {
                    $record->update($request->all());
                    $record2->update($request->all());
                    if ($record)
                        return response()->json(['success' => 'Record updated successfully']);
                    else
                        return response()->json(['error' => 'No changes updated']);
                }
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroy($id) {
        $record = \App\Grade::find($id);
        $record2 = \App\GradeTwo::find($id);
        if ($record) {
            $decrement = \App\Grade::where('directorate_id', $record->directorate_id)->where('position', '>', $record->position)->orderBy('position', 'asc')->decrement('position');
            $decrement2 = \App\GradeTwo::where('directorate_id', $record->directorate_id)->where('position', '>', $record->position)->orderBy('position', 'asc')->decrement('position');
            $delete = $record->delete();
            $delete2 = $record2->delete();
            if ($decrement && $delete) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request, $id) {
        $data_store = [];
        foreach ($request->data as $data_node) {
            $directorate = \App\Directorate::where('trust_id', $id)->where('name', $data_node['directorate'])->get();
            if (count($directorate)) {
                $grade = \App\Grade::where('directorate_id', $directorate[0]->id)->where('name', $data_node['name'])->get();
                $role = \App\Role::where('directorate_id', $directorate[0]->id)->where('name', $data_node['role'])->get();
                if (!count($grade) && count($role))
                    $data_store [] = ['directorate_id' => $directorate[0]->id, 'name' => $data_node['name'], 'position' => $data_node['position'], 'role_id' => $role[0]->id];
            }
        }
        $count = 0;
        foreach ($data_store as $data) {
            $old = \App\Grade::where('directorate_id', $data['directorate_id'])->where('position', '>=', $data['position']);
            if (count($old))
                $old->increment('position');
            $new = \App\Grade::create($data);
            if (count($new))
                $count++;
        }
        if ($count)
            return response()->json(['success' => "Records added successfully"]);
        else
            return response()->json(['error' => "Records are not added"]);
    }

}
