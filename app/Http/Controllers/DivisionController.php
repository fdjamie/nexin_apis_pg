<?php

namespace App\Http\Controllers;

use File;
use App\Division;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DivisionController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateDivision(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:64|unique:tb_trust',
                    'trust_id' => 'required'
        ]);
    }

    public function index() {
        return response()->json(Division::all());
    }

    public function store(Request $request) {
        $validator = $this->validateDivision($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $new = Division::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function show($id) {
        $record = Division::find($id);
        if ($record) {
            $record->trust = $record->trust->name;
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateDivision($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = Division::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['record updated successfully']);
                else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
        return response()->json($request->all());
    }

    public function destroy($id) {
        $record = Division::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record Deleted Successfully.']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
        $data_store = [];
        foreach ($request->data as $data_node) {
            if (!isset($data_node['name']))
                $data_node['name'] = '';
            $trust = \App\Trust::find($request->trust_id);
            if (count($trust)) {
                $division = Division::where('trust_id', $request->trust_id)->where('name', $data_node['name'])->get();
                if (!count($division) && $data_node['name'] != '')
                    $data_store [] = ['trust_id' => $request->trust_id, 'name' => $data_node['name']];
            }
        }
        if (!count($data_store))
            return response()->json(['error' => "No record is added"]);
        $new = Division::insert($data_store);
        if ($new)
            return response()->json(['success' => "Records added successfully"]);
    }
    
    public function delted($id) {
        $result = Division::with('trust')->where('trust_id', $id)->onlyTrashed()->get();
        return response()->json($result);
    }
    
    public function restore($id) {
        $result = Division::where('trust_id', $id)->onlyTrashed()->restore();
        if ($result) {
            return response()->json(['success' => 'All data restored successfully']);
        } else {
            return response()->json(['error' => 'Data cannot be recovered']);
        }
    }

}
