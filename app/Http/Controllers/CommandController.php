<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommandController extends Controller {

    public function index($date) {
        $date = new Carbon($date);
        $database = env('DB_DATABASE_SECOND');
        $schema = env('DB_PGSQL_SCHEMA_SECOND');
        $user = env('DB_USERNAME_SECOND');
        $password = env('DB_PASSWORD_SECOND');
        $week = $date->weekOfYear;
        $day = $date->dayOfWeek;
        chdir('allocation');
//        $command = exec('java -jar OptimalAssignments.jar "connection=jdbc:postgresql://127.0.0.1:5432/postgres?currentSchema=staff_allocation_schema&user=postgres&password=admin123" week='.$date->weekOfYear.' day='.$date->dayOfWeek.';set exitcode=%ERRORLEVEL%;call echo exit code %exitcode%');
        $command = exec('OptimalAssignments.bat '.escapeshellarg($week)." ".escapeshellarg($day)." ".escapeshellarg($database)." ".escapeshellarg($schema)." ".escapeshellarg($user)." ".escapeshellarg($password));
        if ($command == 0)
            return response()->json(['success' => 'Optimal assignments completed successfully']);
        else if($command == 1)
            return response()->json(['error' => 'There are some warnings while doing assignments.']);
        else if($command == 2)
            return response()->json(['error' => 'There are some server errors.']);
        else if($command == 3)
            return response()->json(['error' => 'Optimal assignments crashed.']);
    }

    public function optimalAssignment($date) {
        $date = new Carbon($date);
//        $assignments = \App\Assignment::with('transition_time','service','staff')->get();
        $assignments = \App\AssignmentTask::with('assignment_session.assignment.service','assignment_session.assignment.staff','assignment_session.assignment.transition_time')->where('week', $date->weekOfYear)->where('day', $date->dayOfWeek)->get();
        return response()->json($assignments);
    }

}
