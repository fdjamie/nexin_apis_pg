<?php

namespace App\Http\Controllers;


use App\EmailTypes;
use App\Trust;
use App\User;
use App\UserSignUpRequest;
use App\UserTrust;
use App\UserTrustRoles;
use http\Env\Response;
use Illuminate\Http\Request;
use Validator;
use DB;
use Carbon\Carbon;
use App\Http\Helpers;
use App\Jobs\SendEmail;
use Log;
class UserRequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getRequests($trustId)
    {
        $result = array();
        try {
            $get = Trust::with('requests')->find($trustId);
            if ($get) {
                foreach ($get->requests as $k => $g) {
                    $result[$k]['name'] = $g->name;
                    $result[$k]['email'] = $g->email;
                    $result[$k]['requestId'] = $g->pivot->id;
                    $result[$k]['status'] = $g->pivot->status;
                    if ($g->pivot->proceeded_by_user) {
                        $result[$k]['proceeded_by_user'] = @User::where('id', $g->pivot->proceeded_by_user)->first()->name;
                    } else {
                        $result[$k]['proceeded_by_user'] = "-";
                    }
                    if ($g->pivot->proceeded_at) {
                        $result[$k]['proceeded_at'] = Carbon::createFromTimeStamp(strtotime($g->pivot->proceeded_at))->diffForHumans();
                    } else {
                        $result[$k]['proceeded_at'] = "-";
                    }

                    $result[$k]['created_at'] = Carbon::createFromTimeStamp(strtotime($g->pivot->created_at))->diffForHumans();

                }
            }
        } catch (\Exception $e) {

            return response()->json(['status' => false, 'error' => trans('messages.error')]);
        }


        return response()->json(['status' => true, 'data' => $result]);


    }


    public function getUserStatus($userId)
    {
        try {
            $getAccountStatus = UserSignUpRequest::where('user_id', $userId)->first();
            return response()->json(['status' => true, 'data' => $getAccountStatus]);

        } catch (\Exception $e) {

            return response()->json(['status' => false, 'error' => trans('messages.error')]);
        }


    }

    public function updateUserStatus(Request $request)
    {

        $validator = $this->validateUpdateUserStatus($request->all());
        if (!$validator->fails()) {

            $update = $this->updateUserStatusRequest($request);
            if ($update) {

                return response()->json(['status' => true, 'msg' => $update]);
            } else {
                return response()->json(['status' => false, 'errors' => ['error' => ['Unknown Error Occured! Try Again']]]);
            }
        } else {

            return response()->json(['status' => false, 'errors' => $validator->errors()]);


        }
    }


    public function updateUserStatusRequest($request)
    {

        try {
            if ($request->status == "verified") {
                DB::beginTransaction();
                $getRequest = UserSignUpRequest::find($request->requestId);
                $getRequest->status = "verified";
                $getRequest->proceeded_by_user = $request->updatedBy;
                $getRequest->proceeded_at = $request->updatedAt;
                if ($getRequest->save()) {
                    $assingUserToTrust = UserTrust::create([
                        'user_id' => $getRequest->user_id,
                        'trust_id' => $getRequest->trust_id,

                    ]);

                    if ($assingUserToTrust) {
                        foreach ($request->roles as $role)   // here role id is not form roleId table but from role_trust table
                        {

                            $addRoleToUser = UserTrustRoles::create([
                                'user_trust_id' => $assingUserToTrust->id,
                                'role_trust_id' => $role,
                            ]);

                        }
                        if ($addRoleToUser) {
                            DB::commit();
                            $this->sendEmail($getRequest,'user-request-approved');
                            return true;
                        } else {
                            DB::rollBack();
                            return false;
                        }

                    } else {
                        DB::rollBack();
                        return false;
                    }

                } else {
                    DB::rollBack();
                    return false;
                }
            } else {
                $getRequest = UserSignUpRequest::find($request->requestId);
                $getRequest->status = "rejected";
                $getRequest->proceeded_by_user = $request->updatedBy;
                $getRequest->proceeded_at = $request->updatedAt;
                if ($getRequest->save()) {
                    $this->sendEmail($getRequest,'user-request-rejected');
                    return true;
                } else {
                    return false;
                }
            }

        } catch (\Exception $e) {
            return false;
        }


    }

    protected function validateUpdateUserStatus(array $data)
    {
        if ($data['status'] == "verified") {
            return Validator::make($data, [
                'status' => 'required',
                'requestId' => 'required',
                'roles' => 'required',


            ]);
        } else {

            return Validator::make($data, [
                'status' => 'required',
                'requestId' => 'required',

            ]);
        }


    }

    private function sendEmail($data,$type)
    {

        $emailData=array();
        $emailData['userId']=$data->user_id;
        $emailData['trustId']=$data->trust_id;
        $emailData['type']=$type;
        dispatch(new SendEmail($emailData));

}


}
