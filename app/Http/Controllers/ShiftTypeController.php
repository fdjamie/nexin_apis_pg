<?php

namespace App\Http\Controllers;

use File;
use App\ShiftType;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ShiftTypeController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validateShiftType(array $data) {
        return Validator::make($data, [
                    'directorate_id' => 'required|integer',
                    'rota_group_id' => 'required|integer',
                    'name' => 'required',
                    'directorate_speciality_id' => 'required',
                    'overrides_teaching' => 'required|bool',
                    'start_time' => 'required',
                    'finish_time' => 'required',
        ]);
    }

    public function index() {
        return response()->json(ShiftType::all());
    }

    public function store(Request $request) {
        $validator = $this->validateShiftType($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $new = ShiftType::create($request->all());
            return response()->json(['status' => 'ok', 'data' => $new]);
        }
    }

    public function show($id) {
        $record = ShiftType::with('shift_detail.service.category', 'shift_detail.service.sub_category', 'shift_detail.directorate_service.category', 'shift_detail.directorate_service.sub_category', 'shift_detail.speciality', 'directorate.speciality.service', 'service', 'directorate_service', 'rota_group', 'rota_template')->orderBy('id', 'desc')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validateShiftType($request->all());
        if ($validator->fails())
            return response()->json($validator->messages());
        else {
            $record = ShiftType::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['record updated successfully', $request->all(), $record]);
                else
                    return response()->json(['changes not updated']);
            } else
                return response()->json(['unable to find record']);
        }
    }

    public function destroy($id) {
        $record = ShiftType::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['record deleted successfully']);
            } else {
                return response()->json(['record is not deleted']);
            }
        } else {
            return response()->json(['unable to find record']);
        }
    }

    public function export($id, Request $request) {
        $shifttypes = \App\Directorate::with(['services', 'speciality.service', 'rota_group.shift_type' => function($q) {
                        $q->withTrashed()->with('directorate_speciality', 'service', 'directorate_service');
                    }])->where('trust_id', $id)->get();
        return response()->json($shifttypes);
    }

    public function import(Request $request) {
        $directorates = \App\Directorate::with('shift_type', 'speciality.service', 'rota_group.rota_template', 'services')->where('trust_id', $request->trust_id)->get();
        return response()->json($directorates);
    }

    public function upload(Request $request) {
        $shifts = $this->getFormatedShiftTypes($request->trust_id, $request->data, $request->update_flag);
        if (count($shifts) == 0) {
            return response()->json(['error' => "No record is added"]);
        } else {
            if ($request->update_flag == 1) {
                $result = $this->createOrUpdateShifts($shifts);
                if (count($result)) {
                    return response()->json(['success' => count($result) . " Records updated or created successfully"]);
                } else {
                    return response()->json(['success' => " no changes after upload"]);
                }
            } else {
                $new = ShiftType::insert($shifts);
                if ($new)
                    return response()->json(['success' => "Records added successfully"]);
            }
        }
    }

    public function uploadReferenceData(Request $request) {
        $directorates = \App\Directorate::with(['services', 'speciality.service', 'rota_group.shift_type' => function($q) {
                        $q->withTrashed()->with('directorate_speciality', 'service', 'directorate_service');
                    }])->where('trust_id', $request->trust_id)->get();
        $master_data_ids = $this->getMasterDataWithId($directorates);
        $reference_add = $master_check = $checked_reference = [];
        $success_count = $error_count = 0;
        $reference_add = $this->getReferenceData($request->reference_data);
        $master_check = $this->getMasterData($request->master_data);
        $checked_reference = $this->validatedDataFromMasterData($reference_add, $master_check);
        $store_data = $this->validateDataFromDbase($checked_reference, $master_data_ids);
        list($error_count, $success_count) = $this->storeReferenceData($store_data);
        return response()->json(['error' => ($error_count != 0 ? $error_count . ', records could not be updated successfully.' : '' ), 'success' => ($success_count != 0 ? $success_count . ', records updated successfully.' : '')]);
    }

//################################### PROTECTED FUNCTION ################################

    protected function getDataArrays($trust_id) {
        $directorate_array = $speciality_array = $rotagroup_array = $service_array = $speciality_service = $shiftype_array = $rotatemplate_array = [];
        $directorates = \App\Directorate::with('shift_type', 'speciality.service', 'rota_group.rota_template', 'services')->where('trust_id', $trust_id)->get();
        list($speciality_array, $speciality_service) = $this->getValidationData($directorates, 'speciality', 'service');
        list($rotagroup_array, $rotatemplate_array) = $this->getValidationData($directorates, 'rota_group', 'rota_template');
        $service_array = $this->getValidationData($directorates, 'services');
        $shiftype_array = $this->getValidationData($directorates, 'shift_type');
        $directorate_array = $this->getDirectorateData($directorates);
        return [$directorate_array, $speciality_array, $rotagroup_array, $service_array, $speciality_service, $shiftype_array, $rotatemplate_array];
    }

    protected function getFormatedShiftTypes($trust_id, $data, $update_flag) {
        $service_id = null;
        $service_type = null;
        $shifts = $directorate_array = $speciality_array = $rotagroup_array = $service_array = $speciality_service = $shiftype_array = $rotatemplate_array = [];
        list($directorate_array, $speciality_array, $rotagroup_array, $service_array, $speciality_service, $shiftype_array, $rotatemplate_array) = $this->getDataArrays($trust_id);
        // return $rotatemplate_array;
        foreach ($data as $data_node) {
            $checked_data = $this->getValidData($data_node);
            $directorate_id = array_search($checked_data['directorate'], $directorate_array);
            if ($directorate_id) {
                $speciality_id = array_search($checked_data['speciality'], $speciality_array[$directorate_id]);
                $rotagroup_id = array_search($checked_data['rota_group'], $rotagroup_array[$directorate_id]);
                if ($rotagroup_id && isset($rotatemplate_array[$rotagroup_id]))
                    $rotatemplate_id = array_search($checked_data['rota_template'], $rotatemplate_array[$rotagroup_id]);
                if ($checked_data['service_type'] == 'directorate') {
                    $service_type = 'd';
                    $service_id = array_search($checked_data['service'], $service_array[$directorate_id]);
                }
                if ($checked_data['service_type'] == 'speciality') {
                    $service_type = 's';
                    if ($speciality_id)
                        $service_id = array_search($checked_data['service'], $speciality_service[$speciality_id]);
                    else
                        $service_id = 0;
                }
                if ($checked_data['service_type'] == 'none') {
                    $service_type = 'none';
                    $service_id = 0;
                }
                if ($update_flag == 1)
                    $shifts[] = $this->dataToStore($checked_data, $directorate_id, $speciality_id, $rotagroup_id, $rotatemplate_id, $service_id, $service_type);
                else {
                    $shift = array_search($checked_data['name'], $shiftype_array[$directorate_id]);
                    if (!$shift)
                        $shifts[] = $this->dataToStore($checked_data, $directorate_id, $speciality_id, $rotagroup_id, $rotatemplate_id, $service_id, $service_type);
                }
            }
        }
        return $shifts;
    }

    protected function getValidationData($main_nodes, $attribute, $sub_attribute = null) {
        $result = $sub_result = [];
        foreach ($main_nodes as $main_node) {
            if (isset($main_node->$attribute) && count($main_node->$attribute))
                foreach ($main_node->$attribute as $node) {
                    $result[$main_node->id][$node->id] = $node->name;
                    if ($sub_attribute) {
                        foreach ($node->$sub_attribute as $subnode) {
                            $sub_result[$node->id][$subnode->id] = $subnode->name;
                        }
                    }
                }
        }
        if ($sub_attribute == null)
            return $result;
        else
            return [$result, $sub_result];
    }

    protected function getDirectorateData($directorates) {
        $result = [];
        foreach ($directorates as $directorate) {
            $result[$directorate->id] = $directorate->name;
        }
        return $result;
    }

    protected function getValidData($node) {
        $data = [];
        $data['name'] = $node['name'];
        $data['directorate'] = $node['directorate'];
        $data['speciality'] = (isset($node['speciality']) ? $node['speciality'] : '');
        $data['rota_group'] = (isset($node['rota_group']) ? $node['rota_group'] : '');
        $data['rota_template'] = (isset($node['rota_template']) ? $node['rota_template'] : '');
        $data['service'] = (isset($node['service']) ? $node['service'] : '');
        $data['service_type'] = (isset($node['service_type']) ? $node['service_type'] : 'none');
        $data['start_time'] = (isset($node['start_time']) ? $node['start_time'] : '');
        $data['finish_time'] = (isset($node['finish_time']) ? $node['finish_time'] : '');
        $data['overrides_teaching'] = ((isset($node['overrides_teaching']) && strcasecmp($node['overrides_teaching'], 'yes') == 0) ? 1 : 0);
        $data['nroc'] = ((isset($node['nroc']) && strcasecmp($node['nroc'], 'yes') == 0) ? 1 : 0);
        return $data;
    }

    protected function createOrUpdateShifts($shifts) {
        $records = [];
        foreach ($shifts as $shift) {
            $records [] = ShiftType::updateOrCreate(['directorate_id' => $shift['directorate_id'], 'name' => $shift['name']], [
                        'name' => $shift['name'],
                        'directorate_id' => $shift['directorate_id'],
                        'directorate_speciality_id' => $shift['directorate_speciality_id'],
                        'rota_group_id' => $shift['rota_group_id'],
                        // 'rota_template_id' => $shift['rota_template_id'],
                        'service_id' => $shift['service_id'],
                        'service_type' => $shift['service_type'],
                        'start_time' => $shift['start_time'],
                        'finish_time' => $shift['finish_time'],
                        'overrides_teaching' => $shift['overrides_teaching'],
                        'nroc' => $shift['nroc']
            ]);
        }
        return $records;
    }

    protected function dataToStore($checked_data, $directorate_id, $speciality_id, $rotagroup_id, $rotatemplate_id, $service_id, $service_type) {
        $data = [];
        $data = [
            'name' => $checked_data['name'],
            'directorate_id' => $directorate_id,
            'directorate_speciality_id' => ($speciality_id ? $speciality_id : 0),
            'rota_group_id' => ($rotagroup_id ? $rotagroup_id : 0),
            // 'rota_template_id' => ((isset($rotatemplate_id) && $rotatemplate_id) ? $rotatemplate_id : 0),
            'service_id' => $service_id,
            'service_type' => $service_type,
            'start_time' => $checked_data['start_time'],
            'finish_time' => $checked_data['finish_time'],
            'overrides_teaching' => (($checked_data['overrides_teaching'])),
            'nroc' => $checked_data['nroc']
        ];
        return $data;
    }

    protected function getMasterDataWithId($directorates) {
        $data = [];
        foreach ($directorates as $directorate) {
            $data['directorate'][$directorate->id] = $directorate->name;
            foreach ($directorate->speciality as $speciality) {
                $data['speciality'][$directorate->id][$speciality->id] = $speciality->name;
                foreach ($speciality->service as $service_node) {
                    $data['service'][$speciality->id][$service_node->id] = $service_node->name;
                }
            }
            foreach ($directorate->services as $service) {
                $data['general_service'][$directorate->id][$service->id] = $service->name;
            }
            foreach ($directorate->rota_group as $rota_group) {
                $data['rota_group'][$directorate->id][$rota_group->id] = $rota_group->name;
                foreach ($rota_group->shift_type as $shift_type) {
                    $data['shift_type'][$rota_group->id][$shift_type->id] = $shift_type->name;
                }
            }
        }
        return $data;
    }

    protected function getReferenceData($refernce_data) {
        $data = [];
        foreach ($refernce_data as $data_node) {
            if ((isset($data_node['operations']) &&
                    (
                    $data_node['operations'] == 'create' ||
                    $data_node['operations'] == 'update' ||
                    $data_node['operations'] == 'delete' ||
                    $data_node['operations'] == 'restore'))) {
                $data[] = $data_node;
            }
        }
        return $data;
    }

    protected function getMasterData($master_data) {
        $data = [];
        foreach ($master_data as $data_node) {
            if (isset($data_node['directorate']))
                $data['directorates'][] = $data_node['directorate'];
            if (isset($data_node['service']))
                $data['services'][] = $data_node['service'];
            if (isset($data_node['general_service']))
                $data['general_services'][] = (isset($data_node['general_service']) ? $data_node['name'] : null);
            if (isset($data_node['speciality']))
                $data['specialities'][] = $data_node['speciality'];
            if (isset($data_node['name']))
                $data['shift_types'][] = $data_node['name'];
            if (isset($data_node['service_type']))
                $data['service_types'][] = $data_node['service_type'];
            if (isset($data_node['rota_group']))
                $data['rota_groups'][] = $data_node['rota_group'];
            if (isset($data_node['nroc']))
                $data['nroc'][] = strtolower($data_node['nroc']);
            if (isset($data_node['overrides_teaching']))
                $data['overrides_teaching'][] = strtolower($data_node['overrides_teaching']);
            if (isset($data_node['operations']))
                $data['operations'][] = strtolower($data_node['operations']);
        }
        return $data;
    }

    protected function validatedDataFromMasterData($reference_data, $master_check) {
        $data = [];
        foreach ($reference_data as $data_node) {
            if (
                    in_array($data_node['directorate'], $master_check['directorates']) 
                    && in_array($data_node['service'], $master_check['services']) 
                    && in_array($data_node['name'], $master_check['shift_types']) 
                    && in_array($data_node['speciality'], $master_check['specialities']) 
                    && in_array(strtolower($data_node['service_type']), $master_check['service_types']) 
                    && in_array($data_node['rota_group'], $master_check['rota_groups']) 
                    && in_array(strtolower($data_node['nroc']), $master_check['nroc']) 
                    && in_array(strtolower($data_node['overrides_teaching']), $master_check['overrides_teaching']) 
                    && in_array(strtolower($data_node['operations']), $master_check['operations'])
            )
                $data[] = $data_node;
        }
        return $data;
    }

    protected function validateDataFromDbase($data, $master) {
        $result = [];
        foreach ($data as $node) {
            if (array_search($node['directorate'], $master['directorate'])) {
                $directorate_id = $speciality_id = $service_id = $rotagroup_id = $shifttype_id = 0;
                $service_type = 'none';
                $directorate_id = array_search($node['directorate'], $master['directorate']);
                if ($directorate_id) {
                    $speciality_id = array_search($node['speciality'], $master['speciality'][$directorate_id]);
                    $rotagroup_id = array_search($node['rota_group'], $master['rota_group'][$directorate_id]);
                    if ($rotagroup_id)
                        $shifttype_id = (isset($master['shift_type'][$rotagroup_id])?array_search($node['name'], $master['shift_type'][$rotagroup_id]):null);
                    if($node['service_type'] != 'none'){
                        if($node['service_type'] == 'service'){
                            $service_id = array_search($node['service'], $master['service'][$speciality_id]);
                            $service_type = 's';
                        }else{
                            $service_id = array_search($node['service'], $master['general_service'][$directorate_id]);
                            $service_type = 'd';
                        }
                    }
                }
                $result [] = [
                    'id' => ($shifttype_id ? $shifttype_id : 0),
                    'name' => $node['name'],
                    'directorate_id' => $directorate_id,
                    'directorate_speciality_id' => ($speciality_id ? $speciality_id : -1),
                    'rota_group_id' => $rotagroup_id,
                    'service_id' => $service_id,
                    'service_type' => $service_type,
                    'start_time' => $node['start_time'],
                    'finish_time' => $node['finish_time'],
                    'nroc' => (strtolower($node['nroc']) == 'yes' ? 1 : 0 ),
                    'overrides_teaching' => (strtolower($node['overrides_teaching']) == 'yes' ? 1 : 0),
                    'operations' => strtolower($node['operations']),
                ];
            }
        }
        return $result;
    }
    
    protected function storeReferenceData($store_data) {
        $error_count = $success_count = 0;
        foreach ($store_data as $node) {
            switch ($node['operations']) {
                case 'create':
                    if ($node['id']== 0 ) {
                        if (\App\ShiftType::create($node))
                            $success_count++;
                        else
                            $error_count++;
                        break;
                    }
                    break;


                case 'update':
                    $record = \App\ShiftType::find($node['id']);
                    $update = $record->update($node);
                    if ($update)
                        $success_count++;
                    else
                        $error_count++;
                    break;

                case 'restore':
                    $record = \App\ShiftType::onlyTrashed()->find($node['id']);
                    if (!$record) {
                        $error_count++;
                        break;
                    }
                    $restore = $record->restore();
                    if ($restore)
                        $success_count++;
                    else
                        $error_count++;
                    break;

                case 'delete':
                    $record = \App\ShiftType::find($node['id']);
                    if (!$record) {
                        $error_count++;
                        break;
                    }
                    $delete = $record->delete();
                    if ($delete)
                        $success_count++;
                    else
                        $error_count++;
                    break;


                default:
                    if ($node['id'] != 0) {
                        $record = \App\ShiftType::find($node['id']);
                        $update = $record->update($node);
                        if ($update)
                            $success_count++;
                        else
                            $error_count++;
                        break;
                    }
                    $error_count++;
                    break;
            }
        }
        return [$error_count, $success_count];
    }

}
