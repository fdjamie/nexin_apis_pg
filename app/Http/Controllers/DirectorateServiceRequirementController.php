<?php

namespace App\Http\Controllers;

use File;
use App\DirectorateServiceRequirement;
use Validator;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DirectorateServiceRequirementController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedRequirement(array $data) {
        return Validator::make($data, [
                    'service_id' => 'required|integer',
                    'priority' => 'integer',
                    'color' => 'required|string',
                    'parameter' => 'required|string',
                    'operator' => 'required|string',
                    'value' => 'required|string',
                    'status' => 'required|string',
                    'number' => 'required|integer',
        ]);
    }

    public function index() {
        return response()->json(DirectorateServiceRequirement::all);
    }

    public function store(Request $request) {
        $records = $error = $store = [];
        
        foreach ($request->all() as $node) {
            $validator = $this->validatedRequirement($node);
            if ($validator->fails())
                $error [$node['color']][] = $validator->messages();
            else
                $store[] = $node;
        }
        foreach ($store as $value) {
            $records[] = \App\DirectorateServiceRequirement::updateOrCreate(['id' => $value['id']], [
                        'service_id' => $value['service_id'],
                        'priority' => $value['priority'],
                        'color' => $value['color'],
                        'parameter' => $value['parameter'],
                        'operator' => $value['operator'],
                        'value' => $value['value'],
                        'value_2' => isset($value['value_2'])? $value['value_2']:'',
                        'status' => $value['status'],
                        'number' => $value['number']
            ]);
        }

        return response()->json(['success' => count($records) . ' requirements added or updated successfully.', 'error' => $error]);
    }

    public function show($id) {
        $record = \App\DirectorateServiceRequirement::with('service')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $validator = $this->validatedRequirement($request->all());
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = \App\DirectorateServiceRequirement::find($id);
            if ($record) {
                if ($record->update($request->all()))
                    return response()->json(['updated' => 'Record updated successfully']);
                else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function destroy($id) {
        $record = DirectorateServiceRequirement::find($id);
        if ($record) {
            if ($record->delete()) {
                return response()->json(['error' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

}
