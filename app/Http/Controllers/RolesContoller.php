<?php

namespace App\Http\Controllers;

use App\Roles;
use App\Module;
use App\Permission;
use App\RolePermission;
use App\RoleTrust;
use App\UserTrust;
use App\UserTrustRoles;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class RolesContoller extends Controller
{



    public function store(Request $request)
    {
        $validateRequest = $this->validateRequest($request);
        if (!$validateRequest['status']) {
            return $validateRequest['errors'];
        }
        $response = array();
        try {

            return Roles::createRole($request);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => [trans('messages.error')]]);
        }


    }


    private function validateRequest($request)
    {

        $v = Validator::make($request->all(), [
            'trust_id' => 'required',
            'name' => 'required',
        ]);

        if ($v->fails()) {
            $errors = response()->json(['status' => false, 'msg' => $v->errors()]);
            $res['status'] = false;
            $res['errors'] = $errors;


        } else {
            $res['status'] = true;
        }

        return $res;
    }



    public function getModulesAndPermissions($trustRoleId){

        $response = array();
        try {
            $get=array();
            $get['modules']=Module::all();
            $get['permissions']=Permission::all();
            $getP=RolePermission::where('trust_role_id',base64_decode($trustRoleId))->get();
            $get['trustRolePermissions']=$getP->pluck('moduleId_permissionId');
            return response()->json(['status' => true, 'data' =>$get ]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => [trans('messages.error')]]);
        }


    }



    public function storeRolePermissions(Request $request)
    {
        $validateRequest = $this->validateRolePermissionsRequest($request);
        if (!$validateRequest['status']) {
            return $validateRequest['errors'];
        }
        $response = array();
        try {

            return Roles::assignPermissionToRole($request);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => [trans('messages.error')]]);
        }


    }

    protected function validateRolePermissionsRequest($request){

        $v = Validator::make($request->all(), [
            'trustRoleId' => 'required',
        ]);

        if ($v->fails()) {
            $errors = response()->json(['status' => false, 'msg' => $v->errors()]);
            $res['status'] = false;
            $res['errors'] = $errors;


        } else {
            $res['status'] = true;
        }

        return $res;

    }

    public function getRolePermisions($id)
    {
        $response = array();
        $roles=array();
        $permissions=array();
        $data=array();
        $userTrustId=$id;
        try {
            $ut = UserTrustRoles::where('user_trust_id', $id)->get();
            foreach ($ut as $k => $t) {
                $roles[] = RoleTrust::find($t->role_trust_id)->roles->name;
                $perms = RolePermission::with('permission', 'module')->where('trust_role_id', $t->role_trust_id)->get();

                foreach ($perms as $key => $p) {
                    $permissions[] = $p->module->slug . "_" . $p->permission->name;
                    /*$permissions['module'][]=$p->module->slug;*/
                }


            }
            $data = ['roles' => $roles, 'module_permission' => $permissions];
            return response()->json(['status' => true, 'data' => $data]);
        }catch (\Exception $e) {

            return response()->json(['status' => false, 'msg' => [trans('messages.error')]]);

        }


    }


}
