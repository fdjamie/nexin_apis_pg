<?php

namespace App\Http\Controllers;

use File;
use App\Directorate as dir;
use Validator;
use DB;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DirectorateController extends Controller {

    public function __construct() {
        $this->middleware('auth:api');
    }

    protected function validatedDirectorate(array $data, $id = null) {
        return Validator::make($data, [
//                    'name' => 'required',
                    'name' => 'required|unique_with:tb_directorate,trust_id,' . $id,
                    'trust_id' => 'required',
                    'division_id' => 'integer',
                    'service' => 'boolean',
        ]);
    }

    public function index() {
        return response()->json(Directorate::all());
    }

    public function store(Request $request) {
        $validator = $this->validatedDirectorate($request->all());
        $directorate_two = new \App\DirectorateTwo();
        $directorate_two->setConnection('pgsql2');
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        } else {
            $new = \App\Directorate::create($request->all());
            $new2 = $directorate_two->create($request->all());
            if ($new && $new2) {
                return response()->json(['success' => 'Record added successfully.']);
            } else {
                return response()->json(['error' => 'Record not added.']);
            }
        }
    }

    public function show($id) {
        $record = \App\Directorate::with('speciality.service', 'rota_template', 'additional_parameter')->find($id);
        if ($record) {
            return response()->json($record);
        } else
            return response()->json(['unable to find record']);
    }

    public function update(Request $request, $id) {
        $directorate_two = new \App\DirectorateTwo();

        // $directorate_two->setConnection('pgsql2'); commented out on 06/12/2018
        $validator = $this->validatedDirectorate($request->all(), $id);
        if ($validator->fails())
            return response()->json(['error' => $validator->messages()]);
        else {
            $record = \App\Directorate::find($id);
            // $record2 = $directorate_two->find($id);  commented out on 06/12/2018
            // and below line was added
            $record2 = DB::connection('pgsql2')->table('tb_directorate')->where('id',$id); 
            // $record2->update($request->all()); commented out on 06/12/2018

            if ($record) {
                if ($record->update($request->all()) && $record2->update($request->only($directorate_two->getFillable()))) {
                    // above line was also edited

                    return response()->json(['updated' => 'Record updated successfully']);
                } else
                    return response()->json(['error' => 'Changes not updated']);
            } else
                return response()->json(['error' => 'Unable to find record']);
        }

        return response()->json($request->all());
    }

    public function destroy($id) {
        $directorate_two = new \App\DirectorateTwo();

        // $directorate_two->setConnection('pgsql2'); commented out on 06/12/2018
        $record = \App\Directorate::find($id);
        // $record2 = $directorate_two->find($id); commented out on 06/12/2018
        
        // start below line was newly added
        $record2 = DB::connection('pgsql2')->table('tb_directorate')->where('id',$id);
        if ($record) {
            if ($record->delete() && $record2->delete()) {
                return response()->json(['success' => 'Record deleted successfully']);
            } else {
                return response()->json(['error' => 'Record is not deleted']);
            }
        } else {
            return response()->json(['error' => 'Unable to find record']);
        }
    }

    public function upload(Request $request) {
       
        $data_store = [];
        $trust = \App\Trust::find($request->trust_id);
        
        if(!is_null($request->data) && !empty($request->data) && $request->data )
        {
        foreach ($request->data as $data_node) {
            $division_id = true;
            if ($data_node['trust'] == $trust->name) {
                if ($data_node['division'] != 'N/A')
                    $division = \App\Division::where('trust_id', $request->trust_id)->where('name', $data_node['division'])->get();
                else
                    $division_id = false;

                if (isset($division) && count($division) && $division_id) {
                    $directorate = \App\Directorate::where('trust_id', $request->trust_id)->where('division_id', $division[0]->id)->where('name', $data_node['name'])->get();
                    if (!count($directorate) && ($data_node['service'] == 'Yes' || $data_node['service'] == 'No'))
                        $data_store [] = ['trust_id' => $request->trust_id, 'name' => $data_node['name'], 'division_id' => $division[0]->id];

                }
                else if (!$division_id) {
                    $directorate = \App\Directorate::where('trust_id', $request->trust_id)->where('division_id', 0)->where('name', $data_node['name'])->get();
                    if (!count($directorate) && ($data_node['service'] == 'Yes' || $data_node['service'] == 'No'))
                        $data_store [] = ['trust_id' => $request->trust_id, 'name' => $data_node['name'], 'division_id' => 0];
                }

            }
        }
    }
    // change start on 6/12/2018....
    $new = false; 
    $new2 = false;
   
    if(sizeof($data_store))
    {
        foreach ($data_store as $key => $value) {
            $new = \App\Directorate::create($value);
            $new2 = DB::connection('pgsql2')->table('tb_directorate')->insert($value);
        }
    }    
       // change end on 6/12/2018....
        if (!count($data_store))
        {
            return response()->json(['error' => "No record is added"]);
        }

        if ($new && $new2)
            return response()->json(['success' => "Records added successfully"]);
    

    }
}
