<?php

namespace App\Http;

use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use mail;

class Helpers {

    public static function getAllNodesFromMainNode($nodes, $sub_node_name) {
        $result = [];
        foreach ($nodes as $node) {
            foreach ($node->$sub_node_name as $sub_node) {
                if (isset($sub_node) && !is_null($sub_node)) {
                    array_push($result, $sub_node);
                }
            }
        }
        return $result;
    }

    public static function getAllNodesByIdFromMainNode($nodes, $sub_node_name, $id, $match_id) {
        $result = [];
        foreach ($nodes as $node) {
            foreach ($node->$sub_node_name as $sub_node) {
                if ($sub_node->$match_id == $id)
                    $result[] = $sub_node;
            }
        }
        return $result;
    }




}
