<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrustTemplates extends Model
{
    protected $table="tb_trust_templates";
    protected $fillable=['trust_id','template_id'];
}
