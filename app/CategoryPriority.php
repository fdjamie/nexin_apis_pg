<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryPriority extends Model {

    use SoftDeletes;

    protected $table = 'tb_category_priority';
    protected $fillable = ['directorate_speciality_id', 'type', 'type_id', 'priority', 'requirement_order'];

    public function speciality() {
        return $this->belongsTo('App\DirectorateSpeciality', 'directorate_speciality_id');
    }

    public function priority_set() {
        return $this->hasMany('App\SpecialityPrioritySet', 'priority_id');
    }

    public function category() {
        return $this->morphedByMany('App\Category', 'tb_prioritizable')->with('service.requirement');
    }

    public function subcategory() {
        return $this->morphedByMany('App\SubCategory', 'tb_prioritizable')->with('service.requirement');
    }

    public function service() {
        return $this->morphedByMany('App\Service', 'tb_prioritizable')->with('requirement');
    }

    public function general_service() {
        return $this->morphedByMany('App\DirectorateService', 'tb_prioritizable')->with('requirement');
    }

    public function setRequirementOrderAttribute($value) {
        $this->attributes['requirement_order'] = json_encode($value);
    }

    public function getRequirementOrderAttribute($value) {
        return json_decode($value);
    }

}
