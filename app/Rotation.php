<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rotation extends Model {

    use SoftDeletes;

    protected $table = 'tb_rotation';
    protected $fillable = ['post_id', 'template_id', 'staff_member', 'start_date', 'end_date', 'permanent', 'template_position'];

    public function post() {
        return $this->belongsTo('App\Post');
    }

    public function rota_template() {
        return $this->belongsTo('App\RotaTemplate', 'template_id');
    }

    public function staff() {
        return $this->belongsTo('App\DirectorateStaff', 'staff_member');
    }

}
