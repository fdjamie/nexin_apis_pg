<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceTwo extends Model
{
    use SoftDeletes;

    protected $connection = 'pgsql2';
    protected $table = 'tb_service';
    protected $fillable = ['directorate_speciality_id', 'category_id','sub_category_id', 'name', 'contactable', 'min_standards','template'];
    
    public function setTemplateAttribute($value) {
        $this->attributes['template'] = json_encode($value);
    }

    public function getTemplateAttribute($value) {
        return json_decode($value);
    }

    public function directorate_speciality() {
        return $this->belongsTo('App\DirectorateSpecialityTwo');
    }
    
//    public function requirement() {
//        return $this->hasMany('App\ServiceRequirement','service_id');
//    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (ServiceTwo $service) {

//            foreach ($service->requirement as $requirement) {
//                $requirement->delete();
//            }
        });
        self::restoring(function (Service $service) {
            $requirements = $service->requirement;
            foreach ($requirements as $requirement) {
                $requirement->restore();
            }
        });
    }
}
