<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplates extends Model
{
    protected $table="tb_email_templates";
    protected $guarded = ['id'];

     public function type()
     {
         return $this->belongsTo('App\EmailTypes','email_type_id');

     }




}
