<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTrustRoles extends Model
{
   protected $table='tb_user_trust_roles';
   protected $fillable=['user_trust_id','role_trust_id'];


    public function roles(){
        return $this->belongsTo('App\Roles','role_id');
    }

    public function permissions(){

        return $this->hasMany('App\RolePermission');

    }


}
