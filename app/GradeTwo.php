<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GradeTwo extends Model
{
    use SoftDeletes;    
    protected $connection = 'pgsql2';
    protected $table = 'tb_grades';
    protected $fillable = ['name','directorate_id','role_id','position'];
    
    public function directorate() {
        return $this->belongsTo('App\DirectorateTwo','directorate_id');
    }
    
    public function role() {
        return $this->belongsTo('App\RoleTwo');
    }
    
}
