<?php

namespace App;
use App\RolePermission;
use Illuminate\Database\Eloquent\Model;
use DB;

class Roles extends Model
{
    protected $table = "tb_roles";
    protected $fillable = ['name', 'slug'];


    public function trustRoles()
    {

        return $this->belongsToMany('App\Trust', 'tb_role_trust', 'role_id', 'trust_id');

    }


    public static function createRole($request)
    {
      /*  $createRole = Roles::firstOrCreate(
            ['slug' => str_slug($request->name)],
            ['name' => $request->name]

        );*/
        $createRole=Roles::firstOrNew(['slug' => str_slug($request->name)]);
        $createRole->name=$request->name;
        $createRole->slug=str_slug($request->name);
        $createRole->save();

        if ($createRole) {
            $checkRoleTrust = RoleTrust::where('trust_id', $request->trust_id)->where('role_id', $createRole->id)->count();

            if ($checkRoleTrust > 0) {
                return response()->json(['status' => false, 'msg' => [trans('messages.exist')]]);
            }

            $addRoleToTrust = new RoleTrust;
            $addRoleToTrust->trust_id = $request->trust_id;
            $addRoleToTrust->role_id = $createRole->id;
            if ($addRoleToTrust->save()) {
                return response()->json(['status' => true, 'msg' => trans('messages.added')]);
            } else {
                return response()->json(['status' => false, 'msg' => [trans('messages.error')]]);
            }

        } else {
            return response()->json(['status' => false, 'msg' => [trans('messages.error')]]);
        }


    }

    public static function assignPermissionToRole($request)
    {


        $trustRoleId=base64_decode($request->trustRoleId);  // Here Trsut Role id is from trust_role Table which is pivot of trust and role table
        $modulePermissions=$request->permission;
        $modules=array();
        $permissions=array();
        $ids=array();
        DB::beginTransaction();
        if(!empty($modulePermissions)) {
            foreach ($modulePermissions as $mp) {
                $arr = explode('_', base64_decode($mp));
                $module = $arr[0];
                $permission = $arr[1];
                $assignPermissionToRole = RolePermission::firstOrCreate([
                    'trust_role_id' => $trustRoleId,
                    'permission_id' => $permission,
                    'module_id' => $module

                ]);
                if ($assignPermissionToRole) {
                    $ids[] = $assignPermissionToRole->id;
                    $status = true;
                } else
                {
                    $status = false;
                    break;
                }

            }
            if($status) {
             $dleNoExit= RolePermission::where('trust_role_id',$trustRoleId)->whereNotIn('id', $ids)->delete();

          }
        }

        else{

            $dleAllNoExit=RolePermission::where('trust_role_id',$trustRoleId)->delete();
            if($dleAllNoExit) {
                $status=true;
            }else
            {
                $status=false;
            }
        }

if($status) {
    DB::commit();
    return response()->json(['status' => true, 'msg' => trans('messages.added')]);

}
else{
    DB::rollBack();
    return response()->json(['status' => false, 'msg' => [trans('messages.error')]]);
}




    }




}
