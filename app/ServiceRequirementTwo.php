<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceRequirementTwo extends Model
{
    use SoftDeletes;

    protected $connection = 'pgsql2';
    protected $table = 'tb_service_requirement';
    protected $fillable = ['service_id', 'priority', 'color', 'status', 'parameter', 'operator', 'value', 'value_2', 'status', 'number', 'condition', 'condition_id'];
    protected $casts = [
        'condition_id' => 'array',
    ];

    public function service() {
        return $this->belongsTo('App\Service', 'service_id');
    }

    public function childReqirement() {
        return $this->hasMany('App\ServiceRequirement', 'condition_id');
    }

    public function childRecursive() {
        return $this->childReqirement()->with('childRecursive');
    }
}
