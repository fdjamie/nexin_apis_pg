<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransitionTime extends Model {

    use SoftDeletes;

    protected $table = 'tb_transition_time';
    protected $fillable = ['directorate_id', 'directorate_speciality_id', 'service_id', 'name', 'start_time', 'end_time'];

    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }

    public function directorate_speciality() {
        return $this->belongsTo('App\DirectorateSpeciality');
    }

}
