<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QualificationTwo extends Model {

    use SoftDeletes;

    protected $connection = 'pgsql2';
    protected $table = 'tb_qualification';
    protected $fillable = ['name'];

    public function staff() {
        return $this->hasMany('\App\StaffTwo');
    }

}
