<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DirectorateService extends Model {

    use SoftDeletes;

    protected $table = 'tb_directorate_services';
    protected $fillable = ['directorate_id', 'category_id', 'sub_category_id', 'name', 'contactable', 'min_standards', 'template', 'specialities','holiday_status'];

    public function setTemplateAttribute($value) {
        $this->attributes['template'] = json_encode($value);
    }

    public function getTemplateAttribute($value) {
        return json_decode($value);
    }

    public function setSpecialitiesAttribute($value) {
        $this->attributes['specialities'] = json_encode($value);
    }

    public function getSpecialitiesAttribute($value) {
        return json_decode($value);
    }

    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function Sub_category() {
        return $this->belongsTo('App\SubCategory');
    }

    public function shift_type() {
        return $this->hasMany('App\ShiftType');
    }

    public function shift_detail() {
        return $this->hasMany('App\ShiftDetail');
    }
    
    public function requirement() {
        return $this->hasMany('App\DirectorateServiceRequirement','service_id');
    }
    
    public function priority() {
        return $this->morphToMany('App\CategoryPriority','tb_prioritizable');
    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (DirectorateService $service) {

            foreach ($service->requirement as $requirement) {
                $requirement->delete();
            }
        });
    }

}
