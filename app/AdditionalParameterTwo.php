<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalParameterTwo extends Model
{
    use SoftDeletes;
    
    protected $connection = 'pgsql2';
    protected $table = 'tb_additional_parameter';
    protected $fillable = ['directorate_id','parameter','value'];
    
    public function setParameterAttribute($value) {
        $this->attributes['parameter'] = ucfirst($value);
     }
    public function directorate() {
        return $this->belongsTo('App\Directorate','directorate_id');
    }
}
