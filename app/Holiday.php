<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Holiday extends Model
{
    //use softDeletes;
    protected $fillable = ['directorate_id', 'holiday_name', 'start_date', 'end_date'];

    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }
}
