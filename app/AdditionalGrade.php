<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalGrade extends Model
{
    use SoftDeletes;
    
    protected $table = 'tb_additional_grade';
    protected $fillable = ['name','directorate_speciality_id','grade_id','position'];
    
    public function directorate_speciality() {
        return $this->belongsTo('App\DirectorateSpeciality');
    }
    
    public function grade() {
        return $this->belongsTo('App\Grade');
    }
}
