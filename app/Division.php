<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Division extends Model {

    use SoftDeletes;
    protected $table = 'tb_division';
    protected $fillable = ['name', 'trust_id'];
    protected $dates = ['deleted_at'];

    public function trust() {
        return $this->belongsTo('App\Trust');
    }

    public function directorate() {
        return $this->hasMany('App\Directorate');
    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (Division $division) {

            foreach ($division->directorate as $directorate) {
                $directorate->delete();
            }
        });
    }

}
