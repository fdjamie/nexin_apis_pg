<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $table="tb_role_permission";
    protected $fillable=['module_id','permission_id','trust_role_id'];

    protected $appends = ['moduleId_permissionId'];


    public function getModuleIdPermissionIdAttribute()
    {

        return $this->module_id."_".$this->permission_id;

    }

    public function permission(){
        return $this->belongsTo('App\Permission','permission_id');
    }

    public function module(){
        return $this->belongsTo('App\Module','module_id');
    }



}
