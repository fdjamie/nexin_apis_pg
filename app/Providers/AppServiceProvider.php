<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        \Validator::extend('deletedatcheck', function($attribute, $value, $parameters, $validator) {
            $query = \DB::table('tb_directorate_staff_service')->select('*')->where([$parameters[1] => $parameters[2], $parameters[3] => $parameters[4]])->whereNull('deleted_at')->whereDate('date', '=', Carbon::today())->first();
            if (empty($query))
                return true;
            else
                return false;
        });
        \Validator::extend('uniquetwocolumn', function($attribute, $value, $parameters, $validator) {
            $query = \DB::table($parameters[0])->select('*')->where([$parameters[1] => $parameters[2], $parameters[3] => $parameters[4]])->whereNull('deleted_at')->first();
            if (empty($query))
                return true;
            else
                return false;
        });
        \Validator::extend('uniquenames', function($attribute, $value, $parameters, $validator) {
            $query = \DB::table($parameters[0])->select('*')->where([$parameters[1] => $parameters[2]])->whereNull('deleted_at')->first();
            if (empty($query))
                return true;
            else
                return false;
        });

        \DB::enableQueryLog();
        \DB::listen(function($sql) {
            //
            foreach ($sql->bindings as $i => $binding) {
                if ($binding instanceof \DateTime) {
                    $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                } else {
                    if (is_string($binding)) {
                        $sql->bindings[$i] = "'$binding'";
                    }
                }
            }

            // Insert bindings into query
            $query = str_replace(array('%', '?'), array('%%', '%s'), $sql->sql);

            $query = vsprintf($query, $sql->bindings);

            // Save the query to file
            $logFile = fopen(
                storage_path('logs' . DIRECTORY_SEPARATOR . date('Y-m-d') . '_query.log'),
                'a+'
            );
            //  date('Y-m-d H:i:s') . ': ' .
            fwrite($logFile, $query . PHP_EOL. PHP_EOL);
            fclose($logFile);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
