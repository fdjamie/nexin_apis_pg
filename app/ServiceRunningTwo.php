<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceRunningTwo extends Model
{
    protected $connection = 'pgsql2';
    protected $table = 'tb_service_running';
    protected $fillable = ['service_id', 'transition_time_id', 'date', 'day', 'week', 'start', 'stop'];
    
     public function service() {
        return $this->belongsTo('App\ServiceTwo');
    }
    
    public function transition_time() {
        return $this->belongsTo('App\TransitionTimeTwo');
    }
}
