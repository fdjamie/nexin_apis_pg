<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleTrust extends Model
{
    protected $table="tb_role_trust";



    public function roles(){
        return $this->belongsTo('App\Roles','role_id');
    }


}
