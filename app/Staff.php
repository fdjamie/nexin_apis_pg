<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model {

    use SoftDeletes;

    protected $table = 'tb_staff';
    protected $fillable = ['name', 'gmc', 'speciality_id', 'role_id', 'grade_id', 'qualifications', 'individual_bleep', 'mobile', 'email', 'profile_pic'];

    public function speciality() {
        return $this->belongsTo('App\DirectorateSpeciality');
    }

    public function directorate() {
        return $this->hasMany('App\DirectorateStaff', 'staff_id');
    }

    public function grade() {
        return $this->belongsTo('App\Grade');
    }

    public function role() {
        return $this->belongsTo('App\Role');
    }
    
//    public function post() {
//        return $this->hasOne('App\Post');
//    }
    
    public function rotaion() {
        return $this->hasMany('App\Rotation');
    }
    

//    public function postDetails (){
//        return $this->belongsToMany('App\PostDetails');
//    }
}
