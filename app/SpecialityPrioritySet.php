<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialityPrioritySet extends Model {

    use SoftDeletes;

    protected $table = 'tb_speciality_priority_set';
    protected $fillable = ['priority_set_id', 'priority_id', 'level', 'from', 'to'];

    public function priority_set() {
        return $this->belongsTo('App\PrioritySet');
    }

    public function category_priority() {
        return $this->belongsTo('App\CategoryPriority', 'priority_id');
    }

}
