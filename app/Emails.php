<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    protected $table="tb_emails";
    protected $fillable=['email','user_id','from','type'];
}
