<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Qualification extends Model {

    use SoftDeletes;

    protected $table = 'tb_qualification';
    protected $fillable = ['name'];

    public function staff() {
        return $this->hasMany('\App\Staff');
    }

}
