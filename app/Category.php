<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

    use SoftDeletes;

    protected $table = 'tb_category';
    protected $fillable = ['name', 'directorate_id'];

    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }

    public function sub_category() {
        return $this->hasMany('App\SubCategory');
    }

    public function service() {
        return $this->hasMany('App\Service');
    }
    
    public function directorate_service() {
        return $this->hasMany('App\DirectorateService');
    }
    
   public function priority() {
       return $this->morphToMany('App\CategoryPriority','tb_prioritizable');
   }

    protected static function boot() {
        parent::boot();
        self::deleting(function (Category $category) {
            foreach ($category->sub_category as $subcategory) {
                $subcategory->delete();
            }
            
            foreach ($category->priority as $category_priority) {
                $category_priority->delete();
            }
        });
    }

}
