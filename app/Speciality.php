<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Speciality extends Model {
    
    use SoftDeletes;
    protected $table = 'tb_speciality';
    protected $fillable = ['name', 'abbreviation'];
}
