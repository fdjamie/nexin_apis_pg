<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RotaTemplate extends Model {

    use SoftDeletes;

    protected $table = 'tb_rota_template';
    protected $fillable = ['directorate_id', 'name', 'content', 'description', 'start_date', 'end_date', 'rota_group_id'];

    public function setContentAttribute($value) {
        $this->attributes['content'] = json_encode($value);
    }

    public function getContentAttribute($value) {
        return json_decode($value);
    }

    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }
    
    public function rota_group() {
        return $this->belongsTo('App\RotaGroup');
    }

    public function shift_type() {
        return $this->hasMany('App\ShiftType');
    }

    public function post() {
        return $this->hasMany('App\Post');
    }

    public function rotation() {
        return $this->hasMany('App\Rotation');
    }

//    protected static function boot() {
//        parent::boot();
//        self::deleting(function (RotaTemplate $rota_template) {
//
//            foreach ($rota_template->shift_type as $shift_type) {
//                $shift_type->delete();
//            }
//        });
//    }

}
