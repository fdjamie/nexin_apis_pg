<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSignUpRequest extends Model
{
  protected $table="user_signup_requests";
  protected $fillable=['user_id','trust_id','status','proceeded_by_user','proceeded_at'];


}
