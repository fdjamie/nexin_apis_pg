<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trust extends Model {

    use SoftDeletes;

    protected $table = 'tb_trust';
    protected $fillable = ['name'];


    public function division() {
        return $this->hasMany('App\Division');
    }

    public function directorate() {
        return $this->hasMany('App\Directorate');
    }

    public function site() {
        return $this->hasMany('App\Site');
    }

    public function shift() {
        return $this->hasMany('App\Shift');
    }

    public function template() {
        return $this->hasMany('App\Template');
    }


    public function trustRoles()
    {

        return $this->belongsToMany('App\Roles', 'tb_role_trust', 'trust_id', 'role_id')->withPivot('id');

    }

    public function users(){
        return $this->belongsToMany('App\User','tb_user_trust')->withPivot('id');
    }

    public function requests(){
        return $this->belongsToMany('App\User','user_signup_requests')->withPivot('id','status','proceeded_by_user','proceeded_at','created_at');
    }
    public function templates(){
        return $this->belongsToMany('App\EmailTemplates','tb_trust_templates','trust_id','template_id');
    }
    public function userTrust(){
        return $this->hasMany('App\UserTrust');
    }

    protected static function boot() {
        parent::boot();
        self::deleting(function (Trust $trust) {

            foreach ($trust->division as $division) {
                $division->delete();
            }

            foreach ($trust->site as $site) {
                $site->delete();
            }

        });
    }

}
