<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grade extends Model
{
    use SoftDeletes;    
    protected $table = 'tb_grades';
    protected $fillable = ['name','directorate_id','role_id','position'];
    
    public function directorate() {
        return $this->belongsTo('App\Directorate','directorate_id');
    }
    
    public function role() {
        return $this->belongsTo('App\Role');
    }
    
    public function additional_grade() {
        return $this->hasOne('App\AdditionalGrade');
    }
    
}
