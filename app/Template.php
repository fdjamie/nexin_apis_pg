<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model {
    
    use SoftDeletes;
    protected $table = 'tb_templates';
    protected $fillable = ['name', 'type', 'contents','trust_id'];

    public function setContentsAttribute($value) {
        $this->attributes['contents'] = json_encode($value);
    }

    public function getContentsAttribute($value) {
        return json_decode($value);
    }
    
    public function post() {
     return   $this->hasMany('App\Post');
    }
    
    public function trust() {
        return $this->bleongsTo('App\Trust');
    }
    
    public function rotation() {
        return $this->hasMany('App\Rotation');
    }

}
