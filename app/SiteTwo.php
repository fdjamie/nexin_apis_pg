<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteTwo extends Model
{
    use SoftDeletes;    

    protected $connection = 'pgsql2';
    protected $table = 'tb_sites';
    protected $fillable = ['trust_id', 'name', 'abbreviation', 'address'];

    
    public function location() {
        return $this->hasMany('App\LocationTwo','site_id');
    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (SiteTwo $site) {

            foreach ($site->location as $location) {
                $location->delete();
            }
        });
    }
}
