<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Authenticatable {

    use SoftDeletes;

    protected $table = 'tb_post';

    protected $fillable = ['name', 'directorate_speciality_id', 'rota_template_id', 'staff_id', 'description', 'permanent', 'assigned_week', 'start_date', 'end_date', 'additional_parameters'];

    public function setAdditionalParametersAttribute($value) {
        $this->attributes['additional_parameters'] = json_encode($value);
    }

    public function getAdditionalParametersAttribute($value) {
        return json_decode($value, true);
    }

    public function template() {
        return $this->belongsTo('App\Template');
    }

    public function directorate_speciality() {
        return $this->belongsTo('App\DirectorateSpeciality');
    }

    public function rota_template() {
        return $this->belongsTo('App\RotaTemplate', 'rota_template_id');
    }

    public function staff() {
        return $this->belongsTo('App\DirectorateStaff');
    }

    public function rotation() {
        return $this->hasMany('App\Rotation');
    }

    protected static function boot() {
        parent::boot();
        self::deleting(function (Post $post) {

            foreach ($post->rotation as $rotation) {
                $rotation->delete();
            }
        });
    }

}
