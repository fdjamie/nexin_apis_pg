<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model {

    use SoftDeletes;

    protected $table = 'tb_groups';
    protected $fillable = ['name', 'directorate_id', 'name'];

    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }

    public function subgroup() {
        return $this->hasMany('App\Subgroup');
    }

    public function location() {
        return $this->hasMany('App\Location');
    }

    protected static function boot() {
        parent::boot();
        self::deleting(function (Group $group) {
            foreach ($group->subgroup as $subgroup) {
                $subgroup->delete();
            }
        });
    }

}
