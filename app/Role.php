<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;    
    protected $table = 'tb_role';
    protected $fillable = ['name','directorate_id','role_id','position'];
    
    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }
    
    public function grade() {
        return $this->hasMany('App\Grade');
    }
    
}
