<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentSession extends Model {

    protected $connection = 'pgsql2';
    protected $table = 'assignment_session';
    protected $fillable = ['assignment_task', 'assignment_change', 'sqn_no', 'updated_on', 'updated_by', 'details'];

    public function assignment_task() {
        return $this->belongsTo('App\AssignmentTask', 'assignment_task');
    }

    public function assignment_change() {
        return $this->belongsTo('App\AssignmentChange', 'assignment_change');
    }
    
    public function assignment(){
        return $this->hasMany('App\Assignment','assignment_session');
    }

}
