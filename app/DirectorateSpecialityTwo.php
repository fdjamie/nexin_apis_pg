<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DirectorateSpecialityTwo extends Model
{
    use SoftDeletes;
    
    protected $connection = 'pgsql2';
    protected $table = 'tb_directorate_speciality';
    protected $fillable = ['directorate_id', 'name','abbreviation', 'status'];

    public function directorate() {
        return $this->belongsTo('App\DirectorateTwo','directorate_id');
    }

//    public function post() {
//        return $this->hasMany('App\Post');
//    }
//    
//     public function service() {
//        return $this->hasMany('App\Service');
//    }
    
//    protected static function boot() {
//        parent::boot();
//        self::deleting(function (DirectorateSpecialityTwo $directorate_speciality) {
//
//            foreach ($directorate_speciality->service as $service) {
//                $service->delete();
//            }
//            
//            foreach ($directorate_speciality->post as $post) {
//                $post->delete();
//            }
//        });
//    }
}
