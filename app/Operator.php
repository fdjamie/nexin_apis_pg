<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Operator extends Model
{
    use SoftDeletes;

    protected $table = 'tb_operators';
    protected $fillable = [ 'name', 'value','parameters','type','directorate_id'];
    
    public function setParametersAttribute($value) {
        $this->attributes['parameters'] = json_encode($value);
    }

    public function getParametersAttribute($value) {
        return json_decode($value);
    }
    public function parameter() {
        return $this->belongsTo(\App\Parameter::class);
    }
    
//    public function child_recursive() {
//        
//        return $this->parameter()->whereIn(['1']);
//    }
}
