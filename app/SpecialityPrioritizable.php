<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialityPrioritizable extends Model
{
    use SoftDeletes;
    
    protected $table = 'tb_speciality_prioritizable';
}
