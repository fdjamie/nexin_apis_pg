<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model {

    use SoftDeletes;

    protected $table = 'tb_location';
    protected $fillable = ['site_id', 'group_id', 'subgroup_id', 'name', 'service', 'limit'];

    public function site() {
        return $this->belongsTo('App\Site');
    }

    public function group() {
        return $this->belongsTo('App\Group');
    }

    public function subgroup() {
        return $this->belongsTo('App\Subgroup');
    }

}
