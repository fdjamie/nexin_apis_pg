<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DirectorateTwo extends Model
{
    use SoftDeletes;
    
    protected $connection = 'pgsql2';
    protected $table = 'tb_directorate';
    protected $fillable = ['name', 'trust_id', 'division_id', 'service'];
    
    public function speciality() {
        return $this->hasMany('App\DirectorateSpecialityTwo','directorate_id');
    }
    
    public function grade() {
        return $this->hasMany('App\GradeTwo','directorate_id');
    }

    public function role() {
        return $this->hasMany('App\RoleTwo','directorate_id');
    }
    
    public function additional_parameter() {
        return $this->hasMany('App\AdditionalParameterTwo','directorate_id');
    }
    
    public function transition_time() {
        return $this->hasMany('App\TransitionTimeTwo','directorate_id');
    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (DirectorateTwo $directorate) {

            foreach ($directorate->speciality as $speciality) {
                $speciality->delete();
            }

            foreach ($directorate->grade as $grade) {
                $grade->delete();
            }

            foreach ($directorate->role as $role) {
                $role->delete();
            }
            
            foreach ($directorate->additional_parameter as $parameter) {
                $parameter->delete();
            }
            
            foreach ($directorate->transition_time as $transition_time) {
                $transition_time->delete();
            }
        });
    }
}
