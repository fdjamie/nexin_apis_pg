<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RotaGroup extends Model
{
    use SoftDeletes;
    
    protected $table = 'tb_rota_groups';
    protected $fillable = ['directorate_id', 'name','description'];
    
    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }  
    
    public function shift_type(){
        return $this->hasMany('App\ShiftType');
    }
    
    public function rota_template(){
        return $this->hasMany('App\RotaTemplate');
    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (RotaGroup $rota_group) {

            foreach ($rota_group->rota_template as $rota_template) {
                $rota_template->delete();
            }
            
            foreach ($rota_group->shift_type as $shift_type) {
                $shift_type->delete();
            }
        });
    }
}
