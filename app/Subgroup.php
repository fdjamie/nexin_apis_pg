<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subgroup extends Model {

    use SoftDeletes;

    protected $table = 'tb_subgroups';
    protected $fillable = ['name','name', 'group_id'];

    public function group() {
        return $this->belongsTo('App\Group');
    }

    public function location() {
        return $this->hasMany('App\Location');
    }

    protected static function boot() {
        parent::boot();
        self::deleting(function (Subgroup $subgroup) {

            foreach ($subgroup->location as $location) {
                $location->delete();
            }
        });
    }

}
