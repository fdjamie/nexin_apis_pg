<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTypes extends Model
{
    protected $table="tb_email_types";


    public function templates()
    {

        return $this->hasMany('App\EmailTemplates','email_type_id');


    }

}
