<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model {
    
    use SoftDeletes;    
    protected $table = 'tb_sites';
    protected $fillable = ['trust_id', 'name', 'abbreviation', 'address'];

    public function trust() {
        return $this->belongsTo('App\Trust');
    }
    
    public function location() {
        return $this->hasMany('App\Location');
    }
    
    protected static function boot() {
        parent::boot();
        self::deleting(function (Site $site) {

            foreach ($site->location as $location) {
                $location->delete();
            }
        });
    }

}
