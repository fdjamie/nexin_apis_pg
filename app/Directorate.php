<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Directorate extends Model {

    use SoftDeletes;

    protected $table = 'tb_directorate';
    protected $fillable = ['name', 'trust_id', 'division_id', 'service'];

    public function trust() {
        return $this->belongsTo('App\Trust');
    }

    public function division() {
        return $this->belongsTo('App\Division');
    }

    public function speciality() {
        return $this->hasMany('App\DirectorateSpeciality');
    }

    public function holidays() {
        return $this->hasMany('App\Holiday');
    }

    public function shift_type() {
        return $this->hasMany('App\ShiftType');
    }

    public function rota_group() {
        return $this->hasMany('App\RotaGroup');
    }

    public function rota_template() {
        return $this->hasMany('App\RotaTemplate');
    }

    public function staff() {
        return $this->hasMany('App\DirectorateStaff');
    }

    public function grade() {
        return $this->hasMany('App\Grade');
    }

    public function role() {
        return $this->hasMany('App\Role');
    }

    public function group() {
        return $this->hasMany('App\Group');
    }

    public function category() {
        return $this->hasMany('App\Category');
    }

    public function services() {
        return $this->hasMany('App\DirectorateService');
    }
    
    public function additional_parameter(){
        return $this->hasMany('App\AdditionalParameter');
    }
    
    public function parameters() {
        return $this->hasMany('App\Parameter');
    }
    
    public function transition_time() {
        return $this->hasMany('App\TransitionTime');
    }

    protected static function boot() {
        parent::boot();
        self::deleting(function (Directorate $directorate) {

            foreach ($directorate->speciality as $speciality) {
                $speciality->delete();
            }

            foreach ($directorate->shift_type as $shift_type) {
                $shift_type->delete();
            }

            foreach ($directorate->rota_template as $rota_template) {
                $rota_template->delete();
            }

            foreach ($directorate->staff as $staff) {
                $staff->delete();
            }

            foreach ($directorate->grade as $grade) {
                $grade->delete();
            }

            foreach ($directorate->role as $role) {
                $role->delete();
            }

            foreach ($directorate->group as $group) {
                $group->delete();
            }

            foreach ($directorate->category as $category) {
                $category->delete();
            }

            foreach ($directorate->services as $service) {
                $service->delete();
            }
            
            foreach ($directorate->additional_parameter as $parameter) {
                $parameter->delete();
            }
        });
    }
    
    
}
