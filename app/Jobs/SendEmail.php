<?php

namespace App\Jobs;

use App\EmailTemplates;
use App\Jobs\Job;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Trust;
use App\EmailTypes;
use Mail;
use App\Emails;
class SendEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $templateTypeId = EmailTypes::where('slug', $this->data['type'])->first()->id;
        $trust = Trust::find($this->data['trustId']);
        $user = User::find($this->data['userId']);
        $subject="Nexin";
        $from="admin@nexin.com";
       /* $trustTemplate = Trust::with(['templates' => function ($query) use ($templateTypeId) {
            $query->where('email_type_id', $templateTypeId);
        }])->find($trust->id)->templates;
        if ($trustTemplate->isEmpty()) {
            $trustTemplateHtml = EmailTemplates::where('email_type_id', $templateTypeId)->where('is_default', true)->first()->html;
        } else {
            $trustTemplateHtml = $trustTemplate[0]->html;
        }*/

           $trustTemplate =EmailTemplates::where(['trust_id' => $this->data['trustId'], 'email_type_id' => $templateTypeId, 'is_active' => true])->first();
         if (empty($trustTemplate)) {
             $trustTemplate = EmailTemplates::where('email_type_id', $templateTypeId)->where('is_default', true)->first();
             $trustTemplateHtml=$trustTemplate->html;

         } else {
             $trustTemplateHtml = $trustTemplate->html;
         }
        $trustTemplateHtml=html_entity_decode($trustTemplateHtml);

         if(!empty($trustTemplate->from))
         {
             $from= $trustTemplate->from;
         }
        if(!empty($trustTemplate->subject))
        {
          $subject= $trustTemplate->subject;
        }


        $name = $user->name;
        $trust = $trust->name;
        $type="";

        if ($this->data['type'] == "user-request-approved" || $this->data['type'] == "user-request-rejected") {
            $type="Signup Request Status";
            $trustTemplateHtml = str_replace(["&name&","&amp;name&amp;"], [$name,$name], $trustTemplateHtml);
            $trustTemplateHtml = str_replace(["&trust&","&amp;name&amp;"], [$trust,$trust], $trustTemplateHtml);

        }elseif($this->data['type']=="verification-code"){
            $type="Login Verification Code";
            $trustTemplateHtml = str_replace(["&name&","&amp;name&amp;"], [$name,$name],$trustTemplateHtml);
            $trustTemplateHtml = str_replace(["&code&","&amp;code&amp;"], [$this->data['code'],$this->data['code']], $trustTemplateHtml);

        }





        $send = Mail::send([], [], function ($message) use ($trustTemplateHtml,$user,$subject,$from) {
            $message->to($user->email)
                ->from($from)
                ->subject($subject)
                ->setBody($trustTemplateHtml, 'text/html');
        });

        Log::info($send);

        if($send){
            Emails::create([
                'email'=>$trustTemplateHtml,
                'user_id'=>$user->id,
                'type'=>$type
            ]);
        }
    }
}
