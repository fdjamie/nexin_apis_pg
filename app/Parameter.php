<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parameter extends Model {

    use SoftDeletes;

    protected $table = 'tb_parameters';
    protected $fillable = ['name', 'directorate_id',  'operator'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }

    public function setOperatorAttribute($value) {
        $this->attributes['operator'] = json_encode($value);
    }

    public function getOperatorAttribute($value) {
        return json_decode($value);
    }

    public function directorate() {
        return $this->belongsTo(\App\Directorate::class);
    }

    public function operators() {
        return $this->hasMany(\App\Operator::class);
    }

}
