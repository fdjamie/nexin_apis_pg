<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DirectorateStaffTwo extends Model {

    use SoftDeletes;

    protected $connection = 'pgsql2';
    protected $table = 'tb_directorate_staff';
    protected $fillable = ['directorate_id', 'name', 'gmc', 'directorate_speciality_id', 'role_id', 'grade_id', 'qualifications', 'individual_bleep', 'mobile', 'email', 'profile_pic', 'appointed_from', 'appointed_till'];

    public function directorate() {
        return $this->belongsTo('App\DirectorateTwo');
    }

    public function getAppointed_TillAttribute($value) {
        $date = new Carbon($value);
        return $date->format('Y-m-d');
    }

    public function setQualificationsAttribute($value) {
        $this->attributes['qualifications'] = json_encode($value);
    }

    public function getQualificationsAttribute($value) {
        return json_decode($value);
    }

    public function getAppointed_FromAttribute($value) {
        $date = new Carbon($value);
        return $date->format('Y-m-d');
    }

    public function directorate_speciality() {
        return $this->belongsTo('App\DirectorateSpecialityTwo');
    }

//    public function post() {
//        return $this->hasOne('App\Post', 'staff_id');
//    }

    public function role() {
        return $this->belongsTo('App\RoleTwo');
    }

    public function grade() {
        return $this->belongsTo('App\Grade');
    }

}
