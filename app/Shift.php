<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shift extends Model {
    
    use SoftDeletes;
    protected $table = 'tb_shifts';
    protected $fillable = ['name', 'start_time', 'working_hours','trust_id'];

    public function setStartTimeAttribute($value) {
        $this->attributes['start_time'] = date('H:i', strtotime(date('Y-m-d') . ' ' . $value));
    }

    public function getStartTimeAttribute($value) {
        return date('H:i', strtotime(date('Y-m-d') . ' ' . $value));
    }

    public function end_time() {
        return date('H:i', strtotime(date('Y-m-d') . ' ' . $this->start_time) + 60 * 60 * $this->working_hours);
    }
    
    public function trust(){
        return $this->belongsTo('App\Trust');
    }

}
