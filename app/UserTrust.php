<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTrust extends Model
{
    protected $table = 'tb_user_trust';
    protected $fillable = ['user_id', 'trust_id'];

    /*todo Spelling Mistake Rule is Role as used many times so will change it later*/
    public function userTrustRules()
    {


        return $this->belongsToMany('App\RoleTrust', 'tb_user_trust_roles', 'user_trust_id', 'role_trust_id')->withPivot('id');

    }

    public function user()
    {


        return $this->belongsTo('App\User', 'user_id');

    }

    public function userTrustRolesTbReluserTrustRolesTbRel()
    {


        return $this->hasMany('App\UserTrustRoles', 'user_trust_id');

    }



}


