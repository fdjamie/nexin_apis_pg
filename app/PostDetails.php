<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostDetails extends Model
{
    use SoftDeletes;    
    protected $table = 'tb_post_details';
    protected $fillable = ['post_id','staff_member','rota_template_id','start_date','end_date','assigned_week'];
    
    public function post(){
        return $this->belongsTo('App\Post');
    }
    
//    public function staff (){
//        return $this->belongsTo('App\Staff','staff_member');
//    }
    public function staff (){
        return $this->belongsTo('App\DirectorateStaff','staff_member');
    }
    
    public function rota_template (){
        return $this->belongsTo('App\RotaTemplate');
    }
}
