<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrioritySet extends Model
{
    use SoftDeletes;

    protected $table = 'tb_priority_sets';
    protected $fillable = ['directorate_speciality_id','name', 'start_date', 'end_date'];
    
    public function speciality(){
        return $this->belongsTo('App\DirectorateSpeciality','directorate_speciality_id');
    }
    
    public function priorities(){
        return $this->hasMany('App\SpecialityPrioritySet');
    }
}
