<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model {

    use SoftDeletes;

    protected $table = 'tb_colors';
    protected $fillable = ['name', 'letter', 'position', 'code'];

}
