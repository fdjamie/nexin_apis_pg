<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class DirectorateStaff extends Model {

    use SoftDeletes;

    protected $table = 'tb_directorate_staff';
    protected $fillable = ['directorate_id', 'name', 'gmc', 'directorate_speciality_id', 'role_id', 'grade_id', 'qualifications', 'individual_bleep', 'mobile', 'email', 'profile_pic', 'appointed_from', 'appointed_till'];

    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }

//    public function member() {
//        return $this->belongsTo('App\Staff','staff_id');
//    }

    public function getAppointed_TillAttribute($value) {
        $date = new Carbon($value);
        return $date->format('Y-m-d');
    }

    public function setQualificationsAttribute($value) {
        $this->attributes['qualifications'] = json_encode($value,JSON_NUMERIC_CHECK);
    }

    public function getQualificationsAttribute($value) {
        return json_decode($value);
    }
    
    public function getAppointed_FromAttribute($value) {
        $date = new Carbon($value);
        return $date->format('Y-m-d');
    }

    public function directorate_speciality() {
        return $this->belongsTo('App\DirectorateSpeciality');
    }

    public function post() {
        return $this->hasOne('App\Post', 'staff_id');
    }

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function grade() {
        return $this->belongsTo('App\Grade');
    }
    
    public function assign(){
        return $this->hasOne('App\DirectorateStaffService')->whereDate('created_at','=',Carbon::today()->format('Y-m-d'));
    }
    
    public function service() {
        return $this->belongsToMany(\App\Service::class, 'tb_directorate_staff_service')->withPivot('directorate_staff_id', 'service_id');
    }
    
    
    

}
