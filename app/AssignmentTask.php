<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentTask extends Model {

    protected $connection = 'pgsql2';
    protected $table = 'assignment_task';
    protected $fillable = ['week', 'day', 'updated_by', 'details'];

    public function assignment_session() {
        return $this->hasMany('App\AssignmentSession','assignment_task');
    }

}

