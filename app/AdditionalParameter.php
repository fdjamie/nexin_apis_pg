<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalParameter extends Model
{
    use SoftDeletes;
    
    protected $table = 'tb_additional_parameter';
    protected $fillable = ['directorate_id','parameter','value'];
     public function setParameterAttribute($value) {
        $this->attributes['parameter'] = ucfirst($value);
     }
    public function directorate() {
        return $this->belongsTo('App\Directorate');
    }

}
